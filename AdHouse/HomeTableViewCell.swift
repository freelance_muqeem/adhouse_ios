//
//  HomeTableViewCell.swift
//  AdHouse
//
//  Created by Sharjeel ahmed on 7/25/17.
//  Copyright © 2017 . All rights reserved.
//

import UIKit
import SwiftyJSON
import AlamofireImage

class HomeTableViewCell: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{

    @IBOutlet weak var sectionView:UIView!
    @IBOutlet weak var shadowView:UIView!

    @IBOutlet weak var lbl_txt_vechical:UILabel!
    @IBOutlet weak var ViewAllBtn:UIButton!
    @IBOutlet weak var homeCollectionView:UICollectionView!
    var cellIndexPath:IndexPath?
    var delegate:CellBtnPressedProtocol?
    var category:CategoryType?
 
    var vehiclesArray:[ProductResult]? = [ProductResult]()
    var propertyArray:[ProductResult]? = [ProductResult]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //register cell
        
        let nib = UINib(nibName:String(describing:HomeCollectionViewCell.self), bundle: nil)
        homeCollectionView.register(nib, forCellWithReuseIdentifier:String(describing: HomeCollectionViewCell.self))
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = UICollectionViewScrollDirection.horizontal
        homeCollectionView.collectionViewLayout = flowLayout
        
        self.GetVehicles()
        self.GetProperty()
        self.setFonts()
    
    }
    
    
        func setFonts() {
            
            lbl_txt_vechical.font = FontUtility.getFontWithAdjustedSize(size: 11, desireFontType: .Roboto_Regular)
            ViewAllBtn.titleLabel?.font = FontUtility.getFontWithAdjustedSize(size: 10, desireFontType: .Roboto_Regular)
         }

    
    // MARK: - custom actions
    
    @IBAction func ViewAllBtnPressed(){
    
        delegate?.viewAllBtnPressed(cellIndexPath:cellIndexPath!)

    }

    //MARK:-  Get Vehicles Listing Service
    
    func GetVehicles() {
        
        Alert.showLoader(message: "")
        var lang:String!
//        let selectedLang =  UserDefaults.standard.object(forKey: "AppleLanguages") as? [String] ?? [String]()
        
        if isLanguageEnglish() {
            lang = "en"
        } else {
            lang = "ar"
        }
        
        let userId = (Singleton.sharedInstance.CurrentUser == nil) ? -1 : (Singleton.sharedInstance.CurrentUser!.id!)
        let param = ["user_id":userId, "offset": 0, "limit": 10, "lang": lang!] as [String : Any]
        print(param)
        
        HomeServices.Vehicle(param: param, completionHandler: {(status, response, error) in
            Alert.hideLoader()
            if !status {
                if error != nil {
                    print("Error: \((error as! Error).localizedDescription)")
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                return
            }
            
            print("Main")
            print(response!)
            
            print("SUCCESS")
            
            self.vehiclesArray?.removeAll()
            
            let vehicleResponseArray = response?["Result"].arrayValue
            for resultObj in vehicleResponseArray! {
                let obj =  ProductResult(json: resultObj)
                self.vehiclesArray?.append(obj)
            }
            
            self.homeCollectionView.reloadData()
        })
        
    }
    
    
    //MARK:-  Get Vehicles Listing Service
    
    func GetProperty() {
        
        Alert.showLoader(message: "")
        var lang:String!
        
        if isLanguageEnglish() {
            lang = "en"
        }
        else {
            lang = "ar"
        }
        
        let userId = (Singleton.sharedInstance.CurrentUser == nil) ? -1 : (Singleton.sharedInstance.CurrentUser!.id!)
        let param = ["user_id":userId, "offset": 0, "limit": 10, "lang": lang!] as [String : Any]

        HomeServices.Property(param: param, completionHandler: {(status, response, error) in
            Alert.hideLoader()
            if !status {
                if error != nil {
                    print("Error: \((error as! Error).localizedDescription)")
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                return
            }
            
            print("Main")
            print(response!)
            
            print("SUCCESS")
            
            self.propertyArray?.removeAll()
            
            let propertyResponseArray = response?["Result"].arrayValue
            for resultObj in propertyResponseArray! {
                let obj =  ProductResult(json: resultObj)
                self.propertyArray?.append(obj)
            }
            
            self.homeCollectionView.reloadData()
        })
        
    }
    
    // MARK: - UIcollectionview data source
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1;
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if category == CategoryType.VEHICLES {
        
            return self.vehiclesArray!.count
        
        } else {
            
           return self.propertyArray!.count
        }
    }
    
   
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let  cell:HomeCollectionViewCell = homeCollectionView.dequeueReusableCell(withReuseIdentifier: String(describing:HomeCollectionViewCell.self), for: indexPath) as! HomeCollectionViewCell
        
     
        if category == CategoryType.VEHICLES {
            
            let vehiclesObj:ProductResult = self.vehiclesArray![indexPath.row]
            
            cell.lblName.text = vehiclesObj.title
            cell.lblDetail.text = "QAR \(String(describing: vehiclesObj.price!.decimal))" //"QAR " + (vehiclesObj.price)!
            cell.lblKM.text = "Km \(vehiclesObj.kilometer!.decimal)"
            if vehiclesObj.adImages?.first?.imageUrl != nil {
                cell.imageView.setImageFromUrl(urlStr: (vehiclesObj.adImages?.first?.imageUrl)!)
            }
            
            let filter = vehiclesObj.adDetails?.filter({ (adDetail) -> Bool in
                return adDetail.attributeTitle == "CAR MAKE"
            })
            
            if (filter?.count)! > 0 {
                if let obj = filter?.first {
                    if obj.attributeValue != nil {
                        cell.lblCarMake.text = obj.attributeValue!
                    } else {
                        cell.lblCarMake.text = "N/A"
                    }
                } else {
                    cell.lblCarMake.text = "N/A"
                }
            } else {
                cell.lblCarMake.text = "N/A"
            }
            
            let filterClass = vehiclesObj.adDetails?.filter({ (adDetail) -> Bool in
                return adDetail.attributeTitle == "CLASS"
            })
            if (filterClass?.count)! > 0 {
                if let obj = filterClass?.first {
                    if obj.attributeValue != nil {
                        cell.lblClass.text = obj.attributeValue!
                    } else {
                        cell.lblClass.text = "N/A"
                    }
                } else {
                    cell.lblClass.text = "N/A"
                }
            } else {
                cell.lblClass.text = "N/A"
            }
            
            if vehiclesObj.featured == 0 {
                
                cell.imgFeatured.isHidden = true
                cell.lbl_txt_Feature.isHidden = true
                
                
            } else {
                
                cell.imgFeatured.isHidden = false
                cell.lbl_txt_Feature.isHidden = false
            }
            
            for i in vehiclesObj.adDetails! {
                
                if i.attributeTitle == "صنع السيارة"{
                    cell.lblCarMake.text = i.attributeValue
                    break
                }
            }
            
            for i in vehiclesObj.adDetails! {
                
                if i.attributeTitle == "موديل" {
                   //  if i.attributeTitle == "صف دراسي"{
                    cell.lblClass.text = i.attributeValue
                    break
                }
            }

            
            cell.constraintClassTop.constant = 2
            cell.constraintKMTop.constant = 2
            cell.constraintCarMakeTop.constant = 2
        }
        
       else  {
            
            let propertyObj:ProductResult = self.propertyArray![indexPath.row]
            
            cell.lblName.text = propertyObj.title
            cell.lblDetail.text = "QAR \(String(describing: propertyObj.price!.decimal))"
        //    cell.lblKM.text = propertyObj.area
            cell.imageView.setImageFromUrl(urlStr: (propertyObj.adImages?.first?.imageUrl)!)
            
            let filterClass = propertyObj.adDetails?.filter({ (adDetail) -> Bool in
                return adDetail.attributeTitle == "CITY"
            })
            if (filterClass?.count)! > 0 {
                if let obj = filterClass?.first {
                    if obj.attributeValue != nil {
                        cell.lblKM.text = obj.attributeValue!
                    } else {
                        cell.lblKM.text = "N/A"
                    }
                } else {
                    cell.lblKM.text = "N/A"
                }
            } else {
                cell.lblKM.text = "N/A"
            }
            
            if propertyObj.featured == 0 {
                
                cell.imgFeatured.isHidden = true
                cell.lbl_txt_Feature.isHidden = true
                
                
            } else {
                
                cell.imgFeatured.isHidden = false
                cell.lbl_txt_Feature.isHidden = false
            }
            
            cell.lblCarMake.text = ""
            cell.lblClass.text = ""
            
           // cell.constraintClassTop.constant = 0
           // cell.constraintKMTop.constant = 0
          //  cell.constraintCarMakeTop.constant = 0
            
        }
        
         
        cell.layer.shadowColor = UIColor.lightGray.cgColor
        cell.layer.shadowOffset = CGSize(width: 0, height: 1.0)
        cell.layer.shadowRadius = 1
        cell.layer.shadowOpacity = 0.9
        cell.layer.masksToBounds = false
        cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: cell.contentView.layer.cornerRadius).cgPath

        
             return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let screenWidth =  collectionView.bounds.size.width-20
        var height: CGFloat!

        if category == CategoryType.VEHICLES {
            height = SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 150)
        } else {
            height = SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 150)
        }
//        let height = SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 150)
        return CGSize(width: screenWidth/3, height: height);
        
       // return CGSize(width: 100, height: 200)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 1, 0, 1)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //delegate?.disSelectRowAtIndexPath(cellIndexPath:indexPath)
        
        if category == CategoryType.VEHICLES {
            delegate?.disSelectCategoryObject(cellIndexPath: indexPath, product: (self.vehiclesArray?[indexPath.row])!)
        }
        else if category == CategoryType.PROPERTY {
            delegate?.disSelectCategoryObject(cellIndexPath: indexPath, product: (self.propertyArray?[indexPath.row])!)
        }
        
        
    }
}
