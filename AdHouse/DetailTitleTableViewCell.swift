//
//  DetailTitleTableViewCell.swift
//  AdHouse
//
//  Created by maazulhaq on 26/07/2017.
//  Copyright © 2017 . All rights reserved.
//

import UIKit

class DetailTitleTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var lblLocation:UILabel!
    @IBOutlet weak var lblDate:UILabel!
    @IBOutlet weak var lblTime:UILabel!
    @IBOutlet weak var lblAvailable:UILabel!
    @IBOutlet weak var lblPrice:UILabel!
    @IBOutlet weak var lblViews:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        self.setFonts()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setFonts() {
        
        lblTitle.font = FontUtility.getFontWithAdjustedSize(size: 16, desireFontType: .Roboto_Bold)
        lblLocation.font = FontUtility.getFontWithAdjustedSize(size: 11, desireFontType: .Roboto_Regular)
        lblDate.font = FontUtility.getFontWithAdjustedSize(size: 11, desireFontType: .Roboto_Regular)
        lblTime.font = FontUtility.getFontWithAdjustedSize(size: 11, desireFontType: .Roboto_Regular)
        lblAvailable.font = FontUtility.getFontWithAdjustedSize(size: 11, desireFontType: .Roboto_Bold)
        lblPrice.font = FontUtility.getFontWithAdjustedSize(size: 11, desireFontType: .Roboto_Regular)
        lblViews.font = FontUtility.getFontWithAdjustedSize(size: 16, desireFontType: .Roboto_Regular)
    }
    
}
