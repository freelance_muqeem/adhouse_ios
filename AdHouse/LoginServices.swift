//
//  LoginServices.swift
//  Tutorial
//
//  Created by maazulhaq on 04/08/2017.
//  Copyright © 2017 maazulhaq. All rights reserved.
//

import Foundation
import SwiftyJSON

class LoginServices {
    
    //MARK:-  SignIn Service Method
    static func Login(param:[String:Any],
                               completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Any?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.login, parameters: param, callback: { (response, error) in
            
            Alert.hideLoader()
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                
                Alert.showAlert(title: "Alert", message: String(describing: (error?.localizedDescription)!))
                return
            }
            
            if response?["Response"].stringValue != "2000" {
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                Alert.showAlert(title: "Alert", message: (response?["Message"].stringValue)!)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    //MARK:-  Register Service Method
    static func RegisterWithImage(param:[String:Any], image:UIImage,
                               completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Any?)->Void) {
        
        Request.UploadImage(fromSavedUrl: ServiceApiEndPoints.register, parameters: param, image: image, callback: { (response, error) in
            
            Alert.hideLoader()
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                
                Alert.showAlert(title: "Alert", message: String(describing: (error?.localizedDescription)!))
                return
            }
            
            if response?["Response"].stringValue != "2000" {
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                Alert.showAlert(title: "Alert", message: (response?["Message"].stringValue)!)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    static func RegisterWithoutImage(param:[String:Any],
                      completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Any?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.register, parameters: param, callback: { (response, error) in
            
            Alert.hideLoader()
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                
                Alert.showAlert(title: "Alert", message: String(describing: (error?.localizedDescription)!))
                return
            }
            
            if response?["Response"].stringValue != "2000" {
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                Alert.showAlert(title: "Alert", message: (response?["Message"].stringValue)!)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    
    //MARK:-  Forgot Password Service Method
    static func ForgotPassword(param:[String:Any],
                         completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Any?)->Void) {
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.forgotpassword, parameters: param, callback: { (response, error) in
            
            Alert.hideLoader()
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                
                Alert.showAlert(title: "Alert", message: String(describing: (error?.localizedDescription)!))
                return
            }
            
            if response?["Response"].stringValue != "2000" {
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                Alert.showAlert(title: "Alert", message: (response?["Message"].stringValue)!)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    //MARK:-  Logout Service Method
    static func Logout(param:[String:Any],
                               completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Any?)->Void) {
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.logout, parameters: param, callback: { (response, error) in
            
            Alert.hideLoader()
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                
                Alert.showAlert(title: "Alert", message: String(describing: (error?.localizedDescription)!))
                return
            }
            
            if response?["Response"].stringValue != "2000" {
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                Alert.showAlert(title: "Alert", message: (response?["Message"].stringValue)!)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
}
