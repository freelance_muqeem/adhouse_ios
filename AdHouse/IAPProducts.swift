//
//  IAPProducts.swift
//  IAP
//
//  Created by   on 11/01/2017.
//  Copyright © 2017 Apprasoft. All rights reserved.
//

import UIKit

public struct IAPProducts {
    
    public static let GirlfriendOfDrummerRage =
    "com.simmons.test.Unlimited"
    public static let GirlfriendOfSingerWonder =
    "com.simmons.test.Limited"
    
    fileprivate static let productIdentifiers: Set<ProductIdentifier> = [IAPProducts.GirlfriendOfDrummerRage, IAPProducts.GirlfriendOfSingerWonder]
    
    public static let store = IAPHelper(productIds: IAPProducts.productIdentifiers)
}

func resourceNameForProductIdentifier(_ productIdentifier: String) -> String? {
    return productIdentifier.components(separatedBy: ".").last
}

//cydia
