//
//  HeaderView.swift
//  AdHouse
//
//  Created by  on 8/11/17.
//  Copyright © 2017 . All rights reserved.
//

import UIKit
import DropDown
import ImagePicker
import GooglePlaces

extension HeaderView : CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let loc = manager.location?.coordinate
        locationLatitude = (loc?.latitude)!
        locationLongitude = (loc?.longitude)!
        
        self.paramHelper.params.updateValue("\(locationLatitude!)", forKey: "latitude")
        self.paramHelper.params.updateValue("\(locationLongitude!)", forKey: "longitude")
        
        print("Latitude: \(locationLatitude!) & Longitude: \(locationLongitude!)")
        self.getAddressFromLatLon(locationLatitude!, locationLongitude!)
        self.locationManager.stopUpdatingLocation()
        
    }
    
    func didAccesToLocation() -> Bool {
        
        if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.notDetermined {
            print("Not determine your current location")
            return true
        }
        else if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse {
            print("access to location")
            return true
        }
        else {
            print("not access to location")
            DispatchQueue.main.async {
                // self.callpopUp()
            }
            return false
        }
    }
    
    //    func callpopUp() {
    //
    //        self.alertView.btnOkAction.setTitle("Settings", for: .normal)
    //        self.alertView.btnCancel.setTitle("Cancel", for: .normal)
    //        self.alertView.alertShow(image: FAILURE_IMAGE , title: "Access Permission" , msg: "TAAJ PAY needs to access your location to show your current country", id: 1)
    //        self.alertView.isHidden = false
    //
    //    }
    
    func determineMyCurrentLocation() {
        
        locationManager = CLLocationManager()
        
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    func getAddressFromLatLon(_ Latitude: Double , _ Longitude: Double) {
        
        Alert.showLoader(message: "")
        
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        
        let lat: Double = Double("\(Latitude)")!
        let lon: Double = Double("\(Longitude)")!
        
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                
                if let pm = placemarks {
                    
                    if pm.count > 0 {
                        
                        let pm = placemarks![0]
                        
                        var area:String = ""
                        var addressString : String = ""
                        
                        if pm.subLocality != nil {
                            addressString = addressString + pm.subLocality! + ", "
                            area = addressString
                        }
                        if pm.thoroughfare != nil {
                            addressString = addressString + pm.thoroughfare! + ", "
                            area = addressString
                        }
                        if pm.locality != nil {
                            addressString = addressString + pm.locality! + ", "
                            area = addressString
                        }
                        if pm.country != nil {
                            addressString = addressString + pm.country!
                            area =  addressString
                        }
                        
                        print("Current Address : \(area)")
                        self.txtLocation.text = area
                        self.paramHelper.params.updateValue(area, forKey: "area")

                        Alert.hideLoader()
                        
                    }
                }
                
        })
    }
    
    @IBAction func getCurrentLocationAction(_ sender : UIButton) {
        self.endEditing(true)
        self.didAccesToLocation()
        self.determineMyCurrentLocation()
    }
    
    
}

class HeaderView: UITableViewCell {
    
    //MARK:- View Outlet
    
    @IBOutlet weak var locationView:UIView!
    @IBOutlet weak var titleView:UIView!
    @IBOutlet weak var categoryView:UIView!
    @IBOutlet weak var priceView:UIView!
    @IBOutlet weak var discriptionView:UITextView!
    @IBOutlet weak var featureView:UIView!
    @IBOutlet weak var MobileView:UIView!
    @IBOutlet weak var subCategoryView:UIView!
    
    
    //MARK:- TextField Outlet
    
    @IBOutlet weak var txtLocation:UITextField!
    @IBOutlet weak var txtTitle:UITextField!
    @IBOutlet weak var txtCategory:UITextField!
    @IBOutlet weak var txtPrice:UITextField!
    @IBOutlet weak var txtDiscription:UITextView!
    @IBOutlet weak var txtFeature:UITextField!
    @IBOutlet weak var txtMobile:UITextField!
    @IBOutlet weak var txtSubCategory:UITextField!
    
    
    //MARK:- Label Outlet
    
    @IBOutlet weak var lblCategory:UILabel!
    @IBOutlet weak var lblUploadPhoto:UILabel!
    @IBOutlet weak var lblFeatured:UILabel!
    @IBOutlet weak var lblSubCategory:UILabel!
    
    
    //MARK:- Images Array
    var photoArray:[UIImage] = [UIImage]()
    var thumbArray:[UIImage] = [UIImage]()
    
    @IBOutlet weak var heightSubCategoryConstraint: BaseLayoutConstraint!
    @IBOutlet weak var heightSubCategoryView: BaseLayoutConstraint!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var locationManager = CLLocationManager()
    var locationLatitude:Double?
    var locationLongitude:Double?
    
    //MARK:- DropDown
    
    var categoryIDs: [Int] = [Int]()
    var subCatIDs: [Int] = [Int]()
    
    var selectedCatID: Int?
    var selectedSubCatID: Int?
    
    let categoryDropDown = DropDown()
    let subCategoryDropDown = DropDown()
    let featureDropDown = DropDown()
    
    var delegate:CategoryDelegate?
    //    var typeDelegate:HeaderDelegate?
    
    
    /* Search Area */
    var placeIDs: [String] = [String]()
    var cordinates: GMSPlace!
    let dropDownArea = DropDown()
    /* -- Search Area -- */
    
     var paramHelper : ParamsHolder!
    
    override func awakeFromNib() {
        
        self.didAccesToLocation()
        self.determineMyCurrentLocation()
        
        self.heightSubCategoryConstraint.constant = 0
        self.heightSubCategoryView.constant = 0
        self.subCategoryView.isHidden = true
        
        txtTitle.delegate = self
        txtPrice.delegate = self
        txtMobile.delegate = self
        
        txtTitle.tag = 991
        txtPrice.tag = 992
        txtMobile.tag = 993
        
        
        self.borderView()
        
        self.txtDiscription.placeholder = NSLocalizedString("Description", comment: "")
        
        let nib = UINib(nibName: "AddImagesCollectionViewCell", bundle: nil)
        self.collectionView.register(nib, forCellWithReuseIdentifier: "AddImagesCollectionViewCell")
        
        // Feature DropDown
        featureDropDown.anchorView = txtFeature // UIView or UIBarButtonItem
        featureDropDown.bottomOffset = CGPoint(x: 0, y:(featureDropDown.anchorView?.plainView.bounds.height)!)
        featureDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.txtFeature.text = item
            print("Selected item: \(item) at index: \(index)")
            if item == "Yes" {
                self.paramHelper.params.updateValue(1, forKey: "featured")
            }
            else {
                self.paramHelper.params.updateValue(0, forKey: "featured")
            }
        }
        
        self.featureDropDown.dataSource = [NSLocalizedString("Yes", comment: ""), NSLocalizedString("No", comment: "")]
        
        // Category DropDown
        categoryDropDown.anchorView = txtCategory // UIView or UIBarButtonItem
        categoryDropDown.bottomOffset = CGPoint(x: 0, y:(categoryDropDown.anchorView?.plainView.bounds.height)!)
        categoryDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.txtCategory.text = item
            self.selectedCatID = self.categoryIDs[index]
            self.paramHelper.params.updateValue(self.selectedCatID!, forKey: "category_id")
            
            self.delegate?.didSelectCategory(position: index)
            self.txtSubCategory.text = ""
            self.heightSubCategoryConstraint.constant = 17
            self.heightSubCategoryView.constant = 40
            self.subCategoryView.isHidden = false
            print("Selected item: \(item) at index: \(index)")
        }
        
        // Sub Category DropDown
        subCategoryDropDown.anchorView = txtSubCategory // UIView or UIBarButtonItem
        subCategoryDropDown.bottomOffset = CGPoint(x: 0, y:(subCategoryDropDown.anchorView?.plainView.bounds.height)!)
        subCategoryDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.txtSubCategory.text = item
            self.selectedSubCatID = self.subCatIDs[index]
            self.paramHelper.params.updateValue(self.selectedSubCatID!, forKey: "sub_category_id")
            self.delegate?.didSelectSubCategory(position: index)
            print("Selected item: \(item) at index: \(index)")
        }
        
        
        // Area Dropdown
        dropDownArea.anchorView = txtLocation // UIView or UIBarButtonItem
        dropDownArea.bottomOffset = CGPoint(x: 0, y:(dropDownArea.anchorView?.plainView.bounds.height)!)
        dropDownArea.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.txtLocation.text = item
            self.txtLocation.resignFirstResponder()
            
            let placeID = self.placeIDs[index]
            let placesClient = GMSPlacesClient.init()
            placesClient.lookUpPlaceID(placeID, callback: { (place, error) -> Void in
                if let error = error {
                    print("lookup place id query error: \(error.localizedDescription)")
                    return
                }
                
                guard let place = place else {
                    print("No place details for \(placeID)")
                    return
                }
                
                print("Place name \(place.name)")
                print("Place address \(String(describing: place.formattedAddress))")
                print("Place placeID \(place.placeID)")
                print("Place attributions \(String(describing: place.attributions))")
                print("Place Coordinates \(place.coordinate)")
                self.cordinates = place
                
                self.paramHelper.params.updateValue(item, forKey: "area")
                self.paramHelper.params.updateValue("\(place.coordinate.latitude)", forKey: "latitude")
                self.paramHelper.params.updateValue("\(place.coordinate.longitude)", forKey: "longitude")
            })
        }
        
        txtLocation.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
    }
    
    
    //MARK:-  Border View
    
    func borderView(){
        
        self.locationView.layer.borderWidth = 0.5
        self.locationView.layer.borderColor = UIColor.lightGray.cgColor
        self.locationView.addShadow()
        
        self.titleView.layer.borderWidth = 0.5
        self.titleView.layer.borderColor = UIColor.lightGray.cgColor
        self.titleView.addShadow()
        
        self.categoryView.layer.borderWidth = 0.5
        self.categoryView.layer.borderColor = UIColor.lightGray.cgColor
        self.categoryView.addShadow()
        
        self.priceView.layer.borderWidth = 0.5
        self.priceView.layer.borderColor = UIColor.lightGray.cgColor
        self.priceView.addShadow()
        
        self.txtDiscription.layer.borderWidth = 0.5
        self.txtDiscription.layer.borderColor = UIColor.lightGray.cgColor
        self.txtDiscription.addShadow()
        
        self.featureView.layer.borderWidth = 0.5
        self.featureView.layer.borderColor = UIColor.lightGray.cgColor
        self.featureView.addShadow()
        
        self.MobileView.layer.borderWidth = 0.5
        self.MobileView.layer.borderColor = UIColor.lightGray.cgColor
        self.MobileView.addShadow()
        
        self.subCategoryView.layer.borderWidth = 0.5
        self.subCategoryView.layer.borderColor = UIColor.lightGray.cgColor
        self.subCategoryView.addShadow()
        
    }
    
    
    func setTextViewDelegate(){
        txtDiscription.delegate = self
    }
    
    @IBAction func categoryAction(_ sender : UIButton ) {
        self.categoryDropDown.show()
    }
    
    @IBAction func subCategoryAction(_ sender : UIButton ) {
        self.subCategoryDropDown.show()
    }
    
    @IBAction func featureAction(_ sender : UIButton) {
        self.featureDropDown.show()
    }
    
    //MARK:- Camera Button Action
    
    func openCamera() {
        
        let MaximageCount = 15
        let AlreadySelectedImage:Int = thumbArray.count
        if AlreadySelectedImage == MaximageCount {
            
            Alert.showToast(message: "Maximum number of photos has been reached")
            return
        }

        let imagePickerController = ImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.imageLimit = 1
        UIApplication.shared.keyWindow?.rootViewController?.present(imagePickerController, animated: true, completion: nil)
        
//        let cameraViewController = CameraViewController(croppingEnabled: false) { [weak self] image, asset in
//            
//            if image != nil {
//                self?.thumbArray.append(AppHelper.resizeImage(image: image!, compressionQuality: 0.25))
//            }
//            UIApplication.shared.keyWindow?.rootViewController?.dismiss(animated: true, completion: {() -> Void in
//                self?.collectionView.reloadData()
//            })
//        }
//        UIApplication.shared.keyWindow?.rootViewController?.present(cameraViewController, animated: true, completion: nil)
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if (textField.text?.isEmpty)! || textField.text == "" {
            print("nil")
        }
        else {
            let filter = GMSAutocompleteFilter()
            filter.type = .noFilter
            let placesClient = GMSPlacesClient.init()
            placesClient.autocompleteQuery(self.txtLocation.text!, bounds: nil, filter: filter, callback: {(results, error) -> Void in
                if let error = error {
                    print("Autocomplete error \(error)")
                    return
                }
                if let results = results {
                    var names: [String] = [String]()
                    var ids: [String] = [String]()
                    for result in results {
                        print("Result \(String(describing: result.attributedFullText.string)) with placeID \(String(describing: result.placeID))")
                        names.append(result.attributedFullText.string)
                        ids.append(result.placeID)
                    }
                    self.placeIDs = ids
                    self.dropDownArea.dataSource = names
                    self.dropDownArea.show()
                }
            })
        }
    }
    
}

extension HeaderView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    // MARK: - UIcollectionview data source
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if  thumbArray.count == 15 {
            return thumbArray.count
        }
        else {
            return thumbArray.count + 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let  cell:AddImagesCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing:AddImagesCollectionViewCell.self), for: indexPath) as! AddImagesCollectionViewCell
        
        if indexPath.row != self.thumbArray.count {
            let photo = thumbArray[indexPath.row]
            cell.imgCancel.isHidden = false
            cell.imgView.image = photo
        }
        else {
            cell.imgCancel.isHidden = true
            cell.imgView.image = UIImage(named: "addImage")!
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 30, height: 30)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.row == thumbArray.count{
            openCamera()
        }
        else {
            self.thumbArray.remove(at: indexPath.row)
            paramHelper.imagesArray = self.thumbArray
            
            self.collectionView.reloadData()
        }
    }
    
}

extension HeaderView: ImagePickerDelegate {
    
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        if images.count > 0 {
            self.thumbArray.append(AppHelper.resizeImage(image: images.first!, compressionQuality: 0.25))
            paramHelper.imagesArray = self.thumbArray
        }
        
        //Dismiss Controller
        UIApplication.shared.keyWindow?.rootViewController?.dismiss(animated: true, completion: {() -> Void in
            self.collectionView.reloadData()
        })
    }
    
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
        //Dismiss Controller
        UIApplication.shared.keyWindow?.rootViewController?.dismiss(animated: true, completion: nil)
    }
    
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        
    }
}

extension HeaderView : UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == txtTitle {
             paramHelper.params.updateValue(self.txtTitle.text!, forKey: "title")
        }
        if textField == txtPrice{
             paramHelper.params.updateValue(self.txtPrice.text!, forKey: "price")
        }
        if textField == txtMobile {
            paramHelper.params.updateValue(self.txtMobile.text!, forKey: "phone")
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range:NSRange, replacementString string: String) -> Bool {
        
        let kActualText = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
        print("kActualText", kActualText)
        switch textField.tag
        {
        case 991:
            paramHelper.params.updateValue(kActualText, forKey: "title")
        case 992:
           paramHelper.params.updateValue(kActualText, forKey: "price")
        case 993:
       paramHelper.params.updateValue(kActualText, forKey: "phone")
        default:
            print("It is nothing");
        }
        return true;
    }
    
}

extension HeaderView : UITextViewDelegate {
    func textViewDidEndEditing(_ textView: UITextView) {
    
        if textView == txtDiscription {
            
             paramHelper.params.updateValue(self.txtDiscription.text!, forKey: "description")
        }
    }
    
//    func textViewDidChange(_ textView: UITextView) {
//        
//          print("kActualText", self.txtDiscription.text)
//        paramHelper.params.updateValue(self.txtDiscription.text, forKey: "description")
//    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {

            let kActualText = NSString(string: textView.text!).replacingCharacters(in: range, with: text)
         print("kActualText", kActualText)
         paramHelper.params.updateValue(kActualText, forKey: "description")
        if let placeholderLabel = self.viewWithTag(100) as? UILabel {
            placeholderLabel.isHidden = kActualText.count > 0
        }
        return true;
    }
}
