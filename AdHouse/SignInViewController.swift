//
//  SignInViewController.swift
//  AdHouse
//
//  Created by  on 7/24/17.
//  Copyright © 2017 . All rights reserved.
//

import UIKit
import SwiftyAttributes

class SignInViewController: UIViewController {

    class func instantiateFromStoryboard() -> SignInViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! SignInViewController
    }
    
    @IBOutlet weak var lblGuestUser:UILabel!
    @IBOutlet weak var lblAccount:UILabel!
    @IBOutlet weak var btnSignIn:UIButton!
    @IBOutlet weak var btnForgotPwd:UIButton!
    @IBOutlet weak var btnSignInAsGuest:UIButton!
    @IBOutlet weak var btnRegister:UIButton!
    
    
    @IBOutlet weak var mainView:UIView!
    @IBOutlet weak var emailView:UIView!
    @IBOutlet weak var passwordView:UIView!
    
    @IBOutlet weak var txtEmail:UITextField!
    @IBOutlet weak var txtPwd:UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        self.setFonts()
        self.setupTextFieldPlaceHolder()
        self.navigationController?.navigationBar.isHidden = true
        
        self.mainView.layer.borderWidth = 0.5
        self.mainView.layer.borderColor = UIColor.lightGray.cgColor
        self.mainView.addShadow()
        
        self.emailView.layer.borderWidth = 0.5
        self.emailView.layer.borderColor = UIColor.lightGray.cgColor
        self.emailView.addShadow()
        
        self.passwordView.layer.borderWidth = 0.5
        self.passwordView.layer.borderColor = UIColor.lightGray.cgColor
        self.passwordView.addShadow()
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.isHidden = true
    }
    
    func setFonts() {
        
        lblGuestUser.font = FontUtility.getFontWithAdjustedSize(size: 13, desireFontType: .Roboto_Regular)
        lblAccount.font = FontUtility.getFontWithAdjustedSize(size: 13, desireFontType: .Roboto_Regular)
        btnSignIn.titleLabel?.font = FontUtility.getFontWithAdjustedSize(size: 15, desireFontType: .Roboto_Bold)
        btnSignInAsGuest.titleLabel?.font = FontUtility.getFontWithAdjustedSize(size: 14, desireFontType: .Roboto_Bold)
        btnForgotPwd.titleLabel?.font = FontUtility.getFontWithAdjustedSize(size: 14, desireFontType: .Roboto_Regular)
        btnRegister.titleLabel?.font = FontUtility.getFontWithAdjustedSize(size: 14, desireFontType: .Roboto_Bold)
    }
    
    @IBAction func signInAction(_ sender : UIButton) {

        self.view.endEditing(true)
        
        let emailAddress = txtEmail.text!
        
        if  !AppHelper.isValidEmail(testStr: emailAddress) {
            Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Enter valid Email addreess", comment: ""))
            return
        }
        let password = txtPwd.text!
        if password == "" {
            Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Please Enter Password" ,  comment: ""))
            return
        }
        else {
            Alert.showLoader(message: "")
            let emailAddress =  txtEmail.text!
            let password = txtPwd.text!
            
            let parameters = ["email":emailAddress, "password":password]
            LoginServices.Login(param: parameters, completionHandler: {(status, response, error) in
                
                if !status {
                    if error != nil {
                        print("Error: \((error as! Error).localizedDescription)")
                        return
                    }
                    let msg = response?["Message"].stringValue
                    print("Message: \(String(describing: msg))")
                    return
                }
                
                print("Main")
                print(response!)
                
                print("SUCCESS")
                
                let userResultObj = UserObject(object:(response?["Result"])!)
                UserManager.saveUserObjectToUserDefaults(userResult: userResultObj)
                Singleton.sharedInstance.CurrentUser = UserManager.getUserObjectFromUserDefaults()
                
                let nav = UINavigationController(nibName: "MenuVC", bundle: Bundle.main)
                UIWINDOW?.rootViewController = nav
                let vc = UIStoryboard(name: "SideMenu", bundle: Bundle.main).instantiateViewController(withIdentifier: "MenuVC")
                //nav.navigationBar.tintColor = UIColor.white
                nav.navigationBar.isHidden = true
                nav.pushViewController(vc, animated: true)
            })
        }
    }
    
    @IBAction func signInAsGuestAction(_ sender : UIButton) {
    
        self.view.endEditing(true)
        
        Alert.showLoader(message: "")
        
        let nav = UINavigationController(nibName: "MenuVC", bundle: Bundle.main)
        UIWINDOW?.rootViewController = nav
        let vc = UIStoryboard(name: "SideMenu", bundle: Bundle.main).instantiateViewController(withIdentifier: "MenuVC")
        //nav.navigationBar.tintColor = UIColor.white
        nav.navigationBar.isHidden = true
        nav.pushViewController(vc, animated: true)

    }
    
    @IBAction func forgotPasswordAction(_ sender : UIButton) {
        
        let forgotVc = ForgotPasswordViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(forgotVc, animated: true)
    }
    
    @IBAction func registerAction(_ sender : UIButton) {
        
        let registerVc = SignUpViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(registerVc, animated: true)
        
    }
    
    //MARK:- Placeholder Text Change
    
    func setupTextFieldPlaceHolder(){
        let PlaceHolderAttributedString_emailAddress = NSLocalizedString("Email", comment: "").withAttributes([
            .textColor(.lightGray),
            .font(.systemFont(ofSize: SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 14)))
            ])
        
        let PlaceHolderAttributedString_Password = NSLocalizedString("Password", comment: "").withAttributes([
            .textColor(.lightGray),
            .font(.systemFont(ofSize: SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 14)))
            ])
        
        txtEmail.attributedPlaceholder = PlaceHolderAttributedString_emailAddress
        txtPwd.attributedPlaceholder = PlaceHolderAttributedString_Password
    }
    
}
