//
//  NotificationViewController.swift
//  AdHouse
//
//  Created by maazulhaq on 24/07/2017.
//  Copyright © 2017 . All rights reserved.
//

import UIKit

class NotificationViewController: UIViewController  , UITableViewDelegate , UITableViewDataSource {
    
    @IBOutlet weak var tblView:UITableView!
    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var navBar: UINavigationBar!

    var leftBtn:UIButton?
    var isHome: Bool = false
    
    var notificationArray:[NotificationResult]? = [NotificationResult]()

    
    class func instantiateFromStoryboard() -> NotificationViewController {
        let storyboard = UIStoryboard(name: "SideMenu", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! NotificationViewController
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        if !isHome {
            /*
            if self.revealViewController() != nil {
                menuButton.addTargetForAction(target: self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)))
                self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            }
            */
        }
        else {
            //navBar.isHidden = true
            placeBackBtn()
            self.navigationController?.navigationBar.tintColor = UIColor.white
            self.title = "Notification"
        }
 
        self.navigationController?.navigationBar.isHidden = true

        self.Notification()
        
        // Register Nib Cells
        let cellNib = UINib(nibName: "NotificationViewCell", bundle: nil)
        self.tblView.register(cellNib, forCellReuseIdentifier: String(describing: NotificationViewCell.self))
        
    }
    
    //MARK:- Menu Action
    
    @IBAction func MenuAction(sender: UIBarButtonItem) {
        self.slideMenuController()?.toggleLeft()
    }
    
    //MARK:-  Back Button Code
    
    func placeBackBtn(){
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        self.menuButton.addTargetForAction(target: self, action: #selector(self.BackBtnPressed))
        self.menuButton.image = UIImage(named: "backbtn")!
        
    }
    
    @objc func BackBtnPressed(){
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.popViewController(animated: true)
    }

    
    func Notification() {

   
            
        Alert.showLoader(message: "")
            
            let parameters:[String:Any] = ["user_id": (Singleton.sharedInstance.CurrentUser?.id!)!]
            
            SearchServices.Notification(param: parameters , completionHandler: {(status, response, error) in
                
                if !status {
                    if error != nil {
                        print("Error: \((error as! Error).localizedDescription)")
                        return
                    }
                    let msg = response?["Message"].stringValue
                    print("Message: \(String(describing: msg))")
                    return
                }
                
                print(response!)
                print("SUCCESS")
                Alert.hideLoader()
       
                self.notificationArray?.removeAll()
                
                let notiResponseArray = response?["Result"].arrayValue
                
                for resultObj in notiResponseArray! {
                    let obj =  NotificationResult(json: resultObj)
                    self.notificationArray?.append(obj)
                }
                
                self.tblView.reloadData()
                
            })
    }
    
    
    
   //MARK:-  TableView Delegate
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.notificationArray!.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let notiObj:NotificationResult = self.notificationArray![indexPath.row]
        
         let cell: NotificationViewCell = tableView.dequeueReusableCell(withIdentifier: "NotificationViewCell", for: indexPath) as! NotificationViewCell
        
        cell.lblTitle.text = notiObj.title!
        cell.lblDescription.text = notiObj.message!

        let serverDateFormatter: DateFormatter = {
            let result = DateFormatter()
            result.dateFormat = "yyyy-MM-dd HH:mm:ss"
            result.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone!
            return result
        }()
        
        let serverTime = notiObj.createdAt!
        let localTime = serverDateFormatter.date(from: serverTime)!
        
        let localDateFormatter: DateFormatter = {
            let result = DateFormatter()
            result.dateFormat = "hh:mm a"
            return result
        }()
        
        cell.lblDate.text = localDateFormatter.string(from: localTime)
        
        return cell
    }
    
    
}
