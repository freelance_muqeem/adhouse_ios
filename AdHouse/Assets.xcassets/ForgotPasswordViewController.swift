//
//  ForgotPasswordViewController.swift
//  AdHouse
//
//  Created by  on 7/24/17.
//  Copyright © 2017 . All rights reserved.
//

import UIKit
import SwiftyAttributes

class ForgotPasswordViewController: UIViewController {

    class func instantiateFromStoryboard() -> ForgotPasswordViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! ForgotPasswordViewController
    }
    
    @IBOutlet weak var mainView:UIView!
    @IBOutlet weak var emailView:UIView!
    @IBOutlet weak var txtEmail:UITextField!
    
    @IBOutlet weak var btnSubmit:UIButton!
    
    var leftBtn:UIButton?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setFonts()
        self.setupTextFieldPlaceHolder()
        self.placeBackBtn()
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.title = NSLocalizedString("Forgot Password", comment: "")
        
        self.mainView.layer.borderWidth = 0.5
        self.mainView.layer.borderColor = UIColor.lightGray.cgColor
        self.mainView.addShadow()
        
        self.emailView.layer.borderWidth = 0.5
        self.emailView.layer.borderColor = UIColor.lightGray.cgColor
        self.emailView.addShadow()
        self.txtEmail.autocorrectionType = .no
        self.hideKeyboardWhenTappedAround()
        
    }
    
    //MARK:-  Back Button Code
    
    func placeBackBtn(){
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        leftBtn =  UIButton(type: .custom)
        leftBtn?.setImage(UIImage(named: "backbtn"), for: .normal)
        leftBtn?.addTarget(self, action: #selector(ForgotPasswordViewController.BackBtnPressed), for: .touchUpInside)
        leftBtn?.frame = CGRect(x: 0, y:0 , width: 25, height: 25)
        let  leftBarButton: UIBarButtonItem = UIBarButtonItem(customView: leftBtn!)
        self.navigationItem.setLeftBarButton(leftBarButton, animated: true)
        
    }
    
    @objc func BackBtnPressed(){
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func submitAction(_ sender : UIButton) {
        
         self.view.endEditing(true)
        
        let emailAddress = txtEmail.text!
        
        if  !AppHelper.isValidEmail(testStr: emailAddress) {
            Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Enter valid Email addreess", comment: ""))
            return
        }
        else {
            Alert.showLoader(message: "")
            
            let parameters = ["email":emailAddress]
            LoginServices.ForgotPassword(param: parameters, completionHandler: {(status, response, error) in
                
                if !status {
                    if error != nil {
                        print("Error: \((error as! Error).localizedDescription)")
                        return
                    }
                    let msg = response?["Message"].stringValue
                    print("Message: \(String(describing: msg))")
                    return
                }
                
                print("Main")
                print(response!)
                
                print("SUCCESS")
                
                self.navigationController?.popViewController(animated: true)
                
                Alert.showAlert(title: "Alert", message: (response?["Message"].stringValue)!)
            })
        }
    }
    
    //MARK:- Fonts Size Set
    
    func setFonts() {
        
        btnSubmit.titleLabel?.font = FontUtility.getFontWithAdjustedSize(size: 15, desireFontType: .Roboto_Bold)
        
    }
    
    //MARK:- Placeholder Text Change
    
    func setupTextFieldPlaceHolder(){
   
        let PlaceHolderAttributedString_Email = NSLocalizedString("Email", comment: "").withAttributes([
            .textColor(.lightGray),
            .font(.systemFont(ofSize: SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 14)))
            ])
    
        self.txtEmail.attributedPlaceholder = PlaceHolderAttributedString_Email
        
    }

}
extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
