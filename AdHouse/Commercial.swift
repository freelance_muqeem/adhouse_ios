//
//  Commercial.swift
//
//  Created by  on 8/28/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Commercial: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let freeAds = "free_ads"
    static let status = "status"
    static let adValidity = "ad_validity"
    static let updatedAt = "updated_at"
    static let id = "id"
    static let createdAt = "created_at"
    static let maxAds = "max_ads"
    static let package = "package"
    static let subscriptionType = "subscription_type"
    static let price = "price"
    static let isPopular = "is_popular"
  }

  // MARK: Properties
  public var freeAds: Int?
  public var status: Int?
  public var adValidity: Int?
  public var updatedAt: String?
  public var id: Int?
  public var createdAt: String?
  public var maxAds: Int?
  public var package: String?
  public var subscriptionType: Int?
  public var price: Int?
  public var isPopular: Int?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
    public convenience init(object: JSON) {
        self.init(json: object)
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
    freeAds = json[SerializationKeys.freeAds].int
    status = json[SerializationKeys.status].int
    adValidity = json[SerializationKeys.adValidity].int
    updatedAt = json[SerializationKeys.updatedAt].string
    id = json[SerializationKeys.id].int
    createdAt = json[SerializationKeys.createdAt].string
    maxAds = json[SerializationKeys.maxAds].int
    package = json[SerializationKeys.package].string
    subscriptionType = json[SerializationKeys.subscriptionType].int
    price = json[SerializationKeys.price].int
    isPopular = json[SerializationKeys.isPopular].int
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = freeAds { dictionary[SerializationKeys.freeAds] = value }
    if let value = status { dictionary[SerializationKeys.status] = value }
    if let value = adValidity { dictionary[SerializationKeys.adValidity] = value }
    if let value = updatedAt { dictionary[SerializationKeys.updatedAt] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
    if let value = maxAds { dictionary[SerializationKeys.maxAds] = value }
    if let value = package { dictionary[SerializationKeys.package] = value }
    if let value = subscriptionType { dictionary[SerializationKeys.subscriptionType] = value }
    if let value = price { dictionary[SerializationKeys.price] = value }
    if let value = isPopular { dictionary[SerializationKeys.isPopular] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.freeAds = aDecoder.decodeObject(forKey: SerializationKeys.freeAds) as? Int
    self.status = aDecoder.decodeObject(forKey: SerializationKeys.status) as? Int
    self.adValidity = aDecoder.decodeObject(forKey: SerializationKeys.adValidity) as? Int
    self.updatedAt = aDecoder.decodeObject(forKey: SerializationKeys.updatedAt) as? String
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
    self.createdAt = aDecoder.decodeObject(forKey: SerializationKeys.createdAt) as? String
    self.maxAds = aDecoder.decodeObject(forKey: SerializationKeys.maxAds) as? Int
    self.package = aDecoder.decodeObject(forKey: SerializationKeys.package) as? String
    self.subscriptionType = aDecoder.decodeObject(forKey: SerializationKeys.subscriptionType) as? Int
    self.price = aDecoder.decodeObject(forKey: SerializationKeys.price) as? Int
    self.isPopular = aDecoder.decodeObject(forKey: SerializationKeys.isPopular) as? Int
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(freeAds, forKey: SerializationKeys.freeAds)
    aCoder.encode(status, forKey: SerializationKeys.status)
    aCoder.encode(adValidity, forKey: SerializationKeys.adValidity)
    aCoder.encode(updatedAt, forKey: SerializationKeys.updatedAt)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(createdAt, forKey: SerializationKeys.createdAt)
    aCoder.encode(maxAds, forKey: SerializationKeys.maxAds)
    aCoder.encode(package, forKey: SerializationKeys.package)
    aCoder.encode(subscriptionType, forKey: SerializationKeys.subscriptionType)
    aCoder.encode(price, forKey: SerializationKeys.price)
    aCoder.encode(isPopular, forKey: SerializationKeys.isPopular)
  }

}
