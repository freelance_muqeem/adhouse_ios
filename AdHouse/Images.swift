//
//  Images.swift
//
//  Created by  on 8/28/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Images: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let id = "id"
    static let subscriptionImage = "subscription_image"
    static let images = "images"
  }

  // MARK: Properties
  public var id: Int?
  public var subscriptionImage: String?
  public var images: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
    public convenience init(object: JSON) {
        self.init(json: object)
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
    id = json[SerializationKeys.id].int
    subscriptionImage = json[SerializationKeys.subscriptionImage].string
    images = json[SerializationKeys.images].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = subscriptionImage { dictionary[SerializationKeys.subscriptionImage] = value }
    if let value = images { dictionary[SerializationKeys.images] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
    self.subscriptionImage = aDecoder.decodeObject(forKey: SerializationKeys.subscriptionImage) as? String
    self.images = aDecoder.decodeObject(forKey: SerializationKeys.images) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(subscriptionImage, forKey: SerializationKeys.subscriptionImage)
    aCoder.encode(images, forKey: SerializationKeys.images)
  }

}
