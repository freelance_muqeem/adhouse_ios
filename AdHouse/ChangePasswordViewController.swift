//
//  ChangePasswordViewController.swift
//  AdHouse
//
//  Created by  on 8/7/17.
//  Copyright © 2017 . All rights reserved.
//

import UIKit

class ChangePasswordViewController: UIViewController {

    class func instantiateFromStoryboard() -> ChangePasswordViewController {
        let storyboard = UIStoryboard(name: "SideMenu", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! ChangePasswordViewController
    }

    var leftBtn:UIButton?
    
    
    @IBOutlet weak var mainView:UIView!
    @IBOutlet weak var textView:UIView!
    
    @IBOutlet weak var oldPasswordView:UIView!
    @IBOutlet weak var newPasswordView:UIView!
    @IBOutlet weak var changePasswordView:UIView!
    
    
    @IBOutlet weak var txtOldPassword:UITextField!
    @IBOutlet weak var txtNewPassword:UITextField!
    @IBOutlet weak var txtConfirmPassword:UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        let navBackgroundImage:UIImage! = UIImage(named: "topbox")!
        self.navigationController?.navigationBar.setBackgroundImage(navBackgroundImage, for: .default)
        
        self.title = NSLocalizedString("Change Password", comment: "")
        self.placeBackBtn()
        
        self.mainView.layer.borderWidth = 0.5
        self.mainView.layer.borderColor = UIColor.lightGray.cgColor
        self.mainView.addShadow()

        self.textView.layer.borderWidth = 0.5
        self.textView.layer.borderColor = UIColor.lightGray.cgColor
        self.textView.addShadow()
        
        self.oldPasswordView.layer.borderWidth = 0.5
        self.oldPasswordView.layer.borderColor = UIColor.lightGray.cgColor
        self.oldPasswordView.addShadow()
        
        self.newPasswordView.layer.borderWidth = 0.5
        self.newPasswordView.layer.borderColor = UIColor.lightGray.cgColor
        self.newPasswordView.addShadow()
        
        self.changePasswordView.layer.borderWidth = 0.5
        self.changePasswordView.layer.borderColor = UIColor.lightGray.cgColor
        self.changePasswordView.addShadow()
        
        
    }
    
    
    //MARK:-  Back Button Code
    
    func placeBackBtn(){
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        leftBtn =  UIButton(type: .custom)
        leftBtn?.setImage(UIImage(named: "backbtn"), for: .normal)
        leftBtn?.addTarget(self, action: #selector(ChangePasswordViewController.BackBtnPressed), for: .touchUpInside)
        leftBtn?.frame = CGRect(x: 0, y:0 , width: 25, height: 25)
        let  leftBarButton: UIBarButtonItem = UIBarButtonItem(customView: leftBtn!)
        self.navigationItem.setLeftBarButton(leftBarButton, animated: true)
        
    }
    
    @objc func BackBtnPressed(){
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func saveButtonAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        self.changePassword()
        
    }
    

    func changePassword() {
        
        let oldPassword = self.txtOldPassword.text!
        let newPassword = self.txtNewPassword.text!
        let confirmPassword = self.txtConfirmPassword.text!
        let userId = (Singleton.sharedInstance.CurrentUser?.id)!
        
        guard  !((oldPassword.isEmpty)           || (oldPassword.isEmptyOrWhitespace()))
            && !((newPassword.isEmpty)           || (newPassword.isEmptyOrWhitespace()))
            && !((confirmPassword.isEmpty)       || (confirmPassword.isEmptyOrWhitespace()))
            else {
                Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Please fill all Fields", comment: ""))
                return
        }
        
        
        if newPassword == confirmPassword {
            
            let parameters:[String : Any] = ["old_password": oldPassword , "password": newPassword , "password_confirmation" : confirmPassword , "user_id" : userId ]
            
            Alert.showLoader(message: "")
            
            ProfileServices.ChangePassword(param: parameters , completionHandler: {(status, response, error) in
                
                if !status {
                    if error != nil {
                        print("Error: \((error as! Error).localizedDescription)")
                        return
                    }
                    
                    let msg = response?["Message"].stringValue
                    print("Message: \(String(describing: msg))")
                    return
                }
                
                print("Main")
                print(response!)
                
                print("SUCCESS")
                
                self.navigationController?.popViewController(animated: true)
                
                Alert.showAlert(title: NSLocalizedString("Congratulations", comment: ""), message:NSLocalizedString("Your Password has been changed", comment: "") )
            })
        } else {
            
            Alert.showAlert(title: NSLocalizedString("Alert", comment: "") , message:   NSLocalizedString("Password does not match", comment: "")
            )
            
        }
       
    }
  
}
