//
//  MyAdsCollectionViewCell.swift
//  AdHouse
//
//  Created by maazulhaq on 26/07/2017.
//  Copyright © 2017 . All rights reserved.
//

import UIKit

class MyAdsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var labelTitle: UILabel!
    @IBOutlet var labelDate: UILabel!
    @IBOutlet var btnEdit: UIButton!
    @IBOutlet var btnDelete: UIButton!
    @IBOutlet var imgAds: UIImageView!
    @IBOutlet var btnFeature: UIButton!
    @IBOutlet var imgFeature: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.contentView.layer.borderWidth = 0.5
        self.contentView.layer.borderColor = UIColor.lightGray.cgColor
        self.contentView.addShadow()
        
        self.setFonts()
    }
    
    func setFonts() {
        
        labelTitle.font = FontUtility.getFontWithAdjustedSize(size: 10, desireFontType: .Roboto_Regular)
        labelDate.font = FontUtility.getFontWithAdjustedSize(size: 8, desireFontType: .Roboto_Regular)
        btnFeature.titleLabel?.font = FontUtility.getFontWithAdjustedSize(size: 8, desireFontType: .Roboto_Regular) 

    }
    
}
