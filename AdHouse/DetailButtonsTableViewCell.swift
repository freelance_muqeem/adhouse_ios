//
//  DetailButtonsTableViewCell.swift
//  AdHouse
//
//  Created by maazulhaq on 26/07/2017.
//  Copyright © 2017 . All rights reserved.
//

import UIKit

class DetailButtonsTableViewCell: UITableViewCell {

    @IBOutlet weak var btnProductInfo: UIButton!
    @IBOutlet weak var btnSellerInfo: UIButton!
        
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setFonts()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func ProductInfoButtonAction() {
        
        self.view1.isHidden = false
        self.view2.isHidden = true
        //btnProductInfo.isSelected = true
        //btnSellerInfo.isSelected = false
        btnProductInfo.setTitleColor(UIColor(rgb: 0x176AC1), for: .normal)
        btnSellerInfo.setTitleColor(UIColor(rgb: 0x505050), for: .normal)
    }
    
    @IBAction func SellerInfoButtonAction() {

        self.view2.isHidden = false
        self.view1.isHidden = true
        //btnProductInfo.isSelected = false
        //btnSellerInfo.isSelected = true
        btnProductInfo.setTitleColor(UIColor(rgb: 0x505050), for: .normal) //(UIColor.init("#505050"), for: .normal)
        btnSellerInfo.setTitleColor(UIColor(rgb: 0x176AC1), for: .normal) //(UIColor.init("#176AC1"), for: .normal)
    }
    
    
    func setFonts() {
        
        btnProductInfo.titleLabel?.font = FontUtility.getFontWithAdjustedSize(size: 12, desireFontType: .Roboto_Bold)
        btnSellerInfo.titleLabel?.font = FontUtility.getFontWithAdjustedSize(size: 12, desireFontType: .Roboto_Bold)
        
    }
    
}
