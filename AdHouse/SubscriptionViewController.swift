//
//  SubscriptionViewController.swift
//  AdHouse
//
//  Created by maazulhaq on 27/07/2017.
//  Copyright © 2017 . All rights reserved.
//

import UIKit
import ImageSlideshow
import AlamofireImage
import BraintreeDropIn
import Braintree
import MessageUI

extension SubscriptionViewController: MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate {
    
    func email() {
        
        if MFMailComposeViewController.canSendMail() {
            let composeVC = MFMailComposeViewController()
            composeVC.mailComposeDelegate = self
            composeVC.setToRecipients([(self.contact.email)!])
            composeVC.setMessageBody("<p>Hey AdHouse</p>", isHTML: true)
            
            present(composeVC, animated: true)
        } else {
            // show failure alert
        }
    }
    
    func phoneNumber() {
        //Create the AlertController and add Its action like button in Actionsheet
        
        var num = self.contact.phone!.replacingOccurrences(of: "+", with: "")
        num = num.replacingOccurrences(of: " ", with: "")
        let number = num.replacingOccurrences(of: "^0+", with: "", options: .regularExpression)
        
        let actionSheetController: UIAlertController = UIAlertController(title: "Please select", message: "", preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            print("Cancel")
        }
        actionSheetController.addAction(cancelActionButton)
        
        let callActionButton = UIAlertAction(title: "Phone (call)", style: .default)
        { _ in
            
            
            if let url = URL(string: "telprompt://\(number)"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
        
        actionSheetController.addAction(callActionButton)
        
        let smsActionButton = UIAlertAction(title: "Phone (sms)", style: .default)
        { _ in
            
            let composeVC = MFMessageComposeViewController()
            composeVC.messageComposeDelegate = self
            
            // Configure the fields of the interface.
            composeVC.recipients = [number]
            
            composeVC.body = "Hey AdHouse"
            
            // Present the view controller modally.
            self.present(composeVC, animated: true, completion: nil)
        }
        
        actionSheetController.addAction(smsActionButton)
        
        let whatsAppActionButton = UIAlertAction(title: "WhatsApp", style: .default)
        { _ in
            let url = URL(string: "whatsapp://send?text=Hey%20I%20saw%20your%20ad%20on%20AdHouse&phone=\(number)")

            if UIApplication.shared.canOpenURL(url!) {
                UIApplication.shared.openURL(url!)
            }
        }
        actionSheetController.addAction(whatsAppActionButton)
        
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        controller.dismiss(animated: true, completion: nil)
    }
    
}

class SubscriptionViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var BacgroundViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var scroll:UIScrollView!
    
    class func instantiateFromStoryboard() -> SubscriptionViewController {
        let storyboard = UIStoryboard(name: "Subscription", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! SubscriptionViewController
    }
    
    @IBOutlet weak var shadowView:UIView!
    @IBOutlet weak var callUsView:UIView!
    
    @IBOutlet weak var slideshow: ImageSlideshow!
    @IBOutlet weak var backGroundView: UIView!
    
    @IBOutlet weak var lblAddress:UILabel!
    @IBOutlet weak var lblPhone:UILabel!
    @IBOutlet weak var lblEmailAddress:UILabel!
    
    @IBOutlet weak var menuButton: UIBarButtonItem!
    
    @IBOutlet weak var tblView: UITableView!
    
    var subscription:SubscriptionResult?
    var alamofireSource:[AlamofireSource]? = [AlamofireSource]()
    
    var braintreeClient: BTAPIClient!
    var payPalDriver: BTPayPalDriver!
    
    var index:Int!
    var type = ""
    
    var token = ""
    var contact:ContactDetails!
    
    let servicesGroup = DispatchGroup()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = true
        
        backGroundView.layer.shadowColor = UIColor.black.cgColor
        backGroundView.layer.shadowOpacity = 1
        backGroundView.layer.shadowOffset = CGSize.zero
        backGroundView.layer.shadowRadius = 4
        
        self.shadowView.isHidden = true
        self.callUsView.isHidden = true
        
        callUsView.layer.shadowColor = UIColor.black.cgColor
        callUsView.layer.shadowOpacity = 1
        callUsView.layer.shadowOffset = CGSize.zero
        callUsView.layer.shadowRadius = 4
        
        //        self.GetSubscription()
        
        let height = 2 * SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 176)
        self.BacgroundViewHeightConstraint.constant = height + 150
        
        Alert.showLoader(message: "")
        self.GetSubscription()
        self.GetToken()
        self.GetContactDetails()
        servicesGroup.notify(queue: DispatchQueue.main) {
            print("Finished all requests.")
            Alert.hideLoader()
        }
        
    }
    
    //MARK:-  Get Subscription Service
    
    func GetSubscription() {
        servicesGroup.enter()
        //        Alert.showLoader(message: "")
        
        let userId = (Singleton.sharedInstance.CurrentUser == nil) ? -1 : (Singleton.sharedInstance.CurrentUser!.id!)
        
        let param = ["user_id": userId ]
        
        SubscriptionServices.SubscriptionPackage(param: param , completionHandler: {(status, response, error) in
            self.servicesGroup.leave()
            if !status {
                if error != nil {
                    print("Error: \((error as! Error).localizedDescription)")
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                return
            }
            
            print("Main")
            print(response!)
            //            Alert.hideLoader()
            print("SUCCESS")
            
            self.subscription = SubscriptionResult(object:(response?["Result"])!)
            
            for obj in (self.subscription?.images!)! {
                let almofireObj = AlamofireSource(urlString: obj.subscriptionImage!)
                self.alamofireSource?.append(almofireObj!)
            }
            self.slideshow.setImageInputs(self.alamofireSource!)
            self.slideshow.contentScaleMode = .scaleAspectFill
            
            self.slideshow.zoomEnabled = true
            
            self.braintreeClient = BTAPIClient(authorization: self.subscription!.clientId!)
            //swift 3
            DispatchQueue.main.async{
                self.slideshow.setImageInputs(self.alamofireSource!)
                self.tblView.reloadData()
            }
        })
    }
    
    //MARK:-  Get Token Service
    
    func GetToken() {
        servicesGroup.enter()
        //        Alert.showLoader(message: "")
        
        let userId = (Singleton.sharedInstance.CurrentUser == nil) ? -1 : (Singleton.sharedInstance.CurrentUser!.id!)
        
        let param = ["user_id": userId ]
        
        SubscriptionServices.SubscriptionToken(param: param , completionHandler: {(status, response, error) in
            self.servicesGroup.leave()
            if !status {
                if error != nil {
                    print("Error: \((error as! Error).localizedDescription)")
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                return
            }
            
            print("Main")
            print(response!)
            //            Alert.hideLoader()
            print("SUCCESS")
            
            self.token = (response?["token"].stringValue)!
            
        })
    }
    
    func GetContactDetails() {
        
        Alert.showLoader(message: "")
        
        var param = [String:String]()
        
        if !isLanguageEnglish() {
            param.updateValue("ar", forKey: "lang")
        } else {
            param.updateValue("en", forKey: "lang")
        }
        
        HelpAndAboutServices.ContactDetails(param:param,completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    print("Error: \((error as! Error).localizedDescription)")
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                return
            }
            
            print(response!)
            
            self.contact = ContactDetails(object:(response?["Result"])!)
            self.lblAddress.text = self.contact.address
            self.lblPhone.text = self.contact.phone
            self.lblEmailAddress.text = self.contact.email
            
        })
    }
    
    //MARK:- Menu Action
    
    @IBAction func MenuAction(sender: UIBarButtonItem) {
        self.slideMenuController()?.toggleLeft()
    }
    
    @IBAction func callusAction(_ sender : UIButton) {
        
        self.shadowView.isHidden = false
        self.callUsView.isHidden = false
    }
    
    @IBAction func closeAction(_ sender : UIButton) {
        
        self.shadowView.isHidden = true
        self.callUsView.isHidden = true
    }
    
    @IBAction func openEmail(_ sender : UIButton) {
        
        self.email()
    }
    
    @IBAction func openPhone(_ sender : UIButton) {
        
        self.phoneNumber()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.subscription != nil {
            return 2
        }
        return 0
        
        //        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SubscriptionTableViewCell") as! SubscriptionTableViewCell
        
        cell.delegate = self
        
        if indexPath.row == 0 {
            cell.title.text = NSLocalizedString("Individual User", comment: "")
            cell.individual = (self.subscription?.individual)!
        }
        else {
            cell.title.text = NSLocalizedString("Commercial User", comment: "")
            cell.commercial = (self.subscription?.commercial)!
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Row: \(indexPath.row)")
    }
    
    func showDropIn(clientTokenOrTokenizationKey: String, packageID: Int) {
        
        let request = BTDropInRequest()
        //request.threeDSecureVerification = true
        //        request.amount = "1.00"
        let dropIn = BTDropInController(authorization: self.token, request: request)
        { (controller, result, error) in
            if (error != nil) {
                print("ERROR")
                Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: error!.localizedDescription)
            } else if (result?.isCancelled == true) {
                print("CANCELLED")
            } else if let result = result {
                if let nonce = result.paymentMethod?.nonce {
                    print(nonce)
                    
                    Alert.showLoader(message: "")
                    self.backGroundView.isUserInteractionEnabled = false
                    self.scroll.isUserInteractionEnabled = false
                    
                    let userId = Singleton.sharedInstance.CurrentUser!.id!
                    let param = ["user_id": userId, "package_id": packageID, "nonce": nonce] as [String : Any]
                    
                    SubscriptionServices.SubscriptionUserPackage(param: param , completionHandler: {(status, response, error) in
                        
                        Alert.hideLoader()
                        self.backGroundView.isUserInteractionEnabled = true
                        self.scroll.isUserInteractionEnabled = true
                        
                        if !status {
                            if error != nil {
                                print("Error: \((error as! Error).localizedDescription)")
                                return
                            }
                            let msg = response?["Message"].stringValue
                            print("Message: \(String(describing: msg))")
                            return
                        }
                        
                        print("Main")
                        print(response!)
                        // Alert.hideLoader()
                        print("SUCCESS")
                        
                        let msg = response?["Message"].string
                        Alert.showAlert(title: "Alert", message: msg!)
                        
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: SUBSCRIPTIONUPDATED), object: nil)
                    })
                }
                else
                {
                    Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Some kind of error occures! Please try later", comment: ""))
                }
            }
            controller.dismiss(animated: true, completion: nil)
        }
        
        self.present(dropIn!, animated: true, completion: nil)
    }
    
}

extension SubscriptionViewController: SubscriptionDelegate  {
    
    func didSelectSubscription(packageID: Int) {
        
        let alertController = UIAlertController(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Are you sure you want to Subscribe this Package?", comment: ""), preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: NSLocalizedString("No", comment: ""), style: .cancel){ (_) -> Void in
            
        }
        let doneAction = UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: .default) { (_) -> Void in
            //            self.showDropIn(clientTokenOrTokenizationKey: self.subscription!.clientId!, packageID: packageID)
            //            print(self.subscription!.clientId!)
            //            alertController.dismiss(animated: true, completion: nil)
            
            //            self.showDropIn(clientTokenOrTokenizationKey: "sandbox_p8r6hxd7_3r54wq8ct22jhc68", packageID: packageID) //Testing
            self.showDropIn(clientTokenOrTokenizationKey: self.token, packageID: packageID) //Testing
            
        }
        alertController.addAction(cancelAction)
        alertController.addAction(doneAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
}

