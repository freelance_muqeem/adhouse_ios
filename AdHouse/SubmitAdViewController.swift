//
//  SubmitAdViewController.swift
//  AdHouse
//
//  Created by  on 8/11/17.
//  Copyright © 2017 . All rights reserved.
//

import UIKit
import ImagePicker

protocol SubmitDelegate {
    
    func submitRefresh()
}

enum Type {
    
    case TextFieldType
    case ButtonType
}

extension SubmitAdViewController:CategoryDelegate {
    
    func didSelectCategory(position: Int) {
        let count = self.attributesArray?.count
        for index in 0..<count! {
            let indexPath = IndexPath(row: index+1, section: 0)
            if let cell = self.tblView.cellForRow(at: indexPath) as? DropDownCell {
                cell.txtField.text = ""
            }
            if let cell = self.tblView.cellForRow(at: indexPath) as? TextFieldCell {
                cell.txtField.text = ""
            }
        }
        
        self.tblView.reloadData()
        self.getFields(position: position)
        
        //        if position == 2 {
        self.tblView.reloadData()
        self.getSubCategories(parentId: (self.categoriesArray?[position].id!)!)
        //        }
    }
    
    func didSelectSubCategory(position: Int) {
        self.getFieldsForSubCat(position: position)
    }
    
    func didSelectCountry(position: Int) {
        
    }
}

//extension SubmitAdViewController:HeaderDelegate {
//
//    func changeCategoryType(type: CategoryType) {
//
//        self.selectedCategoryType = type
//        self.tblView.reloadData()
//        print(self.selectedCategoryType!)
//    }
//}


class SubmitAdViewController: UIViewController , UITableViewDelegate , UITableViewDataSource {
    
    
    class func instantiateFromStoryboard() -> SubmitAdViewController {
        let storyboard = UIStoryboard(name: "HomeList", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! SubmitAdViewController
    }
    
    @IBOutlet weak var tblView:UITableView!
    
    var attributesArray:[AttributesResult]? = [AttributesResult]()
    var categoriesArray:[CategoryResult]? = [CategoryResult]()
    var subCategoriesArray:[CategoryResult]? = [CategoryResult]()
    
    var leftBtn:UIButton?
    var type:Type?
    var selectedCategoryType:CategoryType?
    var delegate:SubmitDelegate?
    var selectedLang: [String]!
    
    var selectedParentIds = [Int]()
    var selectedTitles = [String]()
    var paramsHolder = ParamsHolder()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        paramsHolder.params = [:]
        self.navigationController?.navigationBar.isHidden = false
        
        let navBackgroundImage:UIImage! = UIImage(named: "topbox")!
        self.navigationController?.navigationBar.setBackgroundImage(navBackgroundImage, for: .default)
        self.title = NSLocalizedString("Submit Ad", comment: "")
        self.placeBackBtn()
        
        selectedLang =  UserDefaults.standard.object(forKey: "AppleLanguages") as? [String] ?? [String]()
        
        self.type = Type.TextFieldType
        
        //Register Nib
        
        let headerNib = UINib.init(nibName: "HeaderView", bundle: nil)
        tblView.register(headerNib, forCellReuseIdentifier: "HeaderView")
        
        let dropdown = UINib.init(nibName: "DropDownCell", bundle: nil)
        tblView.register(dropdown, forCellReuseIdentifier: "DropDownCell")
        
        let text = UINib.init(nibName: "TextFieldCell", bundle: nil)
        tblView.register(text, forCellReuseIdentifier: "TextFieldCell")
        
        let btn = UINib.init(nibName: "ButtonCell", bundle: nil)
        tblView.register(btn, forCellReuseIdentifier: "ButtonCell")
        
        
        self.getAllCategories()
        
        
    }
    
    //MARK:-  Back Button Code
    
    func placeBackBtn(){
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        leftBtn =  UIButton(type: .custom)
        leftBtn?.setImage(UIImage(named: "backbtn"), for: .normal)
        leftBtn?.addTarget(self, action: #selector(SubmitAdViewController.BackBtnPressed), for: .touchUpInside)
        leftBtn?.frame = CGRect(x: 0, y:0 , width: 25, height: 25)
        let  leftBarButton: UIBarButtonItem = UIBarButtonItem(customView: leftBtn!)
        self.navigationItem.setLeftBarButton(leftBarButton, animated: true)
        
    }
    
    @objc func BackBtnPressed(){
        
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:-  Get Fields
    
    func getFields(position:Int) {
        
        self.view.endEditing(true)
        Alert.showLoader(message: "")
        

        var param = ["category_id": String(describing: self.categoriesArray![position].id!)]
        
        if !isLanguageEnglish() {
            param.updateValue("ar", forKey: "lang")
        } else {
            param.updateValue("en", forKey: "lang")
        }
        
        CategoryServices.GetAttributes(param:param, completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    print("Error: \((error as! Error).localizedDescription)")
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                return
            }
            
            print(response!)
            print("SUCCESS")
            Alert.hideLoader()
            self.attributesArray?.removeAll()
            self.selectedParentIds.removeAll()
            self.selectedTitles.removeAll()
            self.paramsHolder.attribsArray.removeAll()
            
            let adsResponseArray = response?["Result"].arrayValue
            print(adsResponseArray!)
            
            self.selectedParentIds.append(0)
            self.selectedTitles.append("")
            self.selectedParentIds.append(0)
            self.selectedTitles.append("")
            
            for resultObj in adsResponseArray! {
                let obj =  AttributesResult(json: resultObj)
                self.attributesArray?.append(obj)
                self.selectedParentIds.append(0)
                self.selectedTitles.append("")
                self.paramsHolder.attribsArray.append( ["id":0,"value":"","selection_id":0])
            }
            
            self.tblView.reloadData()
        })
    }
    
    //MARK:-  Get Fields
    
    func getFieldsForSubCat(position:Int) {
        
        self.view.endEditing(true)
        Alert.showLoader(message: "")
        
        var param = ["category_id": String(describing: self.subCategoriesArray![position].id!)]
        if !isLanguageEnglish() {
            param.updateValue("ar", forKey: "lang")
        } else {
            param.updateValue("en", forKey: "lang")
        }
        
        CategoryServices.GetAttributes(param:param, completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    print("Error: \((error as! Error).localizedDescription)")
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                return
            }
            
            print(response!)
            print("SUCCESS")
            Alert.hideLoader()
            self.attributesArray?.removeAll()
            self.selectedParentIds.removeAll()
            self.selectedTitles.removeAll()
            self.paramsHolder.attribsArray.removeAll()
            
            let adsResponseArray = response?["Result"].arrayValue
            print(adsResponseArray!)
            self.selectedParentIds.append(0)
            self.selectedTitles.append("")
            self.selectedParentIds.append(0)
            self.selectedTitles.append("")
            
            for resultObj in adsResponseArray! {
                let obj =  AttributesResult(json: resultObj)
                self.attributesArray?.append(obj)
                self.selectedParentIds.append(0)
                self.selectedTitles.append("")
                self.paramsHolder.attribsArray.append( ["id":0,"value":"","selection_id":0])
            }
            
            self.tblView.reloadData()
        })
    }
    
    //MARK:-  Get All Categories
    
    func getAllCategories() {
        
        self.view.endEditing(true)
        Alert.showLoader(message: "")
        
        
        var param = [String:String]()
        
        if !isLanguageEnglish() {
            param.updateValue("ar", forKey: "lang")
        } else {
            param.updateValue("en", forKey: "lang")
        }
        
        CategoryServices.GetCategories(param:param , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    print("Error: \((error as! Error).localizedDescription)")
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                return
            }
            
            print(response!)
            print("SUCCESS")
            self.categoriesArray?.removeAll()
            
            let adsResponseArray = response?["Result"].arrayValue
            print(adsResponseArray!)
            
            for resultObj in adsResponseArray! {
                let obj =  CategoryResult(json: resultObj)
                self.categoriesArray?.append(obj)
            }
            
            self.tblView.reloadData()
        })
    }
    
    //MARK:-  Get Sub Categories
    
    func getSubCategories(parentId: Int) {
        
        self.view.endEditing(true)
        Alert.showLoader(message: "")
       
        var param = [String:String]()
        
        if !isLanguageEnglish() {
            param.updateValue("ar", forKey: "lang")
        } else {
            param.updateValue("en", forKey: "lang")
        }
        
        param.updateValue(String(parentId) , forKey: "category_id")
        CategoryServices.GetSubCategories(param:param, completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    print("Error: \((error as! Error).localizedDescription)")
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                return
            }
            
            print(response!)
            print("SUCCESS")
            
            self.subCategoriesArray?.removeAll()
            
            let adsResponseArray = response?["Result"].arrayValue
            print(adsResponseArray!)
            
            for resultObj in adsResponseArray! {
                let obj =  CategoryResult(json: resultObj)
                self.subCategoriesArray?.append(obj)
            }
            
            self.tblView.reloadData()
            
        })
    }
    
    //MARK:-  TableView Delegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        if indexPath.row == 0 {
            if subCategoriesArray?.count == 0 {
                return GISTUtility.convertToRatio(620)
            }
            return GISTUtility.convertToRatio(680)
            //            if selectedCategoryType == .CLASSIFIED {
            //
            //                return GISTUtility.convertToRatio(633)
            //
            //            } else if (selectedCategoryType == .PROPERTY) {
            //
            //               return GISTUtility.convertToRatio(574)
            //
            //            } else if (selectedCategoryType == .VEHICLES) {
            //
            //                return GISTUtility.convertToRatio(574)
            //
            //            } else{
            //
            //                return GISTUtility.convertToRatio(574)
            //
            //            }
            
            
        } else if indexPath.row == (self.attributesArray?.count)! + 2 {
            
            return GISTUtility.convertToRatio(55)
            
        } else {
            
            return GISTUtility.convertToRatio(80)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return (self.attributesArray?.count)! + 2
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            
            let headerCell:HeaderView = tableView.dequeueReusableCell(withIdentifier: "HeaderView") as! HeaderView
            headerCell.delegate = self
          headerCell.paramHelper = paramsHolder
            
            var ids: [Int] = [Int]()
            var titles: [String] = [String]()
            for result in self.categoriesArray! {
                print("Title \(String(describing: result.name))")
                ids.append(result.id!)
                titles.append(result.name!)
            }
            headerCell.categoryIDs = ids
            headerCell.categoryDropDown.dataSource = titles
            
            var subCatids: [Int] = [Int]()
            var subCatTitles: [String] = [String]()
            for result in self.subCategoriesArray! {
                print("Title \(String(describing: result.name))")
                subCatids.append(result.id!)
                subCatTitles.append(result.name!)
            }
            headerCell.subCatIDs = subCatids
            headerCell.subCategoryDropDown.dataSource = subCatTitles
            
            if titles.count > 0 {
                headerCell.txtCategory.placeholder = titles.first!
            }
            
            if subCatTitles.count > 0
            {
                headerCell.txtSubCategory.placeholder = subCatTitles.first!
                headerCell.heightSubCategoryConstraint.constant = 17
                headerCell.heightSubCategoryView.constant = 40
                headerCell.subCategoryView.isHidden = false
            }
            else {
                headerCell.txtSubCategory.placeholder = ""
                headerCell.txtSubCategory.text = ""
                headerCell.heightSubCategoryConstraint.constant = 0
                headerCell.heightSubCategoryView.constant = 0
                headerCell.subCategoryView.isHidden = true
            }
            headerCell.setTextViewDelegate()
            return headerCell
        }
        else if indexPath.row == (self.attributesArray?.count)! + 1 {
            
            let buttonCell:ButtonCell = tableView.dequeueReusableCell(withIdentifier: "ButtonCell") as! ButtonCell
            buttonCell.btnPost.addTarget(self, action: #selector(self.SubmitAction(sender:)), for: .touchUpInside)
            return buttonCell
        }
        else {
            
            let obj = self.attributesArray?[indexPath.row-1]
            paramsHolder.attribsArray[indexPath.row-1]["id"] = obj!.id! 
            switch obj?.attributeType {
                
            case "dropdown"?:
                
                let dropdownCell : DropDownCell = tableView.dequeueReusableCell(withIdentifier: "DropDownCell") as! DropDownCell
                
                dropdownCell.lblName.text = obj?.attributeName!
                
                var titles: [String] = [String]()
                 var ids = [Int]()
                for result in (obj?.dropdownlist)! {
                    print("Title \(String(describing: result.value))")
                    titles.append(result.value!)
                     ids.append(result.id!)
                }
                dropdownCell.selectedIDs = ids
                dropdownCell.dropDown.dataSource = titles
                dropdownCell.indexPath = indexPath
                
                //  dropdownCell.txtField.text = ""
                dropdownCell.txtField.placeholder = NSLocalizedString("Select Type", comment: "")
                dropdownCell.txtField.text = selectedTitles[indexPath.row]
                
                dropdownCell.selectedID = { [weak self] index , id , title , itemID in
                    
                    
                    let selectedObj = self?.attributesArray?[index-1]
                    let selecteId = selectedObj?.dropdownlist![id].id
                    self?.selectedParentIds[index-1] = selecteId!
                    self?.selectedTitles[index] = title
                    dropdownCell.txtField.text = title
                    
                    //--ww      self?.paramsHolder.params.updateValue(title, forKey: "\(String(describing: selectedObj!.id!))") "\(String(describing:itemID))"
                    self?.paramsHolder.attribsArray[index-1] =  ["id":selectedObj!.id! ,"value":title,"selection_id": itemID]
                    
                    if !(obj?.childIds?.isEmpty)!{
                        if let cell = self?.tblView.cellForRow(at: IndexPath(row:index + 1,section:0)) as? DropDownCell {
                            cell.txtField.text = ""
                            self?.selectedTitles[index + 1] = ""
                            self?.paramsHolder.attribsArray[index]["value"] = ""
                            self?.paramsHolder.attribsArray[index]["selection_id"] = 0
                            
                        }
                    }
                }
                
                dropdownCell.dropDownAction = {
                    
                    let ind = indexPath.row - 2
                    let indexRow = ind > -1 ? ind : 0
                    let parentId = self.selectedParentIds[indexRow]
                    
                    if parentId == 0 && obj?.dropdownlist?[0].parent_attribute_value_Id != 0 {
                        let parentObjTitle = self.attributesArray?[ind].attributeName ?? ""
                        Alert.showAlert(title: "Alert", message: "Please select \(parentObjTitle) first")
                    }else if parentId != 0 {
                        
                        var titles = [String]()
                        var ids = [Int]()
                        var ddList = obj?.dropdownlist?.filter{ $0.parent_attribute_value_Id == parentId}
                        ddList = (ddList?.count)! > 0 ? ddList : obj?.dropdownlist!
                        for result in ddList! {
                            titles.append(result.value!)
                            ids.append(result.id!)
                        }
                        dropdownCell.dropDown.dataSource = titles
                        dropdownCell.selectedIDs = ids
                        dropdownCell.dropDown.show()
                    }else{
                        dropdownCell.dropDown.show()
                    }
                }
                return dropdownCell
                
            case "text"? :
                
                let textCell : TextFieldCell = tableView.dequeueReusableCell(withIdentifier: "TextFieldCell") as! TextFieldCell
                textCell.lblName.text = obj?.attributeName!
                if obj?.keyboardType == "numpad" {
                    if #available(iOS 10.0, *) {
                        textCell.txtField.keyboardType = .asciiCapableNumberPad
                    } else {
                        // Fallback on earlier versions
                        textCell.txtField.keyboardType = .numberPad
                    }
                }
                //  textCell.txtField.text = ""
                textCell.txtField.placeholder = obj?.attributeName
                textCell.paramHelper = paramsHolder
                textCell.itemID = obj?.id!
                textCell.indexPath = indexPath
                return textCell
                
            default:
                
                let cell = UITableViewCell()
                return cell
            }
            
        }
    }
    
    @IBAction func SubmitAction(sender: UIButton) {
        
       
        paramsHolder.params.updateValue((Singleton.sharedInstance.CurrentUser != nil) ? (Singleton.sharedInstance.CurrentUser?.id)! : -1, forKey: "user_id")
        
     if paramsHolder.params["latitude"] == nil {
                Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Kindly provide location", comment: ""))
                return
            }
        
        if paramsHolder.params["title"] == nil || (paramsHolder.params["title"] as! String).isEmptyOrWhitespace() {
            Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Kindly provide title", comment: ""))
            return
        }
       
        
        
        if paramsHolder.params["category_id"] == nil{
            Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Kindly provide cateogory", comment: ""))
            return
        }
        
        if paramsHolder.imagesArray.count < 1 {
            Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Kindly provide at least one image", comment: ""))
            return
        }
        
        if paramsHolder.params["price"] == nil || (paramsHolder.params["price"] as! String).isEmptyOrWhitespace(){
            Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Kindly provide price", comment: ""))
            return
        }
        
        
        if paramsHolder.params["description"] == nil || (paramsHolder.params["description"] as! String).isEmptyOrWhitespace(){
            Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Kindly provide description", comment: ""))
            return
        }
        
        if paramsHolder.params["featured"] == nil {
            Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Kindly provide featured value", comment: ""))
            return
        }
        
         if paramsHolder.params["phone"] == nil || (paramsHolder.params["phone"] as! String).isEmptyOrWhitespace() || (paramsHolder.params["phone"] as! String).count < 7  {
            
            Alert.showAlert(title: NSLocalizedString("Alert", comment: "") , message:   NSLocalizedString("Phone number at least 7 characters", comment: ""))
            return
        }
        
      
       /*
        for item in paramsHolder.attribsArray {
            if (item["value"] as! String).isEmptyOrWhitespace() {
                 Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Please fill all Fields", comment: ""))
                return
            }
        } */
        
        for (index,item) in paramsHolder.attribsArray.enumerated() {
            if (item["value"] as! String).isEmptyOrWhitespace() {
                Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Please fill all Fields", comment: ""))
                return
            }
             print("filter", item["value"] as! String)
            if item["value"] as! String == "0" {
                
                paramsHolder.attribsArray[index]["value"] = "1"
            }
           
        }
        
        paramsHolder.params.updateValue(paramsHolder.attribsArray.jsonString()!, forKey: "attributes")
        
        if !isLanguageEnglish() {
            paramsHolder.params.updateValue("ar", forKey: "lang")
        } else {
            paramsHolder.params.updateValue("en", forKey: "lang")
        }
        
        print(paramsHolder.params)
        
        
        Alert.showLoader(message: "")
        
        
        AdServices.SubmitAd(param: paramsHolder.params, images: paramsHolder.imagesArray, completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    print("Error: \((error as! Error).localizedDescription)")
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                return
            }
            
            print("Main")
            print(response!)
            
            print("SUCCESS")
            
            self.delegate?.submitRefresh()
            self.navigationController?.popViewController(animated: true)
            self.delegate?.submitRefresh()
            
            //            self.perform(#selector(self.performAction), with: nil, afterDelay: 3)
            
            Alert.showAlert(title:NSLocalizedString("Alert", comment: "") , message: NSLocalizedString("Ad Submitted Successfully, pending for approval", comment: ""))
            
        })
        
        return
        
        var parameters = [String:Any]()
        var images: [UIImage] = [UIImage]()
        
        if let cell = self.tblView.cellForRow(at: IndexPath(row:0,section:0)) as? HeaderView {
            if cell.cordinates == nil {
                Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Please fill all Fields", comment: ""))
                return
            }
            else {
                parameters.updateValue(cell.txtLocation.text!, forKey: "area")
                parameters.updateValue("\(cell.cordinates.coordinate.latitude)", forKey: "latitude")
                parameters.updateValue("\(cell.cordinates.coordinate.longitude)", forKey: "longitude")
            }
            
            if (cell.txtTitle.text?.isEmptyOrWhitespace())! {
                Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Please fill all Fields", comment: ""))
                return
            }
            else {
                parameters.updateValue(cell.txtTitle.text!, forKey: "title")
            }
            
            if (cell.txtCategory.text?.isEmptyOrWhitespace())! {
                Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Please fill all Fields", comment: ""))
                return
            }
            else {
                parameters.updateValue(cell.selectedCatID!, forKey: "category_id")
            }
            
            let phone = cell.txtMobile.text
            if (phone?.count)! <= 7  {
                
                Alert.showAlert(title: NSLocalizedString("Alert", comment: "") , message:   NSLocalizedString("Phone number at least 8 characters", comment: ""))
                return
            }
            
            
            if cell.subCatIDs.count != 0 {
                //            if cell.txtCategory.text == "classified" {
                if (cell.txtSubCategory.text?.isEmptyOrWhitespace())! {
                    Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Please fill all Fields", comment: ""))
                    return
                }
                else {
                    parameters.updateValue(cell.selectedSubCatID!, forKey: "sub_category_id")
                }
                //            }
            }
            
            var index = 0
            for item in cell.thumbArray {
                index += 1
                images.append(item)
            }
            
            if index == 0 {
                Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Please fill all Fields", comment: ""))
                return
            }
            
            if (cell.txtPrice.text?.isEmptyOrWhitespace())! {
                Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Please fill all Fields", comment: ""))
                return
            }
            else {
                parameters.updateValue(cell.txtPrice.text!, forKey: "price")
            }
            
            if (cell.txtDiscription.text?.isEmptyOrWhitespace())! {
                Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Please fill all Fields", comment: ""))
                return
            }
            else {
                parameters.updateValue(cell.txtDiscription.text!, forKey: "description")
            }
            
            if (cell.txtFeature.text?.isEmptyOrWhitespace())! {
                Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Please fill all Fields", comment: ""))
                return
            }
            else {
                if cell.txtFeature.text == "Yes" {
                    parameters.updateValue(1, forKey: "featured")
                }
                else {
                    parameters.updateValue(0, forKey: "featured")
                }
            }
            
            if (cell.txtMobile.text?.isEmptyOrWhitespace())! {
                Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Please fill all Fields", comment: ""))
                return
            }
            else {
                parameters.updateValue(cell.txtMobile.text!, forKey: "phone")
            }
            
        }
                
        for (index, item) in (self.attributesArray?.enumerated())! {
            if let cell = self.tblView.cellForRow(at: IndexPath(row:index+1,section:0)) as? DropDownCell {
                if (cell.txtField.text?.isEmptyOrWhitespace())! {
                    Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Please fill all Fields", comment: ""))
                    return
                }
//                parameters.updateValue(cell.txtField.text!, forKey: "\(String(describing: item.id!))")
                parameters.updateValue(item.dropdownlist![cell.selectedIndex].id!, forKey: "\(String(describing: item.id!))")
                continue
            }
            if let cell = self.tblView.cellForRow(at: IndexPath(row:index+1,section:0)) as? TextFieldCell {
                if (cell.txtField.text?.isEmptyOrWhitespace())! {
                    Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Please fill all Fields", comment: ""))
                    return
                }
                parameters.updateValue(cell.txtField.text!, forKey: "\(String(describing: item.id!))")
            }
        }
        
        Alert.showLoader(message: "")
        
        parameters.updateValue((Singleton.sharedInstance.CurrentUser?.id)!, forKey: "user_id")
        
        print("Parameters: \(parameters)")
        
        AdServices.SubmitAd(param: parameters, images: images, completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    print("Error: \((error as! Error).localizedDescription)")
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                return
            }
            
            print("Main")
            print(response!)
            
            print("SUCCESS")
            
            self.delegate?.submitRefresh()
            self.navigationController?.popViewController(animated: true)
            self.delegate?.submitRefresh()
            
            //            self.perform(#selector(self.performAction), with: nil, afterDelay: 3)
            Alert.showAlert(title:NSLocalizedString("Alert", comment: "") , message: NSLocalizedString("Ad Posted Successfully", comment: ""))
            
        })
    }
    
    func performAction(){
        
        self.delegate?.submitRefresh()
    }
}


