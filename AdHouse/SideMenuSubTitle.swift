//
//  SideMenuSubTitle.swift
//  AdHouse
//
//  Created by maazulhaq on 24/07/2017.
//  Copyright © 2017 . All rights reserved.
//

import UIKit

class SideMenuSubTitle: UITableViewCell {
    
    @IBOutlet var labelTitle: UILabel!
    @IBOutlet var labelSubTitle: UILabel!
    @IBOutlet var imageViewIcon: UIImageView!
    @IBOutlet var seperatorView: UIView!
    
    var cellIndexPath:IndexPath?
    
    var color = UIColor(rgb: 0x9C0A00)
    
    func setValues(title: String, subTitle: String, image:UIImage){
        
        labelTitle.text = title
        labelSubTitle.text = title
        imageViewIcon.image = image
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setFonts()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        self.contentView.backgroundColor = selected ? self.color : UIColor.white
        self.labelTitle.textColor = selected ? UIColor.white : UIColor.darkGray
        self.labelSubTitle.textColor = selected ? UIColor.white : UIColor.darkGray
        if selected {
            switch cellIndexPath?.row {
            case 0?:
                self.imageViewIcon.image = UIImage(named: "homewhite")!
                self.seperatorView.isHidden = true
            case 1?:
                self.imageViewIcon.image = UIImage(named: "notificationwhite")!
                self.seperatorView.isHidden = true
            case 2?:
                self.imageViewIcon.image = UIImage(named: "heart-outline")!
                self.seperatorView.isHidden = true
            case 3?:
                self.imageViewIcon.image = UIImage(named: "adswhite")!
                self.seperatorView.isHidden = true
            case 4?:
                self.imageViewIcon.image = UIImage(named: "PROFILEwhite")!
                self.seperatorView.isHidden = true
            case 5?:
                self.imageViewIcon.image = UIImage(named: "helpwhite")!
                self.seperatorView.isHidden = true
            case 6?:
                self.imageViewIcon.image = UIImage(named: "Subscription-Commercialwhite")!
                self.seperatorView.isHidden = true
            case 7?:
                self.imageViewIcon.image = UIImage(named: "logoutwhite")!
                self.seperatorView.isHidden = true
            default:
                break
            }
        }
        else {
            switch cellIndexPath?.row {
            case 0?:
                self.imageViewIcon.image = UIImage(named: "home")!
                self.seperatorView.isHidden = false
            case 1?:
                self.imageViewIcon.image = UIImage(named: "notification")!
                self.seperatorView.isHidden = false
            case 2?:
                self.imageViewIcon.image = UIImage(named: "heart_sidebar")!
                self.seperatorView.isHidden = false
            case 3?:
                self.imageViewIcon.image = UIImage(named: "ads")!
                self.seperatorView.isHidden = false
            case 4?:
                self.imageViewIcon.image = UIImage(named: "PROFILE")!
                self.seperatorView.isHidden = false

            case 5?:
                self.imageViewIcon.image = UIImage(named: "help")!
                self.seperatorView.isHidden = false
            case 6?:
                self.imageViewIcon.image = UIImage(named: "Subscription-Commercial")!
                self.seperatorView.isHidden = false
            case 7?:
                self.imageViewIcon.image = UIImage(named: "logout")!
                self.seperatorView.isHidden = false
            default:
                break
            }
        }
    }
    
    func setFonts() {
        labelTitle.font = FontUtility.getFontWithAdjustedSize(size: 12, desireFontType: .Roboto_Regular)
        labelSubTitle.font = FontUtility.getFontWithAdjustedSize(size: 12, desireFontType: .Roboto_Regular)
    }

}
