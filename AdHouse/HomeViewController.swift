//
//  ViewController.swift
//  AdHouse
//
//  Created by  on 7/22/17.
//  Copyright © 2017 . All rights reserved.
//

import UIKit

extension UIBarButtonItem {
    func addTargetForAction(target: AnyObject, action: Selector) {
        self.target = target
        self.action = action
    }
}

extension HomeViewController: SubmitDelegate {
    
    func submitRefresh() {
        self.homeTbleView.reloadData()
    }
}

extension HomeViewController: RefreshDelegate {
    
    func didRefresh() {
    }
    
    func didRefresh(count: CGFloat) {
        self.numberofcell = count
        self.homeTbleView.reloadRows(at: [IndexPath.init(row: 2, section: 0)], with: .none)
        //self.homeTbleView.reloadData()
    }
    
}

extension HomeViewController:CellBtnPressedProtocol {
    
    func disSelectClassifiedCategory(cellIndexPath: IndexPath, product: ClassifiedCategory) {
        
        let homeListVc = HomeListViewController.instantiateFromStoryboard()
        homeListVc.classified = product
        self.navigationController?.pushViewController(homeListVc, animated: true)
    }
    
    func viewAllBtnPressed(cellIndexPath: IndexPath) {
        
        let homeListVc = HomeListViewController.instantiateFromStoryboard()
        homeListVc.title = self.searchTypeArray![cellIndexPath.row].name!
        if cellIndexPath.row == 0{
            homeListVc.selectedCategory =  CategoryType.VEHICLES
            
        }
            
        else if cellIndexPath.row == 1{
            homeListVc.selectedCategory =  CategoryType.PROPERTY
            
        }
            
        else {
            homeListVc.selectedCategory =  CategoryType.CLASSIFIED
            
        }
        self.navigationController?.pushViewController(homeListVc, animated: true)
        
    }
    
    func disSelectCategoryObject(cellIndexPath:IndexPath, product: ProductResult) {
        let detailVc = DetailViewController.instantiateFromStoryboard()
        detailVc.productID = product.id!
        self.navigationController?.pushViewController(detailVc, animated: true)
    }
    
    func disSelectRowAtIndexPath(cellIndexPath:IndexPath){
        
        //        let detailVc = DetailViewController.instantiateFromStoryboard()
        //        //detailVc.productObj = self.
        //        self.navigationController?.pushViewController(detailVc, animated: true)
    }
    
    func contactSellerBtnPressed()
    {
        
    }
    
}

class HomeViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var homeTbleView:UITableView!
    
    var classifiedCount:Int?
    var numberofcell:CGFloat = 0.0
    var selectedLang: [String]!
    
    class func instantiateFromStoryboard() -> HomeViewController {
        let storyboard = UIStoryboard(name: "SideMenu", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! HomeViewController
    }
    
    @IBOutlet weak var menuButton: UIBarButtonItem!
    
    var searchTypeArray:[searchCategory]? = [searchCategory]()
    
    var leftBtn:UIButton?
    var rightSearchBtn:UIButton?
    var rightNotificationBtn:UIButton?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.placeNavigationBtn()
        
        selectedLang =  UserDefaults.standard.object(forKey: "AppleLanguages") as? [String] ?? [String]()
        //register cell
        let nib = UINib(nibName:String(describing:HomeTableViewCell.self), bundle: nil)
        let nib2 = UINib(nibName:String(describing:home_vertical_TableViewCell.self), bundle: nil)
        
        homeTbleView.register(nib, forCellReuseIdentifier: String(describing: HomeTableViewCell.self))
        homeTbleView.register(nib2, forCellReuseIdentifier: String(describing: home_vertical_TableViewCell.self))
        let navBackgroundImage:UIImage! = UIImage(named: "topbox")!
        self.navigationController?.navigationBar.setBackgroundImage(navBackgroundImage, for: .default)
        
        if Singleton.sharedInstance.CurrentUser != nil {
            if let savedDeviceToken =   UserDefaults.standard.string(forKey: "DEVICE_TOKEN"){
                print(savedDeviceToken)
                Singleton.sharedInstance.CurrentUser?.deviceToken = savedDeviceToken
                APP_DELEGATE.serviceSetDeviceToken(deviceToken: savedDeviceToken)
            }
        }
        
        self.searchType()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black,NSAttributedStringKey.font : UIFont(name: "HelveticaNeue-Medium", size: 17)!]
        if Singleton.sharedInstance.CurrentUser != nil {
            self.Block()
        }
    }
    
    //MARK:-  Back Button Code
    
    func placeNavigationBtn() {
        
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        
        leftBtn = UIButton(type: .custom)
        leftBtn?.setImage(UIImage(named: "bropdown"), for: .normal)
        leftBtn?.addTarget(self, action: #selector(HomeViewController.MenuAction), for: .touchUpInside)
        leftBtn?.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        let leftBarButton: UIBarButtonItem = UIBarButtonItem(customView: leftBtn!)
        self.navigationItem.setLeftBarButton(leftBarButton, animated: true)
        
        rightSearchBtn =  UIButton(type: .custom)
        rightSearchBtn?.setImage(UIImage(named: "searchbtn"), for: .normal)
        rightSearchBtn?.addTarget(self, action: #selector(HomeViewController.searchAction), for: .touchUpInside)
        rightSearchBtn?.frame = CGRect(x: 0, y:0 , width: 30, height: 30)
        let rightBarSearchButton: UIBarButtonItem = UIBarButtonItem(customView: rightSearchBtn!)
        
        rightNotificationBtn =  UIButton(type: .custom)
        rightNotificationBtn?.setImage(UIImage(named: "notification2"), for: .normal)
        rightNotificationBtn?.addTarget(self, action: #selector(HomeViewController.NotificationAction), for: .touchUpInside)
        rightNotificationBtn?.frame = CGRect(x: 0, y:0 , width: 30, height: 30)
        let rightBarNotiButton: UIBarButtonItem = UIBarButtonItem(customView: rightNotificationBtn!)
        
        self.navigationItem.rightBarButtonItems = [ rightBarSearchButton , rightBarNotiButton ]
        
    }

    @objc func MenuAction() {
        self.slideMenuController()?.toggleLeft()
    }
    
    //MARK:-  Search Action
    
    @objc func searchAction() {
        
        let searchVc = SearchViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(searchVc, animated: true)
        
    }
    
    @objc func NotificationAction() {
        
        if Singleton.sharedInstance.CurrentUser == nil {
            
            Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("You have logged in as Guest Member, in order to access this feature. you need to SignUp on App", comment: ""))
            
        } else {
            
            let notificationVc = NotificationViewController.instantiateFromStoryboard()
            notificationVc.isHome = true
            self.navigationController?.pushViewController(notificationVc, animated: true)
            
        }
    }
    
    @IBAction func submitAdAction(_ sender: UIButton) {
        
        if Singleton.sharedInstance.CurrentUser == nil {
            
            Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("You have logged in as Guest Member, in order to access this feature. you need to SignUp on App", comment: ""))
            
        } else {
            
            let submitVc = SubmitAdViewController.instantiateFromStoryboard()
            submitVc.delegate = self
            self.navigationController?.pushViewController(submitVc, animated: true)
            
        }
    }
    
    //MARK:-  Block Service
    
    func Block() {
        
        var lang:String!
        if isLanguageEnglish() {
            lang = "en"
        } else {
            lang = "ar"
        }
        
        Alert.showLoader(message: "")
        
        
        let userId = (Singleton.sharedInstance.CurrentUser == nil) ? -1 : (Singleton.sharedInstance.CurrentUser!.id!)
        let param = ["user_id":userId, "lang": lang] as [String : Any]
        
        HomeServices.Block(param: param, completionHandler: {(status, response, error) in
            Alert.hideLoader()
            if !status {
                if error != nil {
                    print("Error: \((error as! Error).localizedDescription)")
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                return
            }
            
            print("Main")
            print(response!)
            print("SUCCESS")
            
            let status = response?["Result"]["status"].intValue
            
            if status == 0 {
                
                Alert.showAlert(title: NSLocalizedString("Alert", comment: "") , message: NSLocalizedString("Your account has been blocked by admin, contact to admin", comment: ""))
                
                UserDefaults.standard.removeObject(forKey: User_data_userDefault)
                UserDefaults.standard.synchronize()
                
                Singleton.sharedInstance.CurrentUser = nil
                
                let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                let vc = storyboard.instantiateViewController(withIdentifier: "MainNavVC") as! UINavigationController
                UIWINDOW?.rootViewController = vc
                
            }
            
        })
        
    }
    
    //MARK:-  TableView Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.searchTypeArray!.count - 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (indexPath.row == 0) {
            
            let cell:HomeTableViewCell = tableView.dequeueReusableCell(withIdentifier:String(describing:HomeTableViewCell.self)) as!  HomeTableViewCell
            
            
            cell.lbl_txt_vechical.text = self.searchTypeArray![indexPath.row].name!
            //            cell.lbl_txt_vechical.text = NSLocalizedString("VEHICLES", comment: "")
            cell.selectionStyle = .none
            cell.delegate = self
            cell.category = CategoryType.VEHICLES
            cell.cellIndexPath = indexPath;
            
            return cell
            
        }
            
        else if indexPath.row == 1 {
            
            let cell:HomeTableViewCell = tableView.dequeueReusableCell(withIdentifier:String(describing:HomeTableViewCell.self)) as!  HomeTableViewCell
            
            cell.cellIndexPath = indexPath;
            cell.delegate = self
            
            cell.lbl_txt_vechical.text = self.searchTypeArray![indexPath.row].name!
            //            cell.lbl_txt_vechical.text = NSLocalizedString("PROPERTY", comment: "")
            cell.category = CategoryType.PROPERTY
            
            cell.selectionStyle = .none
            
            return cell
            
        }
            
        else {
            
            let cell:home_vertical_TableViewCell = tableView.dequeueReusableCell(withIdentifier:String(describing:home_vertical_TableViewCell.self)) as!  home_vertical_TableViewCell
            cell.cellIndexPath = indexPath;
            cell.delegate = self
            cell.refreshDelegate = self
            cell.selectionStyle = .none
            cell.lbl_txt_vechical.text = self.searchTypeArray![indexPath.row].name!
            //            cell.lbl_txt_vechical.text = NSLocalizedString("CLASSIFIED", comment: "")
            
            return cell
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 0 {
            return SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 220)
        } else if indexPath.row == 1 {
            return SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 220)
        }
        else {
            //number of cell in colleciton view
            
            if self.numberofcell == 0 {
                return 0
            }
            
            let rowHieght: CGFloat = 160
            let nubmerOfCellsPerRow: CGFloat = 3.0
            
            let i = (self.numberofcell / nubmerOfCellsPerRow).rounded(.awayFromZero)
            let j = (i * 10)
            
            print((i * rowHieght) + j)
            return SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: (i * rowHieght) + j)
            
        }
    }
    
    
    func searchType() {
        
        Alert.showLoader(message: "")
        var param = [String:String]()
        
        if !isLanguageEnglish() {
            param.updateValue("ar", forKey: "lang")
        } else {
            param.updateValue("en", forKey: "lang")
        }
        
        SearchServices.SearchCategoryType(param:param , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    print("Error: \((error as! Error).localizedDescription)")
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                return
            }
            
            print(response!)
            
            let typeArray = response?["Result"].arrayValue
            print(typeArray!)
            
            for resultObj in typeArray! {
                let obj =  searchCategory(json: resultObj)
                self.searchTypeArray?.append(obj)
            }
            
            self.homeTbleView.reloadData()
            
        })
        
    }
}



