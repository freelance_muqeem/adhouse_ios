//
//  HelpViewController.swift
//  AdHouse
//
//  Created by maazulhaq on 24/07/2017.
//  Copyright © 2017 . All rights reserved.
//

import UIKit

class HelpViewController: UIViewController {

    class func instantiateFromStoryboard() -> HelpViewController {
        let storyboard = UIStoryboard(name: "SideMenu", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! HelpViewController
    }
    
    @IBOutlet weak var txtView: UITextView!
    
    var leftBtn:UIButton?
    var selectedLang: [String]!

    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        selectedLang =  UserDefaults.standard.object(forKey: "AppleLanguages") as? [String] ?? [String]()

        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        let navBackgroundImage:UIImage! = UIImage(named: "topbox")!
        self.navigationController?.navigationBar.setBackgroundImage(navBackgroundImage, for: .default)
        
        self.title = NSLocalizedString("Help", comment: "")
        self.placeBackBtn()
        
        self.txtView.layer.borderWidth = 0.5
        self.txtView.layer.borderColor = UIColor.lightGray.cgColor
        self.txtView.addShadow()
        
        GetHelpContent()
    }
    
    //MARK:-  Back Button Code
    
    func placeBackBtn(){
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        leftBtn =  UIButton(type: .custom)
        leftBtn?.setImage(UIImage(named: "backbtn"), for: .normal)
        leftBtn?.addTarget(self, action: #selector(HelpViewController.BackBtnPressed), for: .touchUpInside)
        leftBtn?.frame = CGRect(x: 0, y:0 , width: 25, height: 25)
        let  leftBarButton: UIBarButtonItem = UIBarButtonItem(customView: leftBtn!)
        self.navigationItem.setLeftBarButton(leftBarButton, animated: true)
        
    }
    
    @objc func BackBtnPressed(){
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func GetHelpContent() {
        
        Alert.showLoader(message: "")
        
        var param = [String:String]()
        
        if !isLanguageEnglish() {
            param.updateValue("ar", forKey: "lang")
        } else {
            param.updateValue("en", forKey: "lang")
        }
        
        HelpAndAboutServices.Help(param:param , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    print("Error: \((error as! Error).localizedDescription)")
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                return
            }
            
            print("Main")
            print(response!)
            
            print("SUCCESS")
            
            self.txtView.text = response?["Result"]["content"].stringValue
            
        })
    }

}
