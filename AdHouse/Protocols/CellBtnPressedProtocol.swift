//
//  BtnPressedProtocol.swift
//  AdHouse
//
//  Created by Sharjeel ahmed on 7/26/17.
//  Copyright © 2017 . All rights reserved.
//

import Foundation
import UIKit


protocol CellBtnPressedProtocol {
    func viewAllBtnPressed(cellIndexPath:IndexPath)
    func disSelectRowAtIndexPath(cellIndexPath:IndexPath)
    func disSelectCategoryObject(cellIndexPath:IndexPath, product: ProductResult)
    func disSelectClassifiedCategory(cellIndexPath:IndexPath, product: ClassifiedCategory)
    func contactSellerBtnPressed()
}

protocol CategoryDelegate {
    func didSelectCategory(position: Int)
    func didSelectSubCategory(position: Int)
    func didSelectCountry(position: Int)
}

protocol RefreshDelegate {
    func didRefresh()
    func didRefresh(count: CGFloat)
}

protocol NavigationDelegate {
    func didNavigate(productObjID: Int)
}

protocol FilterDelegate {
    func didFilter(param: [String: Any], title: String)
}

protocol AttribDelegate {
    func didAttribSelect(indexPath: IndexPath, value: String)
}

protocol SubscriptionDelegate {
    func didSelectSubscription(packageID: Int)
}

protocol ContactButtonPressedDelegate {
    func guestTapped()
}
