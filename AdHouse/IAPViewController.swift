//
//  IAPViewController.swift
//  IAP
//
//  Created by Naeem Pasha on 25/10/2016.
//  Copyright © 2016 Apprasoft. All rights reserved.
//

import UIKit
import StoreKit

class IAPViewController: UIViewController {

    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var btnUpdate: UIButton!
    var products = [SKProduct]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handlePurchaseNotification(_:)),
                                               name: NSNotification.Name(rawValue: IAPHelper.IAPHelperPurchaseNotification),
                                               object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func PurchaseAction(_ sender: UIButton){
        self.indicator.startAnimating()
        self.btnUpdate.isEnabled = false
        reload()
    }
    
    func reload() {
        products = []
        
        IAPProducts.store.requestProducts{success, products in
            if success {
                self.products = products!
                print("Successfully run products process")
                print(self.products)
                
                if self.products.count > 0 {
                    self.buyButtonHandler?(self.product!)
                }
            }
            self.indicator.stopAnimating()
            self.btnUpdate.isEnabled = true
        }
    }
    
    var buyButtonHandler: ((_ product: SKProduct) -> ())?
    
    var product: SKProduct? {
        didSet {
            guard let product = product else { return }
            print(product.localizedTitle)
            
            if IAPProducts.store.isProductPurchased(product.productIdentifier) {
                print("Purchased")
            } else {
                print("\(product.priceLocale)")
            }
        }
    }
    
    @objc func handlePurchaseNotification(_ notification: Notification) {
        guard let productID = notification.object as? String else { return }
        
        for (index, product) in products.enumerated() {
            guard product.productIdentifier == productID else { continue }
            print("p-\(index)")
        }
    }
}
