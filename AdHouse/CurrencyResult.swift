//
//  CurrencyResult.swift
//  AdHouse
//
//  Created by kkk on 13/02/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import Foundation
import SwiftyJSON

public final class CurrencyResult: NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let name = "name"
        static let symbol = "symbol"
        static let id = "id"
    }
    
    // MARK: Properties
    public var name: String?
    public var symbol: String?
    public var id: Int?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: JSON) {
        self.init(json: object)
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        name = json[SerializationKeys.name].string
        symbol = json[SerializationKeys.symbol].string
        id = json[SerializationKeys.id].int
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = symbol { dictionary[SerializationKeys.symbol] = value }
        if let value = name { dictionary[SerializationKeys.name] = value }
        if let value = id { dictionary[SerializationKeys.id] = value }
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.symbol = aDecoder.decodeObject(forKey: SerializationKeys.symbol) as? String
        self.name = aDecoder.decodeObject(forKey: SerializationKeys.name) as? String
        self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(symbol, forKey: SerializationKeys.symbol)
        aCoder.encode(name, forKey: SerializationKeys.name)
        aCoder.encode(id, forKey: SerializationKeys.id)
    }
    
}
