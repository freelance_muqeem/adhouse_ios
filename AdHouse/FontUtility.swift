//
//  FontUtility.swift
//  KEZA
//
//  Created by NGI-Sharjeel on 15/03/2017.
//  Copyright © 2017 NGI-Noman. All rights reserved.
//

import UIKit

enum fontType {
    
    case Roboto_Black
    case Roboto_BlackItalic
    case Roboto_Bold
    case Roboto_BoldItalic
    case Roboto_Italic
    case Roboto_Light
    case Roboto_LightItalic
    case Roboto_Medium
    case Roboto_MediumItalic
    case Roboto_Regular
    case Roboto_Thin
    case Roboto_ThinItalic
    case RobotoCondensed_Bold_1
    case RobotoCondensed_BoldItalic_1
    case RobotoCondensed_Italic_1
    case RobotoCondensed_Light_1
    case RobotoCondensed_LightItalic_1
    case RobotoCondensed_Regular
   

    
    func getFontName () -> String {
        switch self {
        case .Roboto_Black:
            return "Roboto-Black"
        case .Roboto_BlackItalic:
            return "Roboto-BlackItalic"
        case .Roboto_Bold:
            return "Roboto-Bold"
        case .Roboto_BoldItalic:
            return "Roboto-BoldItalic"
        case .Roboto_Italic:
            return "Roboto-Italic"
        case .Roboto_Light:
            return "Roboto-Light"
        case .Roboto_LightItalic:
            return "Roboto-LightItalic"
        case .Roboto_Medium:
            return "Roboto-Medium"
        case .Roboto_MediumItalic:
            return "Roboto-MediumItalic"
        case .Roboto_Regular:
            return "Roboto-Regular"
        case .Roboto_Thin:
            return "Roboto-Thin"
        case .Roboto_ThinItalic:
            return "Roboto-ThinItalic"
        case .RobotoCondensed_Bold_1:
            return "RobotoCondensed-Bold_1"
        case .RobotoCondensed_BoldItalic_1:
            return "RobotoCondensed-BoldItalic_1"
        case .RobotoCondensed_Light_1:
            return "RobotoCondensed-Light_1"
        case .RobotoCondensed_LightItalic_1:
            return "RobotoCondensed-LightItalic_1"
        case .RobotoCondensed_Regular:
            return "RobotoCondensed-Regular"
        default:
            return "RobotoCondensed-Regular"
        }
    }
}

class FontUtility {
    

    class func getFontWithAdjustedSize(size:CGFloat,desireFontType:fontType)->UIFont{
        
        let fontName = desireFontType.getFontName()
        
        
        let font:UIFont? = UIFont(name:fontName , size: SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: size))
        return font!
      //  return UIFont(name:fontName , size: SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: size))!


    }
    
    
}
