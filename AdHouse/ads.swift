//
//  Ads.swift
//
//  Created by  on 8/15/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Ads: NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let updatedAt = "updated_at"
        static let isLike = "is_like"
        static let descriptionValue = "description"
        static let featured = "featured"
        static let categoryId = "category_id"
        static let price = "price"
        static let subCategoryId = "sub_category_id"
        static let latitude = "latitude"
        static let id = "id"
        static let isProperty = "is_property"
        static let createdAt = "created_at"
        static let phone = "phone"
        static let isClassified = "is_classified"
        static let title = "title"
        static let userId = "user_id"
        static let isVehicle = "is_vehicle"
        static let area = "area"
        static let longitude = "longitude"
    }
    
    // MARK: Properties
    public var updatedAt: String?
    public var isLike: Int?
    public var descriptionValue: String?
    public var featured: Int?
    public var categoryId: Int?
    public var price: Int?
    public var subCategoryId: Int?
    public var latitude: String?
    public var id: Int?
    public var isProperty: Int?
    public var createdAt: String?
    public var phone: String?
    public var isClassified: Int?
    public var title: String?
    public var userId: Int?
    public var isVehicle: Int?
    public var area: String?
    public var longitude: String?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: JSON) {
        self.init(json: object)
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        updatedAt = json[SerializationKeys.updatedAt].string
        isLike = json[SerializationKeys.isLike].int
        descriptionValue = json[SerializationKeys.descriptionValue].string
        featured = json[SerializationKeys.featured].int
        categoryId = json[SerializationKeys.categoryId].int
        price = json[SerializationKeys.price].int
        subCategoryId = json[SerializationKeys.subCategoryId].int
        latitude = json[SerializationKeys.latitude].string
        id = json[SerializationKeys.id].int
        isProperty = json[SerializationKeys.isProperty].int
        createdAt = json[SerializationKeys.createdAt].string
        phone = json[SerializationKeys.phone].string
        isClassified = json[SerializationKeys.isClassified].int
        title = json[SerializationKeys.title].string
        userId = json[SerializationKeys.userId].int
        isVehicle = json[SerializationKeys.isVehicle].int
        area = json[SerializationKeys.area].string
        longitude = json[SerializationKeys.longitude].string
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = updatedAt { dictionary[SerializationKeys.updatedAt] = value }
        if let value = isLike { dictionary[SerializationKeys.isLike] = value }
        if let value = descriptionValue { dictionary[SerializationKeys.descriptionValue] = value }
        if let value = featured { dictionary[SerializationKeys.featured] = value }
        if let value = categoryId { dictionary[SerializationKeys.categoryId] = value }
        if let value = price { dictionary[SerializationKeys.price] = value }
        if let value = subCategoryId { dictionary[SerializationKeys.subCategoryId] = value }
        if let value = latitude { dictionary[SerializationKeys.latitude] = value }
        if let value = id { dictionary[SerializationKeys.id] = value }
        if let value = isProperty { dictionary[SerializationKeys.isProperty] = value }
        if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
        if let value = phone { dictionary[SerializationKeys.phone] = value }
        if let value = isClassified { dictionary[SerializationKeys.isClassified] = value }
        if let value = title { dictionary[SerializationKeys.title] = value }
        if let value = userId { dictionary[SerializationKeys.userId] = value }
        if let value = isVehicle { dictionary[SerializationKeys.isVehicle] = value }
        if let value = area { dictionary[SerializationKeys.area] = value }
        if let value = longitude { dictionary[SerializationKeys.longitude] = value }
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.updatedAt = aDecoder.decodeObject(forKey: SerializationKeys.updatedAt) as? String
        self.isLike = aDecoder.decodeObject(forKey: SerializationKeys.isLike) as? Int
        self.descriptionValue = aDecoder.decodeObject(forKey: SerializationKeys.descriptionValue) as? String
        self.featured = aDecoder.decodeObject(forKey: SerializationKeys.featured) as? Int
        self.categoryId = aDecoder.decodeObject(forKey: SerializationKeys.categoryId) as? Int
        self.price = aDecoder.decodeObject(forKey: SerializationKeys.price) as? Int
        self.subCategoryId = aDecoder.decodeObject(forKey: SerializationKeys.subCategoryId) as? Int
        self.latitude = aDecoder.decodeObject(forKey: SerializationKeys.latitude) as? String
        self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
        self.isProperty = aDecoder.decodeObject(forKey: SerializationKeys.isProperty) as? Int
        self.createdAt = aDecoder.decodeObject(forKey: SerializationKeys.createdAt) as? String
        self.phone = aDecoder.decodeObject(forKey: SerializationKeys.phone) as? String
        self.isClassified = aDecoder.decodeObject(forKey: SerializationKeys.isClassified) as? Int
        self.title = aDecoder.decodeObject(forKey: SerializationKeys.title) as? String
        self.userId = aDecoder.decodeObject(forKey: SerializationKeys.userId) as? Int
        self.isVehicle = aDecoder.decodeObject(forKey: SerializationKeys.isVehicle) as? Int
        self.area = aDecoder.decodeObject(forKey: SerializationKeys.area) as? String
        self.longitude = aDecoder.decodeObject(forKey: SerializationKeys.longitude) as? String
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(updatedAt, forKey: SerializationKeys.updatedAt)
        aCoder.encode(isLike, forKey: SerializationKeys.isLike)
        aCoder.encode(descriptionValue, forKey: SerializationKeys.descriptionValue)
        aCoder.encode(featured, forKey: SerializationKeys.featured)
        aCoder.encode(categoryId, forKey: SerializationKeys.categoryId)
        aCoder.encode(price, forKey: SerializationKeys.price)
        aCoder.encode(subCategoryId, forKey: SerializationKeys.subCategoryId)
        aCoder.encode(latitude, forKey: SerializationKeys.latitude)
        aCoder.encode(id, forKey: SerializationKeys.id)
        aCoder.encode(isProperty, forKey: SerializationKeys.isProperty)
        aCoder.encode(createdAt, forKey: SerializationKeys.createdAt)
        aCoder.encode(phone, forKey: SerializationKeys.phone)
        aCoder.encode(isClassified, forKey: SerializationKeys.isClassified)
        aCoder.encode(title, forKey: SerializationKeys.title)
        aCoder.encode(userId, forKey: SerializationKeys.userId)
        aCoder.encode(isVehicle, forKey: SerializationKeys.isVehicle)
        aCoder.encode(area, forKey: SerializationKeys.area)
        aCoder.encode(longitude, forKey: SerializationKeys.longitude)
    }
    
}
