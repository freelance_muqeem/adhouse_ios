//
//  SignUpViewController.swift
//  AdHouse
//
//  Created by  on 7/24/17.
//  Copyright © 2017 . All rights reserved.
//

import UIKit
import DropDown
import ImagePicker
import SwiftyAttributes
import GooglePlaces
import DropDown


extension SignUpViewController : CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let loc = manager.location?.coordinate
        locationLatitude = (loc?.latitude)!
        locationLongitude = (loc?.longitude)!
        
        self.latitude = "\(locationLatitude!)"
        self.longitude = "\(locationLongitude!)"
        
        print("Latitude: \(locationLatitude!) & Longitude: \(locationLongitude!)")
        self.getAddressFromLatLon(locationLatitude!, locationLongitude!)
        self.locationManager.stopUpdatingLocation()
        
    }
    
    func didAccesToLocation() -> Bool {
        
        if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.notDetermined {
            print("Not determine your current location")
            return true
        }
        else if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse {
            print("access to location")
            return true
        }
        else {
            print("not access to location")
            DispatchQueue.main.async {
                // self.callpopUp()
            }
            return false
        }
    }
    
    //    func callpopUp() {
    //
    //        self.alertView.btnOkAction.setTitle("Settings", for: .normal)
    //        self.alertView.btnCancel.setTitle("Cancel", for: .normal)
    //        self.alertView.alertShow(image: FAILURE_IMAGE , title: "Access Permission" , msg: "TAAJ PAY needs to access your location to show your current country", id: 1)
    //        self.alertView.isHidden = false
    //
    //    }
    
    func determineMyCurrentLocation() {
        
        locationManager = CLLocationManager()
        
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    func getAddressFromLatLon(_ Latitude: Double , _ Longitude: Double) {
        
        Alert.showLoader(message: "")
        
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        
        let lat: Double = Double("\(Latitude)")!
        let lon: Double = Double("\(Longitude)")!
        
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                
                if let pm = placemarks {
                    
                    if pm.count > 0 {
                        
                        let pm = placemarks![0]
                        
                        var area:String = ""
                        var addressString : String = ""
                        
                        if pm.subLocality != nil {
                            addressString = addressString + pm.subLocality! + ", "
                            area = addressString
                        }
                        if pm.thoroughfare != nil {
                            addressString = addressString + pm.thoroughfare! + ", "
                            area = addressString
                        }
                        if pm.locality != nil {
                            addressString = addressString + pm.locality! + ", "
                            area = addressString
                        }
                        if pm.country != nil {
                            addressString = addressString + pm.country!
                            area =  addressString
                        }
                        
                        print("Current Address : \(area)")
                        self.txtAddress.text = area
                        Alert.hideLoader()
                    }
                }
                
        })
    }
    
    @IBAction func getCurrentLocationAction(_ sender : UIButton) {
        self.view.endEditing(true)
        self.didAccesToLocation()
        self.determineMyCurrentLocation()
    }
    
    
}

class SignUpViewController: UIViewController ,  UIImagePickerControllerDelegate , UINavigationControllerDelegate {
    
    class func instantiateFromStoryboard() -> SignUpViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! SignUpViewController
    }
    
    var leftBtn:UIButton?
    
    @IBOutlet weak var mainView:UIView!
    @IBOutlet weak var buttonView:UIView!
    @IBOutlet weak var textView:UIView!
    
    @IBOutlet weak var fullNameView:UIView!
    @IBOutlet weak var emailView:UIView!
    @IBOutlet weak var addressView:UIView!
    @IBOutlet weak var countryView:UIView!
    @IBOutlet weak var cityView:UIView!
    @IBOutlet weak var currencyView:UIView!
    @IBOutlet weak var passwordView:UIView!
    @IBOutlet weak var confirmPasswordView:UIView!
    @IBOutlet weak var phoneView:UIView!
    
    @IBOutlet weak var txtFullName:UITextField!
    @IBOutlet weak var txtEmail:UITextField!
    @IBOutlet weak var txtAddress:UITextField!
    @IBOutlet weak var txtCountry:UITextField!
    @IBOutlet weak var txtCity:UITextField!
    @IBOutlet weak var txtCurrency:UITextField!
    @IBOutlet weak var txtPassword:UITextField!
    @IBOutlet weak var txtConfirmPassword:UITextField!
    @IBOutlet weak var txtPhoneNum:UITextField!
    @IBOutlet weak var userImage:UIImageView!
    
    @IBOutlet weak var individualUser:UIButton!
    @IBOutlet weak var commercialUser:UIButton!
    @IBOutlet weak var btnSignUp:UIButton!
    
    let imagePicker = UIImagePickerController()
    
    var locationManager = CLLocationManager()
    var locationLatitude:Double?
    var locationLongitude:Double?
    
    var fullName:String!
    var emailAddress:String!
    var address:String!
    var countryID:Int!
    var currencyID:Int!
    var country:String!
    var city:String!
    var currency:String!
    var password:String!
    var confirmPassword:String!
    var phone:String!
    var rollID:String = "1"
    var isImage:Bool = false
    
    let dropDownCountry = DropDown()
    var countryArray:[CityResult]? = [CityResult]()
    
    let dropDown = DropDown()
    var cityArray:[CityResult]? = [CityResult]()
    
    let dropDownCurrency = DropDown()
    var currencyArray:[CurrencyResult]? = [CurrencyResult]()
    var currencySingleObj:CurrencyResult?
    var selectedLang: [String]!
    
    /* Search Area */
    var placeIDs: [String] = [String]()
    var cordinates: GMSPlace!
    let dropDownArea = DropDown()
    /* -- Search Area -- */
    
    var location:String!
    var latitude: String!
    var longitude: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        selectedLang =  UserDefaults.standard.object(forKey: "AppleLanguages") as? [String] ?? [String]()
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.title = NSLocalizedString("Sign Up" , comment: "")
        
        self.selectCity()
        self.selectCountry()
        self.selectCurrency()
        
        self.userImage.isHidden = true
        
        self.individualUser.isSelected = true;
        
        self.setupTextFieldPlaceHolder()
        self.placeBackBtn()
        self.setFonts()
        
        // Country DropDown
        dropDownCountry.anchorView = txtCountry // UIView or UIBarButtonItem
        dropDownCountry.bottomOffset = CGPoint(x: 0, y:(dropDownCountry.anchorView?.plainView.bounds.height)!)
        dropDownCountry.selectionAction = { [unowned self] (index: Int, item: String) in
            self.txtCountry.text = item
            self.countryID = self.countryArray![index].id
            print("Selected item: \(item) at index: \(index)")
        }
        
        // City DropDown
        dropDown.anchorView = txtCity // UIView or UIBarButtonItem
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.txtCity.text = item
            print("Selected item: \(item) at index: \(index)")
        }
        
        // Currency DropDown
        dropDownCurrency.anchorView = txtCurrency // UIView or UIBarButtonItem
        dropDownCurrency.bottomOffset = CGPoint(x: 0, y:(dropDownCurrency.anchorView?.plainView.bounds.height)!)
        dropDownCurrency.selectionAction = { [unowned self] (index: Int, item: String) in
            
            self.txtCurrency.text = item
            if self.currencySingleObj != nil {
                self.currencyID = self.currencySingleObj!.id
            } else {
                self.currencyID = self.currencyArray![index].id
            }
            print("Selected item: \(item) at index: \(index)")
        }
        
        self.mainView.layer.borderWidth = 0.5
        self.mainView.layer.borderColor = UIColor.lightGray.cgColor
        self.mainView.addShadow()
        
        self.buttonView.layer.borderWidth = 0.5
        self.buttonView.layer.borderColor = UIColor.lightGray.cgColor
        self.buttonView.addShadow()
        
        self.userImage.layer.borderWidth = 0.5
        self.userImage.layer.borderColor = UIColor.lightGray.cgColor
        self.userImage.addShadow()
        
        //        self.textView.layer.borderWidth = 0.5
        //        self.textView.layer.borderColor = UIColor.lightGray.cgColor
        //        self.textView.addShadow()
        
        self.fullNameView.layer.borderWidth = 0.5
        self.fullNameView.layer.borderColor = UIColor.lightGray.cgColor
        self.fullNameView.addShadow()
        
        self.emailView.layer.borderWidth = 0.5
        self.emailView.layer.borderColor = UIColor.lightGray.cgColor
        self.emailView.addShadow()
        
        self.addressView.layer.borderWidth = 0.5
        self.addressView.layer.borderColor = UIColor.lightGray.cgColor
        self.addressView.addShadow()
        
        self.countryView.layer.borderWidth = 0.5
        self.countryView.layer.borderColor = UIColor.lightGray.cgColor
        self.countryView.addShadow()
        
        self.cityView.layer.borderWidth = 0.5
        self.cityView.layer.borderColor = UIColor.lightGray.cgColor
        self.cityView.addShadow()
        
        self.currencyView.layer.borderWidth = 0.5
        self.currencyView.layer.borderColor = UIColor.lightGray.cgColor
        self.currencyView.addShadow()
        
        self.passwordView.layer.borderWidth = 0.5
        self.passwordView.layer.borderColor = UIColor.lightGray.cgColor
        self.passwordView.addShadow()
        
        self.confirmPasswordView.layer.borderWidth = 0.5
        self.confirmPasswordView.layer.borderColor = UIColor.lightGray.cgColor
        self.confirmPasswordView.addShadow()
        
        self.phoneView.layer.borderWidth = 0.5
        self.phoneView.layer.borderColor = UIColor.lightGray.cgColor
        self.phoneView.addShadow()
        
        self.didAccesToLocation()
        self.determineMyCurrentLocation()
        
        // Area Dropdown
        dropDownArea.anchorView = txtAddress // UIView or UIBarButtonItem
        dropDownArea.bottomOffset = CGPoint(x: 0, y:(dropDownArea.anchorView?.plainView.bounds.height)!)
        dropDownArea.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.txtAddress.text = item
            self.txtAddress.resignFirstResponder()
            
            let placeID = self.placeIDs[index]
            let placesClient = GMSPlacesClient.init()
            placesClient.lookUpPlaceID(placeID, callback: { (place, error) -> Void in
                if let error = error {
                    print("lookup place id query error: \(error.localizedDescription)")
                    return
                }
                
                guard let place = place else {
                    print("No place details for \(placeID)")
                    return
                }
                
                print("Place name \(place.name)")
                print("Place address \(String(describing: place.formattedAddress))")
                print("Place placeID \(place.placeID)")
                print("Place attributions \(String(describing: place.attributions))")
                print("Place Coordinates \(place.coordinate)")
                
                self.cordinates = place
                self.latitude = String(self.cordinates.coordinate.latitude)
                self.longitude = String(self.cordinates.coordinate.longitude)
                print("Latitude: \(self.latitude) , Longitude: \(self.longitude)")
                
                // self.locationCountry = self.cordinates.name
            })
        }
        
        txtAddress.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        
        
        
        
    }
    
    //MARK:-  Back Button Code
    
    func placeBackBtn(){
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        leftBtn =  UIButton(type: .custom)
        leftBtn?.setImage(UIImage(named: "backbtn"), for: .normal)
        leftBtn?.addTarget(self, action: #selector(SignUpViewController.BackBtnPressed), for: .touchUpInside)
        leftBtn?.frame = CGRect(x: 0, y:0 , width: 25, height: 25)
        let  leftBarButton: UIBarButtonItem = UIBarButtonItem(customView: leftBtn!)
        self.navigationItem.setLeftBarButton(leftBarButton, animated: true)
        
    }
    
    @objc func BackBtnPressed(){
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func countryAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        self.dropDownCountry.show()
    }
    
    func selectCountry() {
        
        Alert.showLoader(message: "")
        
        var param = [String:String]()
        
        if !isLanguageEnglish() {
            param.updateValue("ar", forKey: "lang")
        } else {
            param.updateValue("en", forKey: "lang")
        }
        
        HelpAndAboutServices.Country(param: param , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    print("Error: \((error as! Error).localizedDescription)")
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                return
            }
            
            print("Main")
            print(response!)
            
            print("SUCCESS")
            
            self.countryArray?.removeAll()
            
            let countryResponseArray = response?["Result"].arrayValue
            for resultObj in countryResponseArray! {
                let obj =  CityResult(json: resultObj)
                self.countryArray?.append(obj)
            }
            
            var titles: [String] = [String]()
            for result in self.countryArray! {
                print("Title \(String(describing: result.title))")
                titles.append(result.title!)
            }
            self.dropDownCountry.dataSource = titles
            
        })
        
    }
    
    @IBAction func cityAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        self.dropDown.show()
    }
    
    func selectCity() {
        
        Alert.showLoader(message: "")
        
        var param = [String:String]()
        
        if !isLanguageEnglish() {
            param.updateValue("ar", forKey: "lang")
        } else {
            param.updateValue("en", forKey: "lang")
        }
        
        HelpAndAboutServices.City(param: param , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    print("Error: \((error as! Error).localizedDescription)")
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                return
            }
            
            print("Main")
            print(response!)
            
            print("SUCCESS")
            
            self.cityArray?.removeAll()
            
            let cityResponseArray = response?["Result"].arrayValue
            for resultObj in cityResponseArray! {
                let obj =  CityResult(json: resultObj)
                self.cityArray?.append(obj)
            }
            
            
            var titles: [String] = [String]()
            for result in self.cityArray! {
                print("Title \(String(describing: result.title))")
                titles.append(result.title!)
            }
            self.dropDown.dataSource = titles
            
        })
        
    }
    
    @IBAction func currencyAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        self.dropDownCurrency.show()
    }
    
    func selectCurrency() {
        
        Alert.showLoader(message: "")
        
        var param = [String:String]()
        
        if !isLanguageEnglish() {
            param.updateValue("ar", forKey: "lang")
        } else {
            param.updateValue("en", forKey: "lang")
        }
        
        HelpAndAboutServices.Currency(param: param , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    print("Error: \((error as! Error).localizedDescription)")
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                return
            }
            
            print("Main")
            print(response!)
            
            print("SUCCESS")
            
            if let array = response?["Result"].array{
                
                self.currencyArray?.removeAll()
                
                for resultObj in array {
                    let obj =  CurrencyResult(json: resultObj)
                    self.currencyArray?.append(obj)
                }
                
                var titles: [String] = [String]()
                for result in self.currencyArray! {
                    print("Title \(String(describing: result.name))")
                    titles.append(result.name!)
                }
                self.dropDownCurrency.dataSource = titles
            } else {
                
                let currencyResponse = CurrencyResult(object:(response?["Result"])!)
                self.currencySingleObj = currencyResponse
                self.dropDownCurrency.dataSource = [currencyResponse.name] as! [String]
            }
        })
        
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if (textField.text?.isEmpty)! || textField.text == "" {
            print("nil")
        }
        else {
            let filter = GMSAutocompleteFilter()
            filter.type = .noFilter
            let placesClient = GMSPlacesClient.init()
            placesClient.autocompleteQuery(self.txtAddress.text!, bounds: nil, filter: filter, callback: {(results, error) -> Void in
                if let error = error {
                    print("Autocomplete error \(error)")
                    return
                }
                if let results = results {
                    var names: [String] = [String]()
                    var ids: [String] = [String]()
                    for result in results {
                        print("Result \(String(describing: result.attributedFullText.string)) with placeID \(String(describing: result.placeID))")
                        names.append(result.attributedFullText.string)
                        ids.append(result.placeID)
                    }
                    self.placeIDs = ids
                    self.dropDownArea.dataSource = names
                    self.dropDownArea.show()
                }
            })
        }
    }
    
    
    
    @IBAction func signUpAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        
        self.fullName = txtFullName.text!
        self.emailAddress = txtEmail.text!
        self.address = txtAddress.text!
        self.currency = txtCurrency.text!
        self.country = txtCountry.text!
        self.city = txtCity.text!
        self.password = txtPassword.text!
        self.confirmPassword = txtConfirmPassword.text!
        self.phone = txtPhoneNum.text!
        
        
        guard  !((fullName.isEmpty)         || (fullName.isEmptyOrWhitespace()))
            && !((emailAddress.isEmpty)     || (emailAddress.isEmptyOrWhitespace()))
            && !((currency.isEmpty)         || (currency.isEmptyOrWhitespace()))
            && !((country.isEmpty)          || (country.isEmptyOrWhitespace()))
            && !((city.isEmpty)             || (city.isEmptyOrWhitespace()))
            && !((password.isEmpty)         || (password.isEmptyOrWhitespace()))
            && !((confirmPassword.isEmpty)  || (confirmPassword.isEmptyOrWhitespace()))
            && !((phone.isEmpty)            || (phone.isEmptyOrWhitespace()))
            else {
                Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Please fill all Fields", comment: ""))
                return
        }
        
        /*  if isImage == false {
         
         Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Image can't be empty", comment: ""))
         return
         
         }  */
        
        if  !AppHelper.isValidEmail(testStr: emailAddress) {
            Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Enter valid Email addreess", comment: ""))
            return
        }
        
        if password != confirmPassword {
            Alert.showAlert(title: NSLocalizedString("Alert", comment: "") , message:   NSLocalizedString("Password does not match", comment: ""))
            return
        }
        
        if password.count <= 5  {
            print(password.count)
            Alert.showAlert(title: NSLocalizedString("Alert", comment: "") , message:   NSLocalizedString("Password at least 6 characters", comment: ""))
            return
        }
        
        if phone.count <= 7 && phone.count >= 50 {
            print(phone.count)
            Alert.showAlert(title: NSLocalizedString("Alert", comment: "") , message:   NSLocalizedString("Phone number at least 8 characters", comment: ""))
            return
        }
            
            
            
        else {
            Alert.showLoader(message: "")
            
            let parameters:[String:Any] = ["full_name":self.fullName!, "email":self.emailAddress!, "phone":self.phone!, "password":self.password!, "password_confirmation":self.confirmPassword!, "currency_id":self.currencyID!,  "country_id":self.countryID!,  "city":self.city!, "role_id":self.rollID , "address": (self.address == "") ? "" : self.address! , "latitude":(self.latitude == nil) ? "" : self.latitude!  , "longitude":
                (self.longitude == nil ) ? "" : self.longitude! ]
            
            print(parameters)
            
            if (self.isImage == false) {
                
                LoginServices.RegisterWithoutImage(param: parameters, completionHandler: { (status, response, error) in
                    
                    if !status {
                        if error != nil {
                            print("Error: \((error as! Error).localizedDescription)")
                            return
                        }
                        let msg = response?["Message"].stringValue
                        print("Message: \(String(describing: msg))")
                        return
                    }
                    
                    print("Main")
                    print(response!)
                    
                    print("SUCCESS")
                    
                    self.navigationController?.popViewController(animated: true)
                    Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Registerd Successfully , Please Login", comment: ""))
                    
                })
                
            }else{
                
                LoginServices.RegisterWithImage(param: parameters, image:self.userImage.image!, completionHandler: {(status, response, error) in
                    
                    if !status {
                        if error != nil {
                            print("Error: \((error as! Error).localizedDescription)")
                            return
                        }
                        let msg = response?["Message"].stringValue
                        print("Message: \(String(describing: msg))")
                        return
                    }
                    
                    print("Main")
                    print(response!)
                    
                    print("SUCCESS")
                    
                    self.navigationController?.popViewController(animated: true)
                    Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Registerd Successfully , Please Login", comment: ""))
                    
                })
            }
            
        }
    }
    
    @IBAction func IndividualUserAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        
        individualUser.isSelected = true
        commercialUser.isSelected = false
        
        self.rollID = "1"
        
    }
    
    @IBAction func commercialUserAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        
        individualUser.isSelected = false
        commercialUser.isSelected = true
        
        self.rollID = "0"
        
    }
    
    //MARK:- Camera Button Action
    
    @IBAction func cameraAction(){
        self.openCamera()
    }
    
    func openCamera(){
        
        let imagePickerController = ImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.imageLimit = 1
        present(imagePickerController, animated: true, completion: nil)
        
        //        let cameraViewController = CameraViewController(croppingEnabled: false) { [weak self] image, asset in
        //
        //            if image != nil{
        //
        //
        //                let size = (image?.size)!.applying(CGAffineTransform(scaleX: 0.5, y: 0.5))
        //                let hasAlpha = false
        //                let scale: CGFloat = 0.0 // Automatically use scale factor of main screen
        //
        //                UIGraphicsBeginImageContextWithOptions(size, !hasAlpha, scale)
        //                image?.draw(in: CGRect(origin: CGPoint.zero, size: size))
        //
        //                let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        //                UIGraphicsEndImageContext()
        //
        //
        //                self?.userImage.image = scaledImage
        //                self?.userImage.isHidden = false;
        //                self?.isImage = true
        //            }
        //
        //            self?.dismiss(animated: true, completion: nil)
        //        }
        //        present(cameraViewController, animated: true, completion: nil)
    }
    
    //MARK:- Fonts Size Set
    
    func setFonts() {
        
        individualUser.titleLabel?.font = FontUtility.getFontWithAdjustedSize(size: 14, desireFontType: .Roboto_Bold)
        commercialUser.titleLabel?.font = FontUtility.getFontWithAdjustedSize(size: 14, desireFontType: .Roboto_Bold)
        btnSignUp.titleLabel?.font = FontUtility.getFontWithAdjustedSize(size: 15, desireFontType: .Roboto_Bold)
        
    }
    
    //MARK:- Placeholder Text Change
    
    func setupTextFieldPlaceHolder(){
        let PlaceHolderAttributedString_FullName = NSLocalizedString("Full Name", comment: "").withAttributes([
            .textColor(.lightGray),
            .font(.systemFont(ofSize: SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 14)))
        ])
        
        let PlaceHolderAttributedString_Email = NSLocalizedString("Email", comment: "").withAttributes([
            .textColor(.lightGray),
            .font(.systemFont(ofSize: SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 14)))
        ])
        let PlaceHolderAttributedString_Address = NSLocalizedString("Residential Address", comment: "").withAttributes([
            .textColor(.lightGray),
            .font(.systemFont(ofSize: SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 14)))
        ])
        
        let PlaceHolderAttributedString_City = NSLocalizedString("Select City", comment: "").withAttributes([
            .textColor(.lightGray),
            .font(.systemFont(ofSize: SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 14)))
        ])
        
        let PlaceHolderAttributedString_Password = NSLocalizedString("Password", comment: "").withAttributes([
            .textColor(.lightGray),
            .font(.systemFont(ofSize: SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 14)))
        ])
        
        let PlaceHolderAttributedString_ConfirmPassword = NSLocalizedString("Confirm Password", comment: "").withAttributes([
            .textColor(.lightGray),
            .font(.systemFont(ofSize: SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 14)))
        ])
        
        let PlaceHolderAttributedString_PhoneNum = NSLocalizedString("Phone Number", comment: "").withAttributes([
            .textColor(.lightGray),
            .font(.systemFont(ofSize: SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 14)))
        ])
        
        
        self.txtFullName.attributedPlaceholder = PlaceHolderAttributedString_FullName
        self.txtEmail.attributedPlaceholder = PlaceHolderAttributedString_Email
        self.txtAddress.attributedPlaceholder = PlaceHolderAttributedString_Address
        self.txtCity.attributedPlaceholder = PlaceHolderAttributedString_City
        self.txtPassword.attributedPlaceholder = PlaceHolderAttributedString_Password
        self.txtConfirmPassword.attributedPlaceholder = PlaceHolderAttributedString_ConfirmPassword
        self.txtPhoneNum.attributedPlaceholder = PlaceHolderAttributedString_PhoneNum
        
        
    }
    
}


extension SignUpViewController: ImagePickerDelegate {
    
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        if images.count > 0 {
            
            self.userImage.isHidden = false
            self.isImage = true
            
            self.userImage.image = AppHelper.resizeImage(image: images.first!, compressionQuality: 0.25)
        }
        
        //Dismiss Controller
        self.dismiss(animated: true, completion: nil)
    }
    
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
        //Dismiss Controller
        self.dismiss(animated: true, completion: nil)
    }
    
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        
    }
}
