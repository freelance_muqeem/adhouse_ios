//
//  UserPreferences.swift
//  QuranApp
//
//  Created by Uzair Danish on 9/16/15.
//  Copyright (c) 2015 Uzair Danish. All rights reserved.
//

import UIKit

class UserPreferences: NSObject {
    static let sharedUserPreferences = UserPreferences()
    
    //PRIVATE init so that singleton class should not be reinitialized from anyother class
    fileprivate override init() {
        
    }
    
    /*
    class var currentTranslationId:Int {
        get {
            return (NSUserDefaults.standardUserDefaults().objectForKey("TRANLATION_ID") as? Int) ?? 34;
        }
        
        set {
            NSUserDefaults.standardUserDefaults().setInteger(newValue, forKey: "TRANLATION_ID");
            NSUserDefaults.standardUserDefaults().synchronize();
        }
    } //P.E.
    */
    
    
    var languageBundle : Bundle?
    func language() {
        let languageCode = UserDefaults.standard
        if UserDefaults.standard.value(forKey: "language") != nil {
            let language = languageCode.string(forKey: "language")!
            if let path  = Bundle.main.path(forResource: language, ofType: "lproj") {
                languageBundle =  Bundle(path: path)
            }
            else{
                languageBundle = Bundle(path: Bundle.main.path(forResource: "en", ofType: "lproj")!)
            }
        }
        else {
            languageCode.set("en", forKey: "language")
            languageCode.synchronize()
            let language = languageCode.string(forKey: "language")!
            if let path  = Bundle.main.path(forResource: language, ofType: "lproj") {
                languageBundle =  Bundle(path: path)
            }
            else{
                languageBundle = Bundle(path: Bundle.main.path(forResource: "en", ofType: "lproj")!)
            }
        }
    }
    
    
    
    //var sessionUserSubscription: UserSubscription?
    var sessionDeviceToken: String?
    
//    var userInfo:User? {
//        get {
//            if let data: Data = USER_DEFAULTS.object(forKey: "USER_INFO") as? Data {
//                return NSKeyedUnarchiver.unarchiveObject(with: data) as? User
//            }
//            
//            return nil
//        }
//        
//        set {
//            if let user:User = newValue {
//                let data = NSKeyedArchiver.archivedData(withRootObject: user)
//                USER_DEFAULTS.set(data, forKey: "USER_INFO")
//                USER_DEFAULTS.synchronize()
//            }
//        }
//    } //P.E.
    
    func removeUserInfo(){
        USER_DEFAULTS.removeObject(forKey: "USER_INFO")
    } //F.E.
    
    
    
} //CLS END

