//
//  ContactViewController.swift
//  AdHouse
//
//  Created by maazulhaq on 24/07/2017.
//  Copyright © 2017 . All rights reserved.
//

import UIKit

class ContactViewController: UIViewController , UITextViewDelegate {
    
    class func instantiateFromStoryboard() -> ContactViewController {
        let storyboard = UIStoryboard(name: "SideMenu", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! ContactViewController
    }
    
    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var menuButton: UIBarButtonItem!
    
    @IBOutlet weak var mainView:UIView!
    @IBOutlet weak var textFieldView:UIView!
    @IBOutlet weak var textView:UITextView!
    
    
    @IBOutlet weak var FullNameView:UIView!
    @IBOutlet weak var EmailView:UIView!
    @IBOutlet weak var phoneView:UIView!
    
    @IBOutlet weak var txtFullName:UITextField!
    @IBOutlet weak var txtEmail:UITextField!
    @IBOutlet weak var txtPhoneNum:UITextField!
    var isFromSideMenu:Bool?
    var leftBtn:UIButton?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        let navBackgroundImage:UIImage! = UIImage(named: "topbox")!
        self.navigationController?.navigationBar.setBackgroundImage(navBackgroundImage, for: .default)
        
        self.title = NSLocalizedString("Contact Us", comment: "")
        self.placeBackBtn()
        
        self.mainView.layer.borderWidth = 0.5
        self.mainView.layer.borderColor = UIColor.lightGray.cgColor
        self.mainView.addShadow()
        
        self.textView.layer.borderWidth = 0.5
        self.textView.layer.borderColor = UIColor.lightGray.cgColor
        self.textView.addShadow()
        
        self.textFieldView.layer.borderWidth = 0.5
        self.textFieldView.layer.borderColor = UIColor.lightGray.cgColor
        self.textFieldView.addShadow()
        
        self.FullNameView.layer.borderWidth = 0.5
        self.FullNameView.layer.borderColor = UIColor.lightGray.cgColor
        self.FullNameView.addShadow()
        
        self.EmailView.layer.borderWidth = 0.5
        self.EmailView.layer.borderColor = UIColor.lightGray.cgColor
        self.EmailView.addShadow()
        
        self.phoneView.layer.borderWidth = 0.5
        self.phoneView.layer.borderColor = UIColor.lightGray.cgColor
        self.phoneView.addShadow()
        
        self.textView.placeholder = NSLocalizedString("Your text here..", comment: "")
        
        if Singleton.sharedInstance.CurrentUser != nil {
         
            self.txtFullName.text = Singleton.sharedInstance.CurrentUser?.fullName
            self.txtEmail.text = Singleton.sharedInstance.CurrentUser?.email
            self.txtPhoneNum.text = Singleton.sharedInstance.CurrentUser?.phone
            
        }
        
    }
    
    //MARK:-  Back Button Code
    
    func placeBackBtn(){
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        leftBtn =  UIButton(type: .custom)
        leftBtn?.setImage(UIImage(named: "backbtn"), for: .normal)
        leftBtn?.addTarget(self, action: #selector(ContactViewController.BackBtnPressed), for: .touchUpInside)
        leftBtn?.frame = CGRect(x: 0, y:0 , width: 25, height: 25)
        let  leftBarButton: UIBarButtonItem = UIBarButtonItem(customView: leftBtn!)
        self.navigationItem.setLeftBarButton(leftBarButton, animated: true)
        
    }
    
    @objc func BackBtnPressed(){
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func sendAction(_ sender : UIButton) {
        
        let userName = self.txtFullName.text!
        let emailAddress = self.txtEmail.text!
        let phone = self.txtPhoneNum.text!
        let description = self.textView.text!
        
        if userName == "" {
            Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Please Enter Name" ,  comment: ""))
            return
        }
        
        if  !AppHelper.isValidEmail(testStr: emailAddress) {
            Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Enter valid Email addreess", comment: ""))
            return
        }
        
        if phone == "" {
            Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Please Enter Phone Number" ,  comment: ""))
            return
        }
            
        else if (phone.count) <= 7  {
            Alert.showAlert(title: NSLocalizedString("Alert", comment: "") , message:   NSLocalizedString("Phone number at least 8 characters", comment: ""))
            return
        }
        
        
        if description == "" {
            Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Please Enter Description" ,  comment: ""))
            return
        }
            
        else {
            
            Alert.showLoader(message: "")
            
            let parameters = ["user_name":userName, "email":emailAddress, "phone":phone, "description":description]
            print("Parameters: \(parameters)")
            
            ContactServices.Contact(param: parameters, completionHandler: {(status, response, error) in
                
                if !status {
                    if error != nil {
                        print("Error: \((error as! Error).localizedDescription)")
                        return
                    }
                    let msg = response?["Message"].stringValue
                    print("Message: \(String(describing: msg))")
                    return
                }
                
                print("Main")
                print(response!)
                
                print("SUCCESS")
                
                self.navigationController?.popViewController(animated: true)
                Alert.showAlert(title:NSLocalizedString("Alert", comment: ""), message:NSLocalizedString("Contact Form Submitted Successfully", comment: ""))
            })
        }
        
    }
    
    
}

