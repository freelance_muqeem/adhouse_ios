//
//  NotificationViewCell.swift
//  AdHouse
//
//  Created by  on 7/25/17.
//  Copyright © 2017 . All rights reserved.
//

import UIKit

class NotificationViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var lblDescription:UILabel!
    @IBOutlet weak var lblDate:UILabel!
    
    @IBOutlet weak var borderView:UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setFonts()
        self.borderView.layer.borderWidth = 0.5
        self.borderView.layer.borderColor = UIColor.lightGray.cgColor
        self.borderView.addShadow()
        
    }

    func setFonts() {
        
        lblTitle.font = FontUtility.getFontWithAdjustedSize(size: 14, desireFontType: .Roboto_Regular)
        lblDescription.font = FontUtility.getFontWithAdjustedSize(size: 11, desireFontType: .Roboto_Regular)
        lblDate.font = FontUtility.getFontWithAdjustedSize(size: 10, desireFontType: .Roboto_Regular)

    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
