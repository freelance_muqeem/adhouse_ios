//
//  DetailListTableViewCell.swift
//  AdHouse
//
//  Created by maazulhaq on 26/07/2017.
//  Copyright © 2017 . All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class DetailListTableViewCell: UITableViewCell, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    var listDataSource: [String]!
    var keyDataSource: [String]!
    var productObj: ProductResult!
    var address:String! = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.tableView.register(UINib(nibName: "ListTableViewCell", bundle: nil), forCellReuseIdentifier: String(describing: ListTableViewCell.self))
        
    }
    
    func getAddress(latitude:String , longitude:String) {
        
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        
        let lat = Double(latitude)!
        let lon = Double(longitude)!
        
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        DispatchQueue.main.async {
            
            let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
            
            ceo.reverseGeocodeLocation(loc, completionHandler:
                {(placemarks, error) in
                    if (error != nil)
                    {
                        print("reverse geodcode fail: \(error!.localizedDescription)")
                    }
                    let pm = placemarks! as [CLPlacemark]
                    
                    if pm.count > 0 {
                        let pm = placemarks![0]
                        
                        var addressString : String = ""
                        
//                        if pm.thoroughfare != nil {
//                            addressString = addressString + pm.thoroughfare! + ", "
//                        }
                        if pm.locality != nil {
                            addressString = addressString + pm.locality! + ", "
                        }
                        if pm.country != nil {
                            addressString = addressString + pm.country!
                            self.address =  addressString
                        }
                        
                        print(self.address!)
                        
                    }
                    
                    self.tableView.reloadData()
                    
            })
            
        }
        
    }
        
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 44
        }
        
        func numberOfSections(in tableView: UITableView) -> Int {
            return 1
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            
            var count = self.listDataSource.count
            count = count + 2
            return count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ListTableViewCell") as! ListTableViewCell
            
            if indexPath.row == 0 {
                
                cell.lblKey.text = NSLocalizedString("Location", comment: "")
                cell.lblTitle.text = self.productObj.area //self.address
            }
            else if indexPath.row == 1 {
                
                cell.lblKey.text = NSLocalizedString("Phone", comment: "")
                cell.lblTitle.text = self.productObj.phone
                
            }
            else {
                
                cell.lblTitle.text = self.listDataSource[indexPath.row - 2]
                cell.lblKey.text = self.keyDataSource[indexPath.row - 2]
                
            }
            
            if Int(CGFloat(indexPath.row).truncatingRemainder(dividingBy: 2.0)) == 0 {
                cell.keyView.backgroundColor = UIColor(rgb: 0xf2faff)
                cell.titleView.backgroundColor = UIColor(rgb: 0xf2faff)
            }
            
            return cell
        }
        
}


