//
//  Result.swift
//
//  Created by Abdul Muqeem on 05/03/2019
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class ContactDetails: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let email = "email"
    static let id = "id"
    static let updatedAt = "updated_at"
    static let address = "address"
    static let phone = "phone"
    static let createdAt = "created_at"
    static let about = "about"
    static let playstore = "playstore"
    static let appstore = "appstore"
  }

  // MARK: Properties
  public var email: String?
  public var id: Int?
  public var updatedAt: String?
  public var address: String?
  public var phone: String?
  public var createdAt: String?
  public var about: String?
  public var playstore: String?
  public var appstore: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    email = json[SerializationKeys.email].string
    id = json[SerializationKeys.id].int
    updatedAt = json[SerializationKeys.updatedAt].string
    address = json[SerializationKeys.address].string
    phone = json[SerializationKeys.phone].string
    createdAt = json[SerializationKeys.createdAt].string
    about = json[SerializationKeys.about].string
    playstore = json[SerializationKeys.playstore].string
    appstore = json[SerializationKeys.appstore].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = email { dictionary[SerializationKeys.email] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = updatedAt { dictionary[SerializationKeys.updatedAt] = value }
    if let value = address { dictionary[SerializationKeys.address] = value }
    if let value = phone { dictionary[SerializationKeys.phone] = value }
    if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
    if let value = about { dictionary[SerializationKeys.about] = value }
    if let value = playstore { dictionary[SerializationKeys.playstore] = value }
    if let value = appstore { dictionary[SerializationKeys.appstore] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.email = aDecoder.decodeObject(forKey: SerializationKeys.email) as? String
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
    self.updatedAt = aDecoder.decodeObject(forKey: SerializationKeys.updatedAt) as? String
    self.address = aDecoder.decodeObject(forKey: SerializationKeys.address) as? String
    self.phone = aDecoder.decodeObject(forKey: SerializationKeys.phone) as? String
    self.createdAt = aDecoder.decodeObject(forKey: SerializationKeys.createdAt) as? String
    self.about = aDecoder.decodeObject(forKey: SerializationKeys.about) as? String
    self.playstore = aDecoder.decodeObject(forKey: SerializationKeys.playstore) as? String
    self.appstore = aDecoder.decodeObject(forKey: SerializationKeys.appstore) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(email, forKey: SerializationKeys.email)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(updatedAt, forKey: SerializationKeys.updatedAt)
    aCoder.encode(address, forKey: SerializationKeys.address)
    aCoder.encode(phone, forKey: SerializationKeys.phone)
    aCoder.encode(createdAt, forKey: SerializationKeys.createdAt)
    aCoder.encode(about, forKey: SerializationKeys.about)
    aCoder.encode(playstore, forKey: SerializationKeys.playstore)
    aCoder.encode(appstore, forKey: SerializationKeys.appstore)
  }

}
