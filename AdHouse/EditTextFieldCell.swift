//
//  EditTextFieldCell.swift
//  AdHouse
//
//  Created by  on 8/15/17.
//  Copyright © 2017 . All rights reserved.
//

import UIKit
import DropDown

class EditTextFieldCell: UITableViewCell {

    @IBOutlet weak var shadowView:UIView!
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var txtField:UITextField!
    var addDetails : AdDetails!
    
    var AtrribIID = Int()
    var indexPath: IndexPath!
    var attribDelegate: AttribDelegate?
    
    var paramHelper : ParamsHolderEdit!
    var itemID : Int!
 
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.shadowView.layer.borderWidth = 0.5
        self.shadowView.layer.borderColor = UIColor.lightGray.cgColor
        self.shadowView.addShadow()
         txtField.delegate = self
        
      //--ww  self.txtField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        
    }
    
//    func textFieldDidChange(_ textField: UITextField) {
//        if (textField.text?.isEmpty)! || textField.text == "" {
//            print("nil")
//        }
//        else {
//               let index = indexPath.row - 1
//            paramHelper.attribsArray[index] =  ["id":"\(itemID! ))" ,"value":txtField.text!]
//
//           //--ww self.attribDelegate?.didAttribSelect(indexPath: self.indexPath, value: textField.text!)
//        }
//    }
    
}
extension EditTextFieldCell : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range:NSRange, replacementString string: String) -> Bool {
        let kActualText = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
        print("kActualText", kActualText)
        let index = indexPath.row - 1
        paramHelper.attribsArray[index] =  ["id":"\(String(describing: itemID!))" ,"value":kActualText]
        //--ww      paramHelper.params.updateValue(txtField.text!, forKey: "\(String(describing: itemID!))")
        //--ww   paramHelper.params.updateValue(txtField.text!, forKey: "\(String(describing: itemID!))")
        addDetails.attributeValue = kActualText
        return true
    }
    
  
}
