//
//  MyFavouritesViewController.swift
//  AdHouse
//
//  Created by  on 7/25/17.
//  Copyright © 2017 . All rights reserved.
//

import UIKit

extension MyFavouritesViewController: RefreshDelegate {
    
    func didRefresh(count: CGFloat) {
        
    }
    
    func didRefresh() {
        self.GetFavorites()
    }
    
}

class MyFavouritesViewController: UIViewController , UITableViewDelegate , UITableViewDataSource {
    
    class func instantiateFromStoryboard() -> MyFavouritesViewController {
        let storyboard = UIStoryboard(name: "SideMenu", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! MyFavouritesViewController
    }
    
    @IBOutlet weak var tblView:UITableView!
    
    var favoriteArray:[FavoritesResult]? = [FavoritesResult]()
    var isFav = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let cellNib = UINib(nibName: "FavouritesViewCell", bundle: nil)
        self.tblView.register(cellNib, forCellReuseIdentifier: String(describing: FavouritesViewCell.self))
        
        self.GetFavorites()
        
    }
    
    //MARK:- Menu Action
    
    @IBAction func MenuAction(sender: UIBarButtonItem) {
        self.slideMenuController()?.toggleLeft()
    }
    
    //MARK:- Get All Favorite List
    
    func GetFavorites() {
        
        Alert.showLoader(message: "")
        let parameters:[String:Any] = ["user_id": (Singleton.sharedInstance.CurrentUser!.id!)]
        
        FavouriteServices.Get(param: parameters , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    print("Error: \((error as! Error).localizedDescription)")
                    return
                }
                let msg = response?["Message"].stringValue
                self.favoriteArray?.removeAll()
                self.tblView.reloadData()
                print("Message: \(String(describing: msg))")
                return
            }
            
            print(response!)
            print("SUCCESS")
            Alert.hideLoader()
            self.favoriteArray?.removeAll()
            
            let adsResponseArray = response?["Result"].arrayValue
            print(adsResponseArray!)
            
            for resultObj in adsResponseArray! {
                let obj = FavoritesResult(json: resultObj)
                self.favoriteArray?.append(obj)
            }
            
            self.tblView.reloadData()
        })
    }
    
    //MARK:- Add/Delete Favorite
    
    @objc func AddFavoriteAction(sender: UIButton) {
        if !self.isFav {
            self.isFav = !self.isFav
            
            var indexPath: IndexPath!
            if let superview = sender.superview?.superview {
                if let cell = superview.superview as? FavouritesViewCell {
                    indexPath = tblView.indexPath(for: cell)
                    print(indexPath)
                    print("index path: \(indexPath)")
                    
                    if indexPath != nil {
                        
                        let parameters:[String:Any] = [
                            "user_id": (Singleton.sharedInstance.CurrentUser!.id!),
                            "ad_id":self.favoriteArray![indexPath.row].adId!,
                            "state":0
                        ]
                        
                        print("PARAMETERS: \(parameters)")
                        Alert.showLoader(message: "")
                        
                        FavouriteServices.Add(param: parameters , completionHandler: {(status, response, error) in
                            self.isFav = !self.isFav
                            if !status {
                                if error != nil {
                                    print("Error: \((error as! Error).localizedDescription)")
                                    return
                                }
                                let msg = response?["Message"].stringValue
                                print("Message: \(String(describing: msg))")
                                return
                            }
                            
                            print(response!)
                            print("SUCCESS")
                            self.favoriteArray?.remove(at: indexPath.row)
                            self.tblView.deleteRows(at: [indexPath], with: .left)
                            
                        })
                    }
                }
                
            }
        }
    }
    //MARK:-  TableView Delegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return GISTUtility.convertToRatio(120)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.favoriteArray!.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let favObj:FavoritesResult = self.favoriteArray![indexPath.row]
        print(indexPath.row , favObj)
        let cell: FavouritesViewCell = tableView.dequeueReusableCell(withIdentifier: "FavouritesViewCell", for: indexPath) as! FavouritesViewCell
        cell.lblCarMake.isHidden = true
        cell.lblCarClass.isHidden = true
        cell.lblProductName.text = favObj.ads?.title ?? "N/A"
        cell.lblPrice.text = "QAR " + "\(favObj.ads!.price!.decimal)" //String(describing:favObj.ads!.price ?? 0)
        cell.lblLocation.text = favObj.ads?.area
        if favObj.adImages?.first?.imageUrl != nil {
        cell.imgProduct.setImageFromUrl(urlStr: (favObj.adImages?.first?.imageUrl!)!)
        }
        cell.imgHeart.addTarget(self, action: #selector(self.AddFavoriteAction(sender:)), for: .touchUpInside)
        cell.btnViewDetail.addTarget(self, action: #selector(self.ViewDetailAction(sender:)), for: .touchUpInside)
        
        if favObj.ads?.featured == 0 {
            
            cell.btnFeatured.isHidden = true
            cell.imgFeatured.isHidden = true
            
        } else {
            
            cell.btnFeatured.isHidden = false
            cell.imgFeatured.isHidden = false
        }
        cell.imgHeart.setImage(UIImage(named: "heartfilled"), for: .normal)
        
        return cell
    }
    
    
    //MARK:- Get Ad Favorite
    
    @objc func ViewDetailAction(sender: UIButton) {
        
        var indexPath: IndexPath!
        if let superview = sender.superview?.superview {
            if let cell = superview.superview as? FavouritesViewCell {
                indexPath = tblView.indexPath(for: cell)
                print(indexPath)
                print("index path: \(indexPath)")
                
                if indexPath != nil {
                    print("Tapped")
                    let vc = DetailViewController.instantiateFromStoryboard()
                    vc.productID = self.favoriteArray![indexPath.row].adId!
                    vc.isFromFav = true
                    vc.refreshDelegate = self
                    self.navigationController?.navigationBar.isHidden = false
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
}
