//
//  MyProfileViewController.swift
//  AdHouse
//
//  Created by  on 7/25/17.
//  Copyright © 2017 . All rights reserved.
//

import UIKit
import DropDown
import ImagePicker
import GooglePlaces

extension MyProfileViewController : CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let loc = manager.location?.coordinate
        locationLatitude = (loc?.latitude)!
        locationLongitude = (loc?.longitude)!
        
        self.latitude = "\(locationLatitude!)"
        self.longitude = "\(locationLongitude!)"
        
        print("Latitude: \(locationLatitude!) & Longitude: \(locationLongitude!)")
        self.getAddressFromLatLon(locationLatitude!, locationLongitude!)
        self.locationManager.stopUpdatingLocation()
        
    }
    
    func didAccesToLocation() -> Bool {
        
        if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.notDetermined {
            print("Not determine your current location")
            return true
        }
        else if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse {
            print("access to location")
            return true
        }
        else {
            print("not access to location")
            DispatchQueue.main.async {
                // self.callpopUp()
            }
            return false
        }
    }
    
    //    func callpopUp() {
    //
    //        self.alertView.btnOkAction.setTitle("Settings", for: .normal)
    //        self.alertView.btnCancel.setTitle("Cancel", for: .normal)
    //        self.alertView.alertShow(image: FAILURE_IMAGE , title: "Access Permission" , msg: "TAAJ PAY needs to access your location to show your current country", id: 1)
    //        self.alertView.isHidden = false
    //
    //    }
    
    func determineMyCurrentLocation() {
        
        locationManager = CLLocationManager()
        
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    func getAddressFromLatLon(_ Latitude: Double , _ Longitude: Double) {
        
        Alert.showLoader(message: "")
        
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        
        let lat: Double = Double("\(Latitude)")!
        let lon: Double = Double("\(Longitude)")!
        
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                
                if let pm = placemarks {
                    
                    if pm.count > 0 {
                        
                        let pm = placemarks![0]
                        
                        var area:String = ""
                        var addressString : String = ""
                        
                        if pm.subLocality != nil {
                            addressString = addressString + pm.subLocality! + ", "
                            area = addressString
                        }
                        if pm.thoroughfare != nil {
                            addressString = addressString + pm.thoroughfare! + ", "
                            area = addressString
                        }
                        if pm.locality != nil {
                            addressString = addressString + pm.locality! + ", "
                            area = addressString
                        }
                        if pm.country != nil {
                            addressString = addressString + pm.country!
                            area =  addressString
                        }
                        
                        print("Current Address : \(area)")
                        self.txtAddress.text = area
                        Alert.hideLoader()
                    }
                }
                
        })
    }
    
    @IBAction func getCurrentLocationAction(_ sender : UIButton) {
        self.view.endEditing(true)
        self.didAccesToLocation()
        self.determineMyCurrentLocation()
    }
    
    
}

class MyProfileViewController: UIViewController ,  UIImagePickerControllerDelegate , UINavigationControllerDelegate {
    
    class func instantiateFromStoryboard() -> MyProfileViewController {
        let storyboard = UIStoryboard(name: "SideMenu", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! MyProfileViewController
    }
    
    var cityArray:[CityResult]? = [CityResult]()
    let dropDownGender = DropDown()
    let dropDownCity = DropDown()
    
    let dropDownCountry = DropDown()
    var countryArray:[CityResult]? = [CityResult]()
    
    let dropDownCurrency = DropDown()
    var currencyArray:[CurrencyResult]? = [CurrencyResult]()
    
    var locationManager = CLLocationManager()
    var locationLatitude:Double?
    var locationLongitude:Double?
    
    @IBOutlet weak var mainView:UIView!
    @IBOutlet weak var textView:UIView!
    @IBOutlet weak var fullNameView:UIView!
    @IBOutlet weak var emailView:UIView!
    @IBOutlet weak var genderView:UIView!
    @IBOutlet weak var countryView:UIView!
    @IBOutlet weak var cityView:UIView!
    @IBOutlet weak var currencyView:UIView!
    @IBOutlet weak var addressView:UIView!
    @IBOutlet weak var phoneView:UIView!
    
    @IBOutlet weak var txtFullName:UITextField!
    @IBOutlet weak var txtEmail:UITextField!
    @IBOutlet weak var txtGender:UITextField!
    @IBOutlet weak var txtCountry:UITextField!
    @IBOutlet weak var txtCity:UITextField!
    @IBOutlet weak var txtCurrency:UITextField!
    @IBOutlet weak var txtAddress:UITextField!
    @IBOutlet weak var txtPhoneNum:UITextField!
    @IBOutlet weak var userImage:UIImageView!
    
    let imagePicker = UIImagePickerController()
    
    var fullName:String!
    var emailAddress:String!
    var gender:String!
    var country:String!
    var city:String!
    var currency:String!
    var address:String!
    var phone:String!
    var selectedLang: [String]!

    var countryID:Int! = 0
    var currencyID:Int! = 0
    var currencySingleObj:CurrencyResult?
    
    /* Search Area */
    var placeIDs: [String] = [String]()
    var cordinates: GMSPlace!
    let dropDownArea = DropDown()
    /* -- Search Area -- */
    
    var location:String!
    var latitude: String!
    var longitude: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        selectedLang =  UserDefaults.standard.object(forKey: "AppleLanguages") as? [String] ?? [String]()
        
        // Gender DropDown
        dropDownGender.anchorView = txtGender // UIView or UIBarButtonItem
        dropDownGender.bottomOffset = CGPoint(x: 0, y:(dropDownGender.anchorView?.plainView.bounds.height)!)
        dropDownGender.selectionAction = { [unowned self] (index: Int, item: String) in
            self.txtGender.text = item
            print("Selected item: \(item) at index: \(index)")
        }
        
        self.dropDownGender.dataSource = [NSLocalizedString("Male", comment: "") , NSLocalizedString("Female", comment: "")]
        
        self.selectCountry()
        self.selectCity()
        self.selectCurrency()
        
        // Country DropDown
        dropDownCountry.anchorView = txtCountry // UIView or UIBarButtonItem
        dropDownCountry.bottomOffset = CGPoint(x: 0, y:(dropDownCountry.anchorView?.plainView.bounds.height)!)
        dropDownCountry.selectionAction = { [unowned self] (index: Int, item: String) in
            self.txtCountry.text = item
            self.countryID = self.countryArray![index].id
            print("Selected item: \(item) at index: \(index)")
        }
        
        // City DropDown
        dropDownCity.anchorView = txtCity // UIView or UIBarButtonItem
        dropDownCity.bottomOffset = CGPoint(x: 0, y:(dropDownCity.anchorView?.plainView.bounds.height)!)
        dropDownCity.selectionAction = { [unowned self] (index: Int, item: String) in
            self.txtCity.text = item
            print("Selected item: \(item) at index: \(index)")
        }
      
        // Currency DropDown
        dropDownCurrency.anchorView = txtCurrency // UIView or UIBarButtonItem
        dropDownCurrency.bottomOffset = CGPoint(x: 0, y:(dropDownCurrency.anchorView?.plainView.bounds.height)!)
        dropDownCurrency.selectionAction = { [unowned self] (index: Int, item: String) in
            
            self.txtCurrency.text = item
            if self.currencySingleObj != nil {
                self.currencyID = self.currencySingleObj!.id
            } else {
                self.currencyID = self.currencyArray![index].id
            }
            print("Selected item: \(item) at index: \(index)")
        }
        
        
        self.mainView.layer.borderWidth = 0.5
        self.mainView.layer.borderColor = UIColor.lightGray.cgColor
        self.mainView.addShadow()
        
        self.userImage.layer.borderWidth = 2
        self.userImage.layer.borderColor = UIColor(red:160/255.0, green:0/255.0, blue:7/255.0, alpha: 1.0).cgColor
        self.userImage.layer.cornerRadius = 3
        
        
        self.textView.layer.borderWidth = 0.5
        self.textView.layer.borderColor = UIColor.lightGray.cgColor
        self.textView.addShadow()
        
        self.fullNameView.layer.borderWidth = 0.5
        self.fullNameView.layer.borderColor = UIColor.lightGray.cgColor
        self.fullNameView.addShadow()
        
        self.emailView.layer.borderWidth = 0.5
        self.emailView.layer.borderColor = UIColor.lightGray.cgColor
        self.emailView.addShadow()
        
        self.genderView.layer.borderWidth = 0.5
        self.genderView.layer.borderColor = UIColor.lightGray.cgColor
        self.genderView.addShadow()
        
        self.countryView.layer.borderWidth = 0.5
        self.countryView.layer.borderColor = UIColor.lightGray.cgColor
        self.countryView.addShadow()
        
        self.cityView.layer.borderWidth = 0.5
        self.cityView.layer.borderColor = UIColor.lightGray.cgColor
        self.cityView.addShadow()
        
        self.currencyView.layer.borderWidth = 0.5
        self.currencyView.layer.borderColor = UIColor.lightGray.cgColor
        self.currencyView.addShadow()
        
        self.addressView.layer.borderWidth = 0.5
        self.addressView.layer.borderColor = UIColor.lightGray.cgColor
        self.addressView.addShadow()
        
        self.phoneView.layer.borderWidth = 0.5
        self.phoneView.layer.borderColor = UIColor.lightGray.cgColor
        self.phoneView.addShadow()
        
        // Area Dropdown
        dropDownArea.anchorView = txtAddress // UIView or UIBarButtonItem
        dropDownArea.bottomOffset = CGPoint(x: 0, y:(dropDownArea.anchorView?.plainView.bounds.height)!)
        dropDownArea.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.txtAddress.text = item
            self.txtAddress.resignFirstResponder()
            
            let placeID = self.placeIDs[index]
            let placesClient = GMSPlacesClient.init()
            placesClient.lookUpPlaceID(placeID, callback: { (place, error) -> Void in
                if let error = error {
                    print("lookup place id query error: \(error.localizedDescription)")
                    return
                }
                
                guard let place = place else {
                    print("No place details for \(placeID)")
                    return
                }
                
                print("Place name \(place.name)")
                print("Place address \(String(describing: place.formattedAddress))")
                print("Place placeID \(place.placeID)")
                print("Place attributions \(String(describing: place.attributions))")
                print("Place Coordinates \(place.coordinate)")
                
                self.cordinates = place
                self.latitude = String(self.cordinates.coordinate.latitude)
                self.longitude = String(self.cordinates.coordinate.longitude)
                print("Latitude: \(self.latitude) , Longitude: \(self.longitude)")
                
                // self.locationCountry = self.cordinates.name
            })
        }
        
        txtAddress.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        
        
        /* Setting prefilled values */
        if let user = Singleton.sharedInstance.CurrentUser {
            
            self.userImage.setImageFromUrl(urlStr: (user.profileImage)!)
            self.txtFullName.text = user.fullName
            self.txtEmail.text = user.email
            self.txtGender.text = (user.gender != "") ? user.gender?.capitalizingFirstLetter() : "Gender"
            self.txtCity.text = user.city
            self.txtAddress.text = user.address
            self.txtPhoneNum.text = user.phone
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.navigationBar.isHidden = true
    }
    
    
    //MARK:- Menu Action
    
    @IBAction func MenuAction(sender: UIBarButtonItem) {
        self.slideMenuController()?.toggleLeft()
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if (textField.text?.isEmpty)! || textField.text == "" {
            print("nil")
        }
        else {
            let filter = GMSAutocompleteFilter()
            filter.type = .noFilter
            let placesClient = GMSPlacesClient.init()
            placesClient.autocompleteQuery(self.txtAddress.text!, bounds: nil, filter: filter, callback: {(results, error) -> Void in
                if let error = error {
                    print("Autocomplete error \(error)")
                    return
                }
                if let results = results {
                    var names: [String] = [String]()
                    var ids: [String] = [String]()
                    for result in results {
                        print("Result \(String(describing: result.attributedFullText.string)) with placeID \(String(describing: result.placeID))")
                        names.append(result.attributedFullText.string)
                        ids.append(result.placeID)
                    }
                    self.placeIDs = ids
                    self.dropDownArea.dataSource = names
                    self.dropDownArea.show()
                }
            })
        }
    }
    
    
    
    @IBAction func genderAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        self.dropDownGender.show()
        
    }
    
    @IBAction func countryAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        self.dropDownCountry.show()
    }
    
    func selectCountry() {
        
        Alert.showLoader(message: "")
        
        var param = [String:String]()
        
        if !isLanguageEnglish() {
            param.updateValue("ar", forKey: "lang")
        } else {
            param.updateValue("en", forKey: "lang")
        }
        
        HelpAndAboutServices.Country(param: param , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    print("Error: \((error as! Error).localizedDescription)")
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                return
            }
            
            print("Main")
            print(response!)
            
            print("SUCCESS")
            
            self.countryArray?.removeAll()
            
            let countryResponseArray = response?["Result"].arrayValue
            for resultObj in countryResponseArray! {
                let obj =  CityResult(json: resultObj)
                self.countryArray?.append(obj)
            }
            
            var titles: [String] = [String]()
            for result in self.countryArray! {
                print("Title \(String(describing: result.title))")
                titles.append(result.title!)
            }
            
            self.dropDownCountry.dataSource = titles
            
            for a in self.countryArray! {
                if let id = Singleton.sharedInstance.CurrentUser!.countryId {
                    if a.id == id.toInt() {
                        self.txtCountry.text = a.title!
                        self.countryID = a.id!
                    }
                }
            }
        })
        
    }
    
    @IBAction func cityAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        self.dropDownCity.show()
    }
    
    func selectCity() {
        
        Alert.showLoader(message: "")
       
        var param = [String:String]()
        
        if !isLanguageEnglish() {
            param.updateValue("ar", forKey: "lang")
        } else {
            param.updateValue("en", forKey: "lang")
        }
       
        HelpAndAboutServices.City(param: param, completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    print("Error: \((error as! Error).localizedDescription)")
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                return
            }
            
            print("Main")
            print(response!)
            
            print("SUCCESS")
            
            self.cityArray?.removeAll()
            
            let cityResponseArray = response?["Result"].arrayValue
            for resultObj in cityResponseArray! {
                let obj =  CityResult(json: resultObj)
                self.cityArray?.append(obj)
            }
            
            
            var titles: [String] = [String]()
            for result in self.cityArray! {
                print("Title \(String(describing: result.title))")
                titles.append(result.title!)
            }
            self.dropDownCity.dataSource = titles
            
        })
        
    }
 
    @IBAction func currencyAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        self.dropDownCurrency.show()
    }
    
    func selectCurrency() {
        
        Alert.showLoader(message: "")
        
        var param = [String:String]()
        
        if !isLanguageEnglish() {
            param.updateValue("ar", forKey: "lang")
        } else {
            param.updateValue("en", forKey: "lang")
        }
        
        HelpAndAboutServices.Currency(param: param , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    print("Error: \((error as! Error).localizedDescription)")
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                return
            }
            
            print("Main")
            print(response!)
            
            print("SUCCESS")
            
            if let array = response?["Result"].array {
                
                self.currencyArray?.removeAll()
                
                for resultObj in array {
                    let obj =  CurrencyResult(json: resultObj)
                    self.currencyArray?.append(obj)
                }
                
                var titles: [String] = [String]()
                for result in self.currencyArray! {
                    print("Title \(String(describing: result.name))")
                    titles.append(result.name!)
                }
                
                self.dropDownCurrency.dataSource = titles
                
                for a in self.currencyArray! {
                    if let id = Singleton.sharedInstance.CurrentUser!.currencyId {
                        if a.id == id.toInt() {
                            self.txtCurrency.text = a.name!
                            self.currencyID = a.id!
                        }
                    }
                }
                
            } else {
                
                let currencyResponse = CurrencyResult(object:(response?["Result"])!)
                self.currencySingleObj = currencyResponse
                self.dropDownCurrency.dataSource = [currencyResponse.name] as! [String]
                
                if let id = Singleton.sharedInstance.CurrentUser!.currencyId {
                    
                    if currencyResponse.id! == id.toInt() {
                        
                        self.txtCurrency.text = currencyResponse.name!
                        self.currencyID = currencyResponse.id!
                    }
                }
            }
            
        })
        
    }

    
    @IBAction func submitAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        
        self.fullName = txtFullName.text!
        self.gender = txtGender.text?.lowercased()
        self.country = txtCountry.text!
        self.city = txtCity.text!
        self.currency = txtCurrency.text!
        self.address = txtAddress.text!
        self.phone = txtPhoneNum.text!
        
        guard  !((fullName.isEmpty)         || (fullName.isEmptyOrWhitespace()))
            && !((gender.isEmpty)           || (gender.isEmptyOrWhitespace()))
            && !((country.isEmpty)          || (country.isEmptyOrWhitespace()))
            && !((city.isEmpty)             || (city.isEmptyOrWhitespace()))
            && !((currency.isEmpty)         || (currency.isEmptyOrWhitespace()))
            && !((phone.isEmpty)            || (phone.isEmptyOrWhitespace()))
            else {
                Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Please fill all Fields", comment: ""))
                return
        }
        
        if phone.count <= 7  {
            print(phone.count)
            Alert.showAlert(title: NSLocalizedString("Alert", comment: "") , message:   NSLocalizedString("Phone number at least 8 characters", comment: ""))
            return
        }
        if self.userImage.image == nil {
            Alert.showAlert(title: NSLocalizedString("Alert", comment: "") , message:   NSLocalizedString("Image Loading...", comment: ""))
            return
        }
            
        else {
            
            Alert.showLoader(message: "")
            
            let parameters:[String:Any] = ["full_name":self.fullName!, "phone":self.phone!, "address": (self.address == "") ? "" : self.address!, "currency_id":self.currencyID!,  "country_id":self.countryID!, "latitude":(self.latitude == nil) ? "" : self.latitude!, "longitude":
                (self.longitude == nil ) ? "" : self.longitude!, "city":self.city!, "gender":self.gender!, "user_id": (Singleton.sharedInstance.CurrentUser?.id)!]
            
            print(parameters)
            
            ProfileServices.Update(param: parameters, image:self.userImage.image!, completionHandler: {(status, response, error) in
                
                if !status {
                    if error != nil {
                        print("Error: \((error as! Error).localizedDescription)")
                        return
                    }
                    let msg = response?["Message"].stringValue
                    print("Message: \(String(describing: msg))")
                    return
                }
                
                print("Main")
                print(response!)
                
                print("SUCCESS")
                
                let userResultObj = UserObject(object:(response?["Result"])!)
                
                /**/
                UserDefaults.standard.removeObject(forKey: User_data_userDefault)
                UserDefaults.standard.synchronize()
                //
                /**/
                
                UserManager.saveUserObjectToUserDefaults(userResult: userResultObj)
                Singleton.sharedInstance.CurrentUser = userResultObj
                
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "NavHome")
                self.slideMenuController()?.changeMainViewController(vc!, close: true)
                
                Alert.showAlert(title:NSLocalizedString( "Alert", comment: ""), message:NSLocalizedString( "Your Profile has been updated", comment: ""))
            })
        }
    }
    
    //MARK:- Camera Button Action
    
    @IBAction func cameraAction(){
        self.openCamera()
    }
    
    func openCamera(){
        
        let imagePickerController = ImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.imageLimit = 1
        present(imagePickerController, animated: true, completion: nil)
        
//        let cameraViewController = CameraViewController(croppingEnabled: false) { [weak self] image, asset in
//            
//            if image != nil {
//                self?.userImage.image = AppHelper.resizeImage(image: image!, compressionQuality: 0.25)
//            }
//            self?.dismiss(animated: true, completion: nil)
//        }
//        
//        present(cameraViewController, animated: true, completion: nil)
    }
}

extension MyProfileViewController: ImagePickerDelegate {
    
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        if images.count > 0 {
            self.userImage.image = AppHelper.resizeImage(image: images.first!, compressionQuality: 0.25)
        }
        
        //Dismiss Controller
        self.dismiss(animated: true, completion: nil)
    }
    
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
        //Dismiss Controller
        self.dismiss(animated: true, completion: nil)
    }
    
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        
    }
}
