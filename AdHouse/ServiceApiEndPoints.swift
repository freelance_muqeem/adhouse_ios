//
//  ServiceApiEndPoints.swift
//  AdHouse
//
//  Created by maazulhaq on 04/08/2017.
//  Copyright © 2017 . All rights reserved.
//

import UIKit

class ServiceApiEndPoints:NSObject {
    
    static let baseUrl  = "https://adhouseq.com/services/api"
    
    //   static let baseUrl  = "https://www.stagingic.com/portfolio/Adhouse/api" //Live
   // static let baseUrl  = "https://35.160.175.165/portfolio/Adhouse/api" //Local
    
    static let login = baseUrl + "/login"
    static let register = baseUrl + "/register"
    static let forgotpassword = baseUrl + "/forgotpassword"
    static let logout = baseUrl + "/logout"
    
    static let help = baseUrl + "/cms?keyword=help&lang="
    static let about = baseUrl + "/cms?keyword=about&lang="
    static let term = baseUrl + "/cms?keyword=terms&lang="
    static let privacy = baseUrl + "/cms?keyword=policy&lang="
    static let contactDetails = baseUrl + "/contact/web?lang="
    
    static let contact = baseUrl + "/contact"
    
    static let updateDeviceToken = baseUrl + "/update/devicetoken"
    
    static let updatePofile = baseUrl + "/user/update"
    static let getProfile = baseUrl + "/user/profile"
    static let changePassword = baseUrl + "/changepassword"
    static let currency = baseUrl + "/currency?lang="
    static let country = baseUrl + "/countries?lang="
    static let city = baseUrl + "/city?lang="
    static let getVehicles = baseUrl + "/get/vehicle/ads"
    static let getProperty = baseUrl + "/get/property/ads"
    static let myAds = baseUrl + "/get/myads"
    static let search = baseUrl + "/search"
    static let filter = baseUrl + "/filter" // "/ad/filter"
    static let favorite = baseUrl + "/getfavourite"
    static let addFavorite = baseUrl + "/addfavourite"
    static let notification = baseUrl + "/get/notifications"
    
    static let categories = baseUrl + "/categories"
    static let subCategories = baseUrl + "/sub/categories"
    static let attributes = baseUrl + "/attributes"
    static let feature = baseUrl + "/get/category/featured"
    static let classifieds = baseUrl + "/get/classified/ads"
    
   // static let ad = baseUrl + "/ad/description"
    static let submitAd = baseUrl + "/submit/ad" //"/ad"
    static let deleteAd = baseUrl + "/ad/Delete"
    static let updateAd = baseUrl + "/update"//"/ad/update"
    static let userSubscription = baseUrl + "/user/subscription"
    static let subscriptionPackages = baseUrl + "/subscription/pakages"
    static let subscriptionUserPackage = baseUrl + "/subscribe/package"
    static let searchCategoryType = baseUrl + "/search/categories?lang="
    static let sameUserAds = baseUrl + "/get/owner/ads"
    static let blockUser = baseUrl + "/check/block/user"
    static let ad = baseUrl + "/description"
    static let getCompleteAds = baseUrl + "/get/ads"
    static let categoryClassified = baseUrl + "/get/sub/classified/ads"
    static let subscriptionToken = baseUrl + "/client/token"
}
