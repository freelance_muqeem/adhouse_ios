//
//  RootViewController.swift
//  AdHouse
//
//  Created by maazulhaq on 24/07/2017.
//  Copyright © 2017 . All rights reserved.
//

import UIKit
import SlideMenuControllerSwift

class RootViewController: SlideMenuController {

    override func awakeFromNib() {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "NavHome") {
            self.mainViewController = controller
        }
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "RearViewController") {
            self.leftViewController = controller
        }
        SlideMenuOptions.contentViewDrag = true
        SlideMenuOptions.panGesturesEnabled = false
        super.awakeFromNib()
    }

    /*
    func placeNavigationBtn(veiwCon:UIViewController){
        
        //setNavBarTitle(veiwCon: veiwCon)
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        leftBtn =  UIButton(type: .custom)
        leftBtn?.setImage(UIImage(named: "sede-nav"), for: .normal)
        leftBtn?.addTarget(self, action: #selector(RootViewController.SideMenuBtnPressed), for: .touchUpInside)
        leftBtn?.frame = CGRect(x: 0, y:0 , width: 25, height: 25)
        let  leftBarButton: UIBarButtonItem = UIBarButtonItem(customView: leftBtn!)
        self.navigationItem.setLeftBarButton(leftBarButton, animated: true)
        leftBtn?.semanticContentAttribute = .forceLeftToRight;
        
        rightBtn =  UIButton(type: .custom)
        rightBtn?.setImage(UIImage(named: "notofication"), for: .normal)
        rightBtn?.addTarget(self, action: #selector(RootViewController.notificationBtnPressed), for: .touchUpInside)
        rightBtn?.frame = CGRect(x: 0, y:0 , width: 25, height: 25)
        let  rightBarButton: UIBarButtonItem = UIBarButtonItem(customView: rightBtn!)
        rightBtn?.semanticContentAttribute = .forceLeftToRight;
        
        self.navigationItem.setRightBarButton(rightBarButton, animated: true)
        
    }
    
    func SideMenuBtnPressed(){
        if self.revealViewController() != nil {
            
            menuButton.addTargetForAction(target: self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }
    
    func notificationBtnPressed(){
        
        let mainViewController = sideMenuController!
        let notificationsVc = NotificationsViewController.instantiateFromStoryboard()
        let navigationController = mainViewController.rootViewController as! RootNavigationController
        navigationController.pushViewController(notificationsVc, animated: true)
        
        mainViewController.hideLeftView(animated: true, completionHandler: nil)
        
    }
    */

}
