//
//  SearchViewController.swift
//  AdHouse
//
//  Created by  on 7/26/17.
//  Copyright © 2017 . All rights reserved.
//

import UIKit
import DropDown
import SwiftyJSON

extension SearchViewController: RefreshDelegate {
    func didRefresh(count: CGFloat) {
        
    }
    
    func didRefresh() {
        self.searchArray?.removeAll()
        Search()
    }
    
}

extension SearchViewController: FilterDelegate {
    func didFilter(param: [String : Any], title: String) {
        
        self.parameters = param
        if let temp = param["category_id"] as? Int {
            self.categoryId = temp
        }
        else {
            self.categoryId = nil
        }
        if !title.isEmptyOrWhitespace() {
            self.txtSearch.text = title
        }
        offsetForApiCall = 0
        postLimitPerCall = 10
        self.searchArray?.removeAll()
        self.tblView.reloadData()
        
       // Search()
        Filter()
    }
    
}


class SearchViewController: UIViewController , UITableViewDelegate , UITableViewDataSource {
    
    class func instantiateFromStoryboard() -> SearchViewController {
        let storyboard = UIStoryboard(name: "SideMenu", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! SearchViewController
    }
    
    @IBOutlet weak var tblView:UITableView!
    @IBOutlet weak var searchView:UIView!
    @IBOutlet weak var txtSearch:UITextField!
    
    var searchArray:[SearchResult]? = [SearchResult]()
    var isFav = false
    var selectedLang: [String]!
    var leftBtn:UIButton?
    var rightBtn:UIButton?
    var offsetForApiCall:Int? = 0
    var postLimitPerCall = 10
    var parameters = [String: Any]()
    let categoryDropDown = DropDown()
    var searchTypeArray:[searchCategory]? = [searchCategory]()
    var categoryId:Int?
    var lang:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        selectedLang =  UserDefaults.standard.object(forKey: "AppleLanguages") as? [String] ?? [String]()
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.title = NSLocalizedString("Search", comment: "")
        self.placeBackBtn()
        self.searchView.layer.borderWidth = 0.5
        self.searchView.layer.borderColor = UIColor.lightGray.cgColor
        self.searchView.addShadow()
        self.txtSearch.keyboardType = .default
        self.searchType()
        
        // SubCategory DropDown
        
        categoryDropDown.anchorView = txtSearch // UIView or UIBarButtonItem
        categoryDropDown.bottomOffset = CGPoint(x: 0, y:(categoryDropDown.anchorView?.plainView.bounds.height)!)
        categoryDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
//            self.txtSearch.text = item
            
            self.view.endEditing(true)
            
            self.categoryId = self.searchTypeArray?[index].id!
            
            print(self.categoryId!)
            
            print("Selected item: \(item) at index: \(index)")
            
            self.offsetForApiCall = 0
            self.postLimitPerCall = 10
            self.searchArray?.removeAll()
            self.tblView.reloadData()
            self.Search()
            
            
        }
        
        
        // Register Nib Cells
        let cellNib = UINib(nibName: "FavouritesViewCell", bundle: nil)
        self.tblView.register(cellNib, forCellReuseIdentifier: String(describing: FavouritesViewCell.self))
        
        txtSearch.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black,NSAttributedStringKey.font : UIFont(name: "HelveticaNeue-Medium", size: 17)!]
        
    }
    
    //MARK:-  Back Button Code
    
    func placeBackBtn(){
        
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        leftBtn =  UIButton(type: .custom)
        leftBtn?.setImage(UIImage(named: "backbtn"), for: .normal)
        leftBtn?.addTarget(self, action: #selector(SearchViewController.BackBtnPressed), for: .touchUpInside)
        leftBtn?.frame = CGRect(x: 0, y:0 , width: 25, height: 25)
        let  leftBarButton: UIBarButtonItem = UIBarButtonItem(customView: leftBtn!)
        self.navigationItem.setLeftBarButton(leftBarButton, animated: true)
        
        rightBtn =  UIButton(type: .custom)
        rightBtn?.setImage(UIImage(named: "filter"), for: .normal)
        rightBtn?.addTarget(self, action: #selector(SearchViewController.FilterBtnPressed), for: .touchUpInside)
        rightBtn?.frame = CGRect(x: 0, y:0 , width: 25, height: 25)
        let  rightBarButton: UIBarButtonItem = UIBarButtonItem(customView: rightBtn!)
        rightBtn?.semanticContentAttribute = .forceLeftToRight;
        
        self.navigationItem.setRightBarButton(rightBarButton, animated: true)
        
        
    }

    func Search() {
        
        let offset = offsetForApiCall!
        let limit = postLimitPerCall
        
        if !self.txtSearch.text!.isEmpty && !self.txtSearch.text!.isEmptyOrWhitespace() {
            self.parameters.updateValue(self.txtSearch.text!, forKey: "keyword")
        } 
            
            Alert.showLoader(message: "")
            
            let userId = (Singleton.sharedInstance.CurrentUser == nil) ? -1 : (Singleton.sharedInstance.CurrentUser?.id)!
            
            //self.parameters = ["user_id": userId , "title": keyword , "offset" : offset , "limit" : limit ]
            if self.categoryId != nil {
                self.parameters.updateValue(self.categoryId!, forKey: "category_id")
            }else{
               self.parameters.updateValue(self.searchTypeArray?.last?.id ?? 0, forKey: "category_id")
            }
            
            self.parameters.updateValue(userId, forKey: "user_id")
            self.parameters.updateValue(offset, forKey: "offset")
            self.parameters.updateValue(limit, forKey: "limit")
            
            if !isLanguageEnglish() {
                self.parameters.updateValue("ar", forKey: "lang")
            } else {
                self.parameters.updateValue("en", forKey: "lang")
            }
            
            //parameters.updateValue((Singleton.sharedInstance.CurrentUser != nil) ? (Singleton.sharedInstance.CurrentUser?.id)! : -1, forKey: "user_id")
            
            print("Parameters: \(parameters)")
            
            SearchServices.Search(param: parameters, completionHandler: {(status, response, error) in
                
                if !status {
                    if error != nil {
                        print("Error: \((error as! Error).localizedDescription)")
                        return
                    }
                    let msg = response?["Message"].stringValue
                    print("Message: \(String(describing: msg))")
                    return
                }
                
                print(response!)
                print("SUCCESS")
                Alert.hideLoader()
                // self.searchArray?.removeAll()
                
                let adsResponseArray = response?["Result"]["Ad"].arrayValue
                print(adsResponseArray!)
                
                for resultObj in adsResponseArray! {
                    let obj =  SearchResult(json: resultObj)
                    self.searchArray?.append(obj)
                }
                
                self.parameters.removeAll()
                
                self.tblView.reloadData()
                
            })
            
        
    }
    
    
    func Filter() {
        
        let keyword = self.txtSearch.text!
        let offset = offsetForApiCall!
        let limit = postLimitPerCall
        
        print(keyword)
        
//        if keyword.isEmpty && keyword.isEmptyOrWhitespace() {
//            Alert.showAlert(title:NSLocalizedString("Alert", comment: "") , message: NSLocalizedString("Search Something", comment: ""))
//        } else {
        
            Alert.showLoader(message: "")
            
            let userId = (Singleton.sharedInstance.CurrentUser == nil) ? -1 : (Singleton.sharedInstance.CurrentUser?.id)!
            
            //self.parameters = ["user_id": userId , "title": keyword , "offset" : offset , "limit" : limit ]
            if self.categoryId != nil {
                self.parameters.updateValue(self.categoryId!, forKey: "category_id")
            }else{
                self.parameters.updateValue(0, forKey: "category_id")
            }
            
            self.parameters.updateValue(userId, forKey: "user_id")
            self.parameters.updateValue(keyword, forKey: "title")
            self.parameters.updateValue(offset, forKey: "offset")
            self.parameters.updateValue(limit, forKey: "limit")
            
            if !isLanguageEnglish() {
                self.parameters.updateValue("ar", forKey: "lang")
            } else {
                self.parameters.updateValue("en", forKey: "lang")
            }
            

            
            print("Parameters: \(parameters)")
            
            SearchServices.Filter(param: parameters, completionHandler: {(status, response, error) in
                
                if !status {
                    if error != nil {
                        print("Error: \((error as! Error).localizedDescription)")
                        return
                    }
                    let msg = response?["Message"].stringValue
                    print("Message: \(String(describing: msg))")
                    return
                }
                
                print(response!)
                print("SUCCESS")
                Alert.hideLoader()
                // self.searchArray?.removeAll()
                
                let adsResponseArray = response?["Result"].arrayValue
                print(adsResponseArray!)
                
                for resultObj in adsResponseArray! {
                    let obj =  SearchResult(json: resultObj)
                    self.searchArray?.append(obj)
                }
                
                self.parameters.removeAll()
                
                self.tblView.reloadData()
                
            })
            
//        }
    }
    
    func searchType() {
        
        Alert.showLoader(message: "")
        var param = [String:String]()
        
        if !isLanguageEnglish() {
            param.updateValue("ar", forKey: "lang")
        } else {
            param.updateValue("en", forKey: "lang")
        }
        
        SearchServices.SearchCategoryType(param:param , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    print("Error: \((error as! Error).localizedDescription)")
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                return
            }
            
            print(response!)
            
            let typeArray = response?["Result"].arrayValue
            print(typeArray!)
            
            var names: [String] = [String]()
            
            for resultObj in typeArray! {
                let obj =  searchCategory(json: resultObj)
                names.append(obj.name!)
                self.searchTypeArray?.append(obj)
                
            }
            
            self.categoryDropDown.dataSource = names
            
        })
        
    }
    
    @IBAction func searchAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        
        if self.txtSearch.text!.isEmpty && self.txtSearch.text!.isEmptyOrWhitespace() {
            Alert.showAlert(title:NSLocalizedString("Alert", comment: "") , message: NSLocalizedString("Search Something", comment: ""))
            return
        }
        
        self.offsetForApiCall = 0
        self.postLimitPerCall = 10
        self.searchArray?.removeAll()
        self.tblView.reloadData()
        
        self.Search()

        
    }
    
    @objc func BackBtnPressed(){
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func FilterBtnPressed(){
        
        let filterVc = FilterViewController.instantiateFromStoryboard()
        filterVc.delegate = self
        filterVc.txtTitle = self.txtSearch.text
        self.navigationController?.pushViewController(filterVc, animated: true)
    }
    
    //MARK:-  TableView Delegate
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        //print("IndexPath: \(indexPath), NewsFeed Count: \((self.newsfeedArray?.count)!)")
        
        if (indexPath.row == (self.searchArray?.count)! - 1) && ((self.searchArray?.count)! % 10) == 0 {
            print("Load more")
            
            self.offsetForApiCall = self.offsetForApiCall! + 10
            self.postLimitPerCall = self.postLimitPerCall + 10
            self.Search()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return GISTUtility.convertToRatio(120)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.searchArray!.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let searchObj:SearchResult = self.searchArray![indexPath.row]
        
        print(searchObj)
        
        let cell: FavouritesViewCell = tableView.dequeueReusableCell(withIdentifier: "FavouritesViewCell", for: indexPath) as! FavouritesViewCell
        
       cell.imgLocation.isHidden = true
        if searchObj.isVehicle! == 1 {
            
            cell.lblCarMake.isHidden = false
            cell.lblCarClass.isHidden = false
         
            for i in 0..<searchObj.adDetails!.count{
                
                let data = searchObj.adDetails![i].attributeId
                
                if (data == 3){
                    cell.lblCarMake.text = searchObj.adDetails![i].attributeValue
                }
                
                if (data == 4){
                    cell.lblCarClass.text = searchObj.adDetails![i].attributeValue
                }
            }
        }else{
           
             cell.lblCarMake.isHidden = true
            cell.lblCarClass.isHidden = true
        }
        
       
        cell.lblProductName.text = searchObj.title
        cell.lblPrice.text = "QAR " + String(describing:searchObj.price!.decimal)
        
        if searchObj.isVehicle == 1 {
            
            cell.lblLocation.text = "Km " + String(describing: searchObj.kilometer!.decimal)
            
        } else {
            
            cell.lblLocation.text = searchObj.area
            
        }
        
        if searchObj.isLike == 1 {
            cell.imgHeart.setImage(UIImage(named: "heartfilled"), for: .normal)
        }
        else {
            cell.imgHeart.setImage(UIImage(named: "heart"), for: .normal)
        }
        
        if searchObj.featured == 1 {
            cell.imgFeatured.isHidden = false
            cell.btnFeatured.isHidden = false
        }else{
            cell.imgFeatured.isHidden = true
           cell.btnFeatured.isHidden = true
        }
        
        cell.imgProduct.setImageFromUrl(urlStr: (searchObj.adImages?.first?.imageUrl)!)
        cell.btnViewDetail.isHidden = true
        cell.imgHeart.addTarget(self, action: #selector(self.AddFavoriteAction(sender:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let vc = DetailViewController.instantiateFromStoryboard()
        vc.productID = self.searchArray![indexPath.row].id!
        vc.refreshDelegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- Add/Delete Favorite
    
    @objc func AddFavoriteAction(sender: UIButton) {
        
        if Singleton.sharedInstance.CurrentUser == nil {
            
            Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("You have logged in as Guest Member, in order to access this feature. you need to SignUp on App", comment: ""))
        }
        else {
            if !self.isFav {
                self.isFav = !self.isFav
                
                var indexPath: IndexPath!
                if let superview = sender.superview?.superview {
                    if let cell = superview.superview as? FavouritesViewCell {
                        indexPath = tblView.indexPath(for: cell)
                        print(indexPath)
                        print("index path: \(indexPath)")
                        
                        if indexPath != nil {
                            
                             if self.searchArray![indexPath.row].userId! == Singleton.sharedInstance.CurrentUser?.id {
                                self.isFav = !self.isFav
                                Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("You cannot favorite your own ads.", comment: ""))
                                return
                            }
                            
                            
                            let parameters:[String:Any] = [
                                "user_id": (Singleton.sharedInstance.CurrentUser!.id!),
                                "ad_id":self.searchArray![indexPath.row].id!,
                                "state":1-self.searchArray![indexPath.row].isLike!
                            ]
                            
                            print("PARAMETERS: \(parameters)")
                            Alert.showLoader(message: "")
                            
                            FavouriteServices.Add(param: parameters , completionHandler: {(status, response, error) in
                                self.isFav = !self.isFav
                                if !status {
                                    if error != nil {
                                        print("Error: \((error as! Error).localizedDescription)")
                                        return
                                    }
                                    let msg = response?["Message"].stringValue
                                    print("Message: \(String(describing: msg))")
                                    return
                                }
                                
                                print(response!)
                                print("SUCCESS")
                                self.searchArray![indexPath.row].isLike = 1-self.searchArray![indexPath.row].isLike!
                                self.tblView.reloadData()
                                
                            })
                        }
                    }
                }
            }
        }
    }
    
}

extension SearchViewController: UITextFieldDelegate {
    @objc func textFieldDidChange(_ textField: UITextField) {
        if (textField.text?.isEmpty)! || textField.text == "" {
            print("nil")
            self.categoryDropDown.hide()
        }
        else {
            let obj = self.searchTypeArray
            
            var names = [String]()
            
            for item in obj! {
                names.append( textField.text! + NSLocalizedString(" in category ", comment: "") + item.name!)
                self.categoryId = item.id ?? 70
            }
            self.categoryDropDown.dataSource = names
            self.categoryDropDown.show()
        }
    }
}
