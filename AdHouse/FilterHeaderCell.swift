//
//  FilterHeaderCell.swift
//  AdHouse
//
//  Created by  on 8/12/17.
//  Copyright © 2017 . All rights reserved.
//

import UIKit
import DropDown
import GooglePlaces
import RangeSeekSlider

protocol filterHeaderDelegate {
    
    func changeCategoryType(type:CategoryType)
    
}


class FilterHeaderCell: UITableViewCell {
    
    //MARK:- View Outlet
    
    @IBOutlet var seekBarSlider: RangeSeekSlider!
    @IBOutlet weak var locationView:UIView!
    @IBOutlet weak var titleView:UIView!
    @IBOutlet weak var categoryView:UIView!
    @IBOutlet weak var featureView:UIView!
    @IBOutlet weak var subCategoryView:UIView!
    @IBOutlet weak var countryView:UIView!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var lblMaxPrice: UILabel!
    @IBOutlet var lblMinPrice: UILabel!
    
    //MARK:- TextField Outlet
    
    @IBOutlet weak var txtLocation:UITextField!
    @IBOutlet weak var txtTitle:UITextField!
    @IBOutlet weak var txtCategory:UITextField!
    @IBOutlet weak var txtFeature:UITextField!
    @IBOutlet weak var txtSubCategory:UITextField!
    @IBOutlet weak var txtCountry:UITextField!
    @IBOutlet weak var txtMinimum:UITextField!
    @IBOutlet weak var txtMaximum:UITextField!
    
    //MARK:- Label Outlet
    
    @IBOutlet weak var lblCategory:UILabel!
    @IBOutlet weak var lblFeatured:UILabel!
    @IBOutlet weak var lblSubCategory:UILabel!
    @IBOutlet weak var lblCountry:UILabel!
    @IBOutlet weak var lblPrice:UILabel!
    @IBOutlet weak var lblPriceRange:UILabel!
    
    //MARK:- Height Constraint
    
    @IBOutlet weak var heightSubCategoryConstraint: BaseLayoutConstraint!
    @IBOutlet weak var heightSubCategoryView: BaseLayoutConstraint!
    
    //MARK:- DropDown
    
    let categoryDropDown = DropDown()
    let subCategoryDropDown = DropDown()
    let countryDropDown = DropDown()
    let featureDropDown = DropDown()
    
    var delegate:CategoryDelegate?
    var filterDelegate:filterHeaderDelegate?
    
    var categoryIDs: [Int] = [Int]()
    var subCatIDs: [Int] = [Int]()
    var countryIDs: [Int] = [Int]()
    var arrMaxPrice = [String]()
    var arrMinPrice = [String]()
    
    var selectedCatID: Int?
    var selectedSubCatID: Int?
    var selectedCountryID: Int?
    var maxPrice:String?
    var minPrice:String?
    
    var minimumPrice:CGFloat?
    var maximumPrice:CGFloat?
    
    /* Search Area */
    var placeIDs: [String] = [String]()
    var cordinates: GMSPlace!
    let dropDownArea = DropDown()
    /* -- Search Area -- */
    
    var paramHelper : ParamsHolder!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.borderView()
        seekBarSlider.delegate = self
        txtTitle.delegate = self
        self.heightSubCategoryConstraint.constant = 0
        self.heightSubCategoryView.constant = 0
        self.subCategoryView.isHidden = true
        
        self.lblMinPrice.text = "QAR 0"
        self.lblMaxPrice.text = "QAR 5000000"
        self.maxPrice = "0"
        self.minPrice = "5000000"
        self.lblPriceRange.text = "QAR 0 - 5000000"
        seekBarSlider.minValue = 0.0
        seekBarSlider.maxValue = 5000000.0
        seekBarSlider.selectedMinValue = 0.0
        seekBarSlider.selectedMaxValue = 5000000.0
        
//        self.txtMinimum.text = "0"
        self.txtMaximum.text = "5000000"
        self.txtMinimum.keyboardType = .numberPad
        
        self.minimumPrice = 0.0
        self.maximumPrice = 5000000.0
        // Feature DropDown
        featureDropDown.anchorView = txtFeature // UIView or UIBarButtonItem
        featureDropDown.bottomOffset = CGPoint(x: 0, y:(featureDropDown.anchorView?.plainView.bounds.height)!)
        featureDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.txtFeature.text = item
            print("Selected item: \(item) at index: \(index)")
            
            if item == "Yes" {
                self.paramHelper.params.updateValue(1, forKey: "featured")
            }
            else {
                self.paramHelper.params.updateValue(0, forKey: "featured")
            }
        }
        
        self.featureDropDown.dataSource = [NSLocalizedString("Yes", comment: ""), NSLocalizedString("No", comment: "") ]
        
        // Category DropDown
        categoryDropDown.anchorView = txtCategory // UIView or UIBarButtonItem
        categoryDropDown.bottomOffset = CGPoint(x: 0, y:(categoryDropDown.anchorView?.plainView.bounds.height)!)
        categoryDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.txtCategory.text = item
            self.selectedCatID = self.categoryIDs[index]
            self.paramHelper.params.updateValue(self.selectedCatID!, forKey: "category_id")
            
            self.lblMinPrice.text = "QAR"+" "+self.arrMinPrice[index]
            self.lblMaxPrice.text = "QAR"+" "+self.arrMaxPrice[index]
            self.maxPrice = self.arrMaxPrice[index]
            self.minPrice = self.arrMinPrice[index]
            
            
            
            self.seekBarSlider.minValue = (self.arrMinPrice[index]).toCGFloat()
            self.seekBarSlider.maxValue = (self.arrMaxPrice[index]).toCGFloat()
            self.seekBarSlider.selectedMinValue = (self.arrMinPrice[index]).toCGFloat()
            self.seekBarSlider.selectedMaxValue = (self.arrMaxPrice[index]).toCGFloat()
            
            self.minimumPrice = (self.arrMinPrice[index]).toCGFloat()
            self.maximumPrice = (self.arrMaxPrice[index]).toCGFloat()
            
            self.txtMinimum.text = "\((self.arrMinPrice[index]).toCGFloat())"
            self.txtMaximum.text = "\((self.arrMaxPrice[index]).toCGFloat())"
            
            self.lblPriceRange.text = "QAR \(self.arrMinPrice[index]) - \(self.arrMaxPrice[index])"
            
            self.paramHelper.params.updateValue(self.maxPrice!, forKey: "price")
            self.paramHelper.params.updateValue(self.minPrice!, forKey: "price_min")
            self.paramHelper.params.updateValue("", forKey: "area")
            self.paramHelper.params.updateValue("\(0.00000)", forKey: "latitude")
            self.paramHelper.params.updateValue("\(0.00000)", forKey: "longitude")
            self.paramHelper.params.updateValue(0, forKey: "sub_category_id")
            
            self.delegate?.didSelectCategory(position: index)
            
            self.txtSubCategory.text = ""
            self.heightSubCategoryConstraint.constant = 17
            self.heightSubCategoryView.constant = 40
            self.subCategoryView.isHidden = false
            
            //            if self.txtCategory.text == "classified" {
            //
            //                self.heightSubCategoryConstraint.constant = 17
            //                self.heightSubCategoryView.constant = 40
            //                self.subCategoryView.isHidden = false
            //                self.filterDelegate?.changeCategoryType(type: CategoryType.CLASSIFIED)
            //
            //            } else {
            //
            //                self.heightSubCategoryConstraint.constant = 0
            //                self.heightSubCategoryView.constant = 0
            //                self.subCategoryView.isHidden = true
            //                self.filterDelegate?.changeCategoryType(type: CategoryType.PROPERTY)
            //            }
            
            print("Selected item: \(item) at index: \(index)")
        }
        
        // Sub Category DropDown
        subCategoryDropDown.anchorView = txtSubCategory // UIView or UIBarButtonItem
        subCategoryDropDown.bottomOffset = CGPoint(x: 0, y:(subCategoryDropDown.anchorView?.plainView.bounds.height)!)
        subCategoryDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.txtSubCategory.text = item
            self.selectedSubCatID = self.subCatIDs[index]
            self.paramHelper.params.updateValue(self.selectedSubCatID!, forKey: "sub_category_id")
            
            self.delegate?.didSelectSubCategory(position: index)
            print("Selected item: \(item) at index: \(index)")
        }
        
        // Country DropDown
        countryDropDown.anchorView = txtCountry // UIView or UIBarButtonItem
        countryDropDown.bottomOffset = CGPoint(x: 0, y:(countryDropDown.anchorView?.plainView.bounds.height)!)
        countryDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.txtCountry.text = item
            self.selectedCountryID = self.countryIDs[index]
            self.paramHelper.params.updateValue(self.selectedCountryID!, forKey: "country_id")
            
            self.delegate?.didSelectCountry(position: index)
            print("Selected item: \(item) at index: \(index)")
        }
        
        // Area Dropdown
        dropDownArea.anchorView = txtLocation // UIView or UIBarButtonItem
        dropDownArea.bottomOffset = CGPoint(x: 0, y:(dropDownArea.anchorView?.plainView.bounds.height)!)
        dropDownArea.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.txtLocation.text = item
            self.txtLocation.resignFirstResponder()
            
            let placeID = self.placeIDs[index]
            let placesClient = GMSPlacesClient.init()
            placesClient.lookUpPlaceID(placeID, callback: { (place, error) -> Void in
                if let error = error {
                    print("lookup place id query error: \(error.localizedDescription)")
                    return
                }
                
                guard let place = place else {
                    print("No place details for \(placeID)")
                    return
                }
                
                print("Place name \(place.name)")
                print("Place address \(String(describing: place.formattedAddress))")
                print("Place placeID \(place.placeID)")
                print("Place attributions \(String(describing: place.attributions))")
                print("Place Coordinates \(place.coordinate)")
                
                self.cordinates = place
                
                self.paramHelper.params.updateValue(item, forKey: "area")
                self.paramHelper.params.updateValue("\(place.coordinate.latitude)", forKey: "latitude")
                self.paramHelper.params.updateValue("\(place.coordinate.longitude)", forKey: "longitude")
            })
        }
        
        txtLocation.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        txtMinimum.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        txtMaximum.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)

    }
    
    //MARK:-  Border View
    
    func borderView(){
        
        self.locationView.layer.borderWidth = 0.5
        self.locationView.layer.borderColor = UIColor.lightGray.cgColor
        self.locationView.addShadow()
        
        self.titleView.layer.borderWidth = 0.5
        self.titleView.layer.borderColor = UIColor.lightGray.cgColor
        self.titleView.addShadow()
        
        self.categoryView.layer.borderWidth = 0.5
        self.categoryView.layer.borderColor = UIColor.lightGray.cgColor
        self.categoryView.addShadow()
        
        self.countryView.layer.borderWidth = 0.5
        self.countryView.layer.borderColor = UIColor.lightGray.cgColor
        self.countryView.addShadow()
        
        self.featureView.layer.borderWidth = 0.5
        self.featureView.layer.borderColor = UIColor.lightGray.cgColor
        self.featureView.addShadow()
        
        self.subCategoryView.layer.borderWidth = 0.5
        self.subCategoryView.layer.borderColor = UIColor.lightGray.cgColor
        self.subCategoryView.addShadow()
        
    }
    
    //MARK:-  Slider Action
    
    
    /*   @IBAction func sliderAction(_ sender: UISlider) {
     let largeNumber = Int(sender.value)
     let numberFormatter = NumberFormatter()
     numberFormatter.numberStyle = NumberFormatter.Style.decimal
     let formattedNumber = numberFormatter.string(from: NSNumber(value:largeNumber))
     
     self.lblMaxPrice.text = "QAR \(String(describing: formattedNumber!))"
     } */
    
    //MARK:-  DropDown Action
    
    @IBAction func categoryAction(_ sender : UIButton ) {
        self.categoryDropDown.show()
    }
    
    @IBAction func subCategoryAction(_ sender : UIButton ) {
        self.subCategoryDropDown.show()
    }
    
    @IBAction func countryAction(_ sender : UIButton ) {
        self.countryDropDown.show()
    }
    
    @IBAction func featureAction(_ sender : UIButton) {
        self.featureDropDown.show()
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField == self.txtLocation {
            if (textField.text?.isEmpty)! || textField.text == "" {
                print("nil")
            }
            else {
                let filter = GMSAutocompleteFilter()
                filter.type = .noFilter
                let placesClient = GMSPlacesClient.init()
                placesClient.autocompleteQuery(self.txtLocation.text!, bounds: nil, filter: filter, callback: {(results, error) -> Void in
                    if let error = error {
                        print("Autocomplete error \(error)")
                        return
                    }
                    if let results = results {
                        var names: [String] = [String]()
                        var ids: [String] = [String]()
                        for result in results {
                            print("Result \(String(describing: result.attributedFullText.string)) with placeID \(String(describing: result.placeID))")
                            names.append(result.attributedFullText.string)
                            ids.append(result.placeID)
                        }
                        self.placeIDs = ids
                        self.dropDownArea.dataSource = names
                        self.dropDownArea.show()
                    }
                })
            }
        } else if textField == self.txtMinimum && !(textField.text?.isEmpty)! {
//            let selectedLang = UserDefaults.standard.object(forKey: "AppleLanguages") as? [String] ?? [String]()
            
            if isLanguageEnglish() {
                let textInput = Double.init(textField.text!)
                let minimum = CGFloat(min(Int(textInput!), Int(self.seekBarSlider.selectedMaxValue)))
//                if CGFloat(Int(textInput!)) > self.seekBarSlider.selectedMaxValue {
//                    minimum = CGFloat(Int(textInput!))
//                }
                self.txtMinimum.text = "\(Int(minimum))"
                self.seekBarSlider.selectedMinValue = minimum
                self.seekBarSlider.delegate?.rangeSeekSlider(self.seekBarSlider, didChange: minimum, maxValue: self.seekBarSlider.selectedMaxValue)
                self.seekBarSlider.layoutSubviews()
            }
            else {
                let text = textField.text!.englishNumbers()
                let textInput = Double.init(text!)
                let minimum = CGFloat(min(Int(textInput!), Int(self.seekBarSlider.selectedMaxValue)))
                self.txtMinimum.text = "\(Int(minimum))"
                self.seekBarSlider.selectedMinValue = minimum
                self.seekBarSlider.delegate?.rangeSeekSlider(self.seekBarSlider, didChange: minimum, maxValue: self.seekBarSlider.selectedMaxValue)
                self.seekBarSlider.layoutSubviews()
            }
            
        } else if textField == self.txtMaximum && !(textField.text?.isEmpty)! {
//            let selectedLang = UserDefaults.standard.object(forKey: "AppleLanguages") as? [String] ?? [String]()
            
            if isLanguageEnglish() {
                let textInput = Double.init(textField.text!)
                if CGFloat(textInput!) < self.maximumPrice! {
                    var maximum = CGFloat(max(Int(textInput!), Int(self.seekBarSlider.selectedMinValue)))
                    if CGFloat(Int(textInput!)) < self.seekBarSlider.selectedMinValue {
                        maximum = CGFloat(Int(textInput!))
                    }
                    self.txtMaximum.text = "\(Int(maximum))"
                    self.seekBarSlider.selectedMaxValue = maximum
                    self.seekBarSlider.delegate?.rangeSeekSlider(self.seekBarSlider, didChange: self.seekBarSlider.selectedMinValue, maxValue: maximum)
                    self.seekBarSlider.layoutSubviews()
                } else {
                    self.txtMaximum.text = "\(Int(self.maximumPrice!))"
                    self.seekBarSlider.selectedMaxValue = self.maximumPrice!
                    self.seekBarSlider.delegate?.rangeSeekSlider(self.seekBarSlider, didChange: self.seekBarSlider.selectedMinValue, maxValue: self.maximumPrice!)
                    self.seekBarSlider.layoutSubviews()
                }
            }
            else {
                let text = textField.text!.englishNumbers()
                let textInput = Double.init(text!)
                if CGFloat(textInput!) < self.maximumPrice! {
                    var maximum = CGFloat(max(Int(textInput!), Int(self.seekBarSlider.selectedMinValue)))
                    if CGFloat(Int(textInput!)) < self.seekBarSlider.selectedMinValue {
                        maximum = CGFloat(Int(textInput!))
                    }
                    self.txtMaximum.text = "\(Int(maximum))"
                    self.seekBarSlider.selectedMaxValue = maximum
                    self.seekBarSlider.delegate?.rangeSeekSlider(self.seekBarSlider, didChange: self.seekBarSlider.selectedMinValue, maxValue: maximum)
                    self.seekBarSlider.layoutSubviews()
                } else {
                    self.txtMaximum.text = "\(Int(self.maximumPrice!))"
                    self.seekBarSlider.selectedMaxValue = self.maximumPrice!
                    self.seekBarSlider.delegate?.rangeSeekSlider(self.seekBarSlider, didChange: self.seekBarSlider.selectedMinValue, maxValue: self.maximumPrice!)
                    self.seekBarSlider.layoutSubviews()
                }
            }
            
        }
    }
    
}

extension FilterHeaderCell: RangeSeekSliderDelegate {
    
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat){
        
        print(minValue)
        print(maxValue)
        lblMinPrice.text = "QAR " + String(describing: ceil(minValue))
        lblMaxPrice.text = "QAR " + String(describing: ceil(maxValue))
        self.minPrice = String(describing: ceil(minValue))
        self.maxPrice = String(describing: ceil(maxValue))
        
        self.txtMinimum.text = String(describing: Int(ceil(minValue)))
        self.txtMaximum.text = String(describing: Int(ceil(maxValue)))
        
        
        paramHelper.params.updateValue(self.maxPrice!, forKey: "price")
        paramHelper.params.updateValue(self.minPrice!, forKey: "price_min")
    }
}

extension FilterHeaderCell : UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == self.txtTitle {
            paramHelper.params.updateValue(txtTitle.text!, forKey: "title")
        }
    }
}

