//
//  AdsViewController.swift
//  AdHouse
//
//  Created by maazulhaq on 26/07/2017.
//  Copyright © 2017 . All rights reserved.
//

import UIKit
import AlamofireImage

class AdsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, RefreshDelegate {

    class func instantiateFromStoryboard() -> AdsViewController {
        let storyboard = UIStoryboard(name: "Ads", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! AdsViewController
    }
    
    @IBOutlet weak var tblView: UITableView!
    
    var myAdsArray:[AdsResult]? = [AdsResult]()
    var isDeleting = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Register Nib Cells
        
        let nib = UINib.init(nibName: "MyAdsTableViewCell", bundle: nil)
        tblView.register(nib, forCellReuseIdentifier: "MyAdsTableViewCell")
        
         self.getMyAds()
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.isHidden = true
    }
    
    //MARK:- Menu Action
    
    @IBAction func MenuAction(sender: UIBarButtonItem) {
        self.slideMenuController()?.toggleLeft()
    }
    
    func getMyAds() {
        
        Alert.showLoader(message: "")
        
        let parameters:[String:Any] = ["user_id": (Singleton.sharedInstance.CurrentUser?.id)!]
        
        HomeServices.MyAds(param: parameters , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    print("Error: \((error as! Error).localizedDescription)")
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                return
            }
            
            print(response!)
            print("SUCCESS")
            
            Alert.hideLoader()
            self.myAdsArray?.removeAll()
            
            let adsResponseArray = response?["Result"].arrayValue
            print(adsResponseArray!)

            for resultObj in adsResponseArray! {
                let obj =  AdsResult(json: resultObj)
                self.myAdsArray?.append(obj)
            }
            
            self.tblView.reloadData()
            
        })
     
    }
    
    //MARK:-  Edit Button Action
    
    @objc func EditAdAction(sender: UIButton) {
        
        //Utility.showAlert("Alert", message: "This feature will be implemented in next module")
        //return
        
        var indexPath: IndexPath!
            if let superview = sender.superview?.superview {
                if let cell = superview.superview as? MyAdsTableViewCell {
                    indexPath = tblView.indexPath(for: cell)
                    print(indexPath)
                    print("index path: \(indexPath)")
                    
                    if indexPath != nil {
                        
                        let editVc = EditViewController.instantiateFromStoryboard()
                        editVc.ad = self.myAdsArray?[indexPath.row]
                        editVc.delegate = self
                        self.navigationController?.pushViewController(editVc, animated: true)
                    }
                }
                
            }
 
        }

    
    //MARK:-  TableView Delegate
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return GISTUtility.convertToRatio(120)
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return self.myAdsArray!.count
        
    }
    
   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let adsObj : AdsResult = self.myAdsArray![indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyAdsTableViewCell", for: indexPath) as! MyAdsTableViewCell
        
        cell.imgProduct.setImageFromUrl(urlStr: (adsObj.adImages?.first?.imageUrl!)!)
        cell.lblProductName.text = adsObj.title
        cell.lblPrice.text = String(describing: adsObj.price!.decimal) + " QAR"
        
        if adsObj.isVehicle == 1 {
            
            cell.lblLocation.text = "Km " + String(describing: adsObj.kilometer!.decimal)
            
        } else {
            
            cell.lblLocation.text = adsObj.area
            
        }
        cell.btnDelete.addTarget(self, action: #selector(self.DeleteAdAction(sender:)), for: .touchUpInside)
        
        cell.btnEdit.addTarget(self, action: #selector(self.EditAdAction(sender:)), for: .touchUpInside)

        if adsObj.featured == 0 {
            
            cell.imgFeatured.isHidden = true
            cell.btnFeatured.isHidden = true
            
        } else {
            
            cell.imgFeatured.isHidden = false
            cell.btnFeatured.isHidden = false
        }
        
        return cell
     
    }
    
    //MARK:- Delete Ad
    
    @objc func DeleteAdAction(sender: UIButton) {
        //Create the AlertController
        let alert: UIAlertController = UIAlertController(title: "Alert", message: "Are you sure you want to delete this Ad?", preferredStyle: .alert)
        
        //Create and add the Cancel action
        let cancelAction: UIAlertAction = UIAlertAction(title: "No", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        
        //Create and an option action
        let okAction: UIAlertAction = UIAlertAction(title: "Yes", style: .default) { action -> Void in
            //Do some other stuff
            if !self.isDeleting {
                self.isDeleting = !self.isDeleting
                
                var indexPath: IndexPath!
                if let superview = sender.superview?.superview {
                    if let cell = superview.superview as? MyAdsTableViewCell {
                        indexPath = self.tblView.indexPath(for: cell)
                        print(indexPath)
                        print("index path: \(indexPath)")
                        
                        if indexPath != nil {
                            
                            let parameters:[String:Any] = [
                                "id":self.myAdsArray![indexPath.row].id!
                            ]
                            
                            print("PARAMETERS: \(parameters)")
                            Alert.showLoader(message: "")
                            
                            AdServices.DeleteAd(param: parameters , completionHandler: {(status, response, error) in
                                self.isDeleting = !self.isDeleting
                                if !status {
                                    if error != nil {
                                        print("Error: \((error as! Error).localizedDescription)")
                                        return
                                    }
                                    let msg = response?["Message"].stringValue
                                    print("Message: \(String(describing: msg))")
                                    return
                                }
                                
                                print(response!)
                                print("SUCCESS")
                                self.myAdsArray?.remove(at: indexPath.row)
                                self.tblView.deleteRows(at: [indexPath], with: .left)
                                
                            })
                        }
                    }
                    
                }
            }
        }
        alert.addAction(okAction)
        
        //Present the AlertController
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func didRefresh() {
        
        self.getMyAds()
    }
    
    func didRefresh(count: CGFloat) {
        
    }
}
