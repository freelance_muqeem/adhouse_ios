//
//  DetailInfoTableViewCell.swift
//  AdHouse
//
//  Created by maazulhaq on 26/07/2017.
//  Copyright © 2017 . All rights reserved.
//

import UIKit

class DetailInfoTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var txtView:UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setFonts()
    }


    func setFonts() {
        
        lblTitle.font = FontUtility.getFontWithAdjustedSize(size: 16, desireFontType: .Roboto_Bold)
            
    }
    
    
}
