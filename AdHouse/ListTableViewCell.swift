//
//  ListTableViewCell.swift
//  AdHouse
//
//  Created by maazulhaq on 26/07/2017.
//  Copyright © 2017 . All rights reserved.
//

import UIKit

class ListTableViewCell: UITableViewCell {

    @IBOutlet weak var lblKey: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var keyView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setFonts()
        
        self.titleView.layer.borderWidth = 0.5
        self.titleView.layer.borderColor = UIColor.lightGray.cgColor
        self.titleView.addShadow()
        
        
        self.keyView.layer.borderWidth = 0.5
        self.keyView.layer.borderColor = UIColor.lightGray.cgColor
        self.keyView.addShadow()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func setFonts() {
        
        lblKey.font = FontUtility.getFontWithAdjustedSize(size: 15, desireFontType: .Roboto_Bold)
        lblTitle.font = FontUtility.getFontWithAdjustedSize(size: 15, desireFontType: .Roboto_Regular)
        
    }
    
}
