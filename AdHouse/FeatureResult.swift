//
//  Result.swift
//
//  Created by  on 8/12/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class FeatureResult: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let userDetails = "user_details"
    static let updatedAt = "updated_at"
    static let adImages = "ad_images"
    static let adDetails = "ad_details"
    static let isLike = "is_like"
    static let descriptionValue = "description"
    static let featured = "featured"
    static let categoryId = "category_id"
    static let price = "price"
    static let subCategoryId = "sub_category_id"
    static let id = "id"
    static let createdAt = "created_at"
    static let title = "title"
    static let userId = "user_id"
  }

  // MARK: Properties
  public var userDetails: UserDetails?
  public var updatedAt: String?
  public var adImages: [AdImages]?
  public var adDetails: [Any]?
  public var isLike: Int?
  public var descriptionValue: String?
  public var featured: Int?
  public var categoryId: Int?
  public var price: Int?
  public var subCategoryId: Int?
  public var id: Int?
  public var createdAt: String?
  public var title: String?
  public var userId: Int?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
    public convenience init(object: JSON) {
        self.init(json: object)
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
    userDetails = UserDetails(json: json[SerializationKeys.userDetails])
    updatedAt = json[SerializationKeys.updatedAt].string
    if let items = json[SerializationKeys.adImages].array { adImages = items.map { AdImages(json: $0) } }
    if let items = json[SerializationKeys.adDetails].array { adDetails = items.map { $0.object} }
    isLike = json[SerializationKeys.isLike].int
    descriptionValue = json[SerializationKeys.descriptionValue].string
    featured = json[SerializationKeys.featured].int
    categoryId = json[SerializationKeys.categoryId].int
    price = json[SerializationKeys.price].int
    subCategoryId = json[SerializationKeys.subCategoryId].int
    id = json[SerializationKeys.id].int
    createdAt = json[SerializationKeys.createdAt].string
    title = json[SerializationKeys.title].string
    userId = json[SerializationKeys.userId].int
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = userDetails { dictionary[SerializationKeys.userDetails] = value.dictionaryRepresentation() }
    if let value = updatedAt { dictionary[SerializationKeys.updatedAt] = value }
    if let value = adImages { dictionary[SerializationKeys.adImages] = value.map { $0.dictionaryRepresentation() } }
    if let value = adDetails { dictionary[SerializationKeys.adDetails] = value }
    if let value = isLike { dictionary[SerializationKeys.isLike] = value }
    if let value = descriptionValue { dictionary[SerializationKeys.descriptionValue] = value }
    if let value = featured { dictionary[SerializationKeys.featured] = value }
    if let value = categoryId { dictionary[SerializationKeys.categoryId] = value }
    if let value = price { dictionary[SerializationKeys.price] = value }
    if let value = subCategoryId { dictionary[SerializationKeys.subCategoryId] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
    if let value = title { dictionary[SerializationKeys.title] = value }
    if let value = userId { dictionary[SerializationKeys.userId] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.userDetails = aDecoder.decodeObject(forKey: SerializationKeys.userDetails) as? UserDetails
    self.updatedAt = aDecoder.decodeObject(forKey: SerializationKeys.updatedAt) as? String
    self.adImages = aDecoder.decodeObject(forKey: SerializationKeys.adImages) as? [AdImages]
    self.adDetails = aDecoder.decodeObject(forKey: SerializationKeys.adDetails) as? [Any]
    self.isLike = aDecoder.decodeObject(forKey: SerializationKeys.isLike) as? Int
    self.descriptionValue = aDecoder.decodeObject(forKey: SerializationKeys.descriptionValue) as? String
    self.featured = aDecoder.decodeObject(forKey: SerializationKeys.featured) as? Int
    self.categoryId = aDecoder.decodeObject(forKey: SerializationKeys.categoryId) as? Int
    self.price = aDecoder.decodeObject(forKey: SerializationKeys.price) as? Int
    self.subCategoryId = aDecoder.decodeObject(forKey: SerializationKeys.subCategoryId) as? Int
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
    self.createdAt = aDecoder.decodeObject(forKey: SerializationKeys.createdAt) as? String
    self.title = aDecoder.decodeObject(forKey: SerializationKeys.title) as? String
    self.userId = aDecoder.decodeObject(forKey: SerializationKeys.userId) as? Int
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(userDetails, forKey: SerializationKeys.userDetails)
    aCoder.encode(updatedAt, forKey: SerializationKeys.updatedAt)
    aCoder.encode(adImages, forKey: SerializationKeys.adImages)
    aCoder.encode(adDetails, forKey: SerializationKeys.adDetails)
    aCoder.encode(isLike, forKey: SerializationKeys.isLike)
    aCoder.encode(descriptionValue, forKey: SerializationKeys.descriptionValue)
    aCoder.encode(featured, forKey: SerializationKeys.featured)
    aCoder.encode(categoryId, forKey: SerializationKeys.categoryId)
    aCoder.encode(price, forKey: SerializationKeys.price)
    aCoder.encode(subCategoryId, forKey: SerializationKeys.subCategoryId)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(createdAt, forKey: SerializationKeys.createdAt)
    aCoder.encode(title, forKey: SerializationKeys.title)
    aCoder.encode(userId, forKey: SerializationKeys.userId)
  }

}
