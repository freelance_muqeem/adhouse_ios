//
//  SubscriptionSelectCollectionViewCell.swift
//  AdHouse
//
//  Created by maazulhaq on 27/07/2017.
//  Copyright © 2017 . All rights reserved.
//

import UIKit

class SubscriptionSelectCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var backView: UIView!
    
    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet weak var lblMonths: UILabel!
    @IBOutlet weak var lblCode: UILabel!
    
    @IBOutlet weak var freeAds: UILabel!
    @IBOutlet weak var adValidity: UILabel!
    @IBOutlet weak var maxAds: UILabel!
    
    @IBOutlet weak var lblFreeAds: UILabel!
    @IBOutlet weak var lblAdValidity: UILabel!
    @IBOutlet weak var lblMaxAds: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        backView.layer.borderColor = UIColor(rgb: 0x176AC1).cgColor
        backView.layer.borderWidth = 3.0
        
        self.setFonts()
    }
    
    
    func setFonts() {
        
        lblCount.font = FontUtility.getFontWithAdjustedSize(size: 22, desireFontType: .Roboto_Bold)
        lblMonths.font = FontUtility.getFontWithAdjustedSize(size: 15, desireFontType: .Roboto_Bold)
        lblCode.font = FontUtility.getFontWithAdjustedSize(size: 12, desireFontType: .Roboto_Bold)
        freeAds.font = FontUtility.getFontWithAdjustedSize(size: 9, desireFontType: .Roboto_Bold)
        adValidity.font = FontUtility.getFontWithAdjustedSize(size: 9, desireFontType: .Roboto_Bold)
        maxAds.font = FontUtility.getFontWithAdjustedSize(size: 9, desireFontType: .Roboto_Bold)
        lblFreeAds.font = FontUtility.getFontWithAdjustedSize(size: 9, desireFontType: .Roboto_Regular)
        lblAdValidity.font = FontUtility.getFontWithAdjustedSize(size: 9, desireFontType: .Roboto_Regular)
        lblMaxAds.font = FontUtility.getFontWithAdjustedSize(size: 9, desireFontType: .Roboto_Regular)
        
    }
}
