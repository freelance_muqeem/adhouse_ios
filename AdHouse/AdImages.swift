//
//  AdImages.swift
//
//  Created by  on 8/9/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class AdImages: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let updatedAt = "updated_at"
    static let id = "id"
    static let adId = "ad_id"
    static let createdAt = "created_at"
    static let imageUrl = "image_url"
    static let images = "images"
    static let userId = "user_id"
  }

  // MARK: Properties
  public var updatedAt: String?
  public var id: Int?
  public var adId: Int?
  public var createdAt: String?
  public var imageUrl: String?
  public var images: String?
  public var userId: Int?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
    public convenience init(object: JSON) {
        self.init(json: object)
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
    updatedAt = json[SerializationKeys.updatedAt].string
    id = json[SerializationKeys.id].int
    adId = json[SerializationKeys.adId].int
    createdAt = json[SerializationKeys.createdAt].string
    imageUrl = json[SerializationKeys.imageUrl].string
    images = json[SerializationKeys.images].string
    userId = json[SerializationKeys.userId].int
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = updatedAt { dictionary[SerializationKeys.updatedAt] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = adId { dictionary[SerializationKeys.adId] = value }
    if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
    if let value = imageUrl { dictionary[SerializationKeys.imageUrl] = value }
    if let value = images { dictionary[SerializationKeys.images] = value }
    if let value = userId { dictionary[SerializationKeys.userId] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.updatedAt = aDecoder.decodeObject(forKey: SerializationKeys.updatedAt) as? String
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
    self.adId = aDecoder.decodeObject(forKey: SerializationKeys.adId) as? Int
    self.createdAt = aDecoder.decodeObject(forKey: SerializationKeys.createdAt) as? String
    self.imageUrl = aDecoder.decodeObject(forKey: SerializationKeys.imageUrl) as? String
    self.images = aDecoder.decodeObject(forKey: SerializationKeys.images) as? String
    self.userId = aDecoder.decodeObject(forKey: SerializationKeys.userId) as? Int
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(updatedAt, forKey: SerializationKeys.updatedAt)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(adId, forKey: SerializationKeys.adId)
    aCoder.encode(createdAt, forKey: SerializationKeys.createdAt)
    aCoder.encode(imageUrl, forKey: SerializationKeys.imageUrl)
    aCoder.encode(images, forKey: SerializationKeys.images)
    aCoder.encode(userId, forKey: SerializationKeys.userId)
  }

}
