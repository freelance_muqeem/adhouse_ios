//
//  FavouriteSerices.swift
//  AdHouse
//
//  Created by maazulhaq on 10/08/2017.
//  Copyright © 2017 . All rights reserved.
//

import Foundation
import SwiftyJSON

class FavouriteServices {
    
    //MARK:-  Get favorite Service Method
    
    static func Get(param:[String:Any], completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Any?)->Void) {
        
        Request.GetRequest(fromSavedUrl: ServiceApiEndPoints.favorite, parameters: param, callback: { (response, error) in
            
            Alert.hideLoader()
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                
                Alert.showAlert(title: "Alert", message: String(describing: (error?.localizedDescription)!))
                return
            }
            
            if response?["Response"].stringValue != "2000" {
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                Alert.showAlert(title: "Alert", message: (response?["Message"].stringValue)!)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    //MARK:-  Add Favourite Service Method
    
    static func Add(param:[String:Any], completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Any?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.addFavorite, parameters: param, callback: { (response, error) in
            
            Alert.hideLoader()
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                
                Alert.showAlert(title: "Alert", message: String(describing: (error?.localizedDescription)!))
                return
            }
            
            if response?["Response"].stringValue != "2000" {
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                Alert.showAlert(title: "Alert", message: (response?["Message"].stringValue)!)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    //MARK:-  Get favorite Service Method
    
    static func GetAd(param:[String:Any], completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Any?)->Void) {
        
        Request.GetRequest(fromSavedUrl: ServiceApiEndPoints.ad, parameters: param, callback: { (response, error) in
            
            Alert.hideLoader()
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                
                Alert.showAlert(title: "Alert", message: String(describing: (error?.localizedDescription)!))
                return
            }
            
            if response?["Response"].stringValue != "2000" {
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                Alert.showAlert(title: "Alert", message: (response?["Message"].stringValue)!)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
}
