////
////  testViewController.swift
////  AdHouse
////
////  Created by   on 1/13/18.
////  Copyright © 2018 . All rights reserved.
////
//
//import UIKit
//import Braintree
////import BraintreeDropIn
//
//class testViewController: UIViewController, BTViewControllerPresentingDelegate, BTAppSwitchDelegate {
//
//    class func instantiateFromStoryboard() -> testViewController {
//        let storyboard = UIStoryboard(name: "Subscription", bundle: nil)
//        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! testViewController
//    }
//    
//    var braintreeClient: BTAPIClient!
//    
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        // Do any additional setup after loading the view.
//        
//            self.braintreeClient = BTAPIClient(authorization: "eyJ2ZXJzaW9uIjoyLCJhdXRob3JpemF0aW9uRmluZ2VycHJpbnQiOiIwNzIyOTc3YmI3ZTE0NzAzMDc5ZjM4OWMyNTcxMDMxNGFkY2IzMzRkNjU4OTg0N2NjODZjNjg5Y2U1ZmIzODk1fGNsaWVudF9pZD1jbGllbnRfaWQkc2FuZGJveCQ0ZHByYmZjNnBoNTk1Y2NqXHUwMDI2Y3JlYXRlZF9hdD0yMDE4LTAxLTEzVDE2OjE2OjQyLjg1MDkwNjI2MyswMDAwXHUwMDI2bWVyY2hhbnRfaWQ9eGp3bnc4cmRiM3N4YzNzNiIsImNvbmZpZ1VybCI6Imh0dHBzOi8vYXBpLnNhbmRib3guYnJhaW50cmVlZ2F0ZXdheS5jb206NDQzL21lcmNoYW50cy94andudzhyZGIzc3hjM3M2L2NsaWVudF9hcGkvdjEvY29uZmlndXJhdGlvbiIsImNoYWxsZW5nZXMiOltdLCJlbnZpcm9ubWVudCI6InNhbmRib3giLCJjbGllbnRBcGlVcmwiOiJodHRwczovL2FwaS5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tOjQ0My9tZXJjaGFudHMveGp3bnc4cmRiM3N4YzNzNi9jbGllbnRfYXBpIiwiYXNzZXRzVXJsIjoiaHR0cHM6Ly9hc3NldHMuYnJhaW50cmVlZ2F0ZXdheS5jb20iLCJhdXRoVXJsIjoiaHR0cHM6Ly9hdXRoLnZlbm1vLnNhbmRib3guYnJhaW50cmVlZ2F0ZXdheS5jb20iLCJhbmFseXRpY3MiOnsidXJsIjoiaHR0cHM6Ly9jbGllbnQtYW5hbHl0aWNzLnNhbmRib3guYnJhaW50cmVlZ2F0ZXdheS5jb20veGp3bnc4cmRiM3N4YzNzNiJ9LCJ0aHJlZURTZWN1cmVFbmFibGVkIjpmYWxzZSwicGF5cGFsRW5hYmxlZCI6dHJ1ZSwicGF5cGFsIjp7ImRpc3BsYXlOYW1lIjoiSm9obiBQYWdlJ3MgVGVzdCBTdG9yZSIsImNsaWVudElkIjoiQVVxZkhfZU5nMzVzQnc4MnZMelpiSEtxdFJLRFlUWEVlSjVGSlJ2U1g0bnNNYXctVXR1U2Q1QkJVWWFwM2dPU2tyWnlXS09BV2RUNUY4OTkiLCJwcml2YWN5VXJsIjoiaHR0cHM6Ly9leGFtcGxlLmNvbSIsInVzZXJBZ3JlZW1lbnRVcmwiOiJodHRwczovL2V4YW1wbGUuY29tIiwiYmFzZVVybCI6Imh0dHBzOi8vYXNzZXRzLmJyYWludHJlZWdhdGV3YXkuY29tIiwiYXNzZXRzVXJsIjoiaHR0cHM6Ly9jaGVja291dC5wYXlwYWwuY29tIiwiZGlyZWN0QmFzZVVybCI6bnVsbCwiYWxsb3dIdHRwIjp0cnVlLCJlbnZpcm9ubWVudE5vTmV0d29yayI6ZmFsc2UsImVudmlyb25tZW50Ijoib2ZmbGluZSIsInVudmV0dGVkTWVyY2hhbnQiOmZhbHNlLCJicmFpbnRyZWVDbGllbnRJZCI6Im1hc3RlcmNsaWVudDMiLCJiaWxsaW5nQWdyZWVtZW50c0VuYWJsZWQiOnRydWUsIm1lcmNoYW50QWNjb3VudElkIjoiVVNEIiwiY3VycmVuY3lJc29Db2RlIjoiVVNEIn0sIm1lcmNoYW50SWQiOiJ4andudzhyZGIzc3hjM3M2IiwidmVubW8iOiJvZmYifQ==")
//        
////        let customPayPalButton = UIButton(frame: CGRect(x: 0, y: 0, width: 60, height: 120))
////        customPayPalButton.addTarget(self, action: #selector(customPayPalButtonTapped(button:)), for: UIControlEvents.touchUpInside)
////        self.view.addSubview(customPayPalButton)
//        
//        
//        // For client authorization,
//        // get your tokenization key from the Control Panel
//        // or fetch a client token
////        let braintreeClient = BTAPIClient(authorization: "eyJ2ZXJzaW9uIjoyLCJhdXRob3JpemF0aW9uRmluZ2VycHJpbnQiOiIwNzIyOTc3YmI3ZTE0NzAzMDc5ZjM4OWMyNTcxMDMxNGFkY2IzMzRkNjU4OTg0N2NjODZjNjg5Y2U1ZmIzODk1fGNsaWVudF9pZD1jbGllbnRfaWQkc2FuZGJveCQ0ZHByYmZjNnBoNTk1Y2NqXHUwMDI2Y3JlYXRlZF9hdD0yMDE4LTAxLTEzVDE2OjE2OjQyLjg1MDkwNjI2MyswMDAwXHUwMDI2bWVyY2hhbnRfaWQ9eGp3bnc4cmRiM3N4YzNzNiIsImNvbmZpZ1VybCI6Imh0dHBzOi8vYXBpLnNhbmRib3guYnJhaW50cmVlZ2F0ZXdheS5jb206NDQzL21lcmNoYW50cy94andudzhyZGIzc3hjM3M2L2NsaWVudF9hcGkvdjEvY29uZmlndXJhdGlvbiIsImNoYWxsZW5nZXMiOltdLCJlbnZpcm9ubWVudCI6InNhbmRib3giLCJjbGllbnRBcGlVcmwiOiJodHRwczovL2FwaS5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tOjQ0My9tZXJjaGFudHMveGp3bnc4cmRiM3N4YzNzNi9jbGllbnRfYXBpIiwiYXNzZXRzVXJsIjoiaHR0cHM6Ly9hc3NldHMuYnJhaW50cmVlZ2F0ZXdheS5jb20iLCJhdXRoVXJsIjoiaHR0cHM6Ly9hdXRoLnZlbm1vLnNhbmRib3guYnJhaW50cmVlZ2F0ZXdheS5jb20iLCJhbmFseXRpY3MiOnsidXJsIjoiaHR0cHM6Ly9jbGllbnQtYW5hbHl0aWNzLnNhbmRib3guYnJhaW50cmVlZ2F0ZXdheS5jb20veGp3bnc4cmRiM3N4YzNzNiJ9LCJ0aHJlZURTZWN1cmVFbmFibGVkIjpmYWxzZSwicGF5cGFsRW5hYmxlZCI6dHJ1ZSwicGF5cGFsIjp7ImRpc3BsYXlOYW1lIjoiSm9obiBQYWdlJ3MgVGVzdCBTdG9yZSIsImNsaWVudElkIjoiQVVxZkhfZU5nMzVzQnc4MnZMelpiSEtxdFJLRFlUWEVlSjVGSlJ2U1g0bnNNYXctVXR1U2Q1QkJVWWFwM2dPU2tyWnlXS09BV2RUNUY4OTkiLCJwcml2YWN5VXJsIjoiaHR0cHM6Ly9leGFtcGxlLmNvbSIsInVzZXJBZ3JlZW1lbnRVcmwiOiJodHRwczovL2V4YW1wbGUuY29tIiwiYmFzZVVybCI6Imh0dHBzOi8vYXNzZXRzLmJyYWludHJlZWdhdGV3YXkuY29tIiwiYXNzZXRzVXJsIjoiaHR0cHM6Ly9jaGVja291dC5wYXlwYWwuY29tIiwiZGlyZWN0QmFzZVVybCI6bnVsbCwiYWxsb3dIdHRwIjp0cnVlLCJlbnZpcm9ubWVudE5vTmV0d29yayI6ZmFsc2UsImVudmlyb25tZW50Ijoib2ZmbGluZSIsInVudmV0dGVkTWVyY2hhbnQiOmZhbHNlLCJicmFpbnRyZWVDbGllbnRJZCI6Im1hc3RlcmNsaWVudDMiLCJiaWxsaW5nQWdyZWVtZW50c0VuYWJsZWQiOnRydWUsIm1lcmNoYW50QWNjb3VudElkIjoiVVNEIiwiY3VycmVuY3lJc29Db2RlIjoiVVNEIn0sIm1lcmNoYW50SWQiOiJ4andudzhyZGIzc3hjM3M2IiwidmVubW8iOiJvZmYifQ==")!
////        let cardClient = BTCardClient(apiClient: braintreeClient)
////        let card = BTCard(number: "4111111111111111", expirationMonth: "12", expirationYear: "2018", cvv: nil)
////        cardClient.tokenizeCard(card) { (tokenizedCard, error) in
////            // Communicate the tokenizedCard.nonce to your server, or handle error
////            if error != nil {
////                print(tokenizedCard?.lastTwo)
////            }
////        }
//    }
//
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }
//    
//
//    /*
//    // MARK: - Navigation
//
//    // In a storyboard-based application, you will often want to do a little preparation before navigation
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        // Get the new view controller using segue.destinationViewController.
//        // Pass the selected object to the new view controller.
//    }
//    */
//    
//    
//    @IBAction func customPayPalButtonTapped(button: UIButton) {
//        
////        let payPalDriver = BTPayPalDriver(apiClient: self.braintreeClient)
////        payPalDriver.viewControllerPresentingDelegate = self
////        payPalDriver.appSwitchDelegate = self
////        
////        // Start the Vault flow, or...
////        payPalDriver.authorizeAccount() { (tokenizedPayPalAccount, error) -> Void in
//////            ...
////        }
////        
////        // ...start the Checkout flow
////        let payPalRequest = BTPayPalRequest(amount: "1.00")
////        payPalDriver.requestOneTimePayment(payPalRequest) { (tokenizedPayPalAccount, error) -> Void in
//////            ...
////        }
//        
//        
////        var dropInRequest = BTDropInRequest()
////        // To test 3DS
////        dropInRequest.amount = "10.00";
////        dropInRequest.threeDSecureVerification = true;
////        if dropinThemeSwitch.selectedSegmentIndex == 0 {
////            BTUIKAppearance.lightTheme()
////        }
////        else {
////            BTUIKAppearance.darkTheme()
////        }
//        
////        let dropIn = BTDropInController(authorization: "eyJ2ZXJzaW9uIjoyLCJhdXRob3JpemF0aW9uRmluZ2VycHJpbnQiOiIwNzIyOTc3YmI3ZTE0NzAzMDc5ZjM4OWMyNTcxMDMxNGFkY2IzMzRkNjU4OTg0N2NjODZjNjg5Y2U1ZmIzODk1fGNsaWVudF9pZD1jbGllbnRfaWQkc2FuZGJveCQ0ZHByYmZjNnBoNTk1Y2NqXHUwMDI2Y3JlYXRlZF9hdD0yMDE4LTAxLTEzVDE2OjE2OjQyLjg1MDkwNjI2MyswMDAwXHUwMDI2bWVyY2hhbnRfaWQ9eGp3bnc4cmRiM3N4YzNzNiIsImNvbmZpZ1VybCI6Imh0dHBzOi8vYXBpLnNhbmRib3guYnJhaW50cmVlZ2F0ZXdheS5jb206NDQzL21lcmNoYW50cy94andudzhyZGIzc3hjM3M2L2NsaWVudF9hcGkvdjEvY29uZmlndXJhdGlvbiIsImNoYWxsZW5nZXMiOltdLCJlbnZpcm9ubWVudCI6InNhbmRib3giLCJjbGllbnRBcGlVcmwiOiJodHRwczovL2FwaS5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tOjQ0My9tZXJjaGFudHMveGp3bnc4cmRiM3N4YzNzNi9jbGllbnRfYXBpIiwiYXNzZXRzVXJsIjoiaHR0cHM6Ly9hc3NldHMuYnJhaW50cmVlZ2F0ZXdheS5jb20iLCJhdXRoVXJsIjoiaHR0cHM6Ly9hdXRoLnZlbm1vLnNhbmRib3guYnJhaW50cmVlZ2F0ZXdheS5jb20iLCJhbmFseXRpY3MiOnsidXJsIjoiaHR0cHM6Ly9jbGllbnQtYW5hbHl0aWNzLnNhbmRib3guYnJhaW50cmVlZ2F0ZXdheS5jb20veGp3bnc4cmRiM3N4YzNzNiJ9LCJ0aHJlZURTZWN1cmVFbmFibGVkIjpmYWxzZSwicGF5cGFsRW5hYmxlZCI6dHJ1ZSwicGF5cGFsIjp7ImRpc3BsYXlOYW1lIjoiSm9obiBQYWdlJ3MgVGVzdCBTdG9yZSIsImNsaWVudElkIjoiQVVxZkhfZU5nMzVzQnc4MnZMelpiSEtxdFJLRFlUWEVlSjVGSlJ2U1g0bnNNYXctVXR1U2Q1QkJVWWFwM2dPU2tyWnlXS09BV2RUNUY4OTkiLCJwcml2YWN5VXJsIjoiaHR0cHM6Ly9leGFtcGxlLmNvbSIsInVzZXJBZ3JlZW1lbnRVcmwiOiJodHRwczovL2V4YW1wbGUuY29tIiwiYmFzZVVybCI6Imh0dHBzOi8vYXNzZXRzLmJyYWludHJlZWdhdGV3YXkuY29tIiwiYXNzZXRzVXJsIjoiaHR0cHM6Ly9jaGVja291dC5wYXlwYWwuY29tIiwiZGlyZWN0QmFzZVVybCI6bnVsbCwiYWxsb3dIdHRwIjp0cnVlLCJlbnZpcm9ubWVudE5vTmV0d29yayI6ZmFsc2UsImVudmlyb25tZW50Ijoib2ZmbGluZSIsInVudmV0dGVkTWVyY2hhbnQiOmZhbHNlLCJicmFpbnRyZWVDbGllbnRJZCI6Im1hc3RlcmNsaWVudDMiLCJiaWxsaW5nQWdyZWVtZW50c0VuYWJsZWQiOnRydWUsIm1lcmNoYW50QWNjb3VudElkIjoiVVNEIiwiY3VycmVuY3lJc29Db2RlIjoiVVNEIn0sIm1lcmNoYW50SWQiOiJ4andudzhyZGIzc3hjM3M2IiwidmVubW8iOiJvZmYifQ==", request: dropInRequest)
////        { (controller, result, error) in
////            if (error != nil) {
////                print("ERROR")
////                Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: error!.localizedDescription)
////            } else if (result?.isCancelled == true) {
////                print("CANCELLED")
////            } else if let result = result {
////                if let nonce = result.paymentMethod?.nonce {
////                    print(nonce)
////                    
////                }
////                else
////                {
////                    Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Some kind of error occures! Please try later", comment: ""))
////                }
////            }
////            controller.dismiss(animated: true, completion: nil)
////        }
////        
////        self.present(dropIn!, animated: true, completion: nil)
//        
//        
//        
//        
//        
//        let paymentRequest = PKPaymentRequest()
//        paymentRequest.paymentSummaryItems = [PKPaymentSummaryItem(label: "Socks", amount: NSDecimalNumber(string: "100"))]
//        paymentRequest.supportedNetworks = [.visa, .masterCard, .amex, .discover]
//        paymentRequest.merchantCapabilities = .capability3DS
//        paymentRequest.currencyCode = "USD"
//        paymentRequest.countryCode = "US"
//        paymentRequest.merchantIdentifier = "merchant.com.braintreepayments.sandbox.adhouse"
//        
//        let viewController = PKPaymentAuthorizationViewController(paymentRequest: paymentRequest)
//        viewController.delegate = self
//        print("Presenting Apple Pay Sheet")
//        present(viewController , animated: true) {() -> Void in }
//
//        
//        
//        
//    }
//    
////    func tappedToShowDropIn() {
////        let paymentRequest = BTPaymentRequest()
////        paymentRequest.summaryTitle = "Our Fancy Magazine"
////        paymentRequest.summaryDescription = "53 Week Subscription"
////        paymentRequest.displayAmount = "$19.00"
////        paymentRequest.callToActionText = "$19 - Subscribe Now"
////        paymentRequest.shouldHideCallToAction = false
////        let dropIn = BTDropInViewController(apiClient: apiClient)
////        dropIn.delegate = self
////        dropIn.paymentRequest = paymentRequest
////        dropIn.title = "Check Out"
////        if BraintreeDemoSettings.useModalPresentation() {
////            progressBlock("Presenting Drop In Modally")
////            dropIn.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(self.tappedCancel))
////            let nav = UINavigationController(rootViewController: dropIn as? UIViewController ?? UIViewController())
////        }
////    }
//    
//    // MARK: - BTViewControllerPresentingDelegate
//    
//    func paymentDriver(_ driver: Any, requestsPresentationOf viewController: UIViewController) {
//        present(viewController, animated: true, completion: nil)
//    }
//    
//    func paymentDriver(_ driver: Any, requestsDismissalOf viewController: UIViewController) {
//        viewController.dismiss(animated: true, completion: nil)
//    }
//    
//    // MARK: - BTAppSwitchDelegate
//    
//    
//    // Optional - display and hide loading indicator UI
//    func appSwitcherWillPerformAppSwitch(_ appSwitcher: Any) {
//        showLoadingUI()
//        
//        NotificationCenter.default.addObserver(self, selector: #selector(hideLoadingUI), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
//    }
//    
//    func appSwitcherWillProcessPaymentInfo(_ appSwitcher: Any) {
//        hideLoadingUI()
//    }
//    
//    func appSwitcher(_ appSwitcher: Any, didPerformSwitchTo target: BTAppSwitchTarget) {
//        
//    }
//    
//    // MARK: - Private methods
//    
//    func showLoadingUI() {
//        // ...
//    }
//    
//    func hideLoadingUI() {
//        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
//        // ...
//    }
//}
//
//extension testViewController: PKPaymentAuthorizationViewControllerDelegate {
//    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didSelect paymentMethod: PKPaymentMethod, completion: @escaping ([PKPaymentSummaryItem]) -> Void) {
//        
//    }
//    
//    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didSelectShippingContact contact: PKContact, completion: @escaping (PKPaymentAuthorizationStatus, [PKShippingMethod], [PKPaymentSummaryItem]) -> Void) {
//        
//    }
//    
//    func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
//        controller.dismiss(animated: true, completion: nil)
//    }
//    
//    func paymentAuthorizationViewControllerWillAuthorizePayment(_ controller: PKPaymentAuthorizationViewController) {
//        
//    }
//    
//    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didSelect shippingMethod: PKShippingMethod, completion: @escaping (PKPaymentAuthorizationStatus, [PKPaymentSummaryItem]) -> Void) {
//        
//    }
//    
//    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, completion: @escaping (PKPaymentAuthorizationStatus) -> Void) {
//    }
//    
//    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didSelectShippingAddress address: ABRecord, completion: @escaping (PKPaymentAuthorizationStatus, [PKShippingMethod], [PKPaymentSummaryItem]) -> Void) {
//        
//    }
//    
//    
//    
//}
