//
//  MyAdsTableViewCell.swift
//  AdHouse
//
//  Created by  on 8/11/17.
//  Copyright © 2017 . All rights reserved.
//

import UIKit

class MyAdsTableViewCell: UITableViewCell {

    @IBOutlet weak var imgProduct:UIImageView!
    @IBOutlet weak var lblProductName:UILabel!
    @IBOutlet weak var lblPrice:UILabel!
    @IBOutlet weak var lblLocation:UILabel!
    @IBOutlet weak var btnEdit:UIButton!
    @IBOutlet weak var btnDelete:UIButton!
    @IBOutlet weak var btnFeatured:UIButton!
    @IBOutlet weak var mainView:UIView!
    @IBOutlet weak var imgFeatured:UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.mainView.layer.borderWidth = 0.5
        self.mainView.layer.borderColor = UIColor.lightGray.cgColor
        self.mainView.addShadow()
        
        self.setFonts()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    func setFonts() {
        
        lblProductName.font = FontUtility.getFontWithAdjustedSize(size: 14, desireFontType: .Roboto_Regular)
        lblPrice.font = FontUtility.getFontWithAdjustedSize(size: 12, desireFontType: .Roboto_Regular)
        lblLocation.font = FontUtility.getFontWithAdjustedSize(size: 10, desireFontType: .Roboto_Regular)
        btnEdit.titleLabel?.font = FontUtility.getFontWithAdjustedSize(size: 10, desireFontType: .Roboto_Regular)
        btnDelete.titleLabel?.font = FontUtility.getFontWithAdjustedSize(size: 10, desireFontType: .Roboto_Regular)
        btnFeatured.titleLabel?.font = FontUtility.getFontWithAdjustedSize(size: 7, desireFontType: .Roboto_Regular)
    }
 
}
