//
//  HelpAndAboutContent.swift
//  AdHouse
//
//  Created by maazulhaq on 04/08/2017.
//  Copyright © 2017 . All rights reserved.
//

import Foundation
import SwiftyJSON

class HelpAndAboutServices {
    
    //MARK:-  About Service Method
    static func About(param:[String:String],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Any?)->Void) {
        Request.GetRequest(fromSavedUrl: ServiceApiEndPoints.about + (param["lang"]!), parameters: [:], callback: { (response, error) in
           
            Alert.hideLoader()
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                
                Alert.showAlert(title: "Alert", message: String(describing: (error?.localizedDescription)!))
                return
            }
            
            if response?["Response"].stringValue != "2000" {
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                Alert.showAlert(title: "Alert", message: (response?["Message"].stringValue)!)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    //MARK:-  Contact Detail Service Method
    
    static func ContactDetails(param:[String:String],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Any?)->Void) {
        Request.GetRequest(fromSavedUrl: ServiceApiEndPoints.contactDetails + (param["lang"]!), parameters: [:], callback: { (response, error) in
            
            Alert.hideLoader()
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                
                Alert.showAlert(title: "Alert", message: String(describing: (error?.localizedDescription)!))
                return
            }
            
            if response?["Response"].stringValue != "2000" {
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                Alert.showAlert(title: "Alert", message: (response?["Message"].stringValue)!)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    //MARK:-  Help Service Method
    static func Help(param:[String:String],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Any?)->Void) {
        Request.GetRequest(fromSavedUrl: ServiceApiEndPoints.help + (param["lang"]!), parameters: [:], callback: { (response, error) in
            
            Alert.hideLoader()
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                
                Alert.showAlert(title: "Alert", message: String(describing: (error?.localizedDescription)!))
                return
            }
            
            if response?["Response"].stringValue != "2000" {
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                Alert.showAlert(title: "Alert", message: (response?["Message"].stringValue)!)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    //MARK:-  Help Service Method
    static func Terms(param:[String:String],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Any?)->Void) {
        Request.GetRequest(fromSavedUrl: ServiceApiEndPoints.term + (param["lang"]!), parameters: [:], callback: { (response, error) in
            
            Alert.hideLoader()
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                
                Alert.showAlert(title: "Alert", message: String(describing: (error?.localizedDescription)!))
                return
            }
            
            if response?["Response"].stringValue != "2000" {
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                Alert.showAlert(title: "Alert", message: (response?["Message"].stringValue)!)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    //MARK:-  Help Service Method
    static func Privacy(param:[String:String],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Any?)->Void) {
        Request.GetRequest(fromSavedUrl: ServiceApiEndPoints.privacy + (param["lang"]!), parameters: [:], callback: { (response, error) in
            
            Alert.hideLoader()
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                
                Alert.showAlert(title: "Alert", message: String(describing: (error?.localizedDescription)!))
                return
            }
            
            if response?["Response"].stringValue != "2000" {
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                Alert.showAlert(title: "Alert", message: (response?["Message"].stringValue)!)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    //MARK:-  Country Service Method
    static func Country(param:[String:String],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Any?)->Void) {
        Request.GetRequest(fromSavedUrl: ServiceApiEndPoints.country + (param["lang"]!) , parameters: [:], callback: { (response, error) in
            
            Alert.hideLoader()
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                
                Alert.showAlert(title: "Alert", message: String(describing: (error?.localizedDescription)!))
                return
            }
            
            if response?["Response"].stringValue != "2000" {
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                Alert.showAlert(title: "Alert", message: (response?["Message"].stringValue)!)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    //MARK:-  City Service Method
    static func City(param:[String:String],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Any?)->Void) {
        Request.GetRequest(fromSavedUrl: ServiceApiEndPoints.city + (param["lang"]!) , parameters: [:], callback: { (response, error) in

            Alert.hideLoader()
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                
                Alert.showAlert(title: "Alert", message: String(describing: (error?.localizedDescription)!))
                return
            }
            
            if response?["Response"].stringValue != "2000" {
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                Alert.showAlert(title: "Alert", message: (response?["Message"].stringValue)!)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    //MARK:-  Currency Service Method
    static func Currency(param:[String:String],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Any?)->Void) {
        Request.GetRequest(fromSavedUrl: ServiceApiEndPoints.currency + (param["lang"]!) , parameters: [:], callback: { (response, error) in
            
            Alert.hideLoader()
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                
                Alert.showAlert(title: "Alert", message: String(describing: (error?.localizedDescription)!))
                return
            }
            
            if response?["Response"].stringValue != "2000" {
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                Alert.showAlert(title: "Alert", message: (response?["Message"].stringValue)!)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
}
