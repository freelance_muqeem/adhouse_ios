//
//  HomeCollectionViewCell.swift
//  AdHouse
//
//  Created by Sharjeel ahmed on 7/25/17.
//  Copyright © 2017 . All rights reserved.
//

import UIKit

class HomeCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var shadowView:UIView!
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var lblDetail:UILabel!
    @IBOutlet weak var lblCarMake:UILabel!
    @IBOutlet weak var lblClass:UILabel!
    @IBOutlet weak var lblKM:UILabel!
    @IBOutlet weak var lbl_txt_Feature:UILabel!
    @IBOutlet weak var imageView:UIImageView!
    @IBOutlet weak var imgFeatured:UIImageView!
    
    @IBOutlet weak var constraintClassTop:NSLayoutConstraint!
    @IBOutlet weak var constraintKMTop:NSLayoutConstraint!
    @IBOutlet weak var constraintCarMakeTop:NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setFonts()
        
    }
    
    func setFonts() {
        
        lblName.font = FontUtility.getFontWithAdjustedSize(size: 10, desireFontType: .Roboto_Regular)
        lblDetail.font = FontUtility.getFontWithAdjustedSize(size: 9, desireFontType: .Roboto_Regular)
        lbl_txt_Feature.font = FontUtility.getFontWithAdjustedSize(size: 8, desireFontType: .Roboto_Regular)

        lblClass.font = FontUtility.getFontWithAdjustedSize(size: 10, desireFontType: .Roboto_Regular)
        lblCarMake.font = FontUtility.getFontWithAdjustedSize(size: 10, desireFontType: .Roboto_Regular)
        lblKM.font = FontUtility.getFontWithAdjustedSize(size: 10, desireFontType: .Roboto_Regular)
        
    }


}
