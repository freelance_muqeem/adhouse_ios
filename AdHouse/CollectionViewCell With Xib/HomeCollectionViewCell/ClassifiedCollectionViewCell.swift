//
//  ClassifiedCollectionViewCell.swift
//  AdHouse
//
//  Created by Sharjeel ahmed on 7/28/17.
//  Copyright © 2017 . All rights reserved.
//

import UIKit

class ClassifiedCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var shadowView:UIView!
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var lbl_txt_Feature:UILabel!
    @IBOutlet weak var imageView:UIImageView!
    @IBOutlet weak var imgFeatured:UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()

        self.setFonts()
        
    }
    
    func setFonts() {
        
        lblName.font = FontUtility.getFontWithAdjustedSize(size: 10.5, desireFontType: .Roboto_Regular)
        lbl_txt_Feature.font = FontUtility.getFontWithAdjustedSize(size: 8, desireFontType: .Roboto_Regular)
        
        
        
    }
    

}
