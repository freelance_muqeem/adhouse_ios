//
//  home_vertical_TableViewCell.swift
//  AdHouse
//
//  Created by Sharjeel ahmed on 7/26/17.
//  Copyright © 2017 . All rights reserved.
//

import UIKit

class home_vertical_TableViewCell: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    @IBOutlet weak var sectionView:UIView!
    @IBOutlet weak var lbl_txt_vechical:UILabel!
    @IBOutlet weak var ViewAllBtn:UIButton!
    @IBOutlet weak var homeCollectionView:UICollectionView!
    var delegate:CellBtnPressedProtocol?
    var refreshDelegate: RefreshDelegate?
    var cellIndexPath:IndexPath?
    
    //var productObj:[ProductResult]? = [ProductResult]()
    var productObj:[ClassifiedCategory]? = [ClassifiedCategory]()

    var selectedLang: [String]!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        selectedLang =  UserDefaults.standard.object(forKey: "AppleLanguages") as? [String] ?? [String]()

        //register cell
        
        let nib = UINib(nibName:String(describing:ClassifiedCollectionViewCell.self), bundle: nil)
         homeCollectionView.register(nib, forCellWithReuseIdentifier:String(describing: ClassifiedCollectionViewCell.self))
        
        self.setFonts()
        self.GetClassified()
    }
    
    
    func setFonts() {
        
        lbl_txt_vechical.font = FontUtility.getFontWithAdjustedSize(size: 11, desireFontType: .Roboto_Regular)
        ViewAllBtn.titleLabel?.font = FontUtility.getFontWithAdjustedSize(size: 10, desireFontType: .Roboto_Regular)
        
    }
    
    
    @IBAction func ViewAllBtnPressed(){
        
        
        delegate?.viewAllBtnPressed(cellIndexPath:cellIndexPath!)
        
    }
    
    //MARK:-  Get Classified Listing Service
    
//    func GetClassified() {
//        
//        Alert.showLoader(message: "")
//        
//         let userId = (Singleton.sharedInstance.CurrentUser == nil) ? -1 : (Singleton.sharedInstance.CurrentUser!.id!)
//        
//        let param = ["user_id":userId ]
//        
//        HomeServices.Classifieds(param: param, completionHandler: {(status, response, error) in
//            Alert.hideLoader()
//            if !status {
//                if error != nil {
//                    print("Error: \((error as! Error).localizedDescription)")
//                    return
//                }
//                let msg = response?["Message"].stringValue
//                print("Message: \(String(describing: msg))")
//                return
//            }
//            
//            print("Main")
//            print(response!)
//            
//            print("SUCCESS")
//            
//            self.productObj?.removeAll()
//            
//            let vehicleResponseArray = response?["Result"].arrayValue
//            for resultObj in vehicleResponseArray! {
//                let obj =  ProductResult(json: resultObj)
//                self.productObj?.append(obj)
//            }
//            
//            
//            self.refreshDelegate?.didRefresh(count: CGFloat((self.productObj?.count)!))
//            
//            self.homeCollectionView.reloadData()
//        })
//        
//    }

    
    //MARK:-  Get Classified Listing Service
    
    func GetClassified() {
        
        Alert.showLoader(message: "")
        
        let userId = (Singleton.sharedInstance.CurrentUser == nil) ? -1 : (Singleton.sharedInstance.CurrentUser!.id!)
        
        var param:[String:Any] = ["user_id":userId ]
        
        if !isLanguageEnglish() {
            param.updateValue("ar", forKey: "lang")
        } else {
            param.updateValue("en", forKey: "lang")
        }
        
        print("PARAMETERS: \(param)")

        
        HomeServices.CompleteAds(param: param, completionHandler: {(status, response, error) in
            Alert.hideLoader()
            if !status {
                if error != nil {
                    print("Error: \((error as! Error).localizedDescription)")
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                return
            }
            
            print("Main")
            print(response!)
            
            print("SUCCESS")
            
            self.productObj?.removeAll()
            
            let vehicleResponseArray = response?["Result"]["classified_category"].arrayValue
            for resultObj in vehicleResponseArray! {
                let obj =  ClassifiedCategory(json: resultObj)
                self.productObj?.append(obj)
            }
            
            
            self.refreshDelegate?.didRefresh(count: CGFloat((self.productObj?.count)!))
            
            self.homeCollectionView.reloadData()
        })
        
    }
    
    
    //MARK:-  CollectionView Delegate
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       
        return self.productObj!.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let obj:ClassifiedCategory = self.productObj![indexPath.row]
        
        let  cell:ClassifiedCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing:ClassifiedCollectionViewCell.self), for: indexPath) as! ClassifiedCollectionViewCell

        cell.layer.shadowColor = UIColor.lightGray.cgColor
        cell.layer.shadowOffset = CGSize(width: 0, height: 1.0)
        cell.layer.shadowRadius = 1
        cell.layer.shadowOpacity = 0.9
        cell.layer.masksToBounds = false
        cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: cell.contentView.layer.cornerRadius).cgPath
        
        cell.lblName.text = obj.name
        cell.imageView.setImageFromUrl(urlStr: (obj.categoryImage)!)
        
        cell.lbl_txt_Feature.isHidden = true
        cell.imgFeatured.isHidden = true
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        delegate?.disSelectClassifiedCategory(cellIndexPath:indexPath, product: (self.productObj![indexPath.row]))
    }

    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let screenWidth =  collectionView.bounds.size.width - 20
        let height = SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 150)
        return CGSize(width: screenWidth/3, height: height);
        
    }
    
}
