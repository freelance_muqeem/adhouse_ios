//
//  Dropdownlist.swift
//
//  Created by  on 8/11/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Dropdownlist: NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let value = "value"
        static let value_ar = "value_ar"
        static let updatedAt = "updated_at"
        static let id = "id"
        static let attributeId = "attribute_id"
        static let createdAt = "created_at"
        static let child_arrtibuteId = "child_attribute_id"
        static let child_attribute_value_Id = "child_attribute_value_id"
        static let parent_arrtibuteId = "parent_attribute_id"
        static let parent_attribute_value_Id = "parent_attribute_value_id"
        
    }
    
    // MARK: Properties
    public var value: String?
    public var value_ar: String?
    public var updatedAt: String?
    public var id: Int?
    public var attributeId: Int?
    public var createdAt: String?
    public var child_arrtibuteId: String?
    public var child_attribute_value_Id: String?
    public var parent_arrtibuteId: Int?
    public var parent_attribute_value_Id: Int?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: JSON) {
        self.init(json: object)
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        value = json[SerializationKeys.value].string
        value_ar = json[SerializationKeys.value_ar].string
        updatedAt = json[SerializationKeys.updatedAt].string
        id = json[SerializationKeys.id].int
        attributeId = json[SerializationKeys.attributeId].int
        createdAt = json[SerializationKeys.createdAt].string
        child_arrtibuteId = json[SerializationKeys.child_arrtibuteId].string
        child_attribute_value_Id = json[SerializationKeys.child_attribute_value_Id].string
        parent_arrtibuteId = json[SerializationKeys.parent_arrtibuteId].int
        parent_attribute_value_Id = json[SerializationKeys.parent_attribute_value_Id].int
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = value { dictionary[SerializationKeys.value] = value }
        if let value = value_ar { dictionary[SerializationKeys.value_ar] = value }
        if let value = updatedAt { dictionary[SerializationKeys.updatedAt] = value }
        if let value = id { dictionary[SerializationKeys.id] = value }
        if let value = attributeId { dictionary[SerializationKeys.attributeId] = value }
        if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
        if let value = child_arrtibuteId { dictionary[SerializationKeys.child_arrtibuteId] = value }
        if let value = child_attribute_value_Id { dictionary[SerializationKeys.child_attribute_value_Id] = value }
        if let value = parent_arrtibuteId { dictionary[SerializationKeys.parent_arrtibuteId] = value }
        if let value = parent_attribute_value_Id { dictionary[SerializationKeys.parent_attribute_value_Id] = value }
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.value = aDecoder.decodeObject(forKey: SerializationKeys.value) as? String
        self.value_ar = aDecoder.decodeObject(forKey: SerializationKeys.value_ar) as? String
        self.updatedAt = aDecoder.decodeObject(forKey: SerializationKeys.updatedAt) as? String
        self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
        self.attributeId = aDecoder.decodeObject(forKey: SerializationKeys.attributeId) as? Int
        self.createdAt = aDecoder.decodeObject(forKey: SerializationKeys.createdAt) as? String
        self.child_arrtibuteId = aDecoder.decodeObject(forKey: SerializationKeys.child_arrtibuteId) as? String
        self.child_attribute_value_Id = aDecoder.decodeObject(forKey: SerializationKeys.child_attribute_value_Id) as? String
        self.parent_arrtibuteId = aDecoder.decodeObject(forKey: SerializationKeys.parent_arrtibuteId) as? Int
        self.parent_attribute_value_Id = aDecoder.decodeObject(forKey: SerializationKeys.parent_attribute_value_Id) as? Int
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(value, forKey: SerializationKeys.value)
         aCoder.encode(value_ar, forKey: SerializationKeys.value_ar)
        aCoder.encode(updatedAt, forKey: SerializationKeys.updatedAt)
        aCoder.encode(id, forKey: SerializationKeys.id)
        aCoder.encode(attributeId, forKey: SerializationKeys.attributeId)
        aCoder.encode(createdAt, forKey: SerializationKeys.createdAt)
        aCoder.encode(child_arrtibuteId, forKey: SerializationKeys.child_arrtibuteId)
        aCoder.encode(child_attribute_value_Id, forKey: SerializationKeys.child_attribute_value_Id)
        aCoder.encode(parent_arrtibuteId, forKey: SerializationKeys.parent_arrtibuteId)
        aCoder.encode(parent_attribute_value_Id, forKey: SerializationKeys.parent_attribute_value_Id)
    }
    
}
