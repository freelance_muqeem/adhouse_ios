//
//  MapViewController.swift
//  AdHouse
//
//  Created by   on 12/12/17.
//  Copyright © 2017 . All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import GoogleMaps

class MapViewController: UIViewController , CLLocationManagerDelegate , MKMapViewDelegate  {

    class func instantiateFromStoryboard() -> MapViewController {
        let storyboard = UIStoryboard(name: "AdsDetail", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! MapViewController
    }
    
    @IBOutlet weak var mapView:MKMapView!
    @IBOutlet weak var viewMap: GMSMapView!

    
    var leftBtn:UIButton?
    var SellerLocation:String!
    var latitude:Double!
    var longitude:Double!
    var location = CLLocationCoordinate2D(latitude: 0.0 , longitude: 0.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let navBackgroundImage:UIImage! = UIImage(named: "topbox")!
        self.navigationController?.navigationBar.setBackgroundImage(navBackgroundImage, for: .default)
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.title = self.SellerLocation
        
        latitude  = self.latitude == nil ? 0.0 : self.latitude
        longitude  = self.longitude == nil ? 0.0 : self.longitude

        
        self.location = CLLocationCoordinate2D(latitude: latitude , longitude: longitude!)
        
        
        
        self.placeBackBtn()
//        self.getMap()

        
        
        
        // Create a GMSCameraPosition that tells the map to display the
        
        let camera = GMSCameraPosition.camera(withLatitude: latitude, longitude: longitude, zoom: 12)
        self.viewMap.camera = camera
        
        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        marker.title = self.SellerLocation
        marker.map = viewMap
    }

    //MARK:-  Back Button Code
    
    func placeBackBtn(){
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        leftBtn =  UIButton(type: .custom)
        leftBtn?.setImage(UIImage(named: "backbtn"), for: .normal)
        leftBtn?.addTarget(self, action: #selector(MapViewController.BackBtnPressed), for: .touchUpInside)
        leftBtn?.frame = CGRect(x: 0, y:0 , width: 25, height: 25)
        let  leftBarButton: UIBarButtonItem = UIBarButtonItem(customView: leftBtn!)
        self.navigationItem.setLeftBarButton(leftBarButton, animated: true)
        
    }
    
    @objc func BackBtnPressed(){
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func getMap() {
        
        mapView.mapType = MKMapType.standard
        let span = MKCoordinateSpanMake(0.095, 0.095)
        let region = MKCoordinateRegion(center: self.location, span: span)
        let annotation = MKPointAnnotation()
        annotation.coordinate = self.location
        mapView.addAnnotation(annotation)
        mapView.setRegion(region, animated: false)
        
    }

    
    
}
