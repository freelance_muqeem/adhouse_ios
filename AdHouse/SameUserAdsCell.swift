//
//  SameUserAdsCell.swift
//  AdHouse
//
//  Created by   on 12/7/17.
//  Copyright © 2017 . All rights reserved.
//


import UIKit
import AlamofireImage

extension SameUserAdsCell: RefreshDelegate {
    func didRefresh(count: CGFloat) {
    }
    
    func didRefresh() {
        //self.GetFeaturedList()
    }
}

class SameUserAdsCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var collectionView:UICollectionView!
    
    var delegate:CellBtnPressedProtocol?
    
    var featuredArray:[FeatureResult] = [FeatureResult]()
    
    var navigationDelegate: NavigationDelegate?
    
    var ownerID: Int!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setFonts()
        let nib = UINib(nibName:String(describing:HomeCollectionViewCell.self), bundle: nil)
        
        collectionView.register(nib, forCellWithReuseIdentifier:String(describing: HomeCollectionViewCell.self))
        
        let flowLayout = UICollectionViewFlowLayout()

        flowLayout.scrollDirection = UICollectionViewScrollDirection.horizontal
        collectionView.collectionViewLayout = flowLayout
        
        
        
    }
    
    
    func setFonts() {
        
        lblTitle.font = FontUtility.getFontWithAdjustedSize(size: 13, desireFontType: .Roboto_Regular)
        
    }
    
    
    //MARK:-  Get Same User Ads Listing Service
    
    
    func GetSameUserAds(userID:Int) {
        
        ownerID = userID
        
        Alert.showLoader(message: "")
        
        let userID = (Singleton.sharedInstance.CurrentUser?.id != nil) ? Singleton.sharedInstance.CurrentUser?.id : -1
        let parameters:[String:Any] = ["user_id": userID! ,"owner_id":ownerID]
        
        HomeServices.SameUserAds(param: parameters , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    print("Error: \((error as! Error).localizedDescription)")
                     Alert.hideLoader()
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                return
            }
            
            print(response!)
            print("SUCCESS")
            Alert.hideLoader()
            
            self.featuredArray.removeAll()
            
            let featureResponseArray = response?["Result"].arrayValue
            
            for resultObj in featureResponseArray! {
                let obj = FeatureResult(json: resultObj)
                self.featuredArray.append(obj)
            }
            
            self.collectionView.reloadData()
            
        })
        
    }
    
    
    
    // MARK: - UIcollectionview data source
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.featuredArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let obj:FeatureResult = self.featuredArray[indexPath.row]
        
        let  cell:HomeCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing:HomeCollectionViewCell.self), for: indexPath) as! HomeCollectionViewCell
        
        
        cell.lblName.text = obj.title
        cell.lblDetail.text = "QAR " +  "\(obj.price!.decimal)" //String(describing: obj.price!.decimal)
        if obj.adImages?.first?.imageUrl != nil {
        cell.imageView.setImageFromUrl(urlStr: (obj.adImages?.first?.imageUrl)!)
        }
        if obj.featured == 0 {
            
            cell.lbl_txt_Feature.isHidden = true
            cell.imgFeatured.isHidden = true
            
        } else {
            
            cell.lbl_txt_Feature.isHidden = false
            cell.imgFeatured.isHidden = false
            
        }
        
        cell.layer.shadowColor = UIColor.lightGray.cgColor
        cell.layer.shadowOffset = CGSize(width: 0, height: 1.0)
        cell.layer.shadowRadius = 1
        cell.layer.shadowOpacity = 0.9
        cell.layer.masksToBounds = false
        cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: cell.contentView.layer.cornerRadius).cgPath
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let screenWidth =  UIScreen.main.bounds.size.width-20
        let height = SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 150)
        return CGSize(width: screenWidth/3, height: height);
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.navigationDelegate?.didNavigate(productObjID: self.featuredArray[indexPath.row].id!)
        self.featuredArray = [FeatureResult]()
    }
    
}

