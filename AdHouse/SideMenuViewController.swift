//
//  SideMenuViewController.swift
//  AdHouse
//
//  Created by maazulhaq on 24/07/2017.
//  Copyright © 2017 . All rights reserved.
//

import UIKit

class SideMenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var selectedIndex:IndexPath? = IndexPath(row: 1, section: 0)
    
    var sideMenuItems: [(name: String , imageName: String)] = [(name: NSLocalizedString("Home", comment: "") , imageName:"home" )
        ,       (name:NSLocalizedString("Notifications", comment: "") , imageName:"notification" ),
                (name: NSLocalizedString("My Favorites", comment: ""), imageName:"notification" ),
                (name:  NSLocalizedString("My Ads", comment: ""), imageName:"ads" ),
                (name: NSLocalizedString("My Profile", comment: ""), imageName:"PROFILE" ),
                (name: NSLocalizedString("Settings", comment: ""), imageName:"logout" ),
                (name: NSLocalizedString("Subscription-Commercial", comment: ""), imageName:"Subscription-Commercial" ),
                (name: NSLocalizedString("Logout", comment: ""), imageName:"logout" )]
    
    
    @IBOutlet var tableViewSideMenu: UITableView!
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var userImage:UIImageView!
    
    var subscriptionObj: SubscriptionUserResult!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Singleton.sharedInstance.CurrentUser == nil {
            
            sideMenuItems = [(name: NSLocalizedString("Home", comment: "") , imageName:"home" )
                ,       (name:NSLocalizedString("Notifications", comment: "") , imageName:"notification" ),
                        (name: NSLocalizedString("My Favorites", comment: ""), imageName:"notification" ),
                        (name:  NSLocalizedString("My Ads", comment: ""), imageName:"ads" ),
                        (name: NSLocalizedString("My Profile", comment: ""), imageName:"PROFILE" ),
                        (name: NSLocalizedString("Settings", comment: ""), imageName:"logout" ),
                        (name: NSLocalizedString("Subscription-Commercial", comment: ""), imageName:"Subscription-Commercial" ),
                        (name: NSLocalizedString("Log In", comment: ""), imageName:"logout" )]
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.GetUserSubscription), name: NSNotification.Name(rawValue: SUBSCRIPTIONUPDATED), object: nil)
        
        self.userImage.layer.borderWidth = 2
        self.userImage.layer.borderColor = UIColor(red:160/255.0, green:0/255.0, blue:7/255.0, alpha: 1.0).cgColor
        self.userImage.layer.cornerRadius = 3
        
        self.GetUserSubscription()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if Singleton.sharedInstance.CurrentUser == nil {
            
            self.userImage.image = UIImage(named: "default")
            self.lblName.text = NSLocalizedString("Guest User", comment: "")
            self.lblEmail.text = ""
            
        } else {
            
            self.userImage.setImageFromUrl(urlStr: (Singleton.sharedInstance.CurrentUser?.profileImage)!)
            self.lblName.text = Singleton.sharedInstance.CurrentUser?.fullName
            self.lblEmail.text = Singleton.sharedInstance.CurrentUser?.email
            
        }
        
    }
    
    //MARK:-  Get Vehicles Listing Service
    
    @objc func GetUserSubscription() {
        
        Alert.showLoader(message: "")
        
        let userId = (Singleton.sharedInstance.CurrentUser == nil) ? -1 : (Singleton.sharedInstance.CurrentUser!.id!)
        
        let param = ["user_id": userId]
        
        SubscriptionServices.SubscriptionUser(param: param, completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    print("Error: \((error as! Error).localizedDescription)")
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                return
            }
            
            print("Main")
            print(response!)
            Alert.hideLoader()
            print("SUCCESS")
            
            self.subscriptionObj = SubscriptionUserResult(json:  (response?["Result"])!)
            
            self.tableViewSideMenu.reloadData()
        })
        
    }
    
    // MARK: - UITableViewDelegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if indexPath.row == 6 {
//            return GISTUtility.convertToRatio(70)
//        }
//        else {
//            return GISTUtility.convertToRatio(50)
//        }
        
        return GISTUtility.convertToRatio(50)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sideMenuItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
//        if indexPath.row == 6 {
//
//            let cell: SideMenuSubTitle = tableView.dequeueReusableCell(withIdentifier: "SideMenuSubTitleCell", for: indexPath) as! SideMenuSubTitle
//            cell.cellIndexPath = indexPath
//            cell.labelTitle.text = sideMenuItems[indexPath.row].name
//            //            cell.labelSubTitle.text = "You can post 25 ads in 20 days"
//
//            if self.subscriptionObj != nil {
//                cell.labelSubTitle.text = ""
//           //     cell.labelSubTitle.text = NSLocalizedString("You can post", comment: "") + " " + "\(subscriptionObj.remainingAds!)" + " " + NSLocalizedString("ads in", comment: "") +
////                    " " + "\(subscriptionObj.remainingDays!)" + " " + NSLocalizedString("days", comment: "")
//            }
//
//            else {
//
//                cell.labelSubTitle.text = NSLocalizedString("You are logged in as Guest User", comment: "")
//            }
//
//            cell.imageViewIcon.image = UIImage.init(named:  (sideMenuItems[indexPath.row].imageName))
//            cell.labelTitle.semanticContentAttribute = .forceLeftToRight
//
//            return cell
//        }
//        else {
            let cell: SideMenuCell = tableView.dequeueReusableCell(withIdentifier: "SideMenuCell", for: indexPath) as! SideMenuCell
            cell.cellIndexPath = indexPath
            cell.labelTitle.text = sideMenuItems[indexPath.row].name
            cell.imageViewIcon.image = UIImage.init(named:  (sideMenuItems[indexPath.row].imageName))
            cell.labelTitle.semanticContentAttribute = .forceLeftToRight
            
            return cell
 //       }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Selected row: \(indexPath.row)")
        switch indexPath.row {
        case 0:
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "NavHome")
            self.slideMenuController()?.changeMainViewController(vc!, close: true)
            break
        case 1:
            
            if Singleton.sharedInstance.CurrentUser == nil {
                
                Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("You have logged in as Guest Member, in order to access this feature. you need to SignUp on App", comment: ""))
                
            } else {
                let vc = NotificationViewController.instantiateFromStoryboard()
                self.slideMenuController()?.changeMainViewController(vc, close: true)
                break
            }
            
        case 2:
            
            if Singleton.sharedInstance.CurrentUser == nil {
                
                Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("You have logged in as Guest Member, in order to access this feature. you need to SignUp on App", comment: ""))
                
            } else {
                let vc = MyFavouritesViewController.instantiateFromStoryboard()
                self.slideMenuController()?.changeMainViewController(vc, close: true)
                break
            }
            
        case 3:
            if Singleton.sharedInstance.CurrentUser == nil {
                
                Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("You have logged in as Guest Member, in order to access this feature. you need to SignUp on App", comment: ""))
                
            } else {
                
                let vc = AdsViewController.instantiateFromStoryboard()
                self.slideMenuController()?.changeMainViewController(vc, close: true)
                break
            }
        case 4:
            if Singleton.sharedInstance.CurrentUser == nil {
                
                Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("You have logged in as Guest Member, in order to access this feature. you need to SignUp on App", comment: ""))
                
            } else {
                let vc = MyProfileViewController.instantiateFromStoryboard()
                self.slideMenuController()?.changeMainViewController(vc, close: true)
                break
            }
            
        case 5:
            let vc = SettingViewController.instantiateFromStoryboard()
            self.slideMenuController()?.changeMainViewController(vc, close: true)
            break
            
        case 6:
            
            if Singleton.sharedInstance.CurrentUser == nil {
                
                Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("You have logged in as Guest Member, in order to access this feature. you need to SignUp on App", comment: ""))
                
            } else {
                
                let vc = SubscriptionViewController.instantiateFromStoryboard()
                self.slideMenuController()?.changeMainViewController(vc, close: true)
                break
                
            }
            
        case 7:
            //        case 6:
            if Singleton.sharedInstance.CurrentUser == nil {
                
                UserDefaults.standard.removeObject(forKey: User_data_userDefault)
                UserDefaults.standard.synchronize()
                
                Singleton.sharedInstance.CurrentUser = nil
                
                let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                let vc = storyboard.instantiateViewController(withIdentifier: "MainNavVC") as! UINavigationController
                UIWINDOW?.rootViewController = vc
                
            } else {
                
                DispatchQueue.main.async {
                    
                    let alertController = UIAlertController (title:NSLocalizedString("Alert", comment: "") , message: NSLocalizedString("Are you sure you want to Logout?", comment: "") , preferredStyle: .alert)
                    let cancelAction = UIAlertAction(title: NSLocalizedString("No", comment: ""), style: .default){ (_) -> Void in
                        
                    }
                    let doneAction = UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: .default) { (_) -> Void in
                        UserDefaults.standard.removeObject(forKey: User_data_userDefault)
                        UserDefaults.standard.synchronize()
                        
                        Singleton.sharedInstance.CurrentUser = nil
                        
                        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                        let vc = storyboard.instantiateViewController(withIdentifier: "MainNavVC") as! UINavigationController
                        UIWINDOW?.rootViewController = vc
                        
                    }
                    alertController.addAction(cancelAction)
                    alertController.addAction(doneAction)
                    self.present(alertController, animated: true, completion: nil)
                    
                }
                
                
                break
            }
            
        default:
            break
        }
    }
    
    
}

