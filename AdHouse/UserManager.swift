//
//  UserManager.swift
//  AdHouse
//
//  Created by maazulhaq on 04/08/2017.
//  Copyright © 2017 . All rights reserved.
//

import UIKit
import Foundation

class UserManager {

    static func saveUserObjectToUserDefaults(userResult:UserObject){
        
        let archiveData = NSKeyedArchiver.archivedData(withRootObject: userResult)
        UserDefaults.standard.set(archiveData, forKey: User_data_userDefault)
    }
    
    static func removeUserObjectFromUserDefaults() {
        UserDefaults.standard.set(nil, forKey: User_data_userDefault)
        UserDefaults.standard.removeObject(forKey: User_data_userDefault)
        UserDefaults.standard.synchronize()
    }
    
    static func  getUserObjectFromUserDefaults()->UserObject?{
        
        let userData:NSData?  = UserDefaults.standard.value(forKey: User_data_userDefault) as? NSData
        
        if userData != nil {
            let userDataObj = NSKeyedUnarchiver.unarchiveObject(with: userData! as Data) as?  UserObject
            return userDataObj
        }
        else{
            return nil
        }
        
    }
    
    static func isUserLogin()->Bool{
        
        if  self.getUserObjectFromUserDefaults() != nil{
            return true
        }
        else{
            return false
        }
        
    }
    
    
}
