//
//  HomeListViewController.swift
//  AdHouse
//
//  Created by Sharjeel ahmed on 7/26/17.
//  Copyright © 2017 . All rights reserved.
//

import UIKit

enum CategoryType {
    case VEHICLES
    case PROPERTY
    case CLASSIFIED
}


extension HomeListViewController: RefreshDelegate {
    func didRefresh(count: CGFloat) {
        
    }
    
    func didRefresh() {
        self.productObj?.removeAll()
        setViewAsPerType()
    }
    
}

class HomeListViewController: UIViewController , UITableViewDelegate , UITableViewDataSource {
    
    class func instantiateFromStoryboard() -> HomeListViewController {
        let storyboard = UIStoryboard(name: "HomeList", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! HomeListViewController
    }
    
    @IBOutlet weak var tblView:UITableView!
    var leftBtn:UIButton?
    var rightBtn:UIButton?
    var selectedCategory:CategoryType?
    
    var productObj:[ProductResult]? = [ProductResult]()
    var classifiedArray:[SubCategoryClassified]? = [SubCategoryClassified]()
    var classified:ClassifiedCategory?
    
    //    var propertyArray:[ProductResult]? = [ProductResult]()
    //    var classifiedArray:[ProductResult]? = [ProductResult]()
    //
    
    var isFav = false
    
    var offsetForApiCall:Int? = 0
    var postLimitPerCall = 10
    var selectedLang: [String]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Register Nib Cells
        let cellNib = UINib(nibName: "FavouritesViewCell", bundle: nil)
        self.tblView.register(cellNib, forCellReuseIdentifier: String(describing: FavouritesViewCell.self))
        
        let cellNib2 = UINib(nibName: "VehicleTableViewCell", bundle: nil)
        self.tblView.register(cellNib2, forCellReuseIdentifier: String(describing: VehicleTableViewCell.self))
        
        setViewAsPerType()
        
        self.placeBackBtn()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.red,NSAttributedStringKey.font : UIFont(name: "HelveticaNeue-Bold", size: 21)!]
        
    }
    
    
    func setViewAsPerType()  {
        if selectedCategory == CategoryType.PROPERTY{
     //   self.title = NSLocalizedString("Properties", comment: "")
            self.GetProperty()
        }
            
        else if selectedCategory == CategoryType.VEHICLES{
            //            self.title = NSLocalizedString("Vehicle", comment: "")
            self.GetVehicles()
        }
            
        else if  selectedCategory == CategoryType.CLASSIFIED {
            //            self.title = NSLocalizedString("Classified", comment: "")
            self.GetClassified()
        }
            
        else {
            
            self.title = classified?.name!
            self.GetClassifiedCategory()
        }
    }
    //MARK:-  Back Button Code
    
    func placeBackBtn(){
        
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        leftBtn =  UIButton(type: .custom)
        leftBtn?.setImage(UIImage(named: "backbtn"), for: .normal)
        leftBtn?.addTarget(self, action: #selector(HomeListViewController.BackBtnPressed), for: .touchUpInside)
        leftBtn?.frame = CGRect(x: 0, y:0 , width: 25, height: 25)
        let  leftBarButton: UIBarButtonItem = UIBarButtonItem(customView: leftBtn!)
        self.navigationItem.setLeftBarButton(leftBarButton, animated: true)
        
        rightBtn =  UIButton(type: .custom)
        rightBtn?.setImage(UIImage(named: "searchbtn"), for: .normal)
        rightBtn?.addTarget(self, action: #selector(HomeListViewController.searchAction), for: .touchUpInside)
        rightBtn?.frame = CGRect(x: 0, y:0 , width: 25, height: 25)
        let  rightBarButton: UIBarButtonItem = UIBarButtonItem(customView: rightBtn!)
        rightBtn?.semanticContentAttribute = .forceLeftToRight;
        
        self.navigationItem.setRightBarButton(rightBarButton, animated: true)
        
    }
    
    @objc func BackBtnPressed(){
        
        self.navigationController?.popViewController(animated: true)
    }
    
     @objc func searchAction() {
        
        let searchVc = SearchViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(searchVc, animated: true)
        
    }
    
    //MARK:-  Get Vehicle Listing
    
    func GetVehicles() {
        
        Alert.showLoader(message: "")
        let offset = offsetForApiCall!
        let limit = postLimitPerCall
        var lang:String!
        selectedLang =  UserDefaults.standard.object(forKey: "AppleLanguages") as? [String] ?? [String]()
        
        if isLanguageEnglish() {
            lang = "en"
        }
        else {
            lang = "ar"
        }
        
        let userId = (Singleton.sharedInstance.CurrentUser == nil) ? -1 : (Singleton.sharedInstance.CurrentUser!.id!)
        let param = ["user_id":userId, "offset": offset, "limit": limit, "lang": lang!] as [String : Any]
        
        HomeServices.Vehicle(param: param, completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    print("Error: \((error as! Error).localizedDescription)")
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                return
            }
            
            print(response!)
            print("SUCCESS")
            Alert.hideLoader()
            //            self.productObj?.removeAll()
            
            let vehicleResponseArray = response?["Result"].arrayValue
            if vehicleResponseArray?.count == 0 {
                return
            }
            print(vehicleResponseArray!)
            for resultObj in vehicleResponseArray! {
                let obj = ProductResult(json: resultObj)
                self.productObj?.append(obj)
            }
            
            self.tblView.reloadData()
        })
    }
    
    
    //MARK:-  Get Property Listing Service
    
    func GetProperty() {
        
        Alert.showLoader(message: "")
        let offset = offsetForApiCall!
        let limit = postLimitPerCall
        var lang:String!
        
        if isLanguageEnglish() {
            lang = "en"
        }
        else {
            lang = "ar"
        }
        
        let userId = (Singleton.sharedInstance.CurrentUser == nil) ? -1 : (Singleton.sharedInstance.CurrentUser!.id!)
        let param = ["user_id":userId, "offset": offset, "limit": limit, "lang": lang!] as [String : Any]
        
//        let param = ["user_id":userId, "offset": offset, "limit": limit]
        
        HomeServices.Property(param: param, completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    print("Error: \((error as! Error).localizedDescription)")
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                return
            }
            
            
            print("Main")
            print(response!)
            
            print("SUCCESS")
            Alert.hideLoader()
            //            self.productObj?.removeAll()
            
            
            let propertyResponseArray = response?["Result"].arrayValue
            if propertyResponseArray?.count == 0 {
                return
            }
            for resultObj in propertyResponseArray! {
                let obj =  ProductResult(json: resultObj)
                self.productObj?.append(obj)
            }
            
            self.tblView.reloadData()
        })
        
    }
    
    //MARK:-  Get Classified Listing Service
    
    func GetClassified() {
        Alert.showLoader(message: "")
        let offset = offsetForApiCall!
        let limit = postLimitPerCall
        var lang:String!
        selectedLang =  UserDefaults.standard.object(forKey: "AppleLanguages") as? [String] ?? [String]()
        
        if isLanguageEnglish() {
            lang = "en"
        }
        else {
            lang = "ar"
        }
        
        let userId = (Singleton.sharedInstance.CurrentUser == nil) ? -1 : (Singleton.sharedInstance.CurrentUser!.id!)
        let param = ["user_id":userId, "offset": offset, "limit": limit, "lang": lang!] as [String : Any]
        
     //   let param = ["user_id":userId, "offset": offset, "limit": limit]
        
        HomeServices.Classifieds(param: param, completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    print("Error: \((error as! Error).localizedDescription)")
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                return
            }
            
            print("Main")
            print(response!)
            Alert.hideLoader()
            print("SUCCESS")
            
            //            self.productObj?.removeAll()
            
            let vehicleResponseArray = response?["Result"].arrayValue
            if vehicleResponseArray?.count == 0 {
                return
            }
            for resultObj in vehicleResponseArray! {
                let obj =  ProductResult(json: resultObj)
                self.productObj?.append(obj)
            }
            
            self.tblView.reloadData()
        })
        
    }
    
    
    //MARK:-  Get Classified Listing Service
    
    func GetClassifiedCategory() {
        
        Alert.showLoader(message: "")
        let offset = offsetForApiCall!
        let limit = postLimitPerCall
        var lang:String!
        selectedLang =  UserDefaults.standard.object(forKey: "AppleLanguages") as? [String] ?? [String]()
        
        if isLanguageEnglish() {
            lang = "en"
        }
        else {
            lang = "ar"
        }
        
        let userId = (Singleton.sharedInstance.CurrentUser == nil) ? -1 : (Singleton.sharedInstance.CurrentUser!.id!)
        
        let param:[String: Any] = ["user_id":userId, "category_id" :  classified!.id! , "offset": offset, "limit": limit , "lang": lang!]
        print(param)
        HomeServices.ClassifiedCategory(param: param, completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    print("Error: \((error as! Error).localizedDescription)")
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                return
            }
            
            print("Main")
            print(response!)
            Alert.hideLoader()
            print("SUCCESS")
            
            
            let vehicleResponseArray = response?["Result"].arrayValue
            if vehicleResponseArray?.count == 0 {
                return
            }
            for resultObj in vehicleResponseArray! {
                let obj =  SubCategoryClassified(json: resultObj)
                self.classifiedArray?.append(obj)
            }
            
            self.tblView.reloadData()
        })
        
    }
    
    
    
    //MARK:- Add/Delete Favorite
    
    @objc func AddFavoriteAction(sender: UIButton) {
        
        if Singleton.sharedInstance.CurrentUser == nil {
            
            Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("You have logged in as Guest Member, in order to access this feature. you need to SignUp on App", comment: ""))
        }
            
        else {
            
            if !self.isFav {
                self.isFav = !self.isFav
                
                var indexPath: IndexPath!
                if let superview = sender.superview?.superview {
                    if let cell = superview.superview as? FavouritesViewCell {
                        indexPath = tblView.indexPath(for: cell)
                        print(indexPath)
                        print("index path: \(indexPath)")
                        
                        if indexPath != nil {
                            var id = 0
                            var state = 0
                            //                            if self.selectedCategory == CategoryType.PROPERTY {
                            //                                id = self.productObj![indexPath.row].id!
                            //                                state = 1-self.productObj![indexPath.row].isLike!
                            //                            }
                            //                            else if self.selectedCategory == CategoryType.VEHICLES {
                            //                                id = self.productObj![indexPath.row].id!
                            //                                state = 1-self.productObj![indexPath.row].isLike!
                            //                            }
                            //                            else {
                            id = self.productObj![indexPath.row].id!
                            state = 1-self.productObj![indexPath.row].isLike!
                            //                            }
                            
                            if self.productObj![indexPath.row].userId == Singleton.sharedInstance.CurrentUser?.id {
                                
                                Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("You cannot favorite your own ads.", comment: ""))
                                return
                            }
                            
                            let parameters:[String:Any] = [
                                "user_id": (Singleton.sharedInstance.CurrentUser!.id!),
                                "ad_id":id,
                                "state":state
                            ]
                            
                            print("PARAMETERS: \(parameters)")
                            Alert.showLoader(message: "")
                            
                            FavouriteServices.Add(param: parameters , completionHandler: {(status, response, error) in
                                self.isFav = !self.isFav
                                if !status {
                                    if error != nil {
                                        print("Error: \((error as! Error).localizedDescription)")
                                        return
                                    }
                                    let msg = response?["Message"].stringValue
                                    print("Message: \(String(describing: msg))")
                                    return
                                }
                                
                                print(response!)
                                print("SUCCESS")
                                var state = 0
                                //                                if self.selectedCategory == CategoryType.PROPERTY {
                                //                                    state = 1-self.productObj![indexPath.row].isLike!
                                //                                    self.productObj?[indexPath.row].isLike = state
                                //                                }
                                //                                else if self.selectedCategory == CategoryType.VEHICLES {
                                //                                    state = 1-self.productObj![indexPath.row].isLike!
                                //                                    self.productObj?[indexPath.row].isLike = state
                                //                                }
                                //                                else {
                                state = 1-self.productObj![indexPath.row].isLike!
                                self.productObj?[indexPath.row].isLike = state
                                //                                }
                                self.tblView.reloadData()
                            })
                        }
                    }
                    if let cell = superview.superview as? VehicleTableViewCell {
                        indexPath = tblView.indexPath(for: cell)
                        print(indexPath)
                        print("index path: \(indexPath)")
                        
                        if indexPath != nil {
                            var id = 0
                            var state = 0
                            //                            if self.selectedCategory == CategoryType.PROPERTY {
                            //                                id = self.productObj![indexPath.row].id!
                            //                                state = 1-self.productObj![indexPath.row].isLike!
                            //                            }
                            //                            else if self.selectedCategory == CategoryType.VEHICLES {
                            //                                id = self.productObj![indexPath.row].id!
                            //                                state = 1-self.productObj![indexPath.row].isLike!
                            //                            }
                            //                            else {
                            id = self.productObj![indexPath.row].id!
                            state = 1-self.productObj![indexPath.row].isLike!
                            //                            }
                            
                            if self.productObj![indexPath.row].userId == Singleton.sharedInstance.CurrentUser?.id {
                                
                                Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("You cannot favorite your own ads.", comment: ""))
                                return
                            }
                            
                            let parameters:[String:Any] = [
                                "user_id": (Singleton.sharedInstance.CurrentUser!.id!),
                                "ad_id":id,
                                "state":state
                            ]
                            
                            print("PARAMETERS: \(parameters)")
                            Alert.showLoader(message: "")
                            
                            FavouriteServices.Add(param: parameters , completionHandler: {(status, response, error) in
                                self.isFav = !self.isFav
                                if !status {
                                    if error != nil {
                                        print("Error: \((error as! Error).localizedDescription)")
                                        return
                                    }
                                    let msg = response?["Message"].stringValue
                                    print("Message: \(String(describing: msg))")
                                    return
                                }
                                
                                print(response!)
                                print("SUCCESS")
                                var state = 0
                                //                                if self.selectedCategory == CategoryType.PROPERTY {
                                //                                    state = 1-self.productObj![indexPath.row].isLike!
                                //                                    self.productObj?[indexPath.row].isLike = state
                                //                                }
                                //                                else if self.selectedCategory == CategoryType.VEHICLES {
                                //                                    state = 1-self.productObj![indexPath.row].isLike!
                                //                                    self.productObj?[indexPath.row].isLike = state
                                //                                }
                                //                                else {
                                state = 1-self.productObj![indexPath.row].isLike!
                                self.productObj?[indexPath.row].isLike = state
                                //                                }
                                self.tblView.reloadData()
                            })
                        }
                    }
                }
                
            }
        }
    }
    
    
    //MARK:-  TableView Delegate
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        //        if classified != nil {
        //            if (indexPath.row == (self.classifiedArray?.count)! - 1) && ((self.classifiedArray?.count)! % 10) == 0 {
        //                print("Load more")
        //
        //                self.offsetForApiCall = self.offsetForApiCall! + 10
        //                self.postLimitPerCall = self.postLimitPerCall + 10
        //
        //                self.GetClassifiedCategory()
        //
        //            }
        //        }
        //
        if (indexPath.row == (self.productObj?.count)! - 1) && ((self.productObj?.count)! % 10) == 0 {
            print("Load more")
            
            self.offsetForApiCall = self.offsetForApiCall! + 10
            self.postLimitPerCall = self.postLimitPerCall + 10
            
            if selectedCategory == CategoryType.PROPERTY {
                self.GetProperty()
            }
            else if selectedCategory == CategoryType.VEHICLES {
                self.GetVehicles()
            }
            else if selectedCategory == CategoryType.CLASSIFIED {
                self.GetClassified()
            }
            
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return GISTUtility.convertToRatio(120)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if classified != nil {
            
            return self.classifiedArray!.count
        }
        else {
            
            return self.productObj!.count
            
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if classified != nil {
            
            let propertyObj:SubCategoryClassified = self.classifiedArray![indexPath.row]
            
            if selectedCategory == CategoryType.VEHICLES {
                
                let vehicleTableViewCell: VehicleTableViewCell = tableView.dequeueReusableCell(withIdentifier: "VehicleTableViewCell", for: indexPath) as! VehicleTableViewCell
                
                vehicleTableViewCell.lblProductName.text = propertyObj.title
                vehicleTableViewCell.lblPrice.text = "QAR " + String(describing: propertyObj.price!.decimal)
                vehicleTableViewCell.imgProduct.setImageFromUrl(urlStr: (propertyObj.adImages?.first?.imageUrl)!)
                
                vehicleTableViewCell.lblKm.text =  "Km " + String(describing: propertyObj.kilometer!.decimal)
                
                for i in propertyObj.adDetails! {
                    
                    if i.attributeTitle == "CAR MAKE" {
                        vehicleTableViewCell.lblCarMake.text = i.attributeValue
                        break
                    }
                }
                
                for i in propertyObj.adDetails! {
                    
                    if i.attributeTitle == "CLASS" {
                        vehicleTableViewCell.lblCarClass.text = i.attributeValue
                        break
                    }
                }
                
                for i in propertyObj.adDetails! {
                    
                    if i.attributeTitle == "صنع السيارة"{
                        vehicleTableViewCell.lblCarMake.text = i.attributeValue
                        break
                    }
                }
                
                for i in propertyObj.adDetails! {
                    
                    if i.attributeTitle == "موديل" {
                       //  if i.attributeTitle == "صف دراسي"{
                        vehicleTableViewCell.lblCarClass.text = i.attributeValue
                        break
                    }
                }
                
                vehicleTableViewCell.imgHeart.addTarget(self, action: #selector(self.AddFavoriteAction(sender:)), for: .touchUpInside)
                if propertyObj.featured == 0 {
                    
                    vehicleTableViewCell.btnFeatured.isHidden = true
                    vehicleTableViewCell.imgFeatured.isHidden = true
                    
                } else {
                    
                    vehicleTableViewCell.btnFeatured.isHidden = false
                    vehicleTableViewCell.imgFeatured.isHidden = false
                }
                if propertyObj.isLike == 1 {
                    vehicleTableViewCell.imgHeart.setImage(UIImage(named: "heartfilled"), for: .normal)
                }
                else {
                    vehicleTableViewCell.imgHeart.setImage(UIImage(named: "heart"), for: .normal)
                }
                
                return vehicleTableViewCell
            }
            
            //            else {
            //                cell.lblLocation.text = propertyObj.area
            //                cell.lblCarMake.isHidden = true
            //                cell.lblCarClass.isHidden = true
            //            }
            let cell: FavouritesViewCell = tableView.dequeueReusableCell(withIdentifier: "FavouritesViewCell", for: indexPath) as! FavouritesViewCell
            
            cell.lblLocation.text = propertyObj.area
            cell.lblCarMake.isHidden = true
            cell.lblCarClass.isHidden = true
            
            cell.lblProductName.text = propertyObj.title
            cell.lblPrice.text = "QAR " + String(describing: propertyObj.price!.decimal)
            cell.imgProduct.setImageFromUrl(urlStr: (propertyObj.adImages?.first?.imageUrl)!)
            
            cell.imgLocation.isHidden = false
           // cell.LocationImageLeadingConstraints.constant = 0
            
            cell.imgHeart.addTarget(self, action: #selector(self.AddFavoriteAction(sender:)), for: .touchUpInside)
            if propertyObj.featured == 0 {
                
                cell.btnFeatured.isHidden = true
                cell.imgFeatured.isHidden = true
                
            } else {
                
                cell.btnFeatured.isHidden = false
                cell.imgFeatured.isHidden = false
            }
            if propertyObj.isLike == 1 {
                cell.imgHeart.setImage(UIImage(named: "heartfilled"), for: .normal)
            }
            else {
                cell.imgHeart.setImage(UIImage(named: "heart"), for: .normal)
            }
            
            cell.btnViewDetail.isHidden = true
            return cell
        }
        else {
            
            let propertyObj:ProductResult = self.productObj![indexPath.row]
            
            
            if selectedCategory == CategoryType.VEHICLES {
                
                let vehicleTableViewCell: VehicleTableViewCell = tableView.dequeueReusableCell(withIdentifier: "VehicleTableViewCell", for: indexPath) as! VehicleTableViewCell
                
                vehicleTableViewCell.lblProductName.text = propertyObj.title
                vehicleTableViewCell.lblPrice.text = "QAR " + String(describing: propertyObj.price!.decimal)
                if propertyObj.adImages?.first?.imageUrl != nil {
                    vehicleTableViewCell.imgProduct.setImageFromUrl(urlStr: (propertyObj.adImages?.first?.imageUrl)!)
                }
                
                vehicleTableViewCell.lblKm.text =  "Km " + String(describing: propertyObj.kilometer!.decimal)
                
                for i in propertyObj.adDetails! {
                    
                    if i.attributeTitle == "CAR MAKE" {
                        vehicleTableViewCell.lblCarMake.text = i.attributeValue
                        break
                    }
                }
                
                for i in propertyObj.adDetails! {
                    
                    if i.attributeTitle == "CLASS" {
                        vehicleTableViewCell.lblCarClass.text = i.attributeValue
                        break
                    }
                }
                
                for i in propertyObj.adDetails! {
                    
                    if i.attributeTitle == "صنع السيارة"{
                        vehicleTableViewCell.lblCarMake.text = i.attributeValue
                        break
                    }
                }
                
                for i in propertyObj.adDetails! {
                    
                    if i.attributeTitle == "موديل" {
                       //  if i.attributeTitle == "صف دراسي"{
                        vehicleTableViewCell.lblCarClass.text = i.attributeValue
                        break
                    }
                }
                
                vehicleTableViewCell.imgHeart.addTarget(self, action: #selector(self.AddFavoriteAction(sender:)), for: .touchUpInside)
                if propertyObj.featured == 0 {
                    
                    vehicleTableViewCell.btnFeatured.isHidden = true
                    vehicleTableViewCell.imgFeatured.isHidden = true
                    
                } else {
                    
                    vehicleTableViewCell.btnFeatured.isHidden = false
                    vehicleTableViewCell.imgFeatured.isHidden = false
                }
                if propertyObj.isLike == 1 {
                    vehicleTableViewCell.imgHeart.setImage(UIImage(named: "heartfilled"), for: .normal)
                }
                else {
                    vehicleTableViewCell.imgHeart.setImage(UIImage(named: "heart"), for: .normal)
                }
                
                return vehicleTableViewCell
            }
            
            //            else {
            //                cell.lblLocation.text = propertyObj.area
            //                cell.lblCarMake.isHidden = true
            //                cell.lblCarClass.isHidden = true
            //            }
            let cell: FavouritesViewCell = tableView.dequeueReusableCell(withIdentifier: "FavouritesViewCell", for: indexPath) as! FavouritesViewCell
            
            let filterClass = propertyObj.adDetails?.filter({ (adDetail) -> Bool in
                return adDetail.attributeTitle == "CITY"
            })
              if (filterClass?.count)! > 0 {
                if let obj = filterClass?.first {
                    if obj.attributeValue != nil {
                        cell.lblLocation.text = obj.attributeValue!
                    } else {
                        cell.lblLocation.text = "N/A"
                    }
                } else {
                    cell.lblLocation.text = "N/A"
                }
            } else {
                cell.lblLocation.text = "N/A"
            }
            
            //cell.lblLocation.text = propertyObj.area
            cell.lblCarMake.isHidden = true
            cell.lblCarClass.isHidden = true
            
            cell.lblProductName.text = propertyObj.title
            cell.lblPrice.text = "QAR " + String(describing: propertyObj.price!.decimal)
            cell.imgProduct.setImageFromUrl(urlStr: (propertyObj.adImages?.first?.imageUrl)!)
            
            cell.imgLocation.isHidden = false
           // cell.LocationImageLeadingConstraints.constant = 0
            
            cell.imgHeart.addTarget(self, action: #selector(self.AddFavoriteAction(sender:)), for: .touchUpInside)
            if propertyObj.featured == 0 {
                
                cell.btnFeatured.isHidden = true
                cell.imgFeatured.isHidden = true
                
            } else {
                
                cell.btnFeatured.isHidden = false
                cell.imgFeatured.isHidden = false
            }
            if propertyObj.isLike == 1 {
                cell.imgHeart.setImage(UIImage(named: "heartfilled"), for: .normal)
            }
            else {
                cell.imgHeart.setImage(UIImage(named: "heart"), for: .normal)
            }
            
            cell.btnViewDetail.isHidden = true
            
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("Row: \(indexPath.row)")
        
        if classified != nil {
            
            let obj:SubCategoryClassified = self.classifiedArray![indexPath.row]
            let vc = DetailViewController.instantiateFromStoryboard()
            vc.productID = obj.id!
            vc.refreshDelegate = self
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        else {
            
            let obj:ProductResult = self.productObj![indexPath.row]
            let vc = DetailViewController.instantiateFromStoryboard()
            vc.productID = obj.id!
            vc.refreshDelegate = self
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        
        
    }
    
    
}

