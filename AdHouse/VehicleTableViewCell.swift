//
//  VehicleTableViewCell.swift
//  AdHouse
//
//  Created by Admin on 31/08/2018.
//  Copyright © 2018 Abdul Muqeem. All rights reserved.
//

import UIKit

class VehicleTableViewCell: UITableViewCell {

    @IBOutlet weak var imgProduct:UIImageView!
    @IBOutlet weak var imgHeart:UIButton!
    @IBOutlet weak var lblProductName:UILabel!
    @IBOutlet weak var lblPrice:UILabel!
    @IBOutlet weak var lblCarMake:UILabel!
    @IBOutlet weak var lblKm:UILabel!
    @IBOutlet weak var lblCarClass:UILabel!
    @IBOutlet weak var btnFeatured:UIButton!
    @IBOutlet weak var mainView:UIView!
    @IBOutlet weak var imgFeatured:UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.mainView.layer.borderWidth = 0.5
        self.mainView.layer.borderColor = UIColor.lightGray.cgColor
        self.mainView.addShadow()
        
        self.setFonts()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func setFonts() {
        
        lblProductName.font = FontUtility.getFontWithAdjustedSize(size: 14, desireFontType: .Roboto_Regular)
        lblPrice.font = FontUtility.getFontWithAdjustedSize(size: 12, desireFontType: .Roboto_Regular)
        lblKm.font = FontUtility.getFontWithAdjustedSize(size: 10, desireFontType: .Roboto_Regular)
        lblCarMake.font = FontUtility.getFontWithAdjustedSize(size: 10, desireFontType: .Roboto_Regular)
        lblCarClass.font = FontUtility.getFontWithAdjustedSize(size: 10, desireFontType: .Roboto_Regular)
        btnFeatured.titleLabel?.font = FontUtility.getFontWithAdjustedSize(size: 7, desireFontType: .Roboto_Regular)
    }
    
}
