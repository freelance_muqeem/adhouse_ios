//
//  Result.swift
//
//  Created by  on 8/10/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class FavoritesResult: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let updatedAt = "updated_at"
    static let adImages = "ad_images"
    static let id = "id"
    static let adId = "ad_id"
    static let createdAt = "created_at"
    static let ads = "ads"
    static let userId = "user_id"
  }

  // MARK: Properties
  public var updatedAt: String?
  public var adImages: [AdImages]?
  public var id: Int?
  public var adId: Int?
  public var createdAt: String?
  public var ads: Ads?
  public var userId: Int?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
    public convenience init(object: JSON) {
        self.init(json: object)
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
    updatedAt = json[SerializationKeys.updatedAt].string
    if let items = json[SerializationKeys.adImages].array { adImages = items.map { AdImages(json: $0) } }
    id = json[SerializationKeys.id].int
    adId = json[SerializationKeys.adId].int
    createdAt = json[SerializationKeys.createdAt].string
    ads = Ads(json: json[SerializationKeys.ads])
    userId = json[SerializationKeys.userId].int
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = updatedAt { dictionary[SerializationKeys.updatedAt] = value }
    if let value = adImages { dictionary[SerializationKeys.adImages] = value.map { $0.dictionaryRepresentation() } }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = adId { dictionary[SerializationKeys.adId] = value }
    if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
    if let value = ads { dictionary[SerializationKeys.ads] = value.dictionaryRepresentation() }
    if let value = userId { dictionary[SerializationKeys.userId] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.updatedAt = aDecoder.decodeObject(forKey: SerializationKeys.updatedAt) as? String
    self.adImages = aDecoder.decodeObject(forKey: SerializationKeys.adImages) as? [AdImages]
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
    self.adId = aDecoder.decodeObject(forKey: SerializationKeys.adId) as? Int
    self.createdAt = aDecoder.decodeObject(forKey: SerializationKeys.createdAt) as? String
    self.ads = aDecoder.decodeObject(forKey: SerializationKeys.ads) as? Ads
    self.userId = aDecoder.decodeObject(forKey: SerializationKeys.userId) as? Int
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(updatedAt, forKey: SerializationKeys.updatedAt)
    aCoder.encode(adImages, forKey: SerializationKeys.adImages)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(adId, forKey: SerializationKeys.adId)
    aCoder.encode(createdAt, forKey: SerializationKeys.createdAt)
    aCoder.encode(ads, forKey: SerializationKeys.ads)
    aCoder.encode(userId, forKey: SerializationKeys.userId)
  }

}
