//
//  FilterViewController.swift
//  AdHouse
//
//  Created by  on 7/27/17.
//  Copyright © 2017 . All rights reserved.
//

import UIKit
import DropDown

enum FilterType {
    
    case TextFieldType
    case ButtonType
}

extension FilterViewController:CategoryDelegate {
    
    func didSelectCategory(position: Int) {
        
        let count = self.attributesArray?.count
        
        
        for index in 0..<count! {
            let indexPath = IndexPath(row: index+1, section: 0)
            if let cell = self.tblView.cellForRow(at: indexPath) as? FilterDropDownCell {
                cell.txtField.text = ""
            }
            if let cell = self.tblView.cellForRow(at: indexPath) as? FilterTextFieldCell {
                cell.txtField.text = ""
            }
        }
        
        
        self.tblView.reloadData()
        
        self.getFields(position: position)
        //        if position == 2 {
        self.tblView.reloadData()
        self.getSubCategories(parentId: (self.categoriesArray?[position].id!)!)
        //        }
    }
    
    func didSelectSubCategory(position: Int) {
        
        self.getFieldsForSubCat(position: position)
    }
    
    func didSelectCountry(position: Int) {
        
    }
}

//extension FilterViewController:filterHeaderDelegate {
//
//    func changeCategoryType(type: CategoryType) {
//
//        self.selectedCategoryType = type
//        self.tblView.reloadData()
//        print(self.selectedCategoryType!)
//    }
//}


class FilterViewController: UIViewController , UITableViewDelegate , UITableViewDataSource {
    
    
    class func instantiateFromStoryboard() -> FilterViewController {
        let storyboard = UIStoryboard(name: "SideMenu", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! FilterViewController
    }
    
    @IBOutlet weak var tblView:UITableView!
    
    let dropDownCountry = DropDown()
    
    var type:FilterType?
    var leftBtn:UIButton?
    var selectedCategoryType:CategoryType?
    
    var attributesArray:[AttributesResult]? = [AttributesResult]()
    var categoriesArray:[CategoryResult]? = [CategoryResult]()
    var subCategoriesArray:[CategoryResult]? = [CategoryResult]()
    var countriesArray:[CityResult]? = [CityResult]()
    
    var selectedParentIds = [Int]()
    var selectedTitles = [String]()
    
    var dropDownList:[[Dropdownlist]]? = [[Dropdownlist]]()
    var parentarrtibuteID:Int!
    var parentAttributeValueID:Int!
    var DropDownIDs:[Int] = [Int]()
    var selectedDropDownIDs:[Int] = [Int]()
    
    var delegate: FilterDelegate?
    var txtTitle: String?
    var selectedLang: [String]!
    
    var paramsHolder = ParamsHolder()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        paramsHolder.params = [:]
        self.title = NSLocalizedString("Filter", comment: "")
        self.placeBackBtn()
        selectedLang =  UserDefaults.standard.object(forKey: "AppleLanguages") as? [String] ?? [String]()
        self.type = FilterType.TextFieldType
        
        self.selectCountry()
        
        //Register Nib
        
        let headerNib = UINib.init(nibName: "FilterHeaderCell", bundle: nil)
        tblView.register(headerNib, forCellReuseIdentifier: "FilterHeaderCell")
        
        let dropdown = UINib.init(nibName: "FilterDropDownCell", bundle: nil)
        tblView.register(dropdown, forCellReuseIdentifier: "FilterDropDownCell")
        
        let text = UINib.init(nibName: "FilterTextFieldCell", bundle: nil)
        tblView.register(text, forCellReuseIdentifier: "FilterTextFieldCell")
        
        let btn = UINib.init(nibName: "FilterButtonCell", bundle: nil)
        tblView.register(btn, forCellReuseIdentifier: "FilterButtonCell")
        
        self.getAllCategories()
        
    }
    
    //MARK:-  Back Button Code
    
    func placeBackBtn(){
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        leftBtn =  UIButton(type: .custom)
        leftBtn?.setImage(UIImage(named: "backbtn"), for: .normal)
        leftBtn?.addTarget(self, action: #selector(SearchViewController.BackBtnPressed), for: .touchUpInside)
        leftBtn?.frame = CGRect(x: 0, y:0 , width: 25, height: 25)
        let  leftBarButton: UIBarButtonItem = UIBarButtonItem(customView: leftBtn!)
        self.navigationItem.setLeftBarButton(leftBarButton, animated: true)
        
    }
    
    func BackBtnPressed(){
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func FilterButtonAction(_ sender : UIButton) {
        
        //Utility.showAlert("Alert", message: "This feature will be implemented in next module")
        
        var parameters = [String:Any]()
        
        if let cell = self.tblView.cellForRow(at: IndexPath(row:0,section:0)) as? FilterHeaderCell {
            if cell.cordinates == nil {
                Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Please fill all Fields", comment: ""))
                return
            }
            else {
                parameters.updateValue(cell.txtLocation.text!, forKey: "area")
                parameters.updateValue("\(cell.cordinates.coordinate.latitude)", forKey: "latitude")
                parameters.updateValue("\(cell.cordinates.coordinate.longitude)", forKey: "longitude")
            }
            
            if (cell.txtTitle.text?.isEmptyOrWhitespace())! {
                Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Please fill all Fields", comment: ""))
                return
            }
            else {
                parameters.updateValue(cell.txtTitle.text!, forKey: "title")
            }
            
            if (cell.txtCategory.text?.isEmptyOrWhitespace())! {
                Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Please fill all Fields", comment: ""))
                return
            }
            else {
                parameters.updateValue(cell.selectedCatID!, forKey: "category_id")
            }
            
            if cell.txtCategory.text == "classified" {
                if (cell.txtSubCategory.text?.isEmptyOrWhitespace())! {
                    Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Please fill all Fields", comment: ""))
                    return
                }
                else {
                    parameters.updateValue(cell.selectedSubCatID!, forKey: "sub_category_id")
                }
            }
            parameters.updateValue(cell.lblMaxPrice.text!, forKey: "price")
            parameters.updateValue(cell.lblMinPrice.text!, forKey: "price_min")
            // parameters.updateValue(cell.slider.maximumValue, forKey: "price")
            /*
             if (cell.slider.maximumValue) {
             Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Please fill all Fields", comment: ""))
             return
             }
             else {
             parameters.updateValue(cell.slider.maximumValue, forKey: "price")
             }
             */
            
            if (cell.txtFeature.text?.isEmptyOrWhitespace())! {
                Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Please fill all Fields", comment: ""))
                return
            }
            else {
                if cell.txtFeature.text == "Yes" {
                    parameters.updateValue(1, forKey: "featured")
                }
                else {
                    parameters.updateValue(0, forKey: "featured")
                }
            }
            
        }
        
        for (index, item) in (self.attributesArray?.enumerated())! {
            if let cell = self.tblView.cellForRow(at: IndexPath(row:index+1,section:0)) as? FilterDropDownCell {
                if (cell.txtField.text?.isEmptyOrWhitespace())! {
                    Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Please fill all Fields", comment: ""))
                    return
                }
                parameters.updateValue(cell.txtField.text!, forKey: "\(String(describing: item.id!))")
                continue
            }
            if let cell = self.tblView.cellForRow(at: IndexPath(row:index+1,section:0)) as? TextFieldCell {
                if (cell.txtField.text?.isEmptyOrWhitespace())! {
                    Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Please fill all Fields", comment: ""))
                    return
                }
                parameters.updateValue(cell.txtField.text!, forKey: "\(String(describing: item.id!))")
            }
        }
        parameters.updateValue((Singleton.sharedInstance.CurrentUser?.id)!, forKey: "user_id")
        
        print("Parameters: \(parameters)")
        
        
        /*
         Alert.showLoader(message: "")
         
         parameters.updateValue((Singleton.sharedInstance.CurrentUser?.id)!, forKey: "user_id")
         
         print("Parameters: \(parameters)")
         
         AdServices.SubmitAd(param: parameters, images: images, completionHandler: {(status, response, error) in
         
         if !status {
         if error != nil {
         print("Error: \((error as! Error).localizedDescription)")
         return
         }
         let msg = response?["Message"].stringValue
         print("Message: \(String(describing: msg))")
         return
         }
         
         print("Main")
         print(response!)
         
         print("SUCCESS")
         
         self.delegate?.submitRefresh()
         self.navigationController?.popViewController(animated: true)
         self.delegate?.submitRefresh()
         
         //            self.perform(#selector(self.performAction), with: nil, afterDelay: 3)
         
         Alert.showAlert(title: "Alert", message:"Ad Posted Successfully")
         
         })
         */
    }
    
    //MARK:-  Get Fields
    
    func getFields(position:Int) {
        
        self.view.endEditing(true)
        Alert.showLoader(message: "")
        
        var param = ["category_id": String(describing: self.categoriesArray![position].id!)]
        
        if !isLanguageEnglish() {
            param.updateValue("ar", forKey: "lang")
        } else {
            param.updateValue("en", forKey: "lang")
        }
        
        CategoryServices.GetAttributes(param:param, completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    print("Error: \((error as! Error).localizedDescription)")
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                return
            }
            
            print(response!)
            
            print("SUCCESS")
            Alert.hideLoader()
            self.attributesArray?.removeAll()
            self.selectedParentIds.removeAll()
            self.selectedTitles.removeAll()
            self.paramsHolder.attribsArray.removeAll()
            
            let adsResponseArray = response?["Result"].arrayValue
            print(adsResponseArray!)
            self.selectedParentIds.append(0)
            self.selectedTitles.append("")
            self.selectedParentIds.append(0)
            self.selectedTitles.append("")
            
            for resultObj in adsResponseArray! {
                let obj =  AttributesResult(json: resultObj)
                self.attributesArray?.append(obj)
                self.selectedParentIds.append(0)
                self.selectedTitles.append("")
                self.paramsHolder.attribsArray.append( ["id":0,"value":"","selection_id":0])
                
                if (obj.attributeType == "dropdown"){
                    self.dropDownList?.append(obj.dropdownlist!)
                }
            }
            
            self.tblView.reloadData()
        })
    }
    
    //MARK:-  Get Fields
    
    func getFieldsForSubCat(position:Int) {
        
        self.view.endEditing(true)
        Alert.showLoader(message: "")
        
        var param = ["category_id": String(describing: self.subCategoriesArray![position].id!)]
        
        if !isLanguageEnglish() {
            param.updateValue("ar", forKey: "lang")
        } else {
            param.updateValue("en", forKey: "lang")
        }
        
        
        CategoryServices.GetAttributes(param:param, completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    print("Error: \((error as! Error).localizedDescription)")
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                return
            }
            
            print(response!)
            
            print("SUCCESS")
            Alert.hideLoader()
            self.attributesArray?.removeAll()
            self.selectedParentIds.removeAll()
            self.selectedTitles.removeAll()
            self.paramsHolder.attribsArray.removeAll()
            
            let adsResponseArray = response?["Result"].arrayValue
            print(adsResponseArray!)
            self.selectedParentIds.append(0)
            self.selectedTitles.append("")
            self.selectedParentIds.append(0)
            self.selectedTitles.append("")
            self.paramsHolder.params.updateValue(self.subCategoriesArray![position].id ?? 0, forKey: "sub_category_id")
            
            for resultObj in adsResponseArray! {
                let obj =  AttributesResult(json: resultObj)
                self.attributesArray?.append(obj)
                self.selectedParentIds.append(0)
                self.selectedTitles.append("")
                self.paramsHolder.attribsArray.append( ["id":0,"value":"","selection_id":0])
            }
            
            self.tblView.reloadData()
        })
    }
    
    //MARK:-  Get All Countries
    
    func selectCountry() {
        
        Alert.showLoader(message: "")
        
        var param = [String:String]()
        
        if !isLanguageEnglish() {
            param.updateValue("ar", forKey: "lang")
        } else {
            param.updateValue("en", forKey: "lang")
        }
        
        HelpAndAboutServices.Country(param: param , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    print("Error: \((error as! Error).localizedDescription)")
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                return
            }
            
            print("Main")
            print(response!)
            
            print("SUCCESS")
            
            self.countriesArray?.removeAll()
            
            let countryResponseArray = response?["Result"].arrayValue
            for resultObj in countryResponseArray! {
                let obj =  CityResult(json: resultObj)
                self.countriesArray?.append(obj)
            }
            
            var titles: [String] = [String]()
            for result in self.countriesArray! {
                print("Title \(String(describing: result.title))")
                titles.append(result.title!)
            }
            
        })
        
    }
    
    //MARK:-  Get All Categories
    
    func getAllCategories() {
        
        self.view.endEditing(true)
        Alert.showLoader(message: "")
        
        var param = [String:String]()
        
        if !isLanguageEnglish() {
            param.updateValue("ar", forKey: "lang")
        } else {
            param.updateValue("en", forKey: "lang")
        }
        
        CategoryServices.GetCategories(param: param, completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    print("Error: \((error as! Error).localizedDescription)")
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                return
            }
            
            print(response!)
            print("SUCCESS")
            
            self.categoriesArray?.removeAll()
            
            let adsResponseArray = response?["Result"].arrayValue
            print(adsResponseArray!)
            
            for resultObj in adsResponseArray! {
                let obj =  CategoryResult(json: resultObj)
                self.categoriesArray?.append(obj)
            }
            
            self.tblView.reloadData()
        })
    }
    
    //MARK:-  Get Sub Categories
    
    func getSubCategories(parentId: Int) {
        
        self.view.endEditing(true)
        Alert.showLoader(message: "")
        
        var param = [String:String]()
        
        if !isLanguageEnglish() {
            param.updateValue("ar", forKey: "lang")
        } else {
            param.updateValue("en", forKey: "lang")
        }
        
        param.updateValue(String(parentId) , forKey: "category_id")
        CategoryServices.GetSubCategories(param:param, completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    print("Error: \((error as! Error).localizedDescription)")
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                return
            }
            
            print(param)
            print(response!)
            print("SUCCESS")
            
            self.subCategoriesArray?.removeAll()
            
            let adsResponseArray = response?["Result"].arrayValue
            print(adsResponseArray!)
            
            for resultObj in adsResponseArray! {
                let obj =  CategoryResult(json: resultObj)
                self.subCategoriesArray?.append(obj)
            }
            if self.subCategoriesArray!.count > 0 {
                self.getFieldsForSubCat(position: 0)
            }
            self.tblView.reloadData()
            
        })
    }
    
    //MARk:- Algorithm
    
    func algorithm(postion:Int) {
        
        for i in 0..<self.dropDownList!.count{
            self.parentarrtibuteID = dropDownList![i][postion].parent_arrtibuteId!
            self.parentAttributeValueID = dropDownList![i][postion].parent_attribute_value_Id!
            
            if (self.parentarrtibuteID != 0 || self.parentAttributeValueID != 0){
                
                for j in 0..<self.dropDownList!.count{
                    for k in 0..<self.dropDownList![j].count{
                        
                        if (self.dropDownList![j][k].parent_arrtibuteId! == self.parentarrtibuteID){
                            if (self.dropDownList![j][k].parent_attribute_value_Id! != self.parentAttributeValueID){
                                
                            }
                            
                            
                        }
                    }
                }
            }
        }
    }
    
    //MARK:- TableView Delegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 0 {
            if subCategoriesArray?.count == 0 {
                return GISTUtility.convertToRatio(530)//460
            }
            return GISTUtility.convertToRatio(600)//530
            //            if selectedCategoryType == .CLASSIFIED {
            //
            //                return GISTUtility.convertToRatio(455)
            //
            //            } else if (selectedCategoryType == .PROPERTY) {
            //
            //                return GISTUtility.convertToRatio(382)
            //
            //            } else if (selectedCategoryType == .VEHICLES) {
            //
            //                return GISTUtility.convertToRatio(382)
            //
            //            } else{
            //
            //                return GISTUtility.convertToRatio(382)
            //
            //            }
            
            
        } else if indexPath.row == (self.attributesArray?.count)! + 2 {
            
            return GISTUtility.convertToRatio(55)
            
        } else {
            
            return GISTUtility.convertToRatio(75)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return (self.attributesArray?.count)! + 2
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if indexPath.row == 0 {
            
            let headerCell:FilterHeaderCell = tableView.dequeueReusableCell(withIdentifier: "FilterHeaderCell") as! FilterHeaderCell
            headerCell.delegate = self
            headerCell.paramHelper = paramsHolder
            //            headerCell.filterDelegate = self
            
            if self.txtTitle != nil {
                headerCell.txtTitle.text = self.txtTitle!
            }
            var ids: [Int] = [Int]()
            var titles: [String] = [String]()
            var arrMinPrice = [String]()
            var arrMaxPrice = [String]()
            
            for result in self.categoriesArray! {
                //--ww print("Title \(String(describing: result.name))")
                ids.append(result.id!)
                titles.append(result.name!)
                arrMaxPrice.append(String(describing: result.max_price!))
                arrMinPrice.append(String(describing: result.min_price!))
            }
            headerCell.categoryIDs = ids
            headerCell.categoryDropDown.dataSource = titles
            headerCell.arrMaxPrice = arrMaxPrice
            headerCell.arrMinPrice = arrMinPrice
            
            var subCatids: [Int] = [Int]()
            var subCatTitles: [String] = [String]()
            for result in self.subCategoriesArray! {
                print("Title \(String(describing: result.name))")
                subCatids.append(result.id!)
                subCatTitles.append(result.name!)
            }
            headerCell.subCatIDs = subCatids
            headerCell.subCategoryDropDown.dataSource = subCatTitles
            
            var countryIds: [Int] = [Int]()
            var countryTitles: [String] = [String]()
            for result in self.countriesArray! {
                print("Country Name \(String(describing: result.title!))")
                countryIds.append(result.id!)
                countryTitles.append(result.title!)
            }
//            countryIds = [1, 2, 3]
//            countryTitles = ["a", "b", "c"]
            headerCell.countryIDs = countryIds
            headerCell.countryDropDown.dataSource = countryTitles
            
            if titles.count > 0 {
                headerCell.txtCategory.placeholder = titles.first!
            }
            
            if subCatTitles.count > 0
            {
                headerCell.txtSubCategory.placeholder = subCatTitles.first!
                headerCell.heightSubCategoryConstraint.constant = 17
                headerCell.heightSubCategoryView.constant = 40
                headerCell.subCategoryView.isHidden = false
            }
            else {
                headerCell.txtSubCategory.placeholder = ""
                headerCell.txtSubCategory.text = ""
                headerCell.heightSubCategoryConstraint.constant = 0
                headerCell.heightSubCategoryView.constant = 0
                headerCell.subCategoryView.isHidden = true
            }
            
            if countryTitles.count > 0 {
                headerCell.txtCountry.placeholder = countryTitles.first!
            }
            
            return headerCell
        }
        else if indexPath.row == (self.attributesArray?.count)! + 1 {
            
            let buttonCell:FilterButtonCell = tableView.dequeueReusableCell(withIdentifier: "FilterButtonCell") as! FilterButtonCell
            buttonCell.btnFilter.addTarget(self, action: #selector(self.FilterAction(sender:)), for: .touchUpInside)
            return buttonCell
        }
        else {
            
            let obj = self.attributesArray?[indexPath.row-1]
            paramsHolder.attribsArray[indexPath.row-1]["id"] = obj!.id!
            switch obj?.attributeType {
                
            case "dropdown"?:
                
                let dropdownCell : FilterDropDownCell = tableView.dequeueReusableCell(withIdentifier: "FilterDropDownCell") as! FilterDropDownCell
                dropdownCell.lblName.text = obj?.attributeName!
                
                var titles: [String] = [String]()
                var parent_arrtibuteIDs = [Int]()
                var ids = [Int]()
                
                for result in (obj?.dropdownlist)! {
                    titles.append(result.value!)
                    ids.append(result.id!)
                    parent_arrtibuteIDs.append(result.parent_arrtibuteId!)
                    self.DropDownIDs.append(result.id!)
                }
                dropdownCell.selectedIDs = ids
                dropdownCell.dropDown.dataSource = titles
                dropdownCell.indexPath = indexPath
                dropdownCell.txtField.placeholder = NSLocalizedString("Select Type", comment: "")
                dropdownCell.txtField.text = selectedTitles[indexPath.row]
                //--ww  print("count" ,selectedTitles.count , selectedTitles[indexPath.row] , indexPath.row )
                
                
                dropdownCell.selectedID = { [weak self] index , id , title , itemID in
                    
                    
                    let selectedObj = self?.attributesArray?[index-1]
                    let selecteId = selectedObj?.dropdownlist![id].id
                    self?.selectedParentIds[index-1] = selecteId!
                    self?.selectedTitles[index] = title
                    dropdownCell.txtField.text = title
                    
                    //--ww      self?.paramsHolder.params.updateValue(title, forKey: "\(String(describing: selectedObj!.id!))")
                    self?.paramsHolder.attribsArray[index-1] =  ["id":selectedObj!.id! ,"value":"\(String(describing:itemID))","selection_id": itemID]
                    
                    if !(obj?.childIds?.isEmpty)!{
                        if let cell = self?.tblView.cellForRow(at: IndexPath(row:index + 1,section:0)) as? FilterDropDownCell {
                            cell.txtField.text = ""
                            self?.selectedTitles[index + 1] = ""
                            self?.paramsHolder.attribsArray[index]["value"] = ""
                            self?.paramsHolder.attribsArray[index]["selection_id"] = 0
                            
                        }
                    }
                }
                
                dropdownCell.dropDownAction = {
                    
                    let ind = indexPath.row - 2
                    let indexRow = ind > -1 ? ind : 0
                    let parentId = self.selectedParentIds[indexRow]
                    
                    if parentId == 0 && obj?.dropdownlist?[0].parent_attribute_value_Id != 0 {
                        let parentObjTitle = self.attributesArray?[ind].attributeName ?? ""
                        Alert.showAlert(title: "Alert", message: "Please select \(parentObjTitle) first")
                    }else if parentId != 0 {
                        
                        var titles = [String]()
                        var ids = [Int]()
                        var ddList = obj?.dropdownlist?.filter{ $0.parent_attribute_value_Id == parentId}
                        ddList = (ddList?.count)! > 0 ? ddList : obj?.dropdownlist!
                        for result in ddList! {
                            titles.append(result.value!)
                            ids.append(result.id!)
                        }
                        dropdownCell.dropDown.dataSource = titles
                        dropdownCell.selectedIDs = ids
                        dropdownCell.dropDown.show()
                    }else{
                        dropdownCell.dropDown.show()
                    }
                }
                
                return dropdownCell
                
            case "text"? :
                
                let textCell : FilterTextFieldCell = tableView.dequeueReusableCell(withIdentifier: "FilterTextFieldCell") as! FilterTextFieldCell
                if obj?.keyboardType == "numpad" {
                    textCell.txtField.keyboardType = .numberPad
                }
                textCell.lblName.text = obj?.attributeName!
                textCell.txtField.placeholder = obj?.attributeName
                textCell.paramHelper = paramsHolder
                textCell.itemID = obj?.id!
                return textCell
                
                
            default:
                
                let cell = UITableViewCell()
                return cell
            }
            
        }
        
    }
    
    @IBAction func FilterAction(sender: UIButton) {
        
        var arr = [[String : Any]]()
        for item in paramsHolder.attribsArray {
            if !(item["value"] as! String).isEmpty {
                arr.append(item)
            }
        }
        paramsHolder.attribsArray = arr
        
        paramsHolder.params.updateValue(paramsHolder.attribsArray.jsonString()!, forKey: "attributes")
        
        paramsHolder.params.updateValue((Singleton.sharedInstance.CurrentUser != nil) ? (Singleton.sharedInstance.CurrentUser?.id)! : -1, forKey: "user_id")
        
        
        //        if (paramsHolder.params["sub_category_id"] as! Int) == 0 {
        //            paramsHolder.params.removeValue(forKey: "sub_category_id")
        //            paramsHolder.params.removeValue(forKey: "attributes")
        //        }
        
        print(paramsHolder.params)
        
        self.navigationController?.popViewController(animated: true)
        delegate?.didFilter(param: paramsHolder.params, title: (paramsHolder.params["title"] as? String) ?? "")
        
        return
        var parameters = [String:Any]()
        var title = ""
        
        if let cell = self.tblView.cellForRow(at: IndexPath(row:0,section:0)) as? FilterHeaderCell {
            if cell.cordinates != nil {
                parameters.updateValue(cell.txtLocation.text!, forKey: "area")
                parameters.updateValue("\(cell.cordinates.coordinate.latitude)", forKey: "latitude")
                parameters.updateValue("\(cell.cordinates.coordinate.longitude)", forKey: "longitude")
                parameters.updateValue(cell.maxPrice!, forKey: "price")
                parameters.updateValue(cell.minPrice!, forKey: "price_min")
            }
            
            if !(cell.txtTitle.text?.isEmptyOrWhitespace())! {
                title = cell.txtTitle.text!
                parameters.updateValue(cell.txtTitle.text!, forKey: "title")
            }
            
            if !(cell.txtCategory.text?.isEmptyOrWhitespace())! {
                parameters.updateValue(cell.selectedCatID!, forKey: "category_id")
            }
            if cell.subCatIDs.count != 0 {
                //            if cell.txtCategory.text == "classified" {
                if !(cell.txtSubCategory.text?.isEmptyOrWhitespace())! {
                    parameters.updateValue(cell.selectedSubCatID!, forKey: "sub_category_id")
                }
            }
            //            }
            //            if cell.slider.value > 0 {
            //                parameters.updateValue(cell.slider.value, forKey: "price")
            //            }
            
            if !(cell.txtFeature.text?.isEmptyOrWhitespace())! {
                if cell.txtFeature.text == "Yes" {
                    parameters.updateValue(1, forKey: "featured")
                }
                else {
                    parameters.updateValue(0, forKey: "featured")
                }
            }
        }
        
        for (index, item) in (self.attributesArray?.enumerated())! {
            if let cell = self.tblView.cellForRow(at: IndexPath(row:index+1,section:0)) as? FilterDropDownCell {
                parameters.updateValue(cell.txtField.text!, forKey: "\(String(describing: item.id!))")
                // parameters.updateValue("\(String(describing: item.id!))", forKey: "\(String(describing: item.id!))")
                continue
            }
            if let cell = self.tblView.cellForRow(at: IndexPath(row:index+1,section:0)) as? FilterTextFieldCell {
                parameters.updateValue(cell.txtField.text!, forKey: "\(String(describing: item.id!))")
            }
        }
        
        parameters.updateValue((Singleton.sharedInstance.CurrentUser != nil) ? (Singleton.sharedInstance.CurrentUser?.id)! : -1, forKey: "user_id")
        
        
        print("Parameters: \(parameters)")
        
        
        self.navigationController?.popViewController(animated: true)
        delegate?.didFilter(param: parameters, title: title)
        /*
         Alert.showLoader(message: "")
         
         
         SearchServices.Filter(param: parameters, completionHandler: {(status, response, error) in
         
         if !status {
         if error != nil {
         print("Error: \((error as! Error).localizedDescription)")
         return
         }
         let msg = response?["Message"].stringValue
         print("Message: \(String(describing: msg))")
         return
         }
         
         print("Main")
         print(response!)
         
         print("SUCCESS")
         
         })
         */
    }
    
}

class ParamsHolder{
    var params  = [String : Any]()
    var attribsArray = [[String: Any]]()
    var imagesArray = [UIImage]()
}

