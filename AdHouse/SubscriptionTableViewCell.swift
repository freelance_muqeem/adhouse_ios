//
//  SubscriptionTableViewCell.swift
//  AdHouse
//
//  Created by maazulhaq on 27/07/2017.
//  Copyright © 2017 . All rights reserved.
//

import UIKit
import StoreKit

protocol CellUpdateDelegate {
    func cell(indexItem:Int , type:String)
}

class SubscriptionTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var individual: [Individual] = [Individual]()
    var commercial: [Commercial] = [Commercial]()
    
    var products = [SKProduct]()
    var selectedID: Int!
    
    var delegate: SubscriptionDelegate?
    var collectionViewIndex:Int!
    var cellDelegate:CellUpdateDelegate?
    
    var individualSelectedID: Int!
    var commercialselectedID: Int!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    // MARK: - UIcollectionview data source
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1;
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if self.individual.count > 0 {
            return self.individual.count
        }
        else {
            return self.commercial.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if self.individual.count > 0 {
            let obj = self.individual[indexPath.row]
            if indexPath.item == self.collectionViewIndex {
                let cell:SubscriptionSelectCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SubscriptionSelectCollectionViewCell", for: indexPath) as! SubscriptionSelectCollectionViewCell
                
                cell.lblCount.text = obj.package
                cell.lblMonths.text = NSLocalizedString("Months", comment: "")
                cell.lblCode.text = "QAR \(obj.price!.decimal)/mth"
                cell.lblFreeAds.text = "\(obj.freeAds!)"
                cell.lblMaxAds.text = "\(obj.maxAds!) \(NSLocalizedString("Ads", comment: ""))"
                cell.lblAdValidity.text = "\(obj.adValidity!) \(NSLocalizedString("days", comment: ""))"
                
                return cell
            }
            else {
                let cell:SubscriptionDeselectCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SubscriptionDeselectCollectionViewCell", for: indexPath) as! SubscriptionDeselectCollectionViewCell
                
                cell.lblCount.text = obj.package
                cell.lblMonths.text = NSLocalizedString("Months", comment: "")
                cell.lblCode.text = "QAR \(obj.price!.decimal)/mth"
                cell.lblFreeAds.text = "\(obj.freeAds!)"
                cell.lblMaxAds.text = "\(obj.maxAds!) \(NSLocalizedString("Ads", comment: ""))"
                cell.lblAdValidity.text = "\(obj.adValidity!) \(NSLocalizedString("days", comment: ""))"
                
                return cell
            }
        }
        else if self.commercial.count > 0 {
            let obj = self.commercial[indexPath.row]
            if indexPath.item == self.collectionViewIndex  {
                let cell:SubscriptionSelectCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SubscriptionSelectCollectionViewCell", for: indexPath) as! SubscriptionSelectCollectionViewCell
                
                cell.lblCount.text = obj.package
                cell.lblMonths.text = NSLocalizedString("Months", comment: "")
                cell.lblCode.text = "QAR \(obj.price!.decimal)/mth"
                cell.lblFreeAds.text = "\(obj.freeAds!)"
                cell.lblMaxAds.text = "\(obj.maxAds!) \(NSLocalizedString("Ads", comment: ""))"
                cell.lblAdValidity.text = "\(obj.adValidity!) \(NSLocalizedString("days", comment: ""))"
                
                return cell
            }
            else {
                let cell:SubscriptionDeselectCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SubscriptionDeselectCollectionViewCell", for: indexPath) as! SubscriptionDeselectCollectionViewCell
                
                cell.lblCount.text = obj.package
                cell.lblMonths.text = NSLocalizedString("Months", comment: "")
                cell.lblCode.text = "QAR \(obj.price!.decimal)/mth"
                cell.lblFreeAds.text = "\(obj.freeAds!)"
                cell.lblMaxAds.text = "\(obj.maxAds!) \(NSLocalizedString("Ads", comment: ""))"
                cell.lblAdValidity.text = "\(obj.adValidity!) \(NSLocalizedString("days", comment: ""))"
                
                return cell
            }
        }
        
        else {
            let cell = UICollectionViewCell()
            return cell
        }
        
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if self.individual.count > 0 {
            if indexPath.item == self.collectionViewIndex {
                let screenWidth =  collectionView.bounds.size.width - (10 * 2)
                let height = SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 176)
                return CGSize(width: screenWidth/3, height: height);
            }
            else {
                let screenWidth =  collectionView.bounds.size.width - (10 * 2)
                let height = SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 133)
                return CGSize(width: screenWidth/3, height: height);
            }
        }
        else if self.commercial.count > 0  {
            if indexPath.item == self.collectionViewIndex {
                let screenWidth =  collectionView.bounds.size.width - (10 * 2)
                let height = SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 176)
                return CGSize(width: screenWidth/3, height: height);
            }
            else {
                let screenWidth =  collectionView.bounds.size.width - (10 * 2)
                let height = SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 133)
                return CGSize(width: screenWidth/3, height: height);
            }
        }
        else {
            return CGSize(width: 0, height: 0)
        }
 
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        DispatchQueue.main.async {
            
            if self.individual.count > 0 {
                
                self.collectionViewIndex = indexPath.item
//                self.selectedID = self.individual[indexPath.row].id!
//                self.delegate?.didSelectSubscription(packageID: self.selectedID)
            }
            else {
                
                self.collectionViewIndex = indexPath.item
            //    self.collectionView.reloadItems(at: [indexPath])
//                self.selectedID = self.commercial[indexPath.row].id!
//                self.delegate?.didSelectSubscription(packageID: self.selectedID)
            }
            
            self.collectionView.reloadData()

        }
    }
}










//    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
//
//        DispatchQueue.main.async {
//            self.collectionView.reloadData()
//        }
//    }

/*
 
 func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
 if self.individual.count > 0 {
 let obj = self.individual[indexPath.row]
 if obj.isPopular == 1 {
 let cell:SubscriptionSelectCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SubscriptionSelectCollectionViewCell", for: indexPath) as! SubscriptionSelectCollectionViewCell
 
 cell.lblCount.text = obj.package
 cell.lblMonths.text = NSLocalizedString("Months", comment: "")
 cell.lblCode.text = "QAR \(obj.price!.decimal)/mth"
 cell.lblFreeAds.text = "\(obj.freeAds!)"
 cell.lblMaxAds.text = "\(obj.maxAds!) \(NSLocalizedString("Ads", comment: ""))"
 cell.lblAdValidity.text = "\(obj.adValidity!) \(NSLocalizedString("days", comment: ""))"
 
 return cell
 }
 else {
 let cell:SubscriptionDeselectCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SubscriptionDeselectCollectionViewCell", for: indexPath) as! SubscriptionDeselectCollectionViewCell
 
 cell.lblCount.text = obj.package
 cell.lblMonths.text = NSLocalizedString("Months", comment: "")
 cell.lblCode.text = "QAR \(obj.price!.decimal)/mth"
 cell.lblFreeAds.text = "\(obj.freeAds!)"
 cell.lblMaxAds.text = "\(obj.maxAds!) \(NSLocalizedString("Ads", comment: ""))"
 cell.lblAdValidity.text = "\(obj.adValidity!) \(NSLocalizedString("days", comment: ""))"
 
 return cell
 }
 }
 else {
 let obj = self.commercial[indexPath.row]
 if obj.isPopular == 1 {
 let cell:SubscriptionSelectCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SubscriptionSelectCollectionViewCell", for: indexPath) as! SubscriptionSelectCollectionViewCell
 
 cell.lblCount.text = obj.package
 cell.lblMonths.text = NSLocalizedString("Months", comment: "")
 cell.lblCode.text = "QAR \(obj.price!.decimal)/mth"
 cell.lblFreeAds.text = "\(obj.freeAds!)"
 cell.lblMaxAds.text = "\(obj.maxAds!) \(NSLocalizedString("Ads", comment: ""))"
 cell.lblAdValidity.text = "\(obj.adValidity!) \(NSLocalizedString("days", comment: ""))"
 
 return cell
 }
 else {
 let cell:SubscriptionDeselectCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SubscriptionDeselectCollectionViewCell", for: indexPath) as! SubscriptionDeselectCollectionViewCell
 
 cell.lblCount.text = obj.package
 cell.lblMonths.text = NSLocalizedString("Months", comment: "")
 cell.lblCode.text = "QAR \(obj.price!.decimal)/mth"
 cell.lblFreeAds.text = "\(obj.freeAds!)"
 cell.lblMaxAds.text = "\(obj.maxAds!) \(NSLocalizedString("Ads", comment: ""))"
 cell.lblAdValidity.text = "\(obj.adValidity!) \(NSLocalizedString("days", comment: ""))"
 
 return cell
 }
 }
 
 }
 
 func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
 if self.individual.count > 0 {
 if self.individual[indexPath.row].isPopular == 1 {
 let screenWidth =  collectionView.bounds.size.width - (10 * 2)
 let height = SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 176)
 return CGSize(width: screenWidth/3, height: height);
 }
 else {
 let screenWidth =  collectionView.bounds.size.width - (10 * 2)
 let height = SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 133)
 return CGSize(width: screenWidth/3, height: height);
 }
 }
 else {
 if self.commercial[indexPath.row].isPopular == 1 {
 let screenWidth =  collectionView.bounds.size.width - (10 * 2)
 let height = SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 176)
 return CGSize(width: screenWidth/3, height: height);
 }
 else {
 let screenWidth =  collectionView.bounds.size.width - (10 * 2)
 let height = SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 133)
 return CGSize(width: screenWidth/3, height: height);
 }
 }
 
 }
 
 */
