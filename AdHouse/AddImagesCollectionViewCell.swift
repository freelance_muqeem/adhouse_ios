//
//  AddImagesCollectionViewCell.swift
//  AdHouse
//
//  Created by   on 12/6/17.
//  Copyright © 2017 . All rights reserved.
//

import UIKit

class AddImagesCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var imgCancel: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
