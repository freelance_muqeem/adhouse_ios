//
//  SubscriptionDeselectCollectionViewCell.swift
//  AdHouse
//
//  Created by maazulhaq on 27/07/2017.
//  Copyright © 2017 . All rights reserved.
//

import UIKit

class SubscriptionDeselectCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet weak var lblMonths: UILabel!
    @IBOutlet weak var lblCode: UILabel!
    
    @IBOutlet weak var freeAds: UILabel!
    @IBOutlet weak var adValidity: UILabel!
    @IBOutlet weak var maxAds: UILabel!
    
    @IBOutlet weak var lblFreeAds: UILabel!
    @IBOutlet weak var lblAdValidity: UILabel!
    @IBOutlet weak var lblMaxAds: UILabel!
    
    @IBOutlet weak var backView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        backView.layer.borderColor = UIColor.lightGray.cgColor
        backView.layer.borderWidth = 1.0
    }
    
    func setFonts() {
        
        lblCount.font = FontUtility.getFontWithAdjustedSize(size: 22, desireFontType: .Roboto_Regular)
        lblMonths.font = FontUtility.getFontWithAdjustedSize(size: 15, desireFontType: .Roboto_Regular)
        lblCode.font = FontUtility.getFontWithAdjustedSize(size: 12, desireFontType: .Roboto_Regular)
        freeAds.font = FontUtility.getFontWithAdjustedSize(size: 9, desireFontType: .Roboto_Regular)
        adValidity.font = FontUtility.getFontWithAdjustedSize(size: 9, desireFontType: .Roboto_Regular)
        maxAds.font = FontUtility.getFontWithAdjustedSize(size: 9, desireFontType: .Roboto_Regular)
        lblFreeAds.font = FontUtility.getFontWithAdjustedSize(size: 9, desireFontType: .Roboto_Regular)
        lblAdValidity.font = FontUtility.getFontWithAdjustedSize(size: 9, desireFontType: .Roboto_Regular)
        lblMaxAds.font = FontUtility.getFontWithAdjustedSize(size: 9, desireFontType: .Roboto_Regular)
        
    }
}
