//
//  DetailViewController.swift
//  AdHouse
//
//  Created by maazulhaq on 26/07/2017.
//  Copyright © 2017 . All rights reserved.
//

import UIKit
import ImageSlideshow
import AlamofireImage
import Kingfisher
import MessageUI
import GooglePlaces
import GoogleMaps

struct Stack<Element> {
    fileprivate var array: [Element] = []
    
    var isEmpty: Bool {
        return array.isEmpty
    }
    
    var count: Int {
        return array.count
    }
    
    mutating func push(_ element: Element) {
        array.append(element)
    }
    
    mutating func pop() -> Element? {
        return array.popLast()
    }
    
    func peek() -> Element? {
        return array.last
    }
}

enum ProductViewType {
    case ProductInfo
    case SellerInfo
}

extension DetailViewController: SellerLocationDelegate, MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate {
    
    func location() {
        self.mapAction()
    }
    
    func email() {
        //        productObj.userDetails?.email
        if MFMailComposeViewController.canSendMail() {
            let composeVC = MFMailComposeViewController()
            composeVC.mailComposeDelegate = self
            composeVC.setToRecipients([(productObj.userDetails?.email)!])
            composeVC.setMessageBody("<p>Hey I saw your ad on AdHouse</p>", isHTML: true)
            
            present(composeVC, animated: true)
        } else {
            // show failure alert
        }
    }
    
    func phoneNumber() {
        //Create the AlertController and add Its action like button in Actionsheet
        let number = self.productObj.phone!.replacingOccurrences(of: "^0+", with: "", options: .regularExpression)
        
        let actionSheetController: UIAlertController = UIAlertController(title: "Please select", message: "", preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            print("Cancel")
        }
        actionSheetController.addAction(cancelActionButton)
        
        let callActionButton = UIAlertAction(title: "Phone (call)", style: .default)
        { _ in
            
            if let url = URL(string: "telprompt://\(number)"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
        
        actionSheetController.addAction(callActionButton)
        
        let smsActionButton = UIAlertAction(title: "Phone (sms)", style: .default)
        { _ in
            
            let composeVC = MFMessageComposeViewController()
            composeVC.messageComposeDelegate = self
            
            // Configure the fields of the interface.
            composeVC.recipients = [number]
            
            composeVC.body = "Hey I saw your ad on AdHouse"
            
            // Present the view controller modally.
            self.present(composeVC, animated: true, completion: nil)
        }
        
        actionSheetController.addAction(smsActionButton)
        
        let whatsAppActionButton = UIAlertAction(title: "WhatsApp", style: .default)
        { _ in
            let url = URL(string: "whatsapp://send?text=Hey%20I%20saw%20your%20ad%20on%20AdHouse&phone=\(number)")
            
            if UIApplication.shared.canOpenURL(url!) {
                UIApplication.shared.openURL(url!)
            }
        }
        actionSheetController.addAction(whatsAppActionButton)
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        controller.dismiss(animated: true, completion: nil)
    }
}

extension DetailViewController: NavigationDelegate {
    
    func didNavigate(productObjID: Int) {
        
        self.listDataSource = [String]()
        self.keyDataSource = [String]()
        self.productObj = nil
        
        self.detailContentViewHeightConstrain.constant = 0
        
        self.slideshow.setImageInputs([])
        self.slideshow.reloadInputViews()
        
        self.tableView.reloadData()
        
        
        ImageCache.default.clearMemoryCache()
        ImageCache.default.clearDiskCache()
        
        self.ids.push(productID!)
        self.productID = productObjID
        
        //        let vc = DetailViewController.instantiateFromStoryboard()
        //        vc.productID = productObjID
        //        self.navigationController?.pushViewController(vc, animated: true)
        
        self.GetAd()
    }
}

extension ImageSlideshow {
    
    func RemoveAllImages() {
        self.scrollViewImages.removeAll()
    }
}

extension DetailViewController: CellBtnPressedProtocol  {
    
    func disSelectClassifiedCategory(cellIndexPath: IndexPath, product: ClassifiedCategory) {
        
    }
    
    
    func viewAllBtnPressed(cellIndexPath:IndexPath){
        
        
    }
    func disSelectRowAtIndexPath(cellIndexPath:IndexPath){
        
        
    }
    
    func disSelectCategoryObject(cellIndexPath:IndexPath, product: ProductResult) {
    }
    
    func contactSellerBtnPressed(){
        
        if productObj == nil {
            return
        }
        
        if productObj.userId == Singleton.sharedInstance.CurrentUser?.id  {
            
            Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("You cannot contact your own Ad", comment: ""))
            return
            
        }
        
        let contactVc = ContactViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(contactVc, animated: true)
    }
    
}

class DetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var detailContentViewHeightConstrain: NSLayoutConstraint!
    var selectedLang: [String]!
    var address:String?
    
    class func instantiateFromStoryboard() -> DetailViewController {
        let storyboard = UIStoryboard(name: "AdsDetail", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! DetailViewController
    }
    
    @IBOutlet weak var slideshow: ImageSlideshow!
    var leftBtn:UIButton?
    var rightBtn:UIButton?
    var rightBtnSearch:UIButton?
    
    var productObj: ProductResult!
    
    var productID: Int?
    
    var listDataSource = [String]()
    var keyDataSource = [String]()
    
    var isFav = false
    var isFromFav = false
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backGroundView: UIView!
    
    var type: ProductViewType?
    
    var SellerLocation:String!
    var latitude:String!
    var longitude:String!
    var refreshDelegate: RefreshDelegate?
    
    
    var ids = Stack<Int>()
    
    var isGuestTappedContactButton:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        selectedLang =  UserDefaults.standard.object(forKey: "AppleLanguages") as? [String] ?? [String]()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black,NSAttributedStringKey.font : UIFont(name: "HelveticaNeue-Medium", size: 17)!]
        
        self.placeBackBtn()
        
        let navBackgroundImage:UIImage! = UIImage(named: "topbox")!
        self.navigationController?.navigationBar.setBackgroundImage(navBackgroundImage, for: .default)
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.type = ProductViewType.ProductInfo
        
        self.slideshow.contentScaleMode = .scaleAspectFill
        self.slideshow.zoomEnabled = true
        
        backGroundView.layer.shadowColor = UIColor.black.cgColor
        backGroundView.layer.shadowOpacity = 1
        backGroundView.layer.shadowOffset = CGSize.zero
        backGroundView.layer.shadowRadius = 4
        
        self.GetAd()
        
        // Register Nib Cells
        self.tableView.register(UINib(nibName: "DetailTitleTableViewCell", bundle: nil), forCellReuseIdentifier: String(describing: DetailTitleTableViewCell.self))
        self.tableView.register(UINib(nibName: "DetailButtonsTableViewCell", bundle: nil), forCellReuseIdentifier: String(describing: DetailButtonsTableViewCell.self))
        self.tableView.register(UINib(nibName: "DetailListTableViewCell", bundle: nil), forCellReuseIdentifier: String(describing: DetailListTableViewCell.self))
        self.tableView.register(UINib(nibName: "DetailInfoTableViewCell", bundle: nil), forCellReuseIdentifier: String(describing: DetailInfoTableViewCell.self))
        self.tableView.register(UINib(nibName: "DetailBottomTableViewCell", bundle: nil), forCellReuseIdentifier: String(describing: DetailBottomTableViewCell.self))
        self.tableView.register(UINib(nibName: "SameUserAdsCell", bundle: nil), forCellReuseIdentifier: String(describing: SameUserAdsCell.self))
        self.tableView.register(UINib(nibName: "DetailSellerInfoTableViewCell", bundle: nil), forCellReuseIdentifier: String(describing: DetailSellerInfoTableViewCell.self))
        
        
        //        self.ids.push(self.productID!)
        //        let height = 80 + 53 + CGFloat(44 * listDataSource.count) + 133 + SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 250) + SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 250)
        //        self.detailContentViewHeightConstrain.constant = height
        
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.didTap))
        slideshow.addGestureRecognizer(gestureRecognizer)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    @objc func didTap() {
        slideshow.presentFullScreenController(from: self)
    }
    
    //MARK:-  Back Button Code
    
    func placeHeartBtn() {
        
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        rightBtn =  UIButton(type: .custom)
        if self.productObj.isLike == 1 {
            rightBtn?.setImage(UIImage(named: "heartfilled"), for: .normal)
        }
        else {
            rightBtn?.setImage(UIImage(named: "heart"), for: .normal)
        }
        rightBtn?.addTarget(self, action: #selector(DetailViewController.FavoritePressed), for: .touchUpInside)
        rightBtn?.frame = CGRect(x: 20, y:0 , width: 25, height: 25)
        let rightBarButton: UIBarButtonItem = UIBarButtonItem(customView: rightBtn!)
        
        rightBtnSearch =  UIButton(type: .custom)
        rightBtnSearch?.setImage(UIImage(named: "searchbtn"), for: .normal)
        rightBtnSearch?.addTarget(self, action: #selector(DetailViewController.searchAction), for: .touchUpInside)
        rightBtnSearch?.frame = CGRect(x: 0, y:0 , width: 30, height: 30)
        let rightBarSearchButton: UIBarButtonItem = UIBarButtonItem(customView: rightBtnSearch!)
        
        self.navigationItem.rightBarButtonItems = [ rightBarSearchButton , rightBarButton ]
        
    }
    
    func placeBackBtn() {
        
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        leftBtn = UIButton(type: .custom)
        leftBtn?.setImage(UIImage(named: "backbtn"), for: .normal)
        leftBtn?.addTarget(self, action: #selector(DetailViewController.BackBtnPressed), for: .touchUpInside)
        leftBtn?.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        let leftBarButton: UIBarButtonItem = UIBarButtonItem(customView: leftBtn!)
        self.navigationItem.setLeftBarButton(leftBarButton, animated: true)
        
    }
    
    @objc func searchAction() {
        
        let searchVc = SearchViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(searchVc, animated: true)
        
    }
    
    @objc func BackBtnPressed(){
        
        if self.ids.count == 0 {
            if isFromFav {
                self.navigationController?.navigationBar.isHidden = true
            }
            self.navigationController?.popViewController(animated: true)
        }
        else {
            self.productID = self.ids.pop()
            
            self.listDataSource = [String]()
            self.keyDataSource = [String]()
            self.productObj = nil
            
            self.detailContentViewHeightConstrain.constant = 0
            
            self.slideshow.setImageInputs([])
            self.slideshow.reloadInputViews()
            
            self.tableView.reloadData()
            
            
            ImageCache.default.clearMemoryCache()
            ImageCache.default.clearDiskCache()
            
            self.GetAd()
        }
    }
    
    func mapAction() {
        
        let vc = MapViewController.instantiateFromStoryboard()
        vc.SellerLocation = productObj.userDetails?.address
        vc.latitude = Double((productObj.userDetails?.latitude)!)
        vc.longitude = Double((productObj.userDetails?.longitude)!)
        
        
        //        vc.SellerLocation = productObj.area!
        //        vc.latitude = Double((productObj.latitude)!)
        //        vc.longitude = Double((productObj.longitude)!)
        
        
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func sellerFormAction(_ sender : UIButton) {
        
        if Singleton.sharedInstance.CurrentUser == nil {
            
            Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("You have logged in as Guest Member, in order to access this feature. you need to SignUp on App", comment: ""))
            return
            
        }
        
        if productObj == nil {
            return
        }
        
        if productObj.userId == Singleton.sharedInstance.CurrentUser?.id  {
            
            Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("", comment: "You cannot contact your own Ad"))
            return
            
        }
        else {
            
            
            self.listDataSource = [String]()
            self.keyDataSource = [String]()
            self.productObj = nil
            
            self.slideshow.setImageInputs([])
            self.slideshow.reloadInputViews()
            
            self.tableView.reloadData()
            
            ImageCache.default.clearMemoryCache()
            ImageCache.default.clearDiskCache()
            
            let ContactFormVc = ContactViewController.instantiateFromStoryboard()
            self.navigationController?.pushViewController(ContactFormVc, animated: true)
        }
    }
    
    @objc func FavoritePressed() {
        
        if Singleton.sharedInstance.CurrentUser == nil {
            
            Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("You have logged in as Guest Member, in order to access this feature. you need to SignUp on App", comment: ""))
            return
            
        }
        
        if self.productObj.userId == Singleton.sharedInstance.CurrentUser?.id {
            
            Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("You cannot favorite your own ads.", comment: ""))
            return
        }
        
        if !self.isFav {
            self.isFav = !self.isFav
            
            Alert.showLoader(message: "")
            let parameters:[String:Any] = [
                "user_id": (Singleton.sharedInstance.CurrentUser!.id!),
                "ad_id":self.productObj.id!,
                "state":1-productObj.isLike!
            ]
            print("Parameter: \(parameters)")
            
            FavouriteServices.Add(param: parameters , completionHandler: {(status, response, error) in
                self.isFav = !self.isFav
                if !status {
                    if error != nil {
                        print("Error: \((error as! Error).localizedDescription)")
                        return
                    }
                    let msg = response?["Message"].stringValue
                    print("Message: \(String(describing: msg))")
                    return
                }
                
                print(response!)
                print("SUCCESS")
                
                self.refreshDelegate?.didRefresh()
                
                self.productObj.isLike = 1-self.productObj.isLike!
                self.placeHeartBtn()
                Alert.showAlert(title: "Alert", message: (response?["Message"].stringValue)!)
                
            })
        }
    }
    
    func NotificationPressed() {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 80
        case 1:
            return 53
        case 2:
            if type == ProductViewType.ProductInfo {
                return CGFloat(44 * (listDataSource.count + 2))
            }
            return 164
        case 3:
            if type == ProductViewType.ProductInfo {
                return 133
            }
            return SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 270)
        case 4:
            if type == ProductViewType.ProductInfo {
                return SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 270 + 250)
            }
            return SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 220)
        case 5:
            return SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 220)
        case 6:
            return SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 220)
        default:
            return 44
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.productObj == nil {
            return 0
        }
        else if type == ProductViewType.ProductInfo {
            return 6
        }
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "DetailTitleTableViewCell") as! DetailTitleTableViewCell
            cell.lblPrice.text = "QAR " +  "\(self.productObj.price!.decimal)"
            cell.lblTitle.text = self.productObj.title
            cell.lblViews.text = "\(self.productObj.totalViews!)"
            cell.lblLocation.text = self.productObj.area
            
            if self.productObj.isSold == 0 {
                
                cell.lblAvailable.text = "Available"
                
            } else {
                
                cell.lblAvailable.text = "Not Available"
                
            }
            
            // Date
            let serverDateFormatter: DateFormatter = {
                let result = DateFormatter()
                result.dateFormat = "yyyy-MM-dd HH:mm:ss"
                result.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone!
                return result
            }()
            
            let serverTime = productObj.createdAt!
            let localTime = serverDateFormatter.date(from: serverTime)!
            
            let localDateFormatter: DateFormatter = {
                let result = DateFormatter()
                result.dateFormat = "MMM dd, yyyy"
                return result
            }()
            
            cell.lblDate.text = localDateFormatter.string(from: localTime)
            
            //Time
            let serverTimeFormatter: DateFormatter = {
                let result = DateFormatter()
                result.dateFormat = "yyyy-MM-dd HH:mm:ss"
                result.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone!
                return result
            }()
            
            let ServerTime = productObj.createdAt!
            let LocalTime = serverTimeFormatter.date(from: ServerTime)!
            
            let localTimeFormatter: DateFormatter = {
                let result = DateFormatter()
                result.dateFormat = "hh:mm a"
                return result
            }()
            
            cell.lblTime.text = localTimeFormatter.string(from: LocalTime)
            
            
            return cell
            
        case 1:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "DetailButtonsTableViewCell") as! DetailButtonsTableViewCell
            if self.isGuestTappedContactButton == true {
                cell.SellerInfoButtonAction()
            }
            cell.btnProductInfo.addTarget(self, action: #selector(ProductInfoAction(sender:)), for: .touchUpInside)
            cell.btnSellerInfo.addTarget(self, action: #selector(SellerInfoAction(sender:)), for: .touchUpInside)
            return cell
            
        case 2:
            
            if type == ProductViewType.ProductInfo {
                let cell = tableView.dequeueReusableCell(withIdentifier: "DetailListTableViewCell") as! DetailListTableViewCell
                
                cell.listDataSource = self.listDataSource
                cell.keyDataSource = self.keyDataSource
                cell.productObj = self.productObj
                cell.getAddress(latitude: self.productObj.latitude!, longitude: self.productObj.longitude!)
                //cell.tableView.reloadData()
                return cell
            }
            else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "DetailSellerInfoTableViewCell") as! DetailSellerInfoTableViewCell
                cell.lblName.text = productObj.userDetails?.fullName
                cell.lblEmail.text = productObj.userDetails?.email
                cell.lblAddress.text = productObj.userDetails?.address
                cell.lblNumber.text = productObj.userDetails?.phone
                cell.delegate = self
                
                return cell
            }
        case 3:
            if type == ProductViewType.ProductInfo {
                let cell = tableView.dequeueReusableCell(withIdentifier: "DetailInfoTableViewCell") as! DetailInfoTableViewCell
                
                cell.txtView.text = productObj.descriptionValue
                
                return cell
            }
            else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "DetailBottomTableViewCell") as! DetailBottomTableViewCell
                
                cell.delegate = self
                cell.navigationDelegate = self
                cell.btnContactSeller.addTarget(self, action: #selector(ContactGuestAction(sender:)), for: .touchUpInside)
                let temp = productObj.categoryId!
                print(temp)
                cell.mapView.isHidden = true
                cell.mapViewHeightConstraints.constant = 0
                //cell.isFeatured = productObj.categoryId!
                cell.GetFeaturedList(catID: productObj.categoryId!)
                
                return cell
            }
        case 4:
            if type == ProductViewType.ProductInfo {
                let cell = tableView.dequeueReusableCell(withIdentifier: "DetailBottomTableViewCell")as! DetailBottomTableViewCell
                cell.delegate = self
                cell.navigationDelegate = self
                cell.btnContactSeller.addTarget(self, action: #selector(ContactGuestAction(sender:)), for: .touchUpInside)
                cell.isFeatured = productObj.categoryId!
                cell.mapView.isHidden = false
                cell.mapViewHeightConstraints.constant = 250
                cell.getMap(latitude: self.productObj.latitude!, longitude: self.productObj.longitude!)
                cell.GetFeaturedList(catID: productObj.categoryId!)
                
                
                return cell
            }
            else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "SameUserAdsCell")as! SameUserAdsCell
                cell.delegate = self
                cell.navigationDelegate = self
                cell.GetSameUserAds(userID: productObj.userId!)
                if Singleton.sharedInstance.CurrentUser?.roleId == 1 { //individual
                    cell.lblTitle.text = NSLocalizedString("SIMILAR ADS FOR THE SAME ADVERTISER", comment: "")
                } else { //comercial
                    if self.productObj.isVehicle! == 1 {
                        cell.lblTitle.text = NSLocalizedString("SIMILAR ADS FOR THE SAME ADVERTISER", comment: "")
                    } else if self.productObj.isProperty! == 1 {
                        cell.lblTitle.text = NSLocalizedString("SIMILAR ADS FOR THE SAME ADVERTISER", comment: "")
                    } else {
                        cell.lblTitle.text = NSLocalizedString("SIMILAR ADS FOR THE SAME ADVERTISER", comment: "")
                    }
                }
                
                return cell
            }
            
        case 5:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SameUserAdsCell")as! SameUserAdsCell
            cell.delegate = self
            cell.navigationDelegate = self
            if Singleton.sharedInstance.CurrentUser?.roleId == 1 { //individual
                cell.lblTitle.text = NSLocalizedString("SIMILAR ADS FOR THE SAME ADVERTISER", comment: "")
            } else { //comercial
                if self.productObj.isVehicle! == 1 {
                    cell.lblTitle.text = NSLocalizedString("SIMILAR ADS FOR THE SAME ADVERTISER", comment: "")
                } else if self.productObj.isProperty! == 1 {
                    cell.lblTitle.text = NSLocalizedString("SIMILAR ADS FOR THE SAME ADVERTISER", comment: "")
                } else {
                    cell.lblTitle.text = NSLocalizedString("SIMILAR ADS FOR THE SAME ADVERTISER", comment: "")
                }
            }
            
            cell.GetSameUserAds(userID: productObj.userId!)
            
            
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    @IBAction func ProductInfoAction(sender: UIButton) {
       
        print("Product Info")
        
        if type != ProductViewType.ProductInfo {
            self.isGuestTappedContactButton = false
            type = ProductViewType.ProductInfo
            self.tableView.reloadData()
            let height = 80 + 53 + CGFloat(44 * (listDataSource.count + 2)) + 133 + SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 270) + SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 250 + 250)
            
            self.detailContentViewHeightConstrain.constant = height
        }
        
    }
    
    @IBAction func SellerInfoAction(sender: UIButton) {
        
        print("Seller Info")

        if type != ProductViewType.SellerInfo {
            self.isGuestTappedContactButton = false
            type = ProductViewType.SellerInfo
            self.tableView.reloadData()
            let height = 80 + 53 + 164 + SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 270) + SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 250)
            
            self.detailContentViewHeightConstrain.constant = height
        }
    }
    
    @IBAction func ContactGuestAction(sender: UIButton) {
        
        print("Contact Seller Info")
        
        if type != ProductViewType.SellerInfo {
            self.isGuestTappedContactButton = true
            type = ProductViewType.SellerInfo
            self.tableView.reloadData()
         //   self.tableView.scroll(to: .top, animated: true)
            let height = 80 + 53 + 164 + SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 270) + SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 250)
            
            self.detailContentViewHeightConstrain.constant = height
        }
    }
    
    @IBAction func heartAction(_ sender : UIButton) {
    }
    
    func GetAd() {
        let userID = (Singleton.sharedInstance.CurrentUser?.id != nil) ? Singleton.sharedInstance.CurrentUser?.id : -1
        
        var parameters:[String:Any] = ["user_id": userID! , "ad_id":self.productID!]
        
        if !isLanguageEnglish() {
            parameters.updateValue("ar", forKey: "lang")
        } else {
            parameters.updateValue("en", forKey: "lang")
        }
        
        print("PARAMETERS: \(parameters)")
        Alert.showLoader(message: "")
        
        FavouriteServices.GetAd(param: parameters , completionHandler: {(status, response, error) in
            if !status {
                if error != nil {
                    print("Error: \((error as! Error).localizedDescription)")
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                return
            }
            
            print(response!)
            print("SUCCESS")
            
            let obj = ProductResult(json: (response?["Result"])!)
            
            self.productObj = obj
            
            self.title = self.productObj.title
            self.placeHeartBtn()
            
            var source:[KingfisherSource]? = [KingfisherSource]()
            
            for obj in self.productObj.adImages! {
                
                let imgURL = KingfisherSource(url: URL(string: obj.imageUrl!)!)
                source?.append(imgURL)
            }
            self.slideshow.setImageInputs(source!)
            
            for item in self.productObj.adDetails! {
                self.keyDataSource.append(item.attributeTitle!)
            }
            for item in self.productObj.adDetails! {
                self.listDataSource.append(item.attributeValue!)
            }
            
            if self.type == ProductViewType.ProductInfo {
                let height = 80 + 53 + CGFloat(44 * (self.listDataSource.count + 2)) + 133 + SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 270) + SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 250 + 250)
                self.detailContentViewHeightConstrain.constant = height
            }
            else {
                let height = 80 + 53 + 164 + SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 270) + SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 250)
                self.detailContentViewHeightConstrain.constant = height
            }
            
            self.tableView.reloadData()
            
        })
    }
    
}



