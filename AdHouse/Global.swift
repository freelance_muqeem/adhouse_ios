//
//  Global.swift
//  GIST
//
//  Created by Uzair Danish on 02/05/2016.
//  Copyright © 2016 Uzair Danish. All rights reserved.
//

import UIKit

class Global: NSObject {
    
    static let sharedInstance = Global()
    
    //PRIVATE init so that singleton class should not be reinitialized from anyother class
    fileprivate override init() {
        
    } //P.E.

} //CLS END
