//
//  Constant.swift
//  SBG_HealthCare
//
//  Created by Ahsan on 20/05/2015.
//  Copyright (c) 2015 OPCO Limited. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift

let PLACEHOLDER_USER            = "userplaceholder"
let PLACEHOLDER_IMAGE           = "pictureplaceholder"

let User_data_userDefault       = "User_data_userDefault"

let SUBSCRIPTIONUPDATED = "SupbscriptionUpdatedNotification"

let WINDOW_FRAME                = UIScreen.main.bounds
let SCREEN_SIZE                 = UIScreen.main.bounds.size
let WINDOW_WIDTH                = UIScreen.main.bounds.size.width
let WINDOW_HIEGHT               = UIScreen.main.bounds.size.height

let APP_DELEGATE                = UIApplication.shared.delegate as! AppDelegate
let UIWINDOW                    = UIApplication.shared.delegate!.window!

let USER_DEFAULTS               = UserDefaults.standard

let GLOBAL                      = Global.sharedInstance

let USER_PREFERENCES            = UserPreferences.sharedUserPreferences

let STORYBOARD: UIStoryboard    = UIStoryboard(name: "Main", bundle: nil)

//let WEB_CLIENT                  = WebClient.sharedWebClient

//let SIDE_MENU                   = SideMenu.sharedInstance

let RIGHT_SIDE_MENU_CELL_SIZE   = WINDOW_HIEGHT / CGFloat(NUMBER_OF_RIGHT_SIDE_MENU_ITEMS())
let LEFT_SIDE_MENU_CELL_SIZE    = WINDOW_HIEGHT / CGFloat(NUMBER_OF_LEFT_SIDE_MENU_ITEMS())

let RIGHT_SIDE_MENU_ITEMS: [String] = ["camera_icon", "thought_icon", "video_icon", "voice_icon", "music_icon", "location_icon", "link_icon", "friend_icon", "image_clear", "close_icon"]
let LEFT_SIDE_MENU_ITEMS: [String]  = ["image_clear", "image_clear", "image_clear", "image_clear", "music_icon", "camera_icon", "video_icon", "voice_icon", "location_icon", "close_icon"]

func NAVIGATION_CONTROLLER_WITH_IDENTIFIER(_ identifier: String) -> UINavigationController{
    return UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: identifier) as! UINavigationController
    //return STORYBOARD.instantiateViewController(withIdentifier: identifier) as! UINavigationController
}

func VIEWCONTROLLER_WITH_IDENTIFIER(_ identifier: String) -> UIViewController{
    return UIStoryboard.init(name: "SideMenu", bundle: nil).instantiateViewController(withIdentifier: identifier)
    //return STORYBOARD.instantiateViewController(withIdentifier: identifier)
}

func NUMBER_OF_RIGHT_SIDE_MENU_ITEMS() -> Int {
    return RIGHT_SIDE_MENU_ITEMS.count
}

func NUMBER_OF_LEFT_SIDE_MENU_ITEMS() -> Int {
    return LEFT_SIDE_MENU_ITEMS.count
}

func isLanguageEnglish() -> Bool {
    let selectedLang = UserDefaults.standard.object(forKey: "AppleLanguages") as? [String] ?? [String]()
    let lang = selectedLang[0].prefix(2)
    if lang == "en-US" {
        return true
    } else if lang == "en" {
        return true
    } else {
        return false
    }
}
