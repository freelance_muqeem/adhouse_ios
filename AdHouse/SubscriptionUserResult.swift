//
//  Result.swift
//
//  Created by  on 8/28/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class SubscriptionUserResult: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let freeAds = "free_ads"
    static let status = "status"
    static let packageId = "package_id"
    static let id = "id"
    static let subscriptionDuration = "subscription_duration"
    static let expiredTime = "expired_time"
    static let paymentResponse = "payment_response"
    static let userId = "user_id"
    static let subscriptionAds = "subscription_ads"
    static let price = "price"
    static let remainingAds = "remaining_ads"
    static let remainingDays = "remaining_days"
  }

  // MARK: Properties
  public var freeAds: Int?
  public var status: Int?
  public var packageId: Int?
  public var id: Int?
  public var subscriptionDuration: Int?
  public var expiredTime: String?
  public var paymentResponse: String?
  public var userId: Int?
  public var subscriptionAds: Int?
  public var price: Int?
  public var remainingAds: Int?
  public var remainingDays: Int?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
    public convenience init(object: JSON) {
        self.init(json: object)
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
    freeAds = json[SerializationKeys.freeAds].int
    status = json[SerializationKeys.status].int
    packageId = json[SerializationKeys.packageId].int
    id = json[SerializationKeys.id].int
    subscriptionDuration = json[SerializationKeys.subscriptionDuration].int
    expiredTime = json[SerializationKeys.expiredTime].string
    paymentResponse = json[SerializationKeys.paymentResponse].string
    userId = json[SerializationKeys.userId].int
    subscriptionAds = json[SerializationKeys.subscriptionAds].int
    price = json[SerializationKeys.price].int
    remainingAds = json[SerializationKeys.remainingAds].int
    remainingDays = json[SerializationKeys.remainingDays].int
    
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = freeAds { dictionary[SerializationKeys.freeAds] = value }
    if let value = status { dictionary[SerializationKeys.status] = value }
    if let value = packageId { dictionary[SerializationKeys.packageId] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = subscriptionDuration { dictionary[SerializationKeys.subscriptionDuration] = value }
    if let value = expiredTime { dictionary[SerializationKeys.expiredTime] = value }
    if let value = paymentResponse { dictionary[SerializationKeys.paymentResponse] = value }
    if let value = userId { dictionary[SerializationKeys.userId] = value }
    if let value = subscriptionAds { dictionary[SerializationKeys.subscriptionAds] = value }
    if let value = price { dictionary[SerializationKeys.price] = value }
    if let value = remainingAds { dictionary[SerializationKeys.remainingAds] = value }
    if let value = remainingDays { dictionary[SerializationKeys.remainingDays] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.freeAds = aDecoder.decodeObject(forKey: SerializationKeys.freeAds) as? Int
    self.status = aDecoder.decodeObject(forKey: SerializationKeys.status) as? Int
    self.packageId = aDecoder.decodeObject(forKey: SerializationKeys.packageId) as? Int
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
    self.subscriptionDuration = aDecoder.decodeObject(forKey: SerializationKeys.subscriptionDuration) as? Int
    self.expiredTime = aDecoder.decodeObject(forKey: SerializationKeys.expiredTime) as? String
    self.paymentResponse = aDecoder.decodeObject(forKey: SerializationKeys.paymentResponse) as? String
    self.userId = aDecoder.decodeObject(forKey: SerializationKeys.userId) as? Int
    self.subscriptionAds = aDecoder.decodeObject(forKey: SerializationKeys.subscriptionAds) as? Int
    self.price = aDecoder.decodeObject(forKey: SerializationKeys.price) as? Int
    self.remainingAds = aDecoder.decodeObject(forKey: SerializationKeys.remainingAds) as? Int
    self.remainingDays = aDecoder.decodeObject(forKey: SerializationKeys.remainingDays) as? Int
    
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(freeAds, forKey: SerializationKeys.freeAds)
    aCoder.encode(status, forKey: SerializationKeys.status)
    aCoder.encode(packageId, forKey: SerializationKeys.packageId)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(subscriptionDuration, forKey: SerializationKeys.subscriptionDuration)
    aCoder.encode(expiredTime, forKey: SerializationKeys.expiredTime)
    aCoder.encode(paymentResponse, forKey: SerializationKeys.paymentResponse)
    aCoder.encode(userId, forKey: SerializationKeys.userId)
    aCoder.encode(subscriptionAds, forKey: SerializationKeys.subscriptionAds)
    aCoder.encode(price, forKey: SerializationKeys.price)
    aCoder.encode(remainingAds, forKey: SerializationKeys.remainingAds)
    aCoder.encode(remainingDays, forKey: SerializationKeys.remainingDays)
  }

}
