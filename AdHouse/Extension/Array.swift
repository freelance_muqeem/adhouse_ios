//
//  Array.swift
//  AdHouse
//
//  Created by   on 3/3/18.
//  Copyright © 2018 . All rights reserved.
//

import UIKit
import SwiftyJSON

extension Array {
    
    func jsonString() -> String? {
        let jsonData = try? JSONSerialization.data(withJSONObject: self, options: [])
        guard jsonData != nil else {return nil}
        let jsonString = String(data: jsonData!, encoding: .utf8)
        guard jsonString != nil else {return nil}
        return jsonString!
    }
    
}
