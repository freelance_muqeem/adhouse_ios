//
//  UIImageViewExtension.swift
//  AdHouse
//
//  Created by maazulhaq on 05/08/2017.
//  Copyright © 2017 . All rights reserved.
//

import Foundation
import Kingfisher

extension UIImageView {
    
    func setImageFromUrl(urlStr:String) {
        
        let url = URL(string: urlStr)!
        print(url)
        self.kf.indicatorType = .activity
//        self.kf.setImage(with: url,
//                         placeholder: nil,
//                         options: [.transition(.fade(1))],
//                         progressBlock: nil,
//                         completionHandler: nil)
        
        KingfisherManager.shared.retrieveImage(with: url as Resource, options: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) -> () in
            //print(image)
            self.image = image
        })
    }    
}
