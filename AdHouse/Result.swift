//
//  Result.swift
//
//  Created by  on 8/11/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class AttributesResult: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let updatedAt = "updated_at"
    static let id = "id"
    static let attributeName = "attribute_name"
    static let attributeType = "attribute_type"
    static let keyboardType = "keyboard_type"
    static let createdAt = "created_at"
    static let categoryId = "category_id"
    static let dropdownlist = "dropdownlist"
    static let childIds = "childrenIds"
  }

  // MARK: Properties
  public var updatedAt: String?
  public var id: Int?
  public var attributeName: String?
  public var attributeType: String?
  public var keyboardType: String?
  public var createdAt: String?
    public var childIds: String?
  public var categoryId: Int?
  public var dropdownlist: [Dropdownlist]?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
    public convenience init(object: JSON) {
        self.init(json: object)
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
    updatedAt = json[SerializationKeys.updatedAt].string
    id = json[SerializationKeys.id].int
    attributeName = json[SerializationKeys.attributeName].string
    attributeType = json[SerializationKeys.attributeType].string
    keyboardType = json[SerializationKeys.keyboardType].string
    createdAt = json[SerializationKeys.createdAt].string
         childIds = json[SerializationKeys.childIds].string
    categoryId = json[SerializationKeys.categoryId].int
    if let items = json[SerializationKeys.dropdownlist].array { dropdownlist = items.map { Dropdownlist(json: $0) } }
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = updatedAt { dictionary[SerializationKeys.updatedAt] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = attributeName { dictionary[SerializationKeys.attributeName] = value }
    if let value = attributeType { dictionary[SerializationKeys.attributeType] = value }
    if let value = keyboardType { dictionary[SerializationKeys.keyboardType] = value }
    if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
    if let value = categoryId { dictionary[SerializationKeys.categoryId] = value }
    if let value = childIds { dictionary[SerializationKeys.childIds] = value }
    if let value = dropdownlist { dictionary[SerializationKeys.dropdownlist] = value.map { $0.dictionaryRepresentation() } }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.updatedAt = aDecoder.decodeObject(forKey: SerializationKeys.updatedAt) as? String
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
    self.attributeName = aDecoder.decodeObject(forKey: SerializationKeys.attributeName) as? String
    self.attributeType = aDecoder.decodeObject(forKey: SerializationKeys.attributeType) as? String
    self.keyboardType = aDecoder.decodeObject(forKey: SerializationKeys.keyboardType) as? String
    self.createdAt = aDecoder.decodeObject(forKey: SerializationKeys.createdAt) as? String
    self.childIds = aDecoder.decodeObject(forKey: SerializationKeys.childIds) as? String
    self.categoryId = aDecoder.decodeObject(forKey: SerializationKeys.categoryId) as? Int
    self.dropdownlist = aDecoder.decodeObject(forKey: SerializationKeys.dropdownlist) as? [Dropdownlist]
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(updatedAt, forKey: SerializationKeys.updatedAt)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(attributeName, forKey: SerializationKeys.attributeName)
    aCoder.encode(attributeType, forKey: SerializationKeys.attributeType)
    aCoder.encode(keyboardType, forKey: SerializationKeys.keyboardType)
    aCoder.encode(createdAt, forKey: SerializationKeys.createdAt)
    aCoder.encode(categoryId, forKey: SerializationKeys.categoryId)
    aCoder.encode(dropdownlist, forKey: SerializationKeys.dropdownlist)
     aCoder.encode(childIds, forKey: SerializationKeys.childIds)
  }

}
