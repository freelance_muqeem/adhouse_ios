//
//  DetailSellerInfoTableViewCell.swift
//  AdHouse
//
//  Created by maazulhaq on 27/07/2017.
//  Copyright © 2017 . All rights reserved.
//

import UIKit

protocol SellerLocationDelegate  {
    
    func location()
    func email()
    func phoneNumber()
}

class DetailSellerInfoTableViewCell: UITableViewCell {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    
    @IBOutlet weak var btnLocation: UIButton!
    
    var delegate:SellerLocationDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setFonts()
    }

    
    @IBAction func mapLocation(_ sender : UIButton) {
        
        self.delegate?.location()
    }
    
    @IBAction func openEmail(_ sender : UIButton) {
        
        self.delegate?.email()
    }
    
    @IBAction func phoneNumber(_ sender : UIButton) {
        
        self.delegate?.phoneNumber()
    }
    
    func setFonts() {
        
        lblName.font = FontUtility.getFontWithAdjustedSize(size: 12, desireFontType: .Roboto_Regular)
        lblAddress.font = FontUtility.getFontWithAdjustedSize(size: 12, desireFontType: .Roboto_Regular)
        lblNumber.font = FontUtility.getFontWithAdjustedSize(size: 12, desireFontType: .Roboto_Regular)
        lblEmail.font = FontUtility.getFontWithAdjustedSize(size: 12, desireFontType: .Roboto_Regular)

    }
}
