//
//  FilterTextFieldCell.swift
//  AdHouse
//
//  Created by  on 8/12/17.
//  Copyright © 2017 . All rights reserved.
//

import UIKit

class FilterTextFieldCell: UITableViewCell {

    @IBOutlet weak var shadowView:UIView!
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var txtField:UITextField!
    var paramHelper : ParamsHolder!
    var itemID : Int!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        txtField.delegate = self
        self.shadowView.layer.borderWidth = 0.5
        self.shadowView.layer.borderColor = UIColor.lightGray.cgColor
        self.shadowView.addShadow()
        
    }
    
    
}
extension FilterTextFieldCell : UITextFieldDelegate{
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
         paramHelper.params.updateValue(txtField.text!, forKey: "\(String(describing: itemID))")
     //  paramHelper.attribsArray[index] =  ["id":selectedObj!.id! ,"value":"\(String(describing: selectedObj!.id!))","selection_id": selectedObj!.id!]
    }
}
