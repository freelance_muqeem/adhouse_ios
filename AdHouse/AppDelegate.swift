//
//  AppDelegate.swift
//  AdHouse
//
//  Created by  on 7/22/17.
//  Copyright © 2017 . All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import UserNotifications
import GooglePlaces
import GoogleMaps
import Fabric
import Crashlytics
import BraintreeDropIn
import Braintree
import Firebase
import FirebaseMessaging
import FirebaseInstanceID

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate , MessagingDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
       
        // Localization
        UIView.appearance().semanticContentAttribute = .forceLeftToRight
        
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
        
        
        GMSPlacesClient.provideAPIKey("AIzaSyBdauqPhRaDmT46b6O7wrAcCONJQlyv1OQ")
        GMSServices.provideAPIKey("AIzaSyApmF0fVKdhnY6fh4PSRKAD_kOcBjPhDBo")
        
//        GMSPlacesClient.provideAPIKey("AIzaSyBzTbQtJLcISc46HpYe-1g6Kwrde8j0ZjM")
//        GMSServices.provideAPIKey("AIzaSyC1TbMH1vClXndrXkNDCeQTipzgHOi6qjE")

        Fabric.with([Crashlytics.self])
        
        // PayPal
//        BTAppSwitch.setReturnURLScheme("com.ingic.adhouse.payments")
        BTAppSwitch.setReturnURLScheme("com.wamztech.adhouse.payments")
        
        self.navigateTOInitialViewController()
  
         //iOS 10 support
        if #available(iOS 10, *) {
            UNUserNotificationCenter.current().requestAuthorization(options:[.alert, .sound]){ (granted, error) in }
            application.registerForRemoteNotifications()
        }
        
        registerForRemoteNotification()
        
        return true
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("FCM TOKEN: \(fcmToken)" )
        
//        let userDefaults: UserDefaults = UserDefaults.standard
//        userDefaults.set(fcmToken, forKey:"device_token")
//        userDefaults.synchronize()
        
//        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        UserDefaults.standard.set(fcmToken, forKey: "DEVICE_TOKEN")
        if  UserManager.isUserLogin(){
            self.serviceSetDeviceToken(deviceToken: fcmToken)
        }
        
    }
    
    func registerForRemoteNotification() {

        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
        }
        
        
        UIApplication.shared.registerForRemoteNotifications()
    }
    
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("User Info = ",notification.request.content.userInfo)
        completionHandler([.alert, .badge, .sound])
    }
    
    //Called to let your app know which action was selected by the user for a given notification.
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("User Info = ",response.notification.request.content.userInfo)
        completionHandler()
    }
    
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any]) -> Bool {
//        if url.scheme?.localizedCaseInsensitiveCompare("com.ingic.adhouse.payments") == .orderedSame {
//            return BTAppSwitch.handleOpen(url, options: options)
//        }
        if ((url.scheme?.localizedCaseInsensitiveCompare("com.wamztech.adhouse.payments")) != nil) {
            return BTAppSwitch.handleOpen(url, options: options)
        }
        if ((url.scheme?.localizedCaseInsensitiveCompare("com.venmo.touch.v2")) != nil) {
            return BTAppSwitch.handleOpen(url, options: options)
        }
        
        return false
    }
    
    // If you support iOS 7 or 8, add the following method.
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        if ((url.scheme?.localizedCaseInsensitiveCompare("com.wamztech.adhouse.payments")) != nil) {
            return BTAppSwitch.handleOpen(url, sourceApplication: sourceApplication)
        }
        if ((url.scheme?.localizedCaseInsensitiveCompare("com.venmo.touch.v2")) != nil) {
            return BTAppSwitch.handleOpen(url, sourceApplication: sourceApplication)
        }
        
        return false
    }
    
    // Called when APNs has assigned the device a unique token
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
       
//        // Convert token to string
//        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
//        UserDefaults.standard.set(deviceTokenString, forKey: "DEVICE_TOKEN")
//        if  UserManager.isUserLogin(){
//            self.serviceSetDeviceToken(deviceToken: deviceTokenString)
//        }
//
//
//        // Print it to console
//        print("APNs device token: \(deviceTokenString)")
////         Alert.showAlert(title: "Alert", message: deviceTokenString)
        Messaging.messaging().apnsToken = deviceToken
    }
    
    // Called when APNs failed to register the device for push notifications
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        // Print the error to console (you should alert the user that registration failed)
        print("APNs registration failed: \(error)")
    }
    
    // Push notification received
    func application(_ application: UIApplication, didReceiveRemoteNotification data: [AnyHashable : Any]) {
        // Print notification payload data
        print("Push notification received: \(data)")
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func applicationDidReceiveMemoryWarning(_ application: UIApplication) {
        print("something")
        Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Lost Network Connection", comment: ""))
    }
    
    static func getInstatnce() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    func navigateTOInitialViewController(){
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        self.window = UIWindow(frame: UIScreen.main.bounds)
        
        let loginStatus = UserManager.isUserLogin()
        
        if loginStatus == true
        {
            let nav = UINavigationController(nibName: "MenuVC", bundle: Bundle.main)
            self.window?.rootViewController = nav
            let vc = UIStoryboard(name: "SideMenu", bundle: Bundle.main).instantiateViewController(withIdentifier: "MenuVC")
            nav.navigationBar.tintColor = UIColor.white
            nav.navigationBar.isHidden = true
            nav.pushViewController(vc, animated: true)
        }
        else{
            let vc = storyboard.instantiateViewController(withIdentifier: "MainNavVC")
            self.window?.rootViewController = vc
        }
    }
    
    func serviceSetDeviceToken(deviceToken: String){
        
        let id = (Singleton.sharedInstance.CurrentUser?.id)!
        let deviceType = "ios"
        let parameters:[String:Any] = ["user_id":id, "device_token":deviceToken, "device_type":deviceType]
        ProfileServices.UpdateDeviceToken(param: parameters, completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    print("Error: \((error as! Error).localizedDescription)")
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                return
            }
            
            print("Main")
            print(response!)
            
            print("SUCCESS")
            
        })
    
    }
}

