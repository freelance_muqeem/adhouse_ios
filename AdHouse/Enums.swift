//
//  Enums.swift
//  TripCrasher
//
//  Created by Uzair Danish on 11/01/2017.
//  Copyright © 2017 Uzair Danish. All rights reserved.
//

import Foundation

enum MenuType: Int {
    case home
    case myProfile
    case myItems
    case history
    case myChats
    case favourite
    case settings
    case about
}

enum Logout {
    case yes
    case no
}

enum DescriptionType: String {
    case tripName       = "Trip Name"
    case description    = "Description"
}

enum SelectionType: String {
    case gender       = "Gender"
    case itinerary    = "Itinerary"
}

enum LocationType {
    case origin
    case destination
}

enum CreateTripDictionary: String {
    case tripName           = "trip_name"
    case tripDescription    = "trip_detail"
    case age                = "age_bracket"
    case interest           = "trip_interest"
    case gender             = "preferred_gender"
    case itinerary          = "itinerary_id"
    case tripFrom           = "trip_from"
    case tripTo             = "trip_to"
}
