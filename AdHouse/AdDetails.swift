//
//  AdDetails.swift
//
//  Created by  on 8/16/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class AdDetails: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let updatedAt = "updated_at"
    static let attributeTitle = "attribute_title"
    static let id = "id"
    static let adId = "ad_id"
    static let createdAt = "created_at"
    static let attributeValue = "attribute_value"
    static let attributeId = "attribute_id"
    static let attributes = "attributes"
  }

  // MARK: Properties
  public var updatedAt: String?
  public var attributeTitle: String?
  public var id: Int?
  public var adId: Int?
  public var createdAt: String?
  public var attributeValue: String?
  public var attributeId: Int?
  public var attributes: AttributesResult?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
    public convenience init(object: JSON) {
        self.init(json: object)
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
    updatedAt = json[SerializationKeys.updatedAt].string
    attributeTitle = json[SerializationKeys.attributeTitle].string
    id = json[SerializationKeys.id].int
    adId = json[SerializationKeys.adId].int
    createdAt = json[SerializationKeys.createdAt].string
    attributeValue = json[SerializationKeys.attributeValue].string
    attributeId = json[SerializationKeys.attributeId].int
    attributes = AttributesResult(json: json[SerializationKeys.attributes])
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = updatedAt { dictionary[SerializationKeys.updatedAt] = value }
    if let value = attributeTitle { dictionary[SerializationKeys.attributeTitle] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = adId { dictionary[SerializationKeys.adId] = value }
    if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
    if let value = attributeValue { dictionary[SerializationKeys.attributeValue] = value }
    if let value = attributeId { dictionary[SerializationKeys.attributeId] = value }
    if let value = attributes { dictionary[SerializationKeys.attributes] = value.dictionaryRepresentation() }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.updatedAt = aDecoder.decodeObject(forKey: SerializationKeys.updatedAt) as? String
    self.attributeTitle = aDecoder.decodeObject(forKey: SerializationKeys.attributeTitle) as? String
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
    self.adId = aDecoder.decodeObject(forKey: SerializationKeys.adId) as? Int
    self.createdAt = aDecoder.decodeObject(forKey: SerializationKeys.createdAt) as? String
    self.attributeValue = aDecoder.decodeObject(forKey: SerializationKeys.attributeValue) as? String
    self.attributeId = aDecoder.decodeObject(forKey: SerializationKeys.attributeId) as? Int
    self.attributes = aDecoder.decodeObject(forKey: SerializationKeys.attributes) as? AttributesResult
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(updatedAt, forKey: SerializationKeys.updatedAt)
    aCoder.encode(attributeTitle, forKey: SerializationKeys.attributeTitle)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(adId, forKey: SerializationKeys.adId)
    aCoder.encode(createdAt, forKey: SerializationKeys.createdAt)
    aCoder.encode(attributeValue, forKey: SerializationKeys.attributeValue)
    aCoder.encode(attributeId, forKey: SerializationKeys.attributeId)
    aCoder.encode(attributes, forKey: SerializationKeys.attributes)
  }

}
