//
//  Singleton.swift
//  AdHouse
//
//  Created by maazulhaq on 04/08/2017.
//  Copyright © 2017 . All rights reserved.
//

import Foundation
import CoreData

private let singleton = Singleton()

class Singleton {
    
    var  CurrentUser:UserObject? = UserManager.getUserObjectFromUserDefaults()
    
    class var sharedInstance: Singleton{
        return singleton
    }
}
