//
//  SettingViewController.swift
//  AdHouse
//
//  Created by maazulhaq on 23/08/2017.
//  Copyright © 2017 . All rights reserved.
//

import UIKit

class SettingViewController: UIViewController {

    class func instantiateFromStoryboard() -> SettingViewController {
        let storyboard = UIStoryboard(name: "SideMenu", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! SettingViewController
    }
    
    @IBOutlet weak var languageView:UIView!
    @IBOutlet weak var lblLanguage:UILabel!
    @IBOutlet weak var switchToggle:UISwitch!
    
    
    @IBOutlet weak var passwordView:UIView!
    @IBOutlet weak var contactView:UIView!
    @IBOutlet weak var aboutView:UIView!
    @IBOutlet weak var helpView:UIView!
    @IBOutlet weak var termView:UIView!
    @IBOutlet weak var privacyView:UIView!

    
    
    var languageToggle:Bool?
    var selectedLang: [String]!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.shadowView()
        
        selectedLang =  UserDefaults.standard.object(forKey: "AppleLanguages") as? [String] ?? [String]()
        self.switchToggle.semanticContentAttribute = .forceLeftToRight
        
        if !isLanguageEnglish() {
            self.languageToggle = false
            self.switchToggle.isOn  = false
        } else {
            self.switchToggle.isOn  = true
            self.languageToggle = true
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.isHidden = true
    }
    
    func shadowView() {
        
        self.languageView.layer.borderWidth = 0.5
        self.languageView.layer.borderColor = UIColor.lightGray.cgColor
        self.languageView.addShadow()
        
        self.passwordView.layer.borderWidth = 0.5
        self.passwordView.layer.borderColor = UIColor.lightGray.cgColor
        self.passwordView.addShadow()
        
        self.contactView.layer.borderWidth = 0.5
        self.contactView.layer.borderColor = UIColor.lightGray.cgColor
        self.contactView.addShadow()
        
        self.aboutView.layer.borderWidth = 0.5
        self.aboutView.layer.borderColor = UIColor.lightGray.cgColor
        self.aboutView.addShadow()
        
        self.helpView.layer.borderWidth = 0.5
        self.helpView.layer.borderColor = UIColor.lightGray.cgColor
        self.helpView.addShadow()
        
        self.termView.layer.borderWidth = 0.5
        self.termView.layer.borderColor = UIColor.lightGray.cgColor
        self.termView.addShadow()
        
        self.privacyView.layer.borderWidth = 0.5
        self.privacyView.layer.borderColor = UIColor.lightGray.cgColor
        self.privacyView.addShadow()
        
    }
    
    
    //MARK:- Menu Action
    
    @IBAction func MenuAction(sender: UIBarButtonItem) {
        self.slideMenuController()?.toggleLeft()
    }

    //MARK:- Switch Action
    
    @IBAction func LanguageChange(_ sender: UISwitch ) {
        
        self.languageChange()
        
    }
    
    func languageChange() {
        
        if  languageToggle == false {
            
            let alertController = UIAlertController (title:NSLocalizedString("Alert", comment: "") , message: NSLocalizedString("Language has been changed. The app needs to restart to show the changes. Do you want to close Application?", comment: "") , preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .default){ (_) -> Void in
                self.languageToggle = false
                self.switchToggle.isOn = false
            }
            let doneAction = UIAlertAction(title: NSLocalizedString("Done", comment: ""), style: .default) { (_) -> Void in
                UserDefaults.standard.set(["en"], forKey: "AppleLanguages")
                UserDefaults.standard.synchronize()
                exit(0)
            }
            alertController.addAction(cancelAction)
            alertController.addAction(doneAction)
            self.present(alertController, animated: true, completion: nil)
            
        } else {
            let alertController = UIAlertController (title:NSLocalizedString("Alert", comment: "") , message: NSLocalizedString("Language has been changed. The app needs to restart to show the changes. Do you want to close Application?", comment: "") , preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .default){ (_) -> Void in
                self.languageToggle = true
                self.switchToggle.isOn = true
            }
            let doneAction = UIAlertAction(title: NSLocalizedString("Done", comment: ""), style: .default) { (_) -> Void in
                UserDefaults.standard.set(["ar"], forKey: "AppleLanguages")
                UserDefaults.standard.synchronize()
                exit(0)
            }
            alertController.addAction(cancelAction)
            alertController.addAction(doneAction)
            self.present(alertController, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func passwordAction(_ sender : UIButton) {
        
        if Singleton.sharedInstance.CurrentUser == nil {
            
            Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("You have logged in as Guest Member, in order to access this feature. you need to SignUp on App", comment: ""))
            
        } else {
            
            let vc = ChangePasswordViewController.instantiateFromStoryboard()
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
    }
    
    
    @IBAction func contactAction(_ sender : UIButton) {
        
        
        if Singleton.sharedInstance.CurrentUser == nil {
            
            Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("You have logged in as Guest Member, in order to access this feature. you need to SignUp on App", comment: ""))
            
        } else {
            
            let vc = ContactViewController.instantiateFromStoryboard()
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        
    }
    
    @IBAction func aboutAction(_ sender : UIButton) {
        
        let vc = AboutViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func helpAction(_ sender : UIButton) {
        
        let vc = HelpViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func termAction(_ sender : UIButton) {
        
        let vc = TermsViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func privacyAction(_ sender : UIButton) {
        
        let vc = PrivacyViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
        
    }

}
