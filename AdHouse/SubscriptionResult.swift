//
//  Result.swift
//
//  Created by  on 8/28/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class SubscriptionResult: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let individual = "individual"
    static let commercial = "commercial"
    static let images = "images"
    static let clientId = "client_id"
  }

  // MARK: Properties
  public var individual: [Individual]?
  public var commercial: [Commercial]?
  public var images: [Images]?
  public var clientId: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
    public convenience init(object: JSON) {
        self.init(json: object)
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
    if let items = json[SerializationKeys.individual].array { individual = items.map { Individual(json: $0) } }
    if let items = json[SerializationKeys.commercial].array { commercial = items.map { Commercial(json: $0) } }
    if let items = json[SerializationKeys.images].array { images = items.map { Images(json: $0) } }
    clientId = json[SerializationKeys.clientId].string

  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = individual { dictionary[SerializationKeys.individual] = value.map { $0.dictionaryRepresentation() } }
    if let value = commercial { dictionary[SerializationKeys.commercial] = value.map { $0.dictionaryRepresentation() } }
    if let value = images { dictionary[SerializationKeys.images] = value.map { $0.dictionaryRepresentation() } }
    if let value = clientId { dictionary[SerializationKeys.clientId] = value }

    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.individual = aDecoder.decodeObject(forKey: SerializationKeys.individual) as? [Individual]
    self.commercial = aDecoder.decodeObject(forKey: SerializationKeys.commercial) as? [Commercial]
    self.images = aDecoder.decodeObject(forKey: SerializationKeys.images) as? [Images]
    self.clientId = aDecoder.decodeObject(forKey: SerializationKeys.clientId) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(individual, forKey: SerializationKeys.individual)
    aCoder.encode(commercial, forKey: SerializationKeys.commercial)
    aCoder.encode(images, forKey: SerializationKeys.images)
    aCoder.encode(clientId, forKey: SerializationKeys.clientId)
  }

}
