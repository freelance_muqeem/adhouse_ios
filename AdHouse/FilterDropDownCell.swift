//
//  FilterDropDownCell.swift
//  AdHouse
//
//  Created by  on 8/12/17.
//  Copyright © 2017 . All rights reserved.
//

import UIKit
import DropDown

class FilterDropDownCell: UITableViewCell {

    @IBOutlet weak var shadowView:UIView!
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var txtField:UITextField!
    
     var selectedIDs:[Int] = [Int]()
     var selectedNames:[String] = [String]()
    var indexPath:IndexPath!
    let dropDown = DropDown()
    
    var dropDownAction : (()->Void)?
    var selectedID : ((_ indexPathRow : Int, _ id : Int , _ title : String , _ itemId : Int) -> Void)?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        self.shadowView.layer.borderWidth = 0.5
        self.shadowView.layer.borderColor = UIColor.lightGray.cgColor
        self.shadowView.addShadow()
        
        // Category DropDown
        dropDown.anchorView = txtField // UIView or UIBarButtonItem
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            
          //  self.txtField.text = item
          //  print("Selected item: \(item) at index: \(index)")
          //  print(self.indexPath.row)
          //  self.selectedIDs.insert(0, at: self.indexPath.row)
         //   self.selectedNames.insert(item, at: 0)
          //  print(self.selectedNames)
          //  self.txtField.text = item
            self.selectedID!(self.indexPath.row, index , item, self.selectedIDs[index])
            
        }
        
    }
    
    @IBAction func btnAction(_ sender : UIButton) {
        dropDownAction!()
      //--ww  self.dropDown.show()
        
    }

}
