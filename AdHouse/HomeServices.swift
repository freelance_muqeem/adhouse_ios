//
//  HomeServices.swift
//  AdHouse
//
//  Created by  on 8/9/17.
//  Copyright © 2017 . All rights reserved.
//

import Foundation
import SwiftyJSON

class HomeServices {
    
    //MARK:-  Block User Service Method
    
    static func Block(param:[String:Any], completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Any?)->Void) {
        
        Request.GetRequest(fromSavedUrl: ServiceApiEndPoints.blockUser, parameters: param, callback: { (response, error) in
            
            Alert.hideLoader()
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                
                Alert.showAlert(title: "Alert", message: String(describing: (error?.localizedDescription)!))
                return
            }
            
            if response?["Response"].stringValue != "2000" {
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                Alert.showAlert(title: "Alert", message: (response?["Message"].stringValue)!)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    
    //MARK:-  Get Vehicles Service Method
    
    static func Vehicle(param:[String:Any], completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Any?)->Void) {
        
        Request.GetRequest(fromSavedUrl: ServiceApiEndPoints.getVehicles, parameters: param, callback: { (response, error) in
            
            Alert.hideLoader()
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                
                Alert.showAlert(title: "Alert", message: String(describing: (error?.localizedDescription)!))
                return
            }
            
            if response?["Response"].stringValue != "2000" {
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                Alert.showAlert(title: "Alert", message: (response?["Message"].stringValue)!)
                return
            }
            completionHandler(true, response!, nil)
        })
    }

    
    //MARK:-  Get Property Service Method
    
    static func Property(param:[String:Any], completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Any?)->Void) {
        
        Request.GetRequest(fromSavedUrl: ServiceApiEndPoints.getProperty, parameters: param, callback: { (response, error) in
            
            Alert.hideLoader()
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                
                Alert.showAlert(title: "Alert", message: String(describing: (error?.localizedDescription)!))
                return
            }
            
            if response?["Response"].stringValue != "2000" {
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                Alert.showAlert(title: "Alert", message: (response?["Message"].stringValue)!)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    //MARK:-  Get MyAds Service Method
    
    static func MyAds(param:[String:Any], completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Any?)->Void) {
        
        Request.GetRequest(fromSavedUrl: ServiceApiEndPoints.myAds, parameters: param, callback: { (response, error) in
            
            Alert.hideLoader()
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                
                Alert.showAlert(title: "Alert", message: String(describing: (error?.localizedDescription)!))
                return
            }
            
            if response?["Response"].stringValue != "2000" {
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                Alert.showAlert(title: "Alert", message: (response?["Message"].stringValue)!)
                return
            }
            completionHandler(true, response!, nil)
        })
    }

    //MARK:-  Get Featured Service Method
    
    static func Featured(param:[String:Any], completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Any?)->Void) {
        
        Request.GetRequest(fromSavedUrl: ServiceApiEndPoints.feature, parameters: param, callback: { (response, error) in
            
            Alert.hideLoader()
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                
                Alert.showAlert(title: "Alert", message: String(describing: (error?.localizedDescription)!))
                return
            }
            
            if response?["Response"].stringValue != "2000" {
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                Alert.showAlert(title: "Alert", message: (response?["Message"].stringValue)!)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    
    //MARK:-  Get Classified Service Method
    
    static func Classifieds(param:[String:Any], completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Any?)->Void) {
        
        Request.GetRequest(fromSavedUrl: ServiceApiEndPoints.classifieds, parameters: param, callback: { (response, error) in
            
            Alert.hideLoader()
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                
                Alert.showAlert(title: "Alert", message: String(describing: (error?.localizedDescription)!))
                return
            }
            
            if response?["Response"].stringValue != "2000" {
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                Alert.showAlert(title: "Alert", message: (response?["Message"].stringValue)!)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    //MARK:-  Get Same User Service Method
    
    static func SameUserAds(param:[String:Any], completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Any?)->Void) {
        
        Request.GetRequest(fromSavedUrl: ServiceApiEndPoints.sameUserAds, parameters: param, callback: { (response, error) in
            
            Alert.hideLoader()
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                
                Alert.showAlert(title: "Alert", message: String(describing: (error?.localizedDescription)!))
                return
            }
            
            if response?["Response"].stringValue != "2000" {
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                Alert.showAlert(title: "Alert", message: (response?["Message"].stringValue)!)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    //MARK:-  Get All Product Service Method
    
    static func CompleteAds(param:[String:Any], completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Any?)->Void) {
        
        Request.GetRequest(fromSavedUrl: ServiceApiEndPoints.getCompleteAds, parameters: param, callback: { (response, error) in
            
            Alert.hideLoader()
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                
                Alert.showAlert(title: "Alert", message: String(describing: (error?.localizedDescription)!))
                return
            }
            
            if response?["Response"].stringValue != "2000" {
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                Alert.showAlert(title: "Alert", message: (response?["Message"].stringValue)!)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    //MARK:-  Get Category Classified Product Service Method
    
    static func ClassifiedCategory(param:[String:Any], completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Any?)->Void) {
        
        Request.GetRequest(fromSavedUrl: ServiceApiEndPoints.categoryClassified, parameters: param, callback: { (response, error) in
            
            Alert.hideLoader()
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                
                Alert.showAlert(title: "Alert", message: String(describing: (error?.localizedDescription)!))
                return
            }
            
            if response?["Response"].stringValue != "2000" {
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                Alert.showAlert(title: "Alert", message: (response?["Message"].stringValue)!)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
}

