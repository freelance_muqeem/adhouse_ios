//
//  Result.swift
//
//  Created by  on 8/11/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class CategoryResult: NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let name = "name"
        static let parentId = "parent_id"
        static let updatedAt = "updated_at"
        static let id = "id"
        static let createdAt = "created_at"
        static let max_price = "range_max"
        static let min_price = "range_min"
    }
    
    // MARK: Properties
    public var name: String?
    public var parentId: Int?
    public var updatedAt: String?
    public var id: Int?
    public var createdAt: String?
    public var max_price: Int?
    public var min_price: Int?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: JSON) {
        self.init(json: object)
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        
        name = json[SerializationKeys.name].string
        parentId = json[SerializationKeys.parentId].int
        updatedAt = json[SerializationKeys.updatedAt].string
        id = json[SerializationKeys.id].int
        createdAt = json[SerializationKeys.createdAt].string
        max_price = json[SerializationKeys.max_price].int
        min_price = json[SerializationKeys.min_price].int
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = name { dictionary[SerializationKeys.name] = value }
        if let value = parentId { dictionary[SerializationKeys.parentId] = value }
        if let value = updatedAt { dictionary[SerializationKeys.updatedAt] = value }
        if let value = id { dictionary[SerializationKeys.id] = value }
        if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
        if let value = max_price { dictionary[SerializationKeys.max_price] = value }
        if let value = min_price { dictionary[SerializationKeys.min_price] = value }
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.name = aDecoder.decodeObject(forKey: SerializationKeys.name) as? String
        self.parentId = aDecoder.decodeObject(forKey: SerializationKeys.parentId) as? Int
        self.updatedAt = aDecoder.decodeObject(forKey: SerializationKeys.updatedAt) as? String
        self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
        self.createdAt = aDecoder.decodeObject(forKey: SerializationKeys.createdAt) as? String
        self.max_price = aDecoder.decodeObject(forKey: SerializationKeys.max_price) as? Int
        self.min_price = aDecoder.decodeObject(forKey: SerializationKeys.min_price) as? Int
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: SerializationKeys.name)
        aCoder.encode(parentId, forKey: SerializationKeys.parentId)
        aCoder.encode(updatedAt, forKey: SerializationKeys.updatedAt)
        aCoder.encode(id, forKey: SerializationKeys.id)
        aCoder.encode(createdAt, forKey: SerializationKeys.createdAt)
        aCoder.encode(max_price, forKey: SerializationKeys.max_price)
        aCoder.encode(min_price, forKey: SerializationKeys.min_price)
    }
    
}
