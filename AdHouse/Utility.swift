//
//  Utility.swift
//  GIST
//
//  Created by Uzair Danish on 02/05/2016.
//  Copyright © 2016 Uzair Danish. All rights reserved.
//

import UIKit
import AVFoundation

func delay(_ delay:Double, closure:@escaping ()->()) {
    DispatchQueue.main.asyncAfter(
        deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
} //F.E.

/*class Weak<T: AnyObject> {
    weak var value : T?
    init (value: T) {
        self.value = value
    }
}*/
enum DateFormatType{
    
    case kDateFormatTypeMessage,kDateFormatTypeChat

}
class Utility: NSObject {
    //Constants
    class var SECOND:Int  {
        get {return 1;}
    } //P.E.
    
    class var MINUTE:Int {
        get {return (60 * SECOND);}
    } //P.E.
    
    class var HOUR:Int {
        get {return (60 * MINUTE);}
    } //P.E.
    
    class var DAY:Int {
        get {return (24 * HOUR);}
    } //P.E.
    
    class var MONTH:Int {
        get {return (30 * DAY);}
    } //P.E.
    
    class func isEmailAdddressValid(_ email:String)->Bool {
        let emailRegex:String="[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let predicate:NSPredicate=NSPredicate(format: "SELF MATCHES %@", emailRegex)
        
        return predicate.evaluate(with: email)
    } //F.E.
    
    class func validateURL(_ urlString:String)-> Bool {
        
        let candidateURL:URL=URL(string: urlString)!
        
        if (((candidateURL.scheme)?.characters.count)! > 0 && (candidateURL.host!).characters.count > 0) {
            return true
        }
        //--
        return false
    } //F.E.
    
    class func calculateLabelSize(_ lbl:UILabel, width:CGFloat)-> CGSize {
        let attString:NSMutableAttributedString = NSMutableAttributedString(string: lbl.text!);
        //--
        let range:NSRange = NSRange(location: 0, length: attString.length);
        //--
        attString.addAttribute(NSAttributedStringKey.font, value: lbl.font, range: range);
        
        let rect:CGRect = attString.boundingRect(with: CGSize(width: width, height: 3000), options: NSStringDrawingOptions.usesLineFragmentOrigin, context: nil)
        return CGSize(width: width, height: rect.size.height);
    } //F.E.
    
    class func showAlert(_ title:String?, message:String?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default) { _ in })
        DispatchQueue.main.async {
            Utility().topViewController()!.present(alert, animated: true){}
        }
    } //F.E.
    
    class func formateDate(_ date:Date) ->String {
        //Thu, Jan 29, 2015
        return self.formateDate(date, dateFormat: "eee, MMM dd,  YYYY");
    } //F.E.
    
    //IT IS RETURNING DATE IN ISO 8601 WITHOUT COMBINING TIME
    class func formateDateInISO(_ date:Date) ->String {
        //2015-12-15
        return self.formateDate(date, dateFormat: "YYYY-MM-dd");
    } //F.E.
    
    class func formateDate(_ date:Date, dateFormat:String) ->String {
        let formatrer:DateFormatter = DateFormatter();
        formatrer.dateFormat = dateFormat;
        
        return formatrer.string(from: date);
    } //F.E.
    
    class func dateFromString(_ str:String, dateFormat:String, timeZone:TimeZone? = nil) ->Date {
        let formatrer:DateFormatter = DateFormatter();
        formatrer.dateFormat = dateFormat;
        //--
        if (timeZone != nil) {
            formatrer.timeZone = timeZone!;
        }
        
        return formatrer.date(from: str) ?? Date();
    } //F.E.
    
    class func timeIntervaleFromCurrentDateToDate(_ date:Date) -> TimeInterval {
        return date.timeIntervalSince(Date());
    } //F.E.
    
    //Mark -Unit Conversion
    class func convertKilometerToMiles(_ kilometer:Float) -> NSNumber {
        return ceil((kilometer * 0.621371)) as NSNumber;
    } //F.E.
    class func convertFeetInchToCentimeter(_ feet:Float, inch:Float) -> Float {
        return ((((feet * 12.0) + inch)) * 2.54);
    } //F.E.
    
    class func convertCentimeterToFeetInch(_ centimeter:Float) -> (feet:Float, inch:Float) {
        
        var inch:Float = (centimeter / 2.54);
        let feet:Float = ceilf(inch / 12.0);
        //--
        inch = inch.truncatingRemainder(dividingBy: 12);
        
        return (feet,inch);
    } //F.E.
    
    class func convertPoundsToKilogram(_ pounds:Float) -> Float {
        return round(pounds / 2.20462) ;
    } //F.E.
    
    class func convertKilogramToPounds(_ kilogram:Float) -> Float {
        return kilogram * 2.20462;
    } //F.E.
    
    //MARK:- Calculate Age
    class func calculateAge (_ birthday: Date) -> NSInteger
    {
        let calendar : Calendar = Calendar.current
        let unitFlags : NSCalendar.Unit = [NSCalendar.Unit.year, NSCalendar.Unit.month, NSCalendar.Unit.day]
        let dateComponentNow : DateComponents = (calendar as NSCalendar).components(unitFlags, from: Date())
        let dateComponentBirth : DateComponents = (calendar as NSCalendar).components(unitFlags, from: birthday)
        
        if ((dateComponentNow.month! < dateComponentBirth.month!) ||
            ((dateComponentNow.month! == dateComponentBirth.month!) && (dateComponentNow.day! < dateComponentBirth.day!))
            )
        {
            return dateComponentNow.year! - dateComponentBirth.year! - 1
        }
        else {
            return dateComponentNow.year! - dateComponentBirth.year!
        }
    }//F.E.
    
    /************************  Added by Uzair Danish *********************/
    //MARK:- Added by Uzair Danish
    class func isValidUrl(_ urlString:String) -> Bool {
        
        let regexURL: String = "(http://|https://)?((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+"
        let predicate:NSPredicate = NSPredicate(format: "SELF MATCHES %@", regexURL)
        return predicate.evaluate(with: urlString)
        
    } //F.E.
    
    class func isNumeric(_ inputString: String) -> Bool {
        return Double(inputString) != nil
    } //F.E.
    
    class func isAlphabets(_ inputString: String) -> Bool {
        for chr in inputString.characters {
            if (!(chr >= "a" && chr <= "z") && !(chr >= "A" && chr <= "Z") ) {
                return false
            }
        }
        return true
    } //F.E.

    class func getPlistDictionary(_ plistName: String) -> NSMutableDictionary? {
        if let plistPath: String = Bundle.main.path(forResource: plistName, ofType: "plist") {
            return NSMutableDictionary(contentsOfFile: plistPath);
        } else {
            return nil;
        }
    } //F.E.
    
    class func getPlistValues(_ plistName: String) -> NSMutableArray? {
        if let plistPath: String = Bundle.main.path(forResource: plistName, ofType: "plist") {
            return NSMutableArray(contentsOfFile: plistPath)
        } else {
            return nil;
        }
    } //F.E.
    
    func topViewController(_ base: UIViewController? = (APP_DELEGATE).window?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(presented)
        }
        return base
    } //F.E
    
    //MARK:- Solanki Edit 
    class func resizeImage(_ image: UIImage,  targetSize: CGFloat) -> UIImage {
        
        guard (image.size.width > 1024 || image.size.height > 1024) else {
            return image;
        }
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newRect: CGRect = CGRect.zero;
        
        if(image.size.width > image.size.height) {
            newRect.size = CGSize(width: targetSize, height: targetSize * (image.size.height / image.size.width))
        } else {
            newRect.size = CGSize(width: targetSize * (image.size.width / image.size.height), height: targetSize)
        }
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newRect.size, false, 1.0)
        image.draw(in: newRect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    class func thumbnailForVideoAtURL(_ url: URL) -> UIImage? {
        
        let asset = AVAsset(url: url)
        let assetImageGenerator = AVAssetImageGenerator(asset: asset)
        assetImageGenerator.appliesPreferredTrackTransform=true
        
        var time = asset.duration
        time.value = min(time.value, 2)
        
        do {
            let imageRef = try assetImageGenerator.copyCGImage(at: time, actualTime: nil)
            return UIImage(cgImage: imageRef)
        } catch {
            print("error")
            return nil
        }
    }
    class func showSheet(_ completion:@escaping (_ row :Int)->(),buttonlist : [String],title : String)->UIAlertController{
        
        //   self._actionCompletion = completion
        
        
        let actionSheet = UIAlertController(title:title, message: "", preferredStyle:
            UIAlertControllerStyle.actionSheet)
        
        for i in 0...buttonlist.count-1 {
            
            let libButton = UIAlertAction(title:  buttonlist[i], style:.default) { (libSelected) ->  Void in
                
                completion (i)
            }
            
            actionSheet.addAction(libButton)
        }
        
        
        let cancel = UIAlertAction(title:"Cancel", style:.cancel) { (libSelected) ->  Void in
            
        }
        
        actionSheet.addAction(cancel)
        return actionSheet
    }
    class func formatDateWithDateString(_ strDate:String , formatType:DateFormatType)-> String{
        
        
        let formatter:DateFormatter = DateFormatter();
        formatter.dateFormat    = "YYYY-MM-dd HH:mm:ss";
        formatter.timeZone      = TimeZone(secondsFromGMT: 0);
        let date:Date         = formatter.date(from: strDate as String)!;
        
        return self.formatDateWithDate(date,formatType: formatType);
    }
    
    
    class func formatDateWithDate(_ date:Date,formatType:DateFormatType)-> String{
        let currDate:Date = Date();
        return self.formatDateWithStartDate(date, endDate: currDate, formatType: formatType);
    }
    class func formatDateWithStartDate(_ startDate:Date,endDate:Date,formatType:DateFormatType)-> String{
        
        let seconds:Int = 1;
        let minute:Int  = 60 * seconds;
        let hour:Int    = 60 * minute;
        let day:Int     = 24 * hour;
        let month:Int   = 30 * day;
        
        /*
         Message
         Today  : 12:37 pm
         more then a day : Tue
         more than a week: 20 Jan
         more than a year: 20/07/2014
         
         
         Chat
         Today: 12:37 PM
         more than a day : Wed 12:37 PM
         more than a week : 20 Jan 12:37 PM
         more than a year : 27/07/2014 12:37 PM
         */
        
        
        let delta:TimeInterval = endDate.timeIntervalSince(startDate);
        let formatter:DateFormatter = DateFormatter();
        formatter.timeZone = TimeZone(secondsFromGMT: NSTimeZone.local.secondsFromGMT());
        formatter.amSymbol = "AM";
        formatter.pmSymbol = "PM";
        
        
        if( delta > Double(12 * month) ){
            //more than a year
            formatter.dateFormat = ( formatType == DateFormatType.kDateFormatTypeMessage) ? "dd/MM/YYYY" : "dd/MM/YYYY hh:mm a" ;
        }
        else if( delta > Double(7 * day) ){
            //more than a week
            formatter.dateFormat = ( formatType == DateFormatType.kDateFormatTypeMessage) ? "dd MMM" : "dd MMM hh:mm a" ;
        }
        else if( delta > Double(24 * hour) ){
            //more than a day
            formatter.dateFormat = ( formatType == DateFormatType.kDateFormatTypeMessage) ? "eee" : "eee hh:mm a" ;
        }
        else{
            //today
            formatter.dateFormat = ( formatType == DateFormatType.kDateFormatTypeMessage) ? "hh:mm a" : "hh:mm a" ;
        }
        
        let rtnFormat = formatter.string(from: startDate);
        return  rtnFormat;
    }
} //CLS END

//extension Utility {
//    func topViewController(base: UIViewController? = (APP_DELEGATE).window?.rootViewController) -> UIViewController? {
//        if let nav = base as? UINavigationController {
//            return topViewController(nav.visibleViewController)
//        }
//        if let tab = base as? UITabBarController {
//            if let selected = tab.selectedViewController {
//                return topViewController(selected)
//            }
//        }
//        if let presented = base?.presentedViewController {
//            return topViewController(presented)
//        }
//        return base
//    }
//}

