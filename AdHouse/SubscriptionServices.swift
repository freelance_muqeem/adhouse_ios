//
//  SubscriptionServices.swift
//  AdHouse
//
//  Created by  on 8/28/17.
//  Copyright © 2017 . All rights reserved.
//

import Foundation
import SwiftyJSON

class SubscriptionServices {
    
    
    //MARK:-  Get User Subscription
    
    static func SubscriptionUser(param:[String:Any], completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Any?)->Void) {
        
        Request.GetRequest(fromSavedUrl: ServiceApiEndPoints.userSubscription, parameters: param, callback: { (response, error) in
            
            Alert.hideLoader()
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                
                Alert.showAlert(title: "Alert", message: String(describing: (error?.localizedDescription)!))
                return
            }
            
            if response?["Response"].stringValue != "2000" {
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
               // Alert.showAlert(title: "Alert", message: (response?["Message"].stringValue)!)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    
    //MARK:-  Get Subscription Packages
    
    static func SubscriptionPackage(param:[String:Any], completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Any?)->Void) {
        
        Request.GetRequest(fromSavedUrl: ServiceApiEndPoints.subscriptionPackages, parameters: param, callback: { (response, error) in
            
            Alert.hideLoader()
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                
                Alert.showAlert(title: "Alert", message: String(describing: (error?.localizedDescription)!))
                return
            }
            
            if response?["Response"].stringValue != "2000" {
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                // Alert.showAlert(title: "Alert", message: (response?["Message"].stringValue)!)
                return
            }
            completionHandler(true, response!, nil)
        })
    }

    //MARK:-  Post User Subscription Package
    
    static func SubscriptionUserPackage(param:[String:Any], completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Any?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.subscriptionUserPackage, parameters: param, callback: { (response, error) in
            
            //Alert.hideLoader()
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                
                Alert.showAlert(title: "Alert", message: String(describing: (error?.localizedDescription)!))
                return
            }
            
            if response?["Response"].stringValue != "2000" {
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                
                Alert.showAlert(title: "Alert", message: (response?["Message"].stringValue)!)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
    //MARK:-  Get Token Packages
    
    static func SubscriptionToken(param:[String:Any], completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Any?)->Void) {
        
        Request.GetRequest(fromSavedUrl: ServiceApiEndPoints.subscriptionToken, parameters: param, callback: { (response, error) in
            
                        Alert.hideLoader()
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                
                Alert.showAlert(title: "Alert", message: String(describing: (error?.localizedDescription)!))
                return
            }
            
            completionHandler(true, response!, nil)
        })
    }
    
}
