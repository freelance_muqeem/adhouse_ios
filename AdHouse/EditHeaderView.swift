//
//  EditHeaderView.swift
//  AdHouse
//
//  Created by  on 8/15/17.
//  Copyright © 2017 . All rights reserved.
//

import UIKit
import DropDown
import ImagePicker
import GooglePlaces

extension EditHeaderView : CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let loc = manager.location?.coordinate
        locationLatitude = (loc?.latitude)!
        locationLongitude = (loc?.longitude)!
        
        self.paramHelper.params.updateValue("\(locationLatitude!)", forKey: "latitude")
        self.paramHelper.params.updateValue("\(locationLongitude!)", forKey: "longitude")
        
        print("Latitude: \(locationLatitude!) & Longitude: \(locationLongitude!)")
        self.getAddressFromLatLon(locationLatitude!, locationLongitude!)
        self.locationManager.stopUpdatingLocation()
        
    }
    
    func didAccesToLocation() -> Bool {
        
        if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.notDetermined {
            print("Not determine your current location")
            return true
        }
        else if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse {
            print("access to location")
            return true
        }
        else {
            print("not access to location")
            DispatchQueue.main.async {
                // self.callpopUp()
            }
            return false
        }
    }
    
    //    func callpopUp() {
    //
    //        self.alertView.btnOkAction.setTitle("Settings", for: .normal)
    //        self.alertView.btnCancel.setTitle("Cancel", for: .normal)
    //        self.alertView.alertShow(image: FAILURE_IMAGE , title: "Access Permission" , msg: "TAAJ PAY needs to access your location to show your current country", id: 1)
    //        self.alertView.isHidden = false
    //
    //    }
    
    func determineMyCurrentLocation() {
        
        locationManager = CLLocationManager()
        
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    func getAddressFromLatLon(_ Latitude: Double , _ Longitude: Double) {
        
        Alert.showLoader(message: "")
        
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        
        let lat: Double = Double("\(Latitude)")!
        let lon: Double = Double("\(Longitude)")!
        
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                
                if let pm = placemarks {
                    
                    if pm.count > 0 {
                        
                        let pm = placemarks![0]
                        
                        var area:String = ""
                        var addressString : String = ""
                        
                        if pm.subLocality != nil {
                            addressString = addressString + pm.subLocality! + ", "
                            area = addressString
                        }
                        if pm.thoroughfare != nil {
                            addressString = addressString + pm.thoroughfare! + ", "
                            area = addressString
                        }
                        if pm.locality != nil {
                            addressString = addressString + pm.locality! + ", "
                            area = addressString
                        }
                        if pm.country != nil {
                            addressString = addressString + pm.country!
                            area =  addressString
                        }
                        
                        print("Current Address : \(area)")
                        self.txtLocation.text = area
                        self.paramHelper.params.updateValue(area, forKey: "area")

                        Alert.hideLoader()
                        
                    }
                }
                
        })
    }
    
    @IBAction func getCurrentLocationAction(_ sender : UIButton) {
        self.endEditing(true)
        self.didAccesToLocation()
        self.determineMyCurrentLocation()
    }
    
    
}

class EditHeaderView: UITableViewCell{
    
    //MARK:- View Outlet
    
    @IBOutlet weak var locationView:UIView!
    @IBOutlet weak var titleView:UIView!
    @IBOutlet weak var categoryView:UIView!
    @IBOutlet weak var priceView:UIView!
    @IBOutlet weak var discriptionView:UITextView!
    @IBOutlet weak var featureView:UIView!
    @IBOutlet weak var MobileView:UIView!
    @IBOutlet weak var subCategoryView:UIView!
    
    
    //MARK:- TextField Outlet
    
    @IBOutlet weak var txtLocation:UITextField!
    @IBOutlet weak var txtTitle:UITextField!
    @IBOutlet weak var txtCategory:UITextField!
    @IBOutlet weak var txtPrice:UITextField!
    @IBOutlet weak var txtDiscription:UITextView!
    @IBOutlet weak var txtFeature:UITextField!
    @IBOutlet weak var txtMobile:UITextField!
    @IBOutlet weak var txtSubCategory:UITextField!
    
    
    //MARK:- Label Outlet
    
    @IBOutlet weak var lblCategory:UILabel!
    @IBOutlet weak var lblUploadPhoto:UILabel!
    @IBOutlet weak var lblFeatured:UILabel!
    @IBOutlet weak var lblSubCategory:UILabel!
    
    //MARK:- Images Array
    var photoArray:[UIImage] = [UIImage]()
    var images:[AdImages]? = [AdImages]()
    
    @IBOutlet weak var heightSubCategoryConstraint: BaseLayoutConstraint!
    @IBOutlet weak var heightSubCategoryView: BaseLayoutConstraint!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var locationManager = CLLocationManager()
    var locationLatitude:Double?
    var locationLongitude:Double?

    //MARK:- DropDown
    
    var selectedCatID: Int?
    var selectedSubCatID: Int?
    
    let featureDropDown = DropDown()
    
    var delegate:CategoryDelegate?
    //    var typeDelegate:HeaderDelegate?
    var attribDelegate: AttribDelegate?
    
    /* Search Area */
    var placeIDs: [String] = [String]()
    var latitude: String!
    var longitude: String!
    let dropDownArea = DropDown()
    /* -- Search Area -- */
     var paramHelper : ParamsHolderEdit!
    
    var ids = ""
    var deletedIds: [Int] = [Int]()
    
    override func awakeFromNib() {
        
        txtTitle.delegate = self
        txtPrice.delegate = self
        txtMobile.delegate = self
        
        txtTitle.tag = 991
        txtPrice.tag = 992
        txtMobile.tag = 993
        
        self.heightSubCategoryConstraint.constant = 0
        self.heightSubCategoryView.constant = 0
        self.subCategoryView.isHidden = true
        
        let nib = UINib(nibName: "AddImagesCollectionViewCell", bundle: nil)
        self.collectionView.register(nib, forCellWithReuseIdentifier: "AddImagesCollectionViewCell")
        
        self.borderView()
        self.txtDiscription.placeholder = "Description"
        
        // Feature DropDown
        featureDropDown.anchorView = txtFeature // UIView or UIBarButtonItem
        featureDropDown.bottomOffset = CGPoint(x: 0, y:(featureDropDown.anchorView?.plainView.bounds.height)!)
        featureDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.txtFeature.text = item
            print("Selected item: \(item) at index: \(index)")
            if item == "Yes" {
                self.paramHelper.params.updateValue(1, forKey: "featured")
            }
            else {
                self.paramHelper.params.updateValue(0, forKey: "featured")
            }
        }
        
        self.featureDropDown.dataSource = ["Yes", "No"]
        
        
        // Area Dropdown
        dropDownArea.anchorView = txtLocation // UIView or UIBarButtonItem
        dropDownArea.bottomOffset = CGPoint(x: 0, y:(dropDownArea.anchorView?.plainView.bounds.height)!)
        dropDownArea.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.txtLocation.text = item
            self.txtLocation.resignFirstResponder()
            
            let placeID = self.placeIDs[index]
            let placesClient = GMSPlacesClient.init()
            placesClient.lookUpPlaceID(placeID, callback: { (place, error) -> Void in
                if let error = error {
                    print("lookup place id query error: \(error.localizedDescription)")
                    return
                }
                
                guard let place = place else {
                    print("No place details for \(placeID)")
                    return
                }
                
                print("Place name \(place.name)")
                print("Place address \(String(describing: place.formattedAddress))")
                print("Place placeID \(place.placeID)")
                print("Place attributions \(String(describing: place.attributions))")
                print("Place Coordinates \(place.coordinate)")
                
                //self.cordinates = place
                self.latitude = "\(place.coordinate.latitude)"
                self.longitude = "\(place.coordinate.longitude)"
                
                self.paramHelper.params.updateValue(item, forKey: "area")
                self.paramHelper.params.updateValue("\(place.coordinate.latitude)", forKey: "latitude")
                self.paramHelper.params.updateValue("\(place.coordinate.longitude)", forKey: "longitude")
            })
        }
        
        txtLocation.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        
        //categoryDropDown.selectRow(at: selectedCatIndex)
    }
    
    
    func setTextViewDelegate(){
        txtDiscription.delegate = self
        
    }
    /*
     func requestDidFilter(notification: NSNotification){
     
     print("Filter Success")
     let info = notification.userInfo
     self.delegate?.didSelectCategory(position: info?["id"]! as! Int)
     
     if self.txtCategory.text == "classified" {
     
     self.heightSubCategoryConstraint.constant = 17
     self.heightSubCategoryView.constant = 40
     self.subCategoryView.isHidden = false
     self.typeDelegate?.changeCategoryType(type: CategoryType.CLASSIFIED)
     
     } else {
     
     self.heightSubCategoryConstraint.constant = 0
     self.heightSubCategoryView.constant = 0
     self.subCategoryView.isHidden = true
     self.typeDelegate?.changeCategoryType(type: CategoryType.PROPERTY)
     
     
     }
     }
     */
    
    
    //MARK:-  Border View
    
    func borderView(){
        
        self.locationView.layer.borderWidth = 0.5
        self.locationView.layer.borderColor = UIColor.lightGray.cgColor
        self.locationView.addShadow()
        
        self.titleView.layer.borderWidth = 0.5
        self.titleView.layer.borderColor = UIColor.lightGray.cgColor
        self.titleView.addShadow()
        
        self.categoryView.layer.borderWidth = 0.5
        self.categoryView.layer.borderColor = UIColor.lightGray.cgColor
        self.categoryView.addShadow()
        
        self.priceView.layer.borderWidth = 0.5
        self.priceView.layer.borderColor = UIColor.lightGray.cgColor
        self.priceView.addShadow()
        
        self.txtDiscription.layer.borderWidth = 0.5
        self.txtDiscription.layer.borderColor = UIColor.lightGray.cgColor
        self.txtDiscription.addShadow()
        
        self.featureView.layer.borderWidth = 0.5
        self.featureView.layer.borderColor = UIColor.lightGray.cgColor
        self.featureView.addShadow()
        
        self.MobileView.layer.borderWidth = 0.5
        self.MobileView.layer.borderColor = UIColor.lightGray.cgColor
        self.MobileView.addShadow()
        
        self.subCategoryView.layer.borderWidth = 0.5
        self.subCategoryView.layer.borderColor = UIColor.lightGray.cgColor
        self.subCategoryView.addShadow()
        
    }
    
    @IBAction func featureAction(_ sender : UIButton) {
        self.featureDropDown.show()
    }
    
    //MARK:- Camera Button Action
    
    func openCamera() {
        
        let MaximageCount = 15
        let AlreadySelectedImage:Int = photoArray.count + (images?.count)!
        if AlreadySelectedImage == MaximageCount {
            
            Alert.showToast(message: "Maximum number of photos has been reached")
            return
        }
        
        let imagePickerController = ImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.imageLimit = 1
        UIApplication.shared.keyWindow?.rootViewController?.present(imagePickerController, animated: true, completion: nil)
        
        //        let cameraViewController = CameraViewController(croppingEnabled: false) { [weak self] image, asset in
        //
        //            if image != nil {
        //                self?.photoArray.append(AppHelper.resizeImage(image: image!, compressionQuality: 0.25))
        //            }
        //
        //            //Dismiss Controller
        //            UIApplication.shared.keyWindow?.rootViewController?.dismiss(animated: true, completion: {() -> Void in
        //                self?.collectionView.reloadData()
        //            })
        //        }
        //        UIApplication.shared.keyWindow?.rootViewController?.present(cameraViewController, animated: true, completion: nil)
    }
    
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if (textField.text?.isEmpty)! || textField.text == "" {
            print("nil")
        }
        else {
            let filter = GMSAutocompleteFilter()
            filter.type = .noFilter
            let placesClient = GMSPlacesClient.init()
            placesClient.autocompleteQuery(self.txtLocation.text!, bounds: nil, filter: filter, callback: {(results, error) -> Void in
                if let error = error {
                    print("Autocomplete error \(error)")
                    return
                }
                if let results = results {
                    var names: [String] = [String]()
                    var ids: [String] = [String]()
                    for result in results {
                        print("Result \(String(describing: result.attributedFullText.string)) with placeID \(String(describing: result.placeID))")
                        names.append(result.attributedFullText.string)
                        ids.append(result.placeID)
                    }
                    self.placeIDs = ids
                    self.dropDownArea.dataSource = names
                    self.dropDownArea.show()
                }
            })
        }
    }
    
}


extension EditHeaderView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    // MARK: - UIcollectionview data source
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if photoArray.count + (images?.count)! == 15 {
            return photoArray.count +  (images?.count)!
        }
        else if self.images?.count == 0 {
            return photoArray.count + 1
        }
        else {
            return (images?.count)! + photoArray.count + 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let  cell:AddImagesCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing:AddImagesCollectionViewCell.self), for: indexPath) as! AddImagesCollectionViewCell
        
        if indexPath.row != self.photoArray.count + (images?.count)! {
            
            if self.images?.count == 0 {
                let photo = photoArray[indexPath.row]
                cell.imgCancel.isHidden = false
                cell.imgView.image = photo
            }
            else {
                cell.imgCancel.isHidden = false
                
                if indexPath.row >= (images?.count)! {
                    let photo = photoArray[indexPath.row - (images?.count)!]
                    cell.imgView.image = photo
                }
                else {
                    let url = images?[indexPath.row].imageUrl!
                    cell.imgView.setImageFromUrl(urlStr: url!)
                }
            }
        }
        else {
            cell.imgView.image = UIImage(named: "addImage")!
            cell.imgCancel.isHidden = true
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 30, height: 30)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.row == photoArray.count + (images?.count)! {
            openCamera()
        }
        else {
            if (images?.count)! == 0 {
                photoArray.remove(at: indexPath.row)
                 paramHelper.imagesArray = self.photoArray
            }
            else {
                if indexPath.row >= (images?.count)! {
                    photoArray.remove(at: indexPath.row - (images?.count)!)
                     paramHelper.imagesArray = self.photoArray
                }
                else {
                    self.deletedIds.append(self.images![indexPath.row].id!)
                    self.paramHelper.deletedIDs = self.deletedIds
                    self.images?.remove(at: indexPath.row)
                    paramHelper.addedImages = self.images!
                }
            }
            self.collectionView.reloadData()
        }
    }
    
}

extension EditHeaderView: ImagePickerDelegate {
    
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        
        if images.count > 0 {
            self.photoArray.append(AppHelper.resizeImage(image: images.first!, compressionQuality: 0.25))
             paramHelper.imagesArray = self.photoArray
            self.images = paramHelper.addedImages
            
        }
        
        //Dismiss Controller
        UIApplication.shared.keyWindow?.rootViewController?.dismiss(animated: true, completion: {() -> Void in
            self.collectionView.reloadData()
        })
    }
    
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
        //Dismiss Controller
        UIApplication.shared.keyWindow?.rootViewController?.dismiss(animated: true, completion: nil)
    }
    
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        
    }
}

extension EditHeaderView : UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == txtTitle {
            paramHelper.params.updateValue(self.txtTitle.text!, forKey: "title")
        }
        if textField == txtPrice{
            paramHelper.params.updateValue(self.txtPrice.text!, forKey: "price")
        }
        if textField == txtMobile {
            paramHelper.params.updateValue(self.txtMobile.text!, forKey: "phone")
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range:NSRange, replacementString string: String) -> Bool {
        
        let kActualText = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
        print("kActualText", kActualText)
        switch textField.tag
        {
        case 991:
            paramHelper.params.updateValue(kActualText, forKey: "title")
        case 992:
            paramHelper.params.updateValue(kActualText, forKey: "price")
        case 993:
            paramHelper.params.updateValue(kActualText, forKey: "phone")
        default:
            print("It is nothing");
        }
        return true;
    }
    
}

extension EditHeaderView : UITextViewDelegate {
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView == txtDiscription {
            
            paramHelper.params.updateValue(self.txtDiscription.text!, forKey: "description")
        }
    }
    
    //    func textViewDidChange(_ textView: UITextView) {
    //
    //          print("kActualText", self.txtDiscription.text)
    //        paramHelper.params.updateValue(self.txtDiscription.text, forKey: "description")
    //    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        let kActualText = NSString(string: textView.text!).replacingCharacters(in: range, with: text)
        print("kActualText", kActualText)
        paramHelper.params.updateValue(kActualText, forKey: "description")
        if let placeholderLabel = self.viewWithTag(100) as? UILabel {
            placeholderLabel.isHidden = kActualText.count > 0
        }
        return true;
    }
}
