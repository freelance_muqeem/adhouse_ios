//
//  TextFieldCell.swift
//  AdHouse
//
//  Created by  on 8/11/17.
//  Copyright © 2017 . All rights reserved.
//

import UIKit
import DropDown

class TextFieldCell: UITableViewCell {

    @IBOutlet weak var shadowView:UIView!
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var txtField:UITextField!
    var paramHelper : ParamsHolder!
    var itemID : Int!
    var indexPath : IndexPath!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        txtField.delegate = self
        self.shadowView.layer.borderWidth = 0.5
        self.shadowView.layer.borderColor = UIColor.lightGray.cgColor
        self.shadowView.addShadow()
        
    }

}
extension TextFieldCell : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range:NSRange, replacementString string: String) -> Bool {
        let kActualText = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
        print("kActualText", kActualText)
       let index = indexPath.row - 1
     paramHelper.attribsArray[index] =  ["id":"\(String(describing: itemID!))" ,"value":kActualText]
    //--ww      paramHelper.params.updateValue(txtField.text!, forKey: "\(String(describing: itemID!))")
      //--ww   paramHelper.params.updateValue(txtField.text!, forKey: "\(String(describing: itemID!))")
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
   //  let index = indexPath.row - 1
    //  paramHelper.attribsArray[index] =  ["id":"\(String(describing: itemID!))" ,"value":txtField.text!,"selection_id": "\(String(describing: itemID!))"]
      //   paramHelper.params.updateValue(txtField.text!, forKey: "\(String(describing: itemID!))")
      //--ww  paramHelper.params.updateValue(txtField.text!, forKey: "\(String(describing: itemID!))")
        //  paramHelper.attribsArray[index] =  ["id":selectedObj!.id! ,"value":"\(String(describing: selectedObj!.id!))","selection_id": selectedObj!.id!]
    }
}
