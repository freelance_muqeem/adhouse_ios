//
//  Result.swift
//
//  Created by   on 1/1/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Block: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let city = "city"
    static let socialMediaPlatform = "social_media_platform"
    static let email = "email"
    static let socialMediaId = "social_media_id"
    static let fullName = "full_name"
    static let address = "address"
    static let profileImage = "profile_image"
    static let gender = "gender"
    static let deviceType = "device_type"
    static let latitude = "latitude"
    static let status = "status"
    static let roleId = "role_id"
    static let id = "id"
    static let deviceToken = "device_token"
    static let notificationStatus = "notification_status"
    static let phone = "phone"
    static let extra = "extra"
    static let longitude = "longitude"
    static let profilePicture = "profile_picture"
  }

  // MARK: Properties
  public var city: String?
  public var socialMediaPlatform: String?
  public var email: String?
  public var socialMediaId: String?
  public var fullName: String?
  public var address: String?
  public var profileImage: String?
  public var gender: String?
  public var deviceType: String?
  public var latitude: String?
  public var status: Int?
  public var roleId: Int?
  public var id: Int?
  public var deviceToken: String?
  public var notificationStatus: Int?
  public var phone: String?
  public var extra: String?
  public var longitude: String?
  public var profilePicture: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
    public convenience init(object: JSON) {
        self.init(json: object)
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
    city = json[SerializationKeys.city].string
    socialMediaPlatform = json[SerializationKeys.socialMediaPlatform].string
    email = json[SerializationKeys.email].string
    socialMediaId = json[SerializationKeys.socialMediaId].string
    fullName = json[SerializationKeys.fullName].string
    address = json[SerializationKeys.address].string
    profileImage = json[SerializationKeys.profileImage].string
    gender = json[SerializationKeys.gender].string
    deviceType = json[SerializationKeys.deviceType].string
    latitude = json[SerializationKeys.latitude].string
    status = json[SerializationKeys.status].int
    roleId = json[SerializationKeys.roleId].int
    id = json[SerializationKeys.id].int
    deviceToken = json[SerializationKeys.deviceToken].string
    notificationStatus = json[SerializationKeys.notificationStatus].int
    phone = json[SerializationKeys.phone].string
    extra = json[SerializationKeys.extra].string
    longitude = json[SerializationKeys.longitude].string
    profilePicture = json[SerializationKeys.profilePicture].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = city { dictionary[SerializationKeys.city] = value }
    if let value = socialMediaPlatform { dictionary[SerializationKeys.socialMediaPlatform] = value }
    if let value = email { dictionary[SerializationKeys.email] = value }
    if let value = socialMediaId { dictionary[SerializationKeys.socialMediaId] = value }
    if let value = fullName { dictionary[SerializationKeys.fullName] = value }
    if let value = address { dictionary[SerializationKeys.address] = value }
    if let value = profileImage { dictionary[SerializationKeys.profileImage] = value }
    if let value = gender { dictionary[SerializationKeys.gender] = value }
    if let value = deviceType { dictionary[SerializationKeys.deviceType] = value }
    if let value = latitude { dictionary[SerializationKeys.latitude] = value }
    if let value = status { dictionary[SerializationKeys.status] = value }
    if let value = roleId { dictionary[SerializationKeys.roleId] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = deviceToken { dictionary[SerializationKeys.deviceToken] = value }
    if let value = notificationStatus { dictionary[SerializationKeys.notificationStatus] = value }
    if let value = phone { dictionary[SerializationKeys.phone] = value }
    if let value = extra { dictionary[SerializationKeys.extra] = value }
    if let value = longitude { dictionary[SerializationKeys.longitude] = value }
    if let value = profilePicture { dictionary[SerializationKeys.profilePicture] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.city = aDecoder.decodeObject(forKey: SerializationKeys.city) as? String
    self.socialMediaPlatform = aDecoder.decodeObject(forKey: SerializationKeys.socialMediaPlatform) as? String
    self.email = aDecoder.decodeObject(forKey: SerializationKeys.email) as? String
    self.socialMediaId = aDecoder.decodeObject(forKey: SerializationKeys.socialMediaId) as? String
    self.fullName = aDecoder.decodeObject(forKey: SerializationKeys.fullName) as? String
    self.address = aDecoder.decodeObject(forKey: SerializationKeys.address) as? String
    self.profileImage = aDecoder.decodeObject(forKey: SerializationKeys.profileImage) as? String
    self.gender = aDecoder.decodeObject(forKey: SerializationKeys.gender) as? String
    self.deviceType = aDecoder.decodeObject(forKey: SerializationKeys.deviceType) as? String
    self.latitude = aDecoder.decodeObject(forKey: SerializationKeys.latitude) as? String
    self.status = aDecoder.decodeObject(forKey: SerializationKeys.status) as? Int
    self.roleId = aDecoder.decodeObject(forKey: SerializationKeys.roleId) as? Int
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
    self.deviceToken = aDecoder.decodeObject(forKey: SerializationKeys.deviceToken) as? String
    self.notificationStatus = aDecoder.decodeObject(forKey: SerializationKeys.notificationStatus) as? Int
    self.phone = aDecoder.decodeObject(forKey: SerializationKeys.phone) as? String
    self.extra = aDecoder.decodeObject(forKey: SerializationKeys.extra) as? String
    self.longitude = aDecoder.decodeObject(forKey: SerializationKeys.longitude) as? String
    self.profilePicture = aDecoder.decodeObject(forKey: SerializationKeys.profilePicture) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(city, forKey: SerializationKeys.city)
    aCoder.encode(socialMediaPlatform, forKey: SerializationKeys.socialMediaPlatform)
    aCoder.encode(email, forKey: SerializationKeys.email)
    aCoder.encode(socialMediaId, forKey: SerializationKeys.socialMediaId)
    aCoder.encode(fullName, forKey: SerializationKeys.fullName)
    aCoder.encode(address, forKey: SerializationKeys.address)
    aCoder.encode(profileImage, forKey: SerializationKeys.profileImage)
    aCoder.encode(gender, forKey: SerializationKeys.gender)
    aCoder.encode(deviceType, forKey: SerializationKeys.deviceType)
    aCoder.encode(latitude, forKey: SerializationKeys.latitude)
    aCoder.encode(status, forKey: SerializationKeys.status)
    aCoder.encode(roleId, forKey: SerializationKeys.roleId)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(deviceToken, forKey: SerializationKeys.deviceToken)
    aCoder.encode(notificationStatus, forKey: SerializationKeys.notificationStatus)
    aCoder.encode(phone, forKey: SerializationKeys.phone)
    aCoder.encode(extra, forKey: SerializationKeys.extra)
    aCoder.encode(longitude, forKey: SerializationKeys.longitude)
    aCoder.encode(profilePicture, forKey: SerializationKeys.profilePicture)
  }

}
