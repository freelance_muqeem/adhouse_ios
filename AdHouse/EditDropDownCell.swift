//
//  EditDropDownCell.swift
//  AdHouse
//
//  Created by  on 8/15/17.
//  Copyright © 2017 . All rights reserved.
//

import UIKit
import DropDown

class EditDropDownCell: UITableViewCell {

    @IBOutlet weak var shadowView:UIView!
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var txtField:UITextField!
    
    let dropDown = DropDown()
    var AtrribIID = Int()
    var indexPath: IndexPath!
    var attribDelegate: AttribDelegate?
    
    var selectedIndex: Int!
    var selectedIDs:[Int] = [Int]()
    var dropDownAction : (()->Void)?
    var selectedID : ((_ indexPathRow : Int, _ id : Int , _ title : String , _ itemId : Int) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        self.shadowView.layer.borderWidth = 0.5
        self.shadowView.layer.borderColor = UIColor.lightGray.cgColor
        self.shadowView.addShadow()
        
        // Category DropDown
        dropDown.anchorView = txtField // UIView or UIBarButtonItem
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.txtField.text = item
            self.attribDelegate?.didAttribSelect(indexPath: self.indexPath, value: item)
            self.selectedIndex = index
            print("Selected item: \(item) at index: \(index)")
           self.selectedID!(self.indexPath.row, index , item, self.selectedIDs[index])
        }
        
    }
    
    @IBAction func btnAction(_ sender : UIButton) {
        
     //--ww   self.dropDown.show()
         dropDownAction!()
        
    }
    
}
