//
//  EditViewController.swift
//  AdHouse
//
//  Created by  on 8/15/17.
//  Copyright © 2017 . All rights reserved.
//

import UIKit
import ImagePicker
import GooglePlaces

enum EditType {
    
    case TextFieldType
    case ButtonType
}

extension EditViewController: AttribDelegate {
    func didAttribSelect(indexPath: IndexPath, value: String) {
        
        self.ad.adDetails?[indexPath.row-1].attributeValue = value
        
    }
}

class EditViewController: UIViewController , UITableViewDelegate , UITableViewDataSource {
    
    class func instantiateFromStoryboard() -> EditViewController {
        let storyboard = UIStoryboard(name: "HomeList", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! EditViewController
    }
    
    @IBOutlet weak var tblView:UITableView!
    
    var attributesArray:[AttributesResult]? = [AttributesResult]()
    var categoriesArray:[CategoryResult]? = [CategoryResult]()
    var subCategoriesArray:[CategoryResult]? = [CategoryResult]()
    
    var leftBtn:UIButton?
    var type:Type?
    var selectedCategoryType:CategoryType?
    
    var ad: AdsResult!
    
    var delegate: RefreshDelegate?
    var selectedLang: [String]!
    
    
    var selectedParentIds = [Int]()
    var selectedTitles = [String]()
    var paramsHolder = ParamsHolderEdit()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        paramsHolder.params = [:]
        self.navigationController?.navigationBar.isHidden = false
        
        let navBackgroundImage:UIImage! = UIImage(named: "topbox")!
        self.navigationController?.navigationBar.setBackgroundImage(navBackgroundImage, for: .default)
        self.title = "Edit Ad"
        self.placeBackBtn()
        
        selectedLang =  UserDefaults.standard.object(forKey: "AppleLanguages") as? [String] ?? [String]()
        self.selectedCategoryType = CategoryType.VEHICLES
        
        //Register Nib
        
        let headerNib = UINib.init(nibName: "EditHeaderView", bundle: nil)
        tblView.register(headerNib, forCellReuseIdentifier: "EditHeaderView")
        
        let dropdown = UINib.init(nibName: "EditDropDownCell", bundle: nil)
        tblView.register(dropdown, forCellReuseIdentifier: "EditDropDownCell")
        
        let text = UINib.init(nibName: "EditTextFieldCell", bundle: nil)
        tblView.register(text, forCellReuseIdentifier: "EditTextFieldCell")
        
        let btn = UINib.init(nibName: "EditButtonCell", bundle: nil)
        tblView.register(btn, forCellReuseIdentifier: "EditButtonCell")
        
        self.getAllCategories()
        
        //        if self.ad.isClassified == 1 {
        self.getSubCategories(parentId: self.ad.categoryId!)
        //        }
        self.getFields(position: 0)
        if self.ad.subCategoryId != 0 {
            self.getFieldsForSubCat(position: 0)
        }
        
        print(self.ad)
        
        setDataToHolder()
    }
    
    func setDataToHolder() {
        paramsHolder.addedImages = self.ad.adImages!
        paramsHolder.params.updateValue(self.ad.title!, forKey: "title")
        paramsHolder.params.updateValue(String(self.ad.price!), forKey: "price")
        
        paramsHolder.params.updateValue(self.ad.area!, forKey: "area")
        paramsHolder.params.updateValue("\(self.ad.latitude!)", forKey: "latitude")
        paramsHolder.params.updateValue("\(self.ad.longitude!)", forKey: "longitude")
        paramsHolder.params.updateValue(self.ad.phone ?? "123456", forKey: "phone")
        paramsHolder.params.updateValue(self.ad.descriptionValue ?? "", forKey: "description")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
       

    }
    
    //MARK:-  Back Button Code
    
    func placeBackBtn(){
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        leftBtn =  UIButton(type: .custom)
        leftBtn?.setImage(UIImage(named: "backbtn"), for: .normal)
        leftBtn?.addTarget(self, action: #selector(EditViewController.BackBtnPressed), for: .touchUpInside)
        leftBtn?.frame = CGRect(x: 0, y:0 , width: 25, height: 25)
        let  leftBarButton: UIBarButtonItem = UIBarButtonItem(customView: leftBtn!)
        self.navigationItem.setLeftBarButton(leftBarButton, animated: true)
        
    }
    
    @objc func BackBtnPressed(){
        
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:-  Get All Categories
    
    func getAllCategories() {
        
        self.view.endEditing(true)
        Alert.showLoader(message: "")
        
        var param = [String:String]()
        
        if !isLanguageEnglish() {
            param.updateValue("ar", forKey: "lang")
        } else {
            param.updateValue("en", forKey: "lang")
        }
        
        CategoryServices.GetCategories(param: param, completionHandler: {(status, response, error) in
            
            Alert.hideLoader()
            if !status {
                if error != nil {
                    print("Error: \((error as! Error).localizedDescription)")
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                return
            }
            
            print(response!)
            print("SUCCESS")
            self.categoriesArray?.removeAll()
            
            let adsResponseArray = response?["Result"].arrayValue
            print(adsResponseArray!)
            
            for resultObj in adsResponseArray! {
                let obj =  CategoryResult(json: resultObj)
                self.categoriesArray?.append(obj)
            }
            
            //--ww self.tblView.reloadData()
        })
    }
    
    //MARK:-  Get Sub Categories
    
    func getSubCategories(parentId: Int) {
        
        self.view.endEditing(true)
        Alert.showLoader(message: "")
        
        var param = [String:String]()
        
        if !isLanguageEnglish() {
            param.updateValue("ar", forKey: "lang")
        } else {
            param.updateValue("en", forKey: "lang")
        }
        
        param.updateValue(String(parentId) , forKey: "category_id")
        
        CategoryServices.GetSubCategories(param:param, completionHandler: {(status, response, error) in
            Alert.hideLoader()
            if !status {
                if error != nil {
                    print("Error: \((error as! Error).localizedDescription)")
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                return
            }
            
            print(response!)
            print("SUCCESS")
            
            self.subCategoriesArray?.removeAll()
            
            let adsResponseArray = response?["Result"].arrayValue
            print(adsResponseArray!)
            
            for resultObj in adsResponseArray! {
                let obj =  CategoryResult(json: resultObj)
                self.subCategoriesArray?.append(obj)
            }
            //--ww  self.tblView.reloadData()
        })
    }
    
    //MARK:-  Get Fields
    
    func getFields(position:Int) {
        
        self.view.endEditing(true)
        Alert.showLoader(message: "")
        
        
        var param = ["category_id": String(describing: self.ad.categoryId!)]
        
        if !isLanguageEnglish() {
            param.updateValue("ar", forKey: "lang")
        } else {
            param.updateValue("en", forKey: "lang")
        }
        
        CategoryServices.GetAttributes(param:param, completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    print("Error: \((error as! Error).localizedDescription)")
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                return
            }
            
            print(response!)
            print("SUCCESS")
            
            Alert.hideLoader()
            self.attributesArray?.removeAll()
            self.selectedParentIds.removeAll()
            self.selectedTitles.removeAll()
            self.paramsHolder.attribsArray.removeAll()
            
            let adsResponseArray = response?["Result"].arrayValue
            print(adsResponseArray!)
            self.selectedParentIds.append(0)
            self.selectedTitles.append("")
            self.selectedParentIds.append(0)
            self.selectedTitles.append("")
            
            for resultObj in adsResponseArray! {
                let obj =  AttributesResult(json: resultObj)
                self.attributesArray?.append(obj)
                self.selectedParentIds.append(0)
                self.selectedTitles.append("")
                self.paramsHolder.attribsArray.append( ["id":0,"value":"","selection_id":0])
            }
            
            self.tblView.reloadData()
        })
    }
    
    //MARK:-  Get Fields
    
    func getFieldsForSubCat(position:Int) {
        
        self.view.endEditing(true)
        Alert.showLoader(message: "")
        
        var param = ["category_id": String(describing: self.ad.subCategoryId!)]
        if !isLanguageEnglish() {
            param.updateValue("ar", forKey: "lang")
        } else {
            param.updateValue("en", forKey: "lang")
        }
        
        CategoryServices.GetAttributes(param:param, completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    print("Error: \((error as! Error).localizedDescription)")
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                return
            }
            
            print(response!)
            print("SUCCESS")
            Alert.hideLoader()
            self.attributesArray?.removeAll()
            self.selectedParentIds.removeAll()
            self.selectedTitles.removeAll()
            self.paramsHolder.attribsArray.removeAll()
            
            let adsResponseArray = response?["Result"].arrayValue
            print(adsResponseArray!)
            self.selectedParentIds.append(0)
            self.selectedTitles.append("")
            self.selectedParentIds.append(0)
            self.selectedTitles.append("")
            
            for resultObj in adsResponseArray! {
                let obj =  AttributesResult(json: resultObj)
                self.attributesArray?.append(obj)
                self.selectedParentIds.append(0)
                self.selectedTitles.append("")
                self.paramsHolder.attribsArray.append( ["id":0,"value":"","selection_id":0])
            }
            
            self.tblView.reloadData()
        })
    }
    
    //MARK:-  TableView Delegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 0 {
            if subCategoriesArray?.count == 0 {
                return GISTUtility.convertToRatio(620)
            }
            return GISTUtility.convertToRatio(680)
            
        } else if indexPath.row == (self.ad.adDetails?.count)! + 2 {
            
            return GISTUtility.convertToRatio(55)
            
        } else {
            
            return GISTUtility.convertToRatio(75)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if paramsHolder.attribsArray.count == 0 {
            return 0
        }
        return (self.ad.adDetails?.count)! + 2
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            
            let headerCell:EditHeaderView = tableView.dequeueReusableCell(withIdentifier: "EditHeaderView") as! EditHeaderView
           
            headerCell.setTextViewDelegate()
            
            var filteredObj = [CategoryResult]()
            filteredObj = (self.categoriesArray?.filter { item in
                return (item.id == self.ad.categoryId)
                })!
            
            headerCell.txtCategory.text = filteredObj.first?.name!
            self.paramsHolder.params.updateValue(filteredObj.first?.id! ?? 0, forKey: "category_id")
            
            var filteredSubObj = [CategoryResult]()
            filteredSubObj = (self.subCategoriesArray?.filter { item in
                return (item.id == self.ad.subCategoryId)
                })!
            
            
            
            if filteredSubObj.count > 0 {
                headerCell.txtSubCategory.text = filteredSubObj.first?.name!
                self.paramsHolder.params.updateValue(filteredSubObj.first?.id! ?? 0, forKey: "sub_category_id")
                
                headerCell.heightSubCategoryConstraint.constant = 17
                headerCell.heightSubCategoryView.constant = 40
                headerCell.subCategoryView.isHidden = false
                
            }
            
            headerCell.txtTitle.text =  paramsHolder.params["title"] as? String ?? self.ad.title!
            headerCell.txtPrice.text = paramsHolder.params["price"] as? String ?? String(self.ad.price!)
            headerCell.txtLocation.text =  paramsHolder.params["area"] as? String ?? self.ad.area!
            headerCell.longitude =  paramsHolder.params["latitude"] as? String ??  self.ad.longitude
            headerCell.latitude =  paramsHolder.params["longitude"] as? String ?? self.ad.latitude
            
            
            
            headerCell.ids = ""
            headerCell.images = paramsHolder.addedImages
            for item in paramsHolder.addedImages {
                headerCell.ids = headerCell.ids + "\(item.id!)" + ","
            }
            //--ww paramsHolder.addedImages = self.ad.adImages!
            
            print("IDSSSS: \(headerCell.ids)")
            
            headerCell.txtFeature.text = (self.ad.featured == 1) ? "Yes" : "No"
            self.paramsHolder.params.updateValue(self.ad.featured ?? 1, forKey: "featured")
            
            
            headerCell.txtMobile.text = paramsHolder.params["phone"] as? String ?? self.ad.phone
          
            
            if self.ad.descriptionValue!.count > 0 {
                headerCell.txtDiscription.placeholder = ""
                headerCell.txtDiscription.text =  paramsHolder.params["description"] as? String ?? self.ad.descriptionValue
                
            }
            
            
            
            headerCell.selectedCatID = self.ad.categoryId!
            if self.ad.subCategoryId != nil {
                headerCell.selectedSubCatID = self.ad.subCategoryId!
            }
             headerCell.paramHelper = paramsHolder
            
            return headerCell
        }
        else if indexPath.row == (self.ad.adDetails?.count)! + 1 {
            
            let buttonCell:EditButtonCell = tableView.dequeueReusableCell(withIdentifier: "EditButtonCell") as! EditButtonCell
            buttonCell.btnPost.addTarget(self, action: #selector(self.SubmitAction(sender:)), for: .touchUpInside)
            return buttonCell
        }
        else {
            
            let obj = self.ad.adDetails?[indexPath.row-1]
            let objAttr = self.attributesArray?[indexPath.row-1]
            if indexPath.row - 1 < paramsHolder.attribsArray.count{
                paramsHolder.attribsArray[indexPath.row-1]["id"] = obj?.attributes?.id
            }
            switch obj?.attributes?.attributeType {
                
            case "dropdown"?:
                
                let dropdownCell : EditDropDownCell = tableView.dequeueReusableCell(withIdentifier: "EditDropDownCell") as! EditDropDownCell
                dropdownCell.lblName.text = obj?.attributeTitle
                dropdownCell.txtField.text = obj?.attributeValue
                dropdownCell.AtrribIID = (obj?.id!)!
                dropdownCell.attribDelegate = self
                dropdownCell.indexPath = indexPath
                
                var titles: [String] = [String]()
                var ids = [Int]()
                for (index, result) in (objAttr?.dropdownlist)!.enumerated() {
                    print("Title \(String(describing: result.value))")
                    titles.append(result.value!)
                    ids.append(result.id!)
                    if result.id! == Int(obj!.attributeValue!) {
                        print(result.id!)
                        dropdownCell.txtField.text = result.value!
                        
                        dropdownCell.selectedIndex = index
                        if indexPath.row - 1 < paramsHolder.attribsArray.count{
                            selectedTitles[indexPath.row] = result.value!
                            paramsHolder.attribsArray[indexPath.row-1]["value"] = result.value!
                            paramsHolder.attribsArray[indexPath.row-1]["selection_id"] = result.id!
                        }
                    }
                }
                dropdownCell.txtField.text =  selectedTitles[indexPath.row]
                dropdownCell.selectedIDs = ids
                dropdownCell.dropDown.dataSource = titles
                dropdownCell.indexPath = indexPath
                
                dropdownCell.selectedID = { [weak self] index , id , title , itemID in
                    
                    
                    let selectedObj = self?.attributesArray?[index-1]
                    let selecteId = selectedObj?.dropdownlist![id].id
                    self?.selectedParentIds[index-1] = selecteId!
                    self?.selectedTitles[index] = title
                    dropdownCell.txtField.text = title
                    
                    //--ww      self?.paramsHolder.params.updateValue(title, forKey: "\(String(describing: selectedObj!.id!))") "\(String(describing:itemID))"
                    self?.paramsHolder.attribsArray[index-1] =  ["id":selectedObj!.id! ,"value":title,"selection_id": itemID]
                    
                    if !(selectedObj?.childIds?.isEmpty)!{
                        if let cell = self?.tblView.cellForRow(at: IndexPath(row:index + 1,section:0)) as? EditDropDownCell {
                            cell.txtField.text = ""
                            self?.selectedTitles[index + 1] = ""
                            self?.paramsHolder.attribsArray[index]["value"] = ""
                            self?.paramsHolder.attribsArray[index]["selection_id"] = 0
                            
                            let obj = self?.ad.adDetails?[index]
                            obj?.attributeValue = ""
                            
                        }
                    }
                }
                
                
                dropdownCell.dropDownAction = {
                    
                    let ind = indexPath.row - 2
                    let indexRow = ind > -1 ? ind : 0
                    let parentId = self.selectedParentIds[indexRow]
                    
                    if parentId == 0 && obj?.attributes?.dropdownlist?[0].parent_attribute_value_Id != 0 {
                        let parentObjTitle = self.attributesArray?[ind].attributeName ?? ""
                        Alert.showAlert(title: "Alert", message: "Please select \(parentObjTitle) first")
                    }else if parentId != 0 {
                        
                        var titles = [String]()
                        var ids = [Int]()
                        var ddList = obj?.attributes?.dropdownlist?.filter{ $0.parent_attribute_value_Id == parentId}
                        ddList = (ddList?.count)! > 0 ? ddList : obj?.attributes?.dropdownlist!
                        for result in ddList! {
                            titles.append(result.value!)
                            ids.append(result.id!)
                        }
                        dropdownCell.dropDown.dataSource = titles
                        dropdownCell.selectedIDs = ids
                        dropdownCell.dropDown.show()
                    }else{
                        dropdownCell.dropDown.show()
                    }
                }
                
                return dropdownCell
                
            case "text"? :
                
                let textCell : EditTextFieldCell = tableView.dequeueReusableCell(withIdentifier: "EditTextFieldCell") as! EditTextFieldCell
                textCell.addDetails = obj
                textCell.lblName.text = obj?.attributeTitle!
                textCell.txtField.text = obj?.attributeValue
                textCell.AtrribIID = (obj?.id!)!
                textCell.attribDelegate = self
                textCell.indexPath = indexPath
                textCell.paramHelper = paramsHolder
                textCell.itemID = obj?.attributeId!
                paramsHolder.attribsArray[indexPath.row - 1] =  ["id":"\( obj?.attributeId ?? 0)" ,"value":obj?.attributeValue ?? ""]
                if obj?.attributes?.keyboardType == "numpad" {
                    if #available(iOS 10.0, *) {
                        textCell.txtField.keyboardType = .asciiCapableNumberPad
                    } else {
                        // Fallback on earlier versions
                    }
                }
                return textCell
                
            default:
                
                let cell = UITableViewCell()
                return cell
            }
        }
    }
    
    @IBAction func SubmitAction(sender: UIButton) {
        paramsHolder.params.updateValue((Singleton.sharedInstance.CurrentUser?.id)!, forKey: "user_id")
        paramsHolder.params.updateValue(self.ad.id!, forKey: "ad_id")
        
        if paramsHolder.params["area"] == nil ||  (paramsHolder.params["area"] as! String).isEmptyOrWhitespace() {
            Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Kindly provide location", comment: ""))
            return
        }
        
        if paramsHolder.params["title"] == nil || (paramsHolder.params["title"] as! String).isEmptyOrWhitespace() {
            Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Kindly provide title", comment: ""))
            return
        }
        
        
        
        if paramsHolder.params["category_id"] == nil{
            Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Kindly provide cateogory", comment: ""))
            return
        }
        
        if paramsHolder.imagesArray.count < 1  && paramsHolder.addedImages.count < 1{
            Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Kindly provide at least one image", comment: ""))
            return
        }
        
        if paramsHolder.params["price"] == nil || Int((paramsHolder.params["price"] as! String)) == nil || Int((paramsHolder.params["price"] as! String)) == 0{
            Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Kindly provide price", comment: ""))
            return
        }
        
        
        if paramsHolder.params["description"] == nil || (paramsHolder.params["description"] as! String).isEmptyOrWhitespace(){
            Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Kindly provide description", comment: ""))
            return
        }
        
        if paramsHolder.params["featured"] == nil {
            Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Kindly provide featured value", comment: ""))
            return
        }
        
        if paramsHolder.params["phone"] == nil || (paramsHolder.params["phone"] as! String).isEmptyOrWhitespace() || (paramsHolder.params["phone"] as! String).count < 7  {
            
            Alert.showAlert(title: NSLocalizedString("Alert", comment: "") , message:   NSLocalizedString("Phone number at least 7 characters", comment: ""))
            return
        }
        
        
        
        for (index,item) in paramsHolder.attribsArray.enumerated() {
            if (item["value"] as! String).isEmptyOrWhitespace() {
                Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Please fill all Fields", comment: ""))
                return
            }
            
            if item["value"] as! String == "0" {
              paramsHolder.attribsArray[index]["value"] = "1"
            }
        }
        
        
        if paramsHolder.deletedIDs.count > 0 {
            var ids = ""
            for item in paramsHolder.deletedIDs {
                ids = ids + "\(item)" + ","
            }
            let start = ids.index(ids.endIndex, offsetBy: -1)
            let end = ids.endIndex
            let result = ids.replacingCharacters(in: start..<end, with: "")
            paramsHolder.params.updateValue(result as AnyObject, forKey: "ad_images_ids")
        }
        
        
        var txtIDs = ""
        for item in paramsHolder.attribsArray{
            txtIDs += "\(item["id"] ?? ""),"
        }
        if txtIDs.count != 0 {
            let start = txtIDs.index(txtIDs.endIndex, offsetBy: -1)
            let end = txtIDs.endIndex
            let result = txtIDs.replacingCharacters(in: start..<end, with: "")
            paramsHolder.params.updateValue(result as AnyObject, forKey: "ad_attributes_ids")
        }
        
        paramsHolder.params.updateValue(paramsHolder.attribsArray.jsonString()!, forKey: "attributes")
        
        print(paramsHolder.params)
        print(paramsHolder.imagesArray)
        
        Alert.showLoader(message: "")
        
        AdServices.UpdateAd(param: paramsHolder.params, images: paramsHolder.imagesArray, completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    print("Error: \((error as! Error).localizedDescription)")
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                return
            }
            
            print("Main")
            print(response!)
            
            print("SUCCESS")
            self.delegate?.didRefresh()
            
            self.navigationController?.popViewController(animated: true)
            
            Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Post Updated Successfully", comment: ""))
            
        })
        
        return
        
        var parameters = [String:Any]()
        var images: [UIImage] = [UIImage]()
        
        if !isLanguageEnglish() {
            parameters.updateValue("ar", forKey: "lang")
        } else {
            parameters.updateValue("en", forKey: "lang")
        }
        
        if let cell = self.tblView.cellForRow(at: IndexPath(row:0,section:0)) as? EditHeaderView {
            if cell.latitude == nil || cell.longitude == nil {
                Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Please fill all Fields", comment: ""))
                return
            }
            else {
                parameters.updateValue(cell.txtLocation.text!, forKey: "area")
                parameters.updateValue("\(cell.latitude!)", forKey: "latitude")
                parameters.updateValue("\(cell.longitude!)", forKey: "longitude")
            }
            
            if (cell.txtTitle.text?.isEmptyOrWhitespace())! {
                Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Please fill all Fields", comment: ""))
                return
            }
            else {
                parameters.updateValue(cell.txtTitle.text!, forKey: "title")
            }
            
            if (cell.txtCategory.text?.isEmptyOrWhitespace())! {
                Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Please fill all Fields", comment: ""))
                return
            }
            else {
                parameters.updateValue(cell.selectedCatID!, forKey: "category_id")
            }
            
            if cell.deletedIds.count > 0 {
                var ids = ""
                for item in cell.deletedIds {
                    ids = ids + "\(item)" + ","
                }
                let start = ids.index(ids.endIndex, offsetBy: -1)
                let end = ids.endIndex
                let result = ids.replacingCharacters(in: start..<end, with: "")
                parameters.updateValue(result as AnyObject, forKey: "ad_images_ids")
            }
            
            
            
            if cell.selectedSubCatID != nil {
                if cell.selectedSubCatID != 0 {
                    parameters.updateValue(cell.selectedSubCatID!, forKey: "sub_category_id")
                }
            }
            
            
            //            var index = 0
            if cell.images?.count == 0 && cell.photoArray.count == 0 {
                Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Please fill all Fields", comment: ""))
                return
            }
            for item in cell.photoArray {
                //                index += 1
                images.append(item)
            }
            
            
            if (cell.txtPrice.text?.isEmptyOrWhitespace())! {
                Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Please fill all Fields", comment: ""))
                return
            }
            else {
                parameters.updateValue(cell.txtPrice.text!, forKey: "price")
            }
            
            if (cell.txtDiscription.text?.isEmptyOrWhitespace())! {
                Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Please fill all Fields", comment: ""))
                return
            }
            else {
                parameters.updateValue(cell.txtDiscription.text!, forKey: "description")
            }
            
            if (cell.txtFeature.text?.isEmptyOrWhitespace())! {
                Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Please fill all Fields", comment: ""))
                return
            }
            else {
                if cell.txtFeature.text == "Yes" {
                    parameters.updateValue(1, forKey: "featured")
                }
                else {
                    parameters.updateValue(0, forKey: "featured")
                }
            }
            
            if (cell.txtMobile.text?.isEmptyOrWhitespace())! {
                Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Please fill all Fields", comment: ""))
                return
            }
            else {
                parameters.updateValue(cell.txtMobile.text!, forKey: "phone")
            }
            
        }
        
        var ids = [Int]()
        
        var attribsArray = [[String: Any]]()
        
        
        for (index, item) in (self.ad.adDetails?.enumerated())! {
            if let cell = self.tblView.cellForRow(at: IndexPath(row:index+1,section:0)) as? EditDropDownCell {
                if (cell.txtField.text?.isEmptyOrWhitespace())! {
                    Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Please fill all Fields", comment: ""))
                    return
                }
                var temp: [String: Any] = [:]
                ids.append(cell.AtrribIID)
                //                parameters.updateValue(cell.txtField.text!, forKey: "\(String(describing: item.attributeId!))")
                //                parameters.updateValue(item.attributes!.dropdownlist![cell.selectedIndex].id!, forKey: "\(String(describing: item.attributes!.id!))")
                temp.updateValue(item.attributes!.id!, forKey: "id")
                temp.updateValue(item.attributes!.dropdownlist![cell.selectedIndex].value!, forKey: "value")
                temp.updateValue(item.attributes!.dropdownlist![cell.selectedIndex].id!, forKey: "selection_id")
                attribsArray.append(temp)
                continue
            }
            if let cell = self.tblView.cellForRow(at: IndexPath(row:index+1,section:0)) as? EditTextFieldCell {
                if (cell.txtField.text?.isEmptyOrWhitespace())! {
                    Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Please fill all Fields", comment: ""))
                    return
                }
                ids.append(cell.AtrribIID)
                //                parameters.updateValue(cell.txtField.text!, forKey: "\(String(describing: item.attributeId!))")
                var temp: [String: Any] = [:]
                temp.updateValue(item.attributeId!, forKey: "id")
                temp.updateValue(cell.txtField.text!, forKey: "value")
                attribsArray.append(temp)
            }
        }
        
        parameters.updateValue(attribsArray.jsonString()!, forKey: "attributes")
        
        Alert.showLoader(message: "")
        
        parameters.updateValue((Singleton.sharedInstance.CurrentUser?.id)!, forKey: "user_id")
        parameters.updateValue(self.ad.id!, forKey: "ad_id")
        
        var txtID = ""
        for item in ids {
            txtID += "\(item),"
        }
        if txtID.count != 0 {
            let start = txtID.index(txtID.endIndex, offsetBy: -1)
            let end = txtID.endIndex
            let result = txtID.replacingCharacters(in: start..<end, with: "")
            parameters.updateValue(result as AnyObject, forKey: "ad_attributes_ids")
        }
        
        print("Parameters: \(parameters)")
        
        AdServices.UpdateAd(param: parameters, images: images, completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    print("Error: \((error as! Error).localizedDescription)")
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                return
            }
            
            print("Main")
            print(response!)
            
            print("SUCCESS")
            self.delegate?.didRefresh()
            
            self.navigationController?.popViewController(animated: true)
            
            Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Post Updated Successfully", comment: ""))
            
        })
    }
    
    
}

class ParamsHolderEdit{
    var params  = [String : Any]()
    var attribsArray = [[String: Any]]()
    var imagesArray = [UIImage]()
    var deletedIDs = [Int]()
    var addedImages = [AdImages]()
}

