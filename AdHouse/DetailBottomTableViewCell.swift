//
//  DetailBottomTableViewCell.swift
//  AdHouse
//
//  Created by maazulhaq on 26/07/2017.
//  Copyright © 2017 . All rights reserved.
//

import UIKit
import AlamofireImage
import GoogleMaps


extension DetailBottomTableViewCell: RefreshDelegate , GMSMapViewDelegate {
    
    func didRefresh(count: CGFloat) {
    }
    
    func didRefresh() {
        //self.GetFeaturedList()
    }
}

class DetailBottomTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var btnContactSeller: UIButton!
    @IBOutlet weak var lblFeatured: UILabel!
    @IBOutlet weak var collectionView:UICollectionView!
    
    @IBOutlet weak var mapView:GMSMapView!
    @IBOutlet weak var mapViewHeightConstraints:NSLayoutConstraint!
    
    var latitude:Double! = 0.0
    var longitude:Double! = 0.0
    var location = CLLocationCoordinate2D(latitude: 0.0 , longitude: 0.0)
    
    var isFeatured:Int!
    
    var delegate:CellBtnPressedProtocol?
   // var contactButtonDelegate:ContactButtonPressedDelegate?
    
    var featuredArray:[FeatureResult] = [FeatureResult]()
    
    var navigationDelegate: NavigationDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setFonts()
        let nib = UINib(nibName:String(describing:HomeCollectionViewCell.self), bundle: nil)
        
        self.mapView.delegate = self
        collectionView.register(nib, forCellWithReuseIdentifier:String(describing: HomeCollectionViewCell.self))
        
        let flowLayout = UICollectionViewFlowLayout()
        // flowLayout.itemSize = CGSize(width: 200, height: 200)
        //flowLayout.sectionInset = UIEdgeInsetsMake(0, 5, 0, 5)
        flowLayout.scrollDirection = UICollectionViewScrollDirection.horizontal
        //flowLayout.minimumInteritemSpacing = 0.0
        collectionView.collectionViewLayout = flowLayout
        
        
        
    }
    
    
    func setFonts() {
        
        btnContactSeller.titleLabel?.font = FontUtility.getFontWithAdjustedSize(size: 15, desireFontType: .Roboto_Bold)
        lblFeatured.font = FontUtility.getFontWithAdjustedSize(size: 13, desireFontType: .Roboto_Regular)
        
    }
    
    func getMap(latitude:String, longitude:String) {
        
        let camera = GMSCameraPosition.camera(withLatitude: Double(latitude)!, longitude: Double(longitude)!, zoom: 12)
        self.mapView.camera = camera
        
        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: Double(latitude)!, longitude: Double(longitude)!)
        // marker.title = self.SellerLocation
        marker.map = mapView
        
    }
    
    //MARK:-  Get Featured Ads Listing Service
    
    
    func GetFeaturedList(catID:Int) {
        
        Alert.showLoader(message: "")
        
        let userId = (Singleton.sharedInstance.CurrentUser == nil) ? -1 : (Singleton.sharedInstance.CurrentUser!.id!)
        
        
        let parameters:[String:Any] = ["user_id": userId , "category_id":catID]
        
        HomeServices.Featured(param: parameters , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    print("Error: \((error as! Error).localizedDescription)")
                    Alert.hideLoader()
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                return
            }
            
            print(response!)
            print("SUCCESS")
            Alert.hideLoader()
            
            self.featuredArray.removeAll()
            
            let featureResponseArray = response?["Result"].arrayValue
            
            for resultObj in featureResponseArray! {
                let obj =  FeatureResult(json: resultObj)
                self.featuredArray.append(obj)
            }
            
            self.collectionView.reloadData()
            
        })
        
    }
    
    
    
    // MARK: - UIcollectionview data source
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.featuredArray.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let obj:FeatureResult = self.featuredArray[indexPath.row]
        
        let  cell:HomeCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing:HomeCollectionViewCell.self), for: indexPath) as! HomeCollectionViewCell
        
        
        cell.lblName.text = obj.title
        cell.lblDetail.text = "QAR " + String(describing: obj.price!.decimal)
        cell.imageView.setImageFromUrl(urlStr: (obj.adImages?.first?.imageUrl)!)
        
        //        let filter = obj.adDetails?.filter({ (adDetail) -> Bool in
        //            return adDetail.attributeTitle == "CAR MAKE"
        //        })
        //        if (filter?.count)! > 0 {
        //            if let obj = filter?.first {
        //                if obj.attributeValue != nil {
        //                    cell.lblCarMake.text = obj.attributeValue!
        //                } else {
        //                    cell.lblCarMake.text = "NIL"
        //                }
        //            } else {
        //                cell.lblCarMake.text = "NIL"
        //            }
        //        } else {
        //            cell.lblCarMake.text = "NIL"
        //        }
        //
        //        let filterClass = obj.adDetails?.filter({ (adDetail) -> Bool in
        //            return adDetail.attributeTitle == "CLASS"
        //        })
        //        if (filterClass?.count)! > 0 {
        //            if let obj = filterClass?.first {
        //                if obj.attributeValue != nil {
        //                    cell.lblClass.text = obj.attributeValue!
        //                } else {
        //                    cell.lblClass.text = "NIL"
        //                }
        //            } else {
        //                cell.lblClass.text = "NIL"
        //            }
        //        } else {
        //            cell.lblClass.text = "NIL"
        //        }
        
        if obj.featured == 0 {
            
            cell.lbl_txt_Feature.isHidden = true
            cell.imgFeatured.isHidden = true
            
        } else {
            
            cell.lbl_txt_Feature.isHidden = false
            cell.imgFeatured.isHidden = false
            
        }
        
        // cell.addBorder(UIColor.gray, width: 1)
        cell.layer.shadowColor = UIColor.lightGray.cgColor
        cell.layer.shadowOffset = CGSize(width: 0, height: 1.0)
        cell.layer.shadowRadius = 1
        cell.layer.shadowOpacity = 0.9
        cell.layer.masksToBounds = false
        cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: cell.contentView.layer.cornerRadius).cgPath
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let screenWidth =  collectionView.bounds.size.width-20
        let height = SizeUtil.convertIphone6SizeToOtherPhonesRespectively(size: 140)
        return CGSize(width: screenWidth/2.5, height: height);
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.navigationDelegate?.didNavigate(productObjID: self.featuredArray[indexPath.row].id!)
        self.featuredArray = [FeatureResult]()
    }
    
    //    func contactSellerButtonAction() {
    //
    //        self.view2.isHidden = false
    //        self.view1.isHidden = true
    //        btnSellerInfo.setTitleColor(UIColor(rgb: 0x176AC1), for: .normal)
    //    }
    
    @IBAction func contactFormBtnPressed(_ sender:UIButton){
        
        if Singleton.sharedInstance.CurrentUser == nil {
            
          //  self.contactButtonDelegate?.guestTapped()
            
            //            Alert.showAlert(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("You have logged in as Guest Member, in order to access this feature. you need to SignUp on App", comment: ""))
        }
        else {
            
            self.delegate?.contactSellerBtnPressed()
        }
    }
    
}


